package com.psk.hms.redis.controller;

import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.redis.service.RedisService;
import com.psk.hms.base.constant.common.RedisConstant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/redisController")
public class RedisController{
	
	private static final Log log = LogFactory.getLog(RedisController.class);
	
	@Autowired
    private RedisService redisService;

	/**
     * 写入缓存
     * @param {"key": [String](必填), "value": [Object](必填), "expireTime": [Long](非必填)}
     * @return
     */
    @PostMapping("set")
	public JsonResult set(@RequestBody(required = false) Map<String, Object> params) {
    	JsonResult jsonResult = new JsonResult();
    	if(StringUtil.isBlank1(params.get(RedisConstant.KEY))){
			jsonResult.create(BaseBizExceptionCode.CACHE_REDIS_SET_ERROR, RedisConstant.KEY+"必须");
		}
		if(StringUtil.isBlank1(params.get(RedisConstant.VALUE))){
			jsonResult.create(BaseBizExceptionCode.CACHE_REDIS_SET_ERROR, RedisConstant.VALUE+"必须");
		}
		
		if(StringUtil.isBlank1(params.get(RedisConstant.EXPIRE_TIME))){
			redisService.set(params.get(RedisConstant.KEY).toString(), params.get(RedisConstant.VALUE));
		}else{
			try {		    	
				Long expireTime = Long.valueOf(params.get(RedisConstant.EXPIRE_TIME).toString()) ;
				
				redisService.set(params.get(RedisConstant.KEY).toString(), params.get(RedisConstant.VALUE), expireTime);
			} catch (Exception e) {
				log.error(e);
				return jsonResult.create(BaseBizExceptionCode.PARAM_ERROR, RedisConstant.EXPIRE_TIME+"必须为Long类型");
			}
		}
		
		jsonResult.success();
    	
        return jsonResult;
    }
    
    /**
	 * 查看某个key的剩余生存时间单位[秒]
	 * 永久生存或者不存在的都返回-1
	 * 
	 * @param key
	 *            键 . <br/>
	 * @return expireInSeconds .
	 */
    @GetMapping("ttl/{key}")
	public JsonResult ttl(@PathVariable String key) {
        return new JsonResult().success(redisService.ttl(key));
	}
	
    /**
	 * 删除(支持批量)
	 * 
	 * @param keys
	 *            键 . <br/>
	 * @return expireInSeconds .
	 */
    @GetMapping("remove/{key}")
	public JsonResult remove(@PathVariable String key) {
    	redisService.remove(key);
        return new JsonResult().success();
	}
    
    /**
     * 判断缓存中是否有对应的value
     * @param key
     * @return
     */
    @GetMapping("exists/{key}")
	public JsonResult exists(@PathVariable String key) {
        return new JsonResult().success(redisService.exists(key));
    }
    
    /**
     * 读取缓存
     * @param key
     * @return
     */
    @GetMapping("get/{key}")
	public JsonResult get(@PathVariable String key) {
        return new JsonResult().success(redisService.get(key));
    }

}
