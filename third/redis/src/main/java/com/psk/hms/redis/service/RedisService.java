package com.psk.hms.redis.service;

import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.BaseUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service("redisService")
public class RedisService {

	private static final Log log = LogFactory.getLog(RedisService.class);
	
	@SuppressWarnings("rawtypes")
	@Autowired
    private RedisTemplate redisTemplate;
	
	@SuppressWarnings("unchecked")
	public boolean multiSet(List<Map<String, Object>> paramList) {
		List<Object> txResults = null;
		if(!BaseUtil.isEmpty(paramList)) {
			//execute a transaction
			txResults = (List<Object>) redisTemplate.execute(new SessionCallback<List<Object>>() {
				public List<Object> execute(RedisOperations operations) throws DataAccessException {

					operations.multi();
					//这里不能直接调用下面的重载方法，因为要控制事务
					paramList.forEach(params ->{
						params.forEach((key, value) ->{
							operations.opsForValue().set(key, value);
						});
					});

					return operations.exec();
				}
			});
		}
		
		return txResults == null ? false : true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean multiSet(Map<String, Object> params) {
		List<Object> txResults = null;
		if(!BaseUtil.isEmpty(params)) {
			//execute a transaction
			txResults = (List<Object>) redisTemplate.execute(new SessionCallback<List<Object>>() {
				public List<Object> execute(RedisOperations operations) throws DataAccessException {

					operations.multi();

					params.forEach((key, value) ->{
						operations.opsForValue().set(key, value);
					});

					return operations.exec();
				}
			});
		}
		
		return txResults == null ? false : true;
	}
	
    /**
     * 写入缓存
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            result = true;
        } catch (Exception e) {
        	log.error(e);
        	throw new BizException(BaseBizExceptionCode.CACHE_REDIS_SET_ERROR, "缓存写入失败");
        }
        return result;
    }
    
    /**
	 * 查看某个key的剩余生存时间单位[秒]
	 * 永久生存或者不存在的都返回-1
	 * 
	 * @param key
	 *            键 . <br/>
	 * @return expireInSeconds .
	 */
	@SuppressWarnings("unchecked")
	public long ttl(Object key) {
		return redisTemplate.getExpire(key);
	}
	
    /**
     * 写入缓存设置时效时间
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean set(final String key, Object value, Long expireTime) {
        boolean result = false;
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
        	log.error(e);
        	throw new BizException(BaseBizExceptionCode.CACHE_REDIS_SET_ERROR, "缓存写入失败");
        }
        return result;
    }
    /**
     * 批量删除对应的value
     * @param keys
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            remove(key);
        }
    }

    /**
     * 批量删除key
     * @param pattern
     */
    @SuppressWarnings("unchecked")
	public void removePattern(final String pattern) {
        Set<Serializable> keys = redisTemplate.keys(pattern);
        if (keys.size() > 0)
            redisTemplate.delete(keys);
    }
    /**
     * 删除对应的value
     * @param key
     */
    @SuppressWarnings("unchecked")
	public void remove(final String key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }
    /**
     * 判断缓存中是否有对应的value
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean exists(final String key) {
        return redisTemplate.hasKey(key);
    }
    /**
     * 读取缓存
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object get(final String key) {
        Object result = null;
        ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
        result = operations.get(key);
        return result;
    }
    /**
     * 哈希 添加
     * @param key
     * @param hashKey
     * @param value
     */
    @SuppressWarnings("unchecked")
	public void hmSet(String key, Object hashKey, Object value){
        HashOperations<String, Object, Object> hash = redisTemplate.opsForHash();
        hash.put(key,hashKey,value);
    }

    /**
     * 哈希获取数据
     * @param key
     * @param hashKey
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object hmGet(String key, Object hashKey){
        HashOperations<String, Object, Object>  hash = redisTemplate.opsForHash();
        return hash.get(key,hashKey);
    }

    /**
     * 列表添加
     * @param k
     * @param v
     */
    @SuppressWarnings("unchecked")
	public void lPush(String k,Object v){
        ListOperations<String, Object> list = redisTemplate.opsForList();
        list.leftPush(k, v);
    }

    /**
     * 列表获取
     * @param k
     * @param l
     * @param l1
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<Object> lRange(String k, long l, long l1){
        ListOperations<String, Object> list = redisTemplate.opsForList();
        return list.range(k,l,l1);
    }

    /**
     * 集合添加
     * @param key
     * @param value
     */
    @SuppressWarnings("unchecked")
	public void add(String key,Object value){
        SetOperations<String, Object> set = redisTemplate.opsForSet();
        set.add(key,value);
    }

    /**
     * 集合获取
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public Set<Object> setMembers(String key){
        SetOperations<String, Object> set = redisTemplate.opsForSet();
        return set.members(key);
    }

    /**
     * 有序集合添加
     * @param key
     * @param value
     * @param scoure
     */
    @SuppressWarnings("unchecked")
	public void zAdd(String key,Object value,double scoure){
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        zset.add(key,value,scoure);
    }

    /**
     * 有序集合获取
     * @param key
     * @param scoure
     * @param scoure1
     * @return
     */
    @SuppressWarnings("unchecked")
	public Set<Object> rangeByScore(String key,double scoure,double scoure1){
        ZSetOperations<String, Object> zset = redisTemplate.opsForZSet();
        return zset.rangeByScore(key, scoure, scoure1);
    }
}
