package com.psk.hms.activemq.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.jms.Queue;

@Service
public class MessageProducerService {

	@Resource
    private JmsMessagingTemplate jmsMessagingTemplate;
	
    @Autowired
    private Queue queue;
    
    public void sendMessage(String msg) {
        this.jmsMessagingTemplate.convertAndSend(this.queue, msg);
    }

}
