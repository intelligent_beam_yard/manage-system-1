package com.psk.hms.core.service.impl;

import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.entity.CommonTreeParam;
import com.psk.hms.base.entity.DragTreeNodeBean;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.BaseService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述: service 基类实现
 * 作者: xiangjz
 * 时间: 2017年8月27日
 * 版本: 1.0
 *
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {

	protected static Log log = LogFactory.getLog(BaseServiceImpl.class);
	
	protected abstract BaseDao getDao();

	public T findById(String id) {
		return this.getDao().findById(id);
	}

	/**
	 * 分页查询 .
	 * @param sqlId
	 * @param args
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public PageBean listPage(String sqlId, Object args, int pageNum, int pageSize) {
        return this.getDao().listPage(sqlId, args, pageNum, pageSize);
    }
	@SuppressWarnings("rawtypes")
	public PageBean listPage(Object args, int pageNum, int pageSize) {
        return this.getDao().listPage(args, pageNum, pageSize);
    }

	/**
	 * 根据条件查询 listBy: <br/>
	 * 
	 * @return 返回集合
	 */
	public List<T> listBy(Map<String, Object> params) {
		return this.getDao().listBy(params);
	}

	public List<Object> listBy(Map<String, Object> params, String sqlId) {
		return this.getDao().listBy(params, sqlId);
	}

	/**
	 * 根据条件查询 listBy:
	 * 
	 * @return 返回实体
	 */
	public T findBy(Map<String, Object> params) {
		return this.getDao().findBy(params);
	}

	public Object findBy(Map<String, Object> params, String sqlId) {
		return this.getDao().findBy(params, sqlId);
	}
	
	/**
	 * 拖拽改变节点
	 * @param node 原始节点
	 * @param destParentId 目标父节点id
	 * @param destLeftSort 目标左排序
	 * @param destRightSort 目标右排序
	 * 
	 * @version 1.0.1  增加了树节点自定义筛选条件otherCondition字段
	 * 
	 * @description
	 * 参数node中，所有后缀为FiledName的字段为对应的表字段名的驼峰命名
	 * 默认存在这些字段
	 * 
	 * 注意：
	 * 如果调用者所在的表结构字段名不是默认值，则自行set
	 * 		例如：父节点id在表中字段为father_id，则setParentIdFieldName("fatherId");
	 * 如果调用者所在的表结构没有某些字段，则set为空
	 * 		例如：levels层级字段在我的表中没有这个字段，则setLevelsFieldName(""); 或者setLevelsFieldName(null);
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public void dragTreeNode(DragTreeNodeBean node) {
		
		/**
		 * 拖拽节点的逻辑实现
		 * 1、计算新节点的位置和层次，即sort、levels
		 * 2、根据sort判断是否需要更改新节点位置之后的节点的数据(sort)
		 * 3、根据新节点isParent判断是否需要更改新节点下所有子节点的数据(parentIds、levels)
		 * 
		 * 4、更改新节点之后的节点的sort，如果目标节点下没有节点，则改变新节点的父节点的isParent
		 * 5、更改新节点的相关数据，主要是(parentId、parentIds、levels、sort)
		 * 6、更改新节点的所有子节点的levels、parentIds
		 * 7、更改原始节点之后的节点的sort
		 * 8、如果原始节点位置移动之后，原始parentNode没有子节点，则改变父节点的isParent
		 */
		
		// -------------------- 计算需要的参数 start --------------------
		//是否包含的字段
		boolean hasParentIds = BaseUtil.isEmpty(node.getParentIdsFieldName())?false:true;
		boolean hasIsParent = BaseUtil.isEmpty(node.getIsParentFieldName())?false:true;
		boolean hasLevels = BaseUtil.isEmpty(node.getLevelsFieldName())?false:true;
		boolean hasSort = BaseUtil.isEmpty(node.getSortFieldName())?false:true;
		//查询原始的节点信息
		Map<String, Object> origNode = this.getDao().findById(node.getId());
		if(BaseUtil.isEmpty(origNode))
			return;
		//原始的parentId
		String origParentId = BaseUtil.retStr(origNode.get(node.getParentIdFieldName()));
		//计算目标位置
		int destSort = 0;
		//是否需要更改拖拽目标点之后的node
		boolean changeDestRight = false;
		
		//原始的排序sort
		int origSort = 0;
		//如果有排序字段，则计算目标的sort
		if(hasSort) {
			origSort = BaseUtil.retInt(origNode.get(node.getSortFieldName()));
			int destLeftSort = node.getDestLeftSort();
			int destRightSort = node.getDestRightSort();
			if(!BaseUtil.isEmpty(destLeftSort) && !BaseUtil.isEmpty(destRightSort)) {
				//拖拽到两节点中间，以左节点为基准+1，且需要改变右边之后的节点
				destSort = destLeftSort + 1;
				changeDestRight = true;
			}else if(BaseUtil.isEmpty(destRightSort)) {
				//拖拽到最右边，以左节点为基准+1
				destSort = destLeftSort + 1;
			}else if(BaseUtil.isEmpty(destLeftSort)) {
				//只需要改变右边之后的节点
				changeDestRight = true;
			}
		}
		try {
			//查询被拖拽节点的parentIds
			String destParentId = node.getDestParentId();
			String destParentIds = "";
			int destLevels = 0;
			if(!BaseUtil.isEmpty(destParentId)) {
				Map<String, Object> destParentNode = (Map<String, Object>) this.findById(destParentId);
				if(!BaseUtil.isEmpty(destParentNode)) {
					destParentIds = BaseUtil.retStr(destParentNode.get(node.getParentIdsFieldName())) 
									+ BaseUtil.retStr(destParentNode.get(node.getIdFieldName()))
									+ ",";
					destLevels = BaseUtil.retInt(destParentNode.get(node.getLevelsFieldName())) + 1;
				}
			}
			// -------------------- 计算需要的参数 end --------------------
			
			// -------------------- 执行修改 start --------------------
			//第4步：修改新node之后的所有节点的sort
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put(node.getParentIdFieldName(), destParentId);
			if(!BaseUtil.isEmpty(node.getOtherCondition()))
				condition.putAll(node.getOtherCondition());
			//查询目标node所在那一层的集合，并修改新node之后的sort
			List<Map<String, Object>> destGroup = (List<Map<String, Object>>) this.listBy(condition);
			if(hasSort && !BaseUtil.isEmpty(destGroup) && changeDestRight) {
				//循环比对，sort大于等于destSort的全部+1
				for (int index = 0; index < destGroup.size(); index ++) {
					Map<String, Object> destItem = destGroup.get(index);
					int destItemSort = BaseUtil.retInt(destItem.get(node.getSortFieldName()));
					if(destItemSort >= destSort) {
						destItem.put(node.getSortFieldName(), 
								BaseUtil.retInt(destItem.get(node.getSortFieldName())) + 1);
						this.getDao().update(destItem);
					}
				}
			}
			if(hasIsParent && BaseUtil.isEmpty(destGroup)) {
				//如果目标节点下没有节点，则改变新节点的父节点的isParent
				Map<String, Object> destParentNode = (Map<String, Object>) this.findById(destParentId);
				destParentNode.put(node.getIsParentFieldName(), "1");
				this.getDao().update(destParentNode);
			}
			
			//第5步：修改原始节点为新节点
			Map<String, Object> obj = (Map<String, Object>) this.findById(node.getId());
			obj.put(node.getParentIdFieldName(), destParentId);
			if(hasParentIds)
				obj.put(node.getParentIdsFieldName(),destParentIds);
			if(hasLevels)
				obj.put(node.getLevelsFieldName(), destLevels);
			if(hasSort)
				obj.put(node.getSortFieldName(), destSort);
			this.getDao().update(obj);
			
			//第6步：更改新node下的parentIds和levels
			if(hasLevels || hasSort)
				cycleChangeChild(node, node.getId(), destLevels);
			
			//第7步：更改原始节点之后的节点的sort
			condition = new HashMap<String, Object>();
			condition.put(node.getParentIdFieldName(), origParentId);
			if(!BaseUtil.isEmpty(node.getOtherCondition()))
				condition.putAll(node.getOtherCondition());
			List<Map<String, Object>> origGroup = (List<Map<String, Object>>) this.listBy(condition);
			if(hasSort && !BaseUtil.isEmpty(origGroup)) {
				//循环比对，sort大于origSort的全部-1
				for (int index = 0; index < origGroup.size(); index ++) {
					Map<String, Object> destItem = origGroup.get(index);
					int destItemSort = BaseUtil.retInt(destItem.get(node.getSortFieldName()));
					if(destItemSort > origSort) {
						destItem.put(node.getSortFieldName(), 
								BaseUtil.retInt(destItem.get(node.getSortFieldName())) - 1);
						this.getDao().update(destItem);
					}
				}
			}
			if(hasIsParent && BaseUtil.isEmpty(origGroup)) {
				//第8步：如果原始节点位置移动之后，原始parentNode没有子节点，则改变父节点的isParent
				Map<String, Object> origParentNode = (Map<String, Object>) this.findById(origParentId);
				origParentNode.put(node.getIsParentFieldName(), "0");
				this.getDao().update(origParentNode);
			}
		} catch (IllegalAccessException e) {
			log.debug(e);
			throw new BizException("拖拽时服务端错误");
		} catch (InvocationTargetException e) {
			log.debug(e);
			throw new BizException("拖拽时服务端错误");
		} catch (Exception e) {
			log.debug(e);
			throw new BizException("拖拽时服务端错误");
		}
		// -------------------- 执行修改 end --------------------
	}
	
	/**
	 * 递归更改新节点下所有子节点的parentIds和levels
	 * @param node
	 * @param id
	 * @param levels
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	@SuppressWarnings("unchecked")
	private void cycleChangeChild(DragTreeNodeBean node, String id, int levels) throws IllegalAccessException, InvocationTargetException {
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put(node.getIdFieldName(), id);
		Map<String, Object> thisNode = this.getDao().findById(id);
		if(BaseUtil.isEmpty(thisNode))
			return;
		//获取当前节点的parentIds
		String destParentIds = BaseUtil.retStr(thisNode.get(node.getParentIdsFieldName()));
		condition = new HashMap<String, Object>();
		condition.put(node.getParentIdFieldName(), id);
		if(!BaseUtil.isEmpty(node.getOtherCondition()))
			condition.putAll(node.getOtherCondition());
		//查询目标node的所有子节点
		List<Map<String, Object>> destChildren = (List<Map<String, Object>>) this.listBy(condition);
		if(!BaseUtil.isEmpty(destChildren)) {
			for (int index = 0; index < destChildren.size(); index ++) {
				Map<String, Object> destChild = destChildren.get(index);
				if(!BaseUtil.isEmpty(node.getLevelsFieldName()))
					destChild.put(node.getLevelsFieldName(), levels + 1);
				if(!BaseUtil.isEmpty(node.getParentIdsFieldName()))
					destChild.put(node.getParentIdsFieldName(), destParentIds + id + ",");
				this.getDao().update(destChild);
				//查询节点是否拥有子节点，有则递归更改
				cycleChangeChild(node, 
						BaseUtil.retStr(destChild.get(node.getIdFieldName())), 
						BaseUtil.retInt(destChild.get(node.getLevelsFieldName())));
			}
		}
	}



	//------------------向下递归子 start------------------

	/**
	 * 向下递归获取list
	 * @param ctp
	 * @return
	 */
	public List<T> getChildrenById(CommonTreeParam ctp) {
		Integer realLevels = null;
		Map<String, Object> thizMap = null;
		String rootId = null;
		//如果没有指定id，则找父节点为0的，即根节点
		if(!BaseUtil.isEmpty(ctp.getId()))
			thizMap = this.getDao().findById(ctp.getId());
		else {
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("parentId", "0");
			List<Map<String, Object>> resultList = this.getDao().listBy(condition);
			if(!BaseUtil.isEmpty(resultList))
				thizMap = resultList.get(0);
		}
		if(!BaseUtil.isEmpty(thizMap)) {
			if(BaseUtil.isEmpty(ctp.getLevels())) {

			}else if(ctp.getLevels() == 0) {
				//只查询本层级的数据
				realLevels = -1;
			} else {
				Integer thizLevels = BaseUtil.retInt(thizMap.get(ctp.getLevelsName()));
				realLevels = ctp.getLevels() + thizLevels;
			}
			rootId = BaseUtil.retStr(thizMap.get("id"));
		} else {
			return new ArrayList<T>();
		}
		return cycleChildren(rootId, ctp.getCycleName(), ctp.getLevels(), ctp.getLevelsName(), realLevels, ctp.getSqlId(), ctp.getSqlParam());
	}

	private List<T> cycleChildren(String id, String cycleName, Integer levels, String levelsName, Integer realLevels, String sqlId, Map<String, Object> sqlParam) {
		Map<String, Object> params = BaseUtil.isEmpty(sqlParam)?new HashMap<String, Object>():sqlParam;
		params.put(cycleName, id);
		List<T> resultList = this.getDao().getList(sqlId, params);
		if(!BaseUtil.isEmpty(resultList)) {
			for (Object resultObj : resultList) {
				Map<String, Object> result = (Map<String, Object>) resultObj;
				if(!BaseUtil.isEmpty(realLevels) && (realLevels < 0 || (0 <= realLevels && realLevels < BaseUtil.retInt(result.get(levelsName))))) {
					continue;
				}
				params.put(cycleName, result.get("id"));
				List<T> childrenList =
						cycleChildren(BaseUtil.retStr(result.get("id")), cycleName, levels, levelsName, realLevels, sqlId, sqlParam);
				if(!BaseUtil.isEmpty(childrenList)) {
					result.put("children", childrenList);
					result.put("childrenNum", childrenList.size());
				}
			}
		}
		return resultList;
	}
	//------------------向下递归子 end------------------

	//------------------向上递归父 start------------------

	/**
	 * 向上递归获取list，子在前，父在后
	 * @param ctp
	 * @return
	 */
	public List<T> getAllParentsById(CommonTreeParam ctp) {
		List<T> listObject = new ArrayList<T>();
		getAllParentsById(listObject, ctp.getId(), ctp.getCycleName());
		return listObject;
	}

	private void getAllParentsById(List<T> listObject, String id, String cycleName) {
		T result = this.getDao().findById(id);
		if(!BaseUtil.isEmpty(result)) {
			listObject.add(result);
			//获得查询出来的父id
			Map<String, Object> resultMap = (Map<String, Object>) result;
			String parantId = BaseUtil.retStr(resultMap.get(cycleName));
			if(!BaseUtil.isEmpty(parantId) && !"0".equals(parantId) && !"root".equals(parantId)) {
				//有父id，则继续递归
				this.getAllParentsById(listObject, parantId, cycleName);
			}
		}
	}

	/**
	 * 向上递归获取父ids，_隔开，父在前，子在后
	 * @param ctp
	 * @return
	 */
	public String getAllParentIdsById(CommonTreeParam ctp) {
		StringBuffer parentIds = new StringBuffer();
		getAllParentIdsById(parentIds, ctp.getId(), ctp.getCycleName());
		String parentIdsStr = parentIds.toString();
		if(!"".equals(parentIdsStr))
			parentIdsStr = parentIdsStr.substring(0, parentIdsStr.length()-1);
		return parentIdsStr;
	}

	private void getAllParentIdsById(StringBuffer parentIds, String id, String cycleName) {
		Map<String, Object> result = this.getDao().findById(id);
		if(!BaseUtil.isEmpty(result)) {
			//获得查询出来的父id
			String parantId = BaseUtil.retStr(result.get(cycleName));
			if(!BaseUtil.isEmpty(parantId) && !"0".equals(parantId)) {
				//有父id，则继续递归
				parentIds.insert(0, parantId+Constant.PARENT_IDS_SEPARATOR);
				this.getAllParentIdsById(parentIds, parantId, cycleName);
			}
		}
	}
	//------------------向上递归父 end------------------

	/**
	 * 返回树结构的参数
	 * @param ctp
	 * @return
	 */
	public Map<String, Object> getTreeParamResult(CommonTreeParam ctp) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> params = ctp.getOtherParams();
		//这里主要来验证前端parentId是否传递，若无，则视为根节点下第一级数据
		if(!BaseUtil.isEmpty(params)) {
			String parentId = BaseUtil.retStr(params.get(ctp.getCycleName()));
			if(BaseUtil.isEmpty(parentId)) {
				//parentId为空，则为根节点下一级菜单
				Map<String, Object> condition = new HashMap<String, Object>();
				condition.put(ctp.getCycleName(), "0");
				Map<String, Object> root = this.getDao().findBy(condition);
				if(BaseUtil.isEmpty(root))
					throw new BizException().DB_SELECTONE_IS_NULL;
				result.put(CommonTreeParam.ROOT_ID, root.get("id"));
				ctp.setId(BaseUtil.retStr(root.get("id")));
			}
		}

		String parentIds = getAllParentIdsById(ctp);
		Integer levels = 0;
		if(!BaseUtil.isEmpty(parentIds))
			levels = parentIds.split(Constant.PARENT_IDS_SEPARATOR).length;
		result.put("parentIds", parentIds);
		result.put("levels", levels);
		return result;
	}

}
