package com.psk.hms.core.dao;

import java.util.List;
import java.util.Map;

/**
 * Created by CISDI on 2017/10/18.
 */
public interface BaseDao {
	
	/**
	 * 根据条件查询 listBy: <br/>
	 * @param <T>
	 * 
	 * @param paramMap
	 * @return 返回集合
	 */
	<T> List<T> listBy(Map<String, Object> paramMap);

    List<Object> listBy(Map<String, Object> paramMap, String sqlId);

    /**
     * 调用指定sqlID执行查询返回结果List，
     * @param sqlId
     * @param args
     * @param <E> 结果List泛型
     * @return 结果List
     */
    <E> List<E> getList(String sqlId, Object args);

    /**
     * 调用指定sqlID执行查询返回指定页的结果
     * @param sqlId
     * @param args
     * @param pageNum 页码
     * @param pageSize 页大小
     * @param <T> 分页结果泛型
     * @return 结果List
     */
    <T> T listPage(String sqlId, Object args, int pageNum, int pageSize);
    <T> T listPage(Object args, int pageNum, int pageSize);
    
    /**
     * 调用指定sqlID执行查询返回结果
     * @param sqlId
     * @param args
     * @param <T> 结果泛型
     * @return
     */
    <T> T getOneBySqlId(String sqlId, Object args);
    <T> T findBy(Object args);
    Object findBy(Object args, String sqlId);

    /**
	 * 通过实体id查找实体
	 *
	 * 参数: 实体id
	 * 返回: 实体对象
     * @param <T>
	 */
	<T> T findById(String id);

    /**
     * 执行更新语句，返回受影响行数
     * @param sqlId sqlId
     * @param args 更新参数
     * @return 受影响行数
     */
    int update(String sqlId, Object args);
    int update(Object args);

    /**
     * 执行插入语句，返回受影响行数
     * @param sqlId sqlId
     * @param args 插入参数
     * @return 受影响行数
     */
    int insert(String sqlId, Object args);

    int insert(Object args);
    /**
     * 执行删除语句，返回受影响行数
     * @param sqlId sqlId
     * @param args 删除参数
     * @return 受影响行数
     */
    int delete(String sqlId, Object args);
	
}
