package com.psk.hms.core.service;

import com.psk.hms.base.entity.PageBean;

import java.util.List;
import java.util.Map;

/**
 * 描述: 基类service接口
 * 作者: xiangjz
 * 时间: 2017年8月27日
 * 版本: 1.0
 *
 */
public interface BaseService<T> {

	/**
	 * 根据ID查找记录.
	 * 
	 * @param id
	 *            .
	 * @return entity .
	 */
	T findById(String id);

	/**
	 * 根据条件查询 listBy: <br/>
	 * 
	 * @param params
	 * @return 返回集合
	 */
	List<T> listBy(Map<String, Object> params);

	List<Object> listBy(Map<String, Object> params, String sqlId);

	/**
	 * 根据条件查询 listBy: <br/>
	 * 
	 * @param params
	 * @return 返回实体
	 */
	T findBy(Map<String, Object> params);

	Object findBy(Map<String, Object> params, String sqlId);
	
	@SuppressWarnings("rawtypes")
	PageBean listPage(String sqlId, Object args, int pageNum, int pageSize);
	
	@SuppressWarnings("rawtypes")
	PageBean listPage(Object args, int pageNum, int pageSize);

}
