package com.psk.hms.core.dao.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.psk.hms.base.constant.common.PageBeanConstant;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CISDI on 2017/10/18.
 */
public class BaseDaoImpl implements BaseDao {
	/**
	 * sqlId
	 */
	public static final String SQL_ID = "sqlId";
	
	public static final String SQL_UPDATE = "update";
	
	public static final String SQL_DELETE = "delete";

	public static final String SQL_INSERT = "insert";
	
	public static final String SQL_FIND_BY = "findBy";
	
	public static final String SQL_LIST_BY = "listBy";
	
	public static final String SQL_FIND_BY_ID = "findById";
	
	public static final String SQL_LIST_PAGE = "listBy";
	
	public static final String SQL_COUNT_BY_PAGE_PARAM = "countByPageParam"; // 根据当前分页参数进行统计

    @Autowired
    protected SqlSession sqlSession;


    /**
     * 调用指定sqlID执行查询返回结果List，
     *
     * @param sqlId
     * @param args
     * @param <E>   结果List泛型
     * @return 结果List
     */
    @Override
    public <E> List<E> getList(String sqlId, Object args) {
        return sqlSession.selectList(getStatement(sqlId), args);

    }

    /**
     * 调用指定sqlID执行查询返回指定页的结果
     *
     * @param sqlId
     * @param args
     * @param pageNum  页码
     * @param pageSize 页大小
     * @return 结果List
     */
    @SuppressWarnings({"rawtypes", "unchecked" })
	public PageBean listPage(String sqlId, Object args, int pageNum, int pageSize) {
    	PageHelper.startPage(pageNum, pageSize);
        if(!StringUtil.isBlank1(((Map<String, Object>)args).get(PageBeanConstant.ORDER_COLUNM)) && !StringUtil.isBlank1(((Map<String, Object>)args).get(PageBeanConstant.ORDER_MODE))){
        	PageHelper.orderBy(((Map<String, Object>)args).get(PageBeanConstant.ORDER_COLUNM).toString()+" "+((Map<String, Object>)args).get(PageBeanConstant.ORDER_MODE));
        }
    	PageBean pageBean = new PageBean<>((Page) sqlSession.selectList(getStatement(sqlId), args));
    	pageBean.setOrderColunm(((Map<String, Object>)args).get(PageBeanConstant.ORDER_COLUNM));
		pageBean.setOrderMode(((Map<String, Object>)args).get(PageBeanConstant.ORDER_MODE));
        return pageBean;
    }
    @SuppressWarnings({"rawtypes", "unchecked"})
	public  PageBean listPage(Object args, int pageNum, int pageSize) {
        return this.listPage(SQL_LIST_PAGE, args, pageNum, pageSize);
    }

    /**
     * 调用指定sqlID执行查询返回结果
     *
     * @param sqlId
     * @param args
     * @param <T>   结果泛型
     * @return
     */
    @Override
    public <T> T getOneBySqlId(String sqlId, Object args) {
        return sqlSession.selectOne(getStatement(sqlId), args);
    }
	@SuppressWarnings("unchecked")
	public <T> T findBy(Object args) {
		return (T) this.findBy(args, SQL_FIND_BY);
	}
    @Override
    public Object findBy(Object args, String sqlId) {
        return sqlSession.selectOne(getStatement(sqlId), args);
    }
    
    public <T> T findById(String id) {
		return sqlSession.selectOne(getStatement(SQL_FIND_BY_ID), id);
	}

    /**
     * 执行更新语句，返回受影响行数
     *
     * @param sqlId sqlId
     * @param args  更新参数
     * @return 受影响行数
     */
    @Override
    public int update(String sqlId, Object args) {
        return sqlSession.update(getStatement(sqlId), args);
    }
    
    @Override
    public int update(Object args) {
        return sqlSession.update(getStatement(SQL_UPDATE), args);
    }

    /**
     * 执行插入语句，返回受影响行数
     *
     * @param sqlId sqlId
     * @param args  插入参数
     * @return 受影响行数
     */
    @Override
    public int insert(String sqlId, Object args) {
        return sqlSession.insert(getStatement(sqlId), args);
    }
    
    @Override
    public int insert(Object args) {
        return sqlSession.insert(getStatement(SQL_INSERT), args);
    }

    /**
     * 执行删除语句，返回受影响行数
     *
     * @param sqlId sqlId
     * @param args  删除参数
     * @return 受影响行数
     */
    @Override
    public int delete(String sqlId, Object args) {
        return sqlSession.delete(getStatement(sqlId), args);
    }
    
    public int delete(Object args) {
        return sqlSession.delete(getStatement(SQL_DELETE), args);
    }
    
    
    /*********************add********************/
    
    public String getStatement(String sqlId) {
		String name = this.getClass().getName();
		StringBuffer sb = new StringBuffer().append(name).append(".").append(sqlId);
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> listBy(Map<String, Object> paramMap) {
		return (List<T>) this.listBy(paramMap, SQL_LIST_BY);
	}

	@Override
	public List<Object> listBy(Map<String, Object> paramMap, String sqlId) {
		if (paramMap == null)
			paramMap = new HashMap<String, Object>();

		return sqlSession.selectList(getStatement(sqlId), paramMap);
	}

}
