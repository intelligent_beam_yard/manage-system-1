package com.psk.hms.service.base.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.Map;


/**
 * BaseRoleEmployeeDao
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-22
 * @version 1.0.0
 */
public interface BaseRoleEmployeeDao extends BaseDao {
    /**
	 * 逻辑删除
	 * @param params
	 * @return
	 */
	public int logicDelete(Map<String, Object> params);
}