package com.psk.hms.service.base.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.List;
import java.util.Map;

public interface BaseUserInfoDao extends BaseDao {
    //逻辑删除
    int logicDelete(Map<String, Object> params);

    //查询总数
    Integer count(Map<String, Object> params);

    /**
     * 根据员工id查询用户ids
     *
     * @param params
     * @return
     */
    List<String> getUserIdListByEmplyeeId(Map<String, Object> params);


    List<Map<String, Object>> listByForView(Map<String, Object> params);
}
