package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.constant.base.BaseUserInfoConstant;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.rsa.IDEAUtil;
import com.psk.hms.base.util.security.Pbkdf2Util;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.constant.LoginConstant;
import com.psk.hms.service.base.constant.UserConstant;
import com.psk.hms.service.base.service.BaseUserInfoService;
import com.psk.hms.service.base.service.BaseUserLoginService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("baseUserLoginService")
public class BaseUserLoginServiceImpl extends BaseServiceImpl implements BaseUserLoginService {

    @Autowired
    private BaseUserInfoService baseUserInfoService;

    @Override
    protected BaseDao getDao() {
        return null;
    }

    @Override
    public Map<String, Object> login(Map<String, Object> params) {
        // 验证用户账号密码
        String userName = BaseUtil.retStr(params.get(BaseUserInfoConstant.USER_NAME));
        String password = BaseUtil.retStr(params.get(BaseUserInfoConstant.PASSWORD));
        return this.checkUser(userName, password);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> checkUser(String userName, String password) {
        Map<String, Object> userInfoParams = new HashMap<>();
        userInfoParams.put(BaseUserInfoConstant.USER_NAME, userName);
        Map<String, Object> user = (Map<String, Object>) baseUserInfoService.findBy(userInfoParams);
        if (BaseUtil.isEmpty(user)) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "用户不存在");
        }

        // 验证用户状态 是否冻结..
        if (UserConstant.STATE_FROZEN.equals(user.get(BaseUserInfoConstant.STATE))) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户被冻结");
        }
        // 3.密码错误次数超限
        int errorCount = (Integer) user.get(BaseUserInfoConstant.ERROR_COUNT);
        int passErrorCount = LoginConstant.WEB_PWD_INPUT_ERROR_LIMIT;
        if (errorCount >= passErrorCount) {
            Date stopDate = (Date) user.get("stopDate");
            int hourSpace = DateUtil.getDateHourSpace(stopDate, DateUtil.getDate());
            int passErrorHour = LoginConstant.PASSERRORHOUR;
            if (hourSpace < passErrorHour) {
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR,
                        "超过最大密码错误次数，" + LoginConstant.PASSERRORHOUR + "小时不能登录");
            }
        }

        // 验证密码
        String saltStr = BaseUtil.retStr(user.get(BaseUserInfoConstant.SALT)); // 密码盐
        byte[] salt = Base64.decodeBase64(saltStr);
        byte[] encryptedPassword = null;
        try {
            encryptedPassword = Pbkdf2Util.getEncryptedPassword(password, salt);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        userInfoParams.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
        Map<String, Object> userInfoMap1 = (Map<String, Object>) baseUserInfoService.findBy(userInfoParams);
        if (userInfoMap1 == null) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "密码错误");
        } else
            return userInfoMap1;
    }

    @Override
    public Map<String, Object> register(Map<String, Object> params) {
        // 验证用户是否已注册
        Map<String, Object> userParams = new HashMap<>();
        userParams.put(BaseUserInfoConstant.USER_NAME, params.get(BaseUserInfoConstant.USER_NAME));
        Map<String, Object> user = (Map<String, Object>) baseUserInfoService.findBy(userParams);
        if (!BaseUtil.isEmpty(user)) {
            throw new BizException(BaseBizExceptionCode.USER_REGISTER_ERROR, "用户已被注册");
        }
        try {
            // 密码加密
            byte[] salt = Pbkdf2Util.generateSalt();// 密码盐
            String password = BaseUtil.retStr(params.get(BaseUserInfoConstant.PASSWORD));
            byte[] encryptedPassword = Pbkdf2Util.getEncryptedPassword(password, salt);
            params.put(BaseUserInfoConstant.SALT, Base64.encodeBase64String(salt));
            params.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
            params.put(BaseUserInfoConstant.SECRET_KEY, Base64.encodeBase64String(IDEAUtil.initKey()));
            params.put(BaseUserInfoConstant.PASSWORD_TYPE, BaseUserInfoConstant.PASSWORD_TYPE_2);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.USER_REGISTER_ERROR, "加密失败");
        }
        // 保存新的用户信息
        baseUserInfoService.addOrUpdate(params);
        return params;
    }

    @Override
    public Map<String, Object> adminLogin(Map<String, Object> params) {
        // 验证用户账号密码
        String userName = BaseUtil.retStr(params.get(BaseUserInfoConstant.USER_NAME));
        String password = BaseUtil.retStr(params.get(BaseUserInfoConstant.PASSWORD));
        return checkUser(userName, password);
    }

    @Override
    public Map<String, Object> getEmployeeStates(String employeeId) {
        return null;
    }
}
