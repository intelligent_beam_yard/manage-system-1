package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseRoleEmployeeDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * BaseRoleEmployeeDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-22
 * @version 1.0.0
 */
@Repository("baseRoleEmployeeDao")
public class BaseRoleEmployeeDaoImpl extends BaseDaoImpl implements BaseRoleEmployeeDao {
	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}