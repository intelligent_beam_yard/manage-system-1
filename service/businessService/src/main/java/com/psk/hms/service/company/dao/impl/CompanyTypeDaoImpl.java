package com.psk.hms.service.company.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.company.dao.CompanyTypeDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * CompanyTypeDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-22
 * @version 1.0.0
 */
@Repository("companyTypeDao")
public class CompanyTypeDaoImpl extends BaseDaoImpl implements CompanyTypeDao {

	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}