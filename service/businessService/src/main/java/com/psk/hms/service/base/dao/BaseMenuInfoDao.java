package com.psk.hms.service.base.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.Map;

/**
 * 描述： 菜单dao
 * 作者： xiangjz
 * 时间： 2018年4月2日
 * 版本： 1.0v
 */
public interface BaseMenuInfoDao extends BaseDao {
    /**
     * 逻辑删除
     * @param params
     * @return
     */
    public int logicDelete(Map<String,Object> params);
}
