package com.psk.hms.service.base.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * 用户登录Service，此service用于处理轻筑登录相关业务逻辑
 * 
 * @author xiangjz
 * @date 2018年12月25日
 * @version 1.1.0
 *
 */
public interface BaseUserLoginService {


	/**
	 * 网页用户登录
	 * 
	 * @param params
	 * @return
	 */
	public Map<String, Object> login(Map<String,Object> params);
	

	/**
	 * 校验用户有效性
	 * 
	 * @param params
	 * @return
	 */
	public Map<String,Object> checkUser(String userName, String password);
	
	/**
	 * 用户注册
	 * 
	 * @param params
	 * @return
	 */
	public Map<String,Object> register(Map<String, Object> params);
	
	
	/**
	 * admin端用户登录验证
	 * 
	 * @param params ==> userName	账号
	 * @param params ==> passWord	密码
	 * @param params ==> autoLogin	是否自动登录
	 * @return
	 */
	public Map<String, Object> adminLogin(Map<String, Object> params);
	

	/**
	 * 获取登录用户首屏相关参数
	 * 
	 * @param employeeId
	 * @return
	 */
	Map<String, Object> getEmployeeStates(String employeeId);
	
}
