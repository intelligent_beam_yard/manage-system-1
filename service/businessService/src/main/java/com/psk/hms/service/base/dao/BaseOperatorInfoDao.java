package com.psk.hms.service.base.dao;

import com.psk.hms.core.dao.BaseDao;

/**
 * 描述： 操作dao
 * 作者： xiangjz
 * 时间： 2018年4月2日
 * 版本： 1.0v
 */
public interface BaseOperatorInfoDao extends BaseDao {

}
