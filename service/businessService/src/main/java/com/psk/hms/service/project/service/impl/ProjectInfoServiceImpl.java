package com.psk.hms.service.project.service.impl;

import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.project.ProjectBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.project.dao.ProjectInfoDao;
import com.psk.hms.service.project.service.ProjectInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @link ProjectInfoServiceImpl 实现类
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@Service
public class ProjectInfoServiceImpl extends BaseServiceImpl<Map<String, Object>> implements ProjectInfoService {

    @Autowired
    private ProjectInfoDao projectInfoDao;

    @Override
	protected BaseDao getDao() {
		return projectInfoDao;
	}

    @SuppressWarnings("unchecked")
	@Transactional
	@Override
	public JsonResult addOrUpdate(Map<String,Object> params) {
		//获取记录id值
		String id = BaseUtil.retStr(params.get("id"));
		//获取当前时间
		Timestamp timestamp = DateUtil.getSqlTimestamp();
		if(BaseUtil.isEmpty(id)){
			id = StringUtil.getUuid(true);
			params.put("id", id);
			params.put("createUserId", params.get("userId"));
			params.put("createTime", DateUtil.getSqlTimestamp());
			params.put("editUserId", params.get("userId"));
			params.put("editTime", params.get("createTime"));
			int retVal = projectInfoDao.insert(params);
			if(retVal == 0) {
				throw new BizException(ProjectBizExceptionCode.PROJECT_INFO_SAVE_ERROR,
						"保存project_info失败");
			}
		}else{//修改
			Map<String, Object> dnMap = projectInfoDao.findById(id);
			dnMap.putAll(params);
			dnMap.put("editUserId", params.get("userId"));
			dnMap.put("editTime", timestamp);
			int retVal = projectInfoDao.update(dnMap);
			// 更新操作是否成功
			if(0 == retVal)
				throw new BizException(ProjectBizExceptionCode.PROJECT_INFO_UPDATE_ERROR,
						"修改project_info失败");
		}
		JsonResult jsonResult = new JsonResult();
		return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
	}
	
	@Transactional
	@Override
	public Map<String, Object> logicDelete(List<String> ids) {
		Map<String, Object> result = new HashMap<>(); 
		int count = 0;
		for (String id : ids) {
			if(!BaseUtil.isEmpty(id)) {
				Map<String, Object> tpn = this.findById(id);
				int retVal = projectInfoDao.logicDelete(tpn);
				if(retVal == 0)
					throw new BizException(ProjectBizExceptionCode.PROJECT_INFO_DELETE_ERROR,
							"删除project_info失败");
				count += retVal;
			}
		}
		result.put("count", count);
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public PageBean listPage(Map<String, Object> params, Integer pageNum, Integer pageSize) {
		PageBean page = projectInfoDao.listPage(params, pageNum, pageSize);
		return page;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Map<String, Object>> listAll(Map<String, Object> params) {
		List<Map<String, Object>> list = projectInfoDao.getList("listBy", params);
		return list;
	}
    
}