package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseOperatorPackageMapDao;
import org.springframework.stereotype.Repository;


@Repository("baseOperatorPackageMapDao")
public class BaseOperatorPackageMapDaoImpl extends BaseDaoImpl implements BaseOperatorPackageMapDao {

}
