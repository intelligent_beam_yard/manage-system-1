package com.psk.hms.service.base.service;

import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.core.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * BaseMenuInfoService
 *
 * @author xiangjz
 * @editBy
 * @date 2018-08-20
 * @version 1.0.0
 */
public interface BaseMenuInfoService extends BaseService<Map<String, Object>> {

	/**
	 * 添加、修改
	 * @param params
	 * @return
	 */
	public JsonResult addOrUpdate(Map<String,Object> params);


	/**
	 * 逻辑删除[<b>批量</b>]
	 *
	 * @param ids
	 * @return
	 */
	Map<String,Object> logicDelete(List<String> ids);

	/**
	 * 分页查询所有
	 * @param params
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	PageBean<Map<String, Object>> listPageSelf(Map<String, Object> params, Integer pageNum, Integer pageSize);

	/**
	 * 查询所有
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> listAll(Map<String, Object> params);

	/**
	 * 查询菜单树
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> menuTree(Map<String, Object> params);

	/**
	 * 查询菜单树(不验证权限，拉出所有的菜单树)
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> menuTreeForAdmin(Map<String, Object> params);
}