package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.entity.CommonTreeParam;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseRoleEmployeeDao;
import com.psk.hms.service.base.dao.BaseRoleInfoDao;
import com.psk.hms.service.base.service.BaseRoleInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("baseRoleInfoService")
public class BaseRoleInfoServiceImpl extends BaseServiceImpl implements BaseRoleInfoService {

    @Autowired
    private BaseRoleInfoDao baseRoleInfoDao;
    @Autowired
    private BaseRoleEmployeeDao baseRoleEmployeeDao;

    @Override
    protected BaseDao getDao() {
        return baseRoleInfoDao;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public JsonResult unBindEmployee(Map<String,Object> params) {
        JsonResult jsonResult = new JsonResult();
        if(BaseUtil.isEmpty(params) || BaseUtil.isEmpty(params.get("roleId"))
                || BaseUtil.isEmpty(params.get("employeeId")))
            return jsonResult.failure("参数为空，解绑失败");
        int retVal = baseRoleEmployeeDao.delete("deleteByParam", params);
        if(retVal == 0) {
            throw new BizException(BaseBizExceptionCode.ROLE_EMPLOYEE_MANAGE_DELETE_ERROR,
                    "角色真解绑失败");
        }
        return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public JsonResult bindEmployee(Map<String,Object> params) {
        //获取当前时间
        Timestamp timestamp = DateUtil.getSqlTimestamp();
        String id = null;
        params.put("createUserId", params.get("userId"));
        params.put("createTime", timestamp);
        params.put("editUserId", params.get("userId"));
        params.put("editTime", params.get("createTime"));

        params.put("employeeId", params.get("bindEmployeeId"));
        //接收两种参数，如果有单条roleId，新增单条；如果有roleIds集合，则新增多条，优先考虑多条
        if(!BaseUtil.isEmpty(params.get("roleIds"))) {
            //新增多条的时候，默认是前端勾选多个权限之后一起提交的，所以先真删除所有对应的权限，然后再批量新增
            //1、真删除所有绑定关联
            Map<String, Object> deleteParam = new HashMap<String, Object>();
            deleteParam.put("employeeId", params.get("employeeId"));
            baseRoleEmployeeDao.delete("deleteByParam", deleteParam);
            //2、批量新增
            List<String> roleIds = (List<String>) params.get("roleIds");
            roleIds.forEach(roleId ->{
                params.put("roleId", roleId);
                insert(params);
            });
        } else if(!BaseUtil.isEmpty(params.get("roleId"))) {
            params.put("roleId", params.get("roleId"));
            id = insert(params);
        }
        JsonResult jsonResult = new JsonResult();
        return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
    }

    private String insert(Map<String, Object> param) {
        String id = StringUtil.getUuid(true);
        param.put("id", id);
        int retVal = baseRoleEmployeeDao.insert(param);
        if(retVal == 0) {
            throw new BizException(BaseBizExceptionCode.ROLE_EMPLOYEE_MANAGE_SAVE_ERROR,
                    "角色绑定失败");
        }
        return id;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public JsonResult addOrUpdate(Map<String,Object> params) {
        //查询是否有重复数据，不允许code或者name重复
        Map<String, Object> validCondition = new HashMap<String, Object>();
		/*validCondition.put("code", params.get("code"));
		validCondition.put("notId", params.get("id"));
		List<Map<String, Object>> validList = baseRoleInfoDao.listBy(validCondition);
		if(!BaseUtil.isEmpty(validList))
			throw new BaseBizException(BaseBizException.BASE_ROLE_INFO_VALIDSAME_ERROR,
					"数据重复，不允许有相同code的角色");*/
        validCondition = new HashMap<String, Object>();
        validCondition.put("name", params.get("name"));
        validCondition.put("notId", params.get("id"));
        List<Map<String, Object>> validList = baseRoleInfoDao.listBy(validCondition);
        if(!BaseUtil.isEmpty(validList))
            throw new BizException(BaseBizExceptionCode.ROLE_MANAGE_ERROR,
                    "数据重复，不允许有相同name的角色");

        //获取记录id值
        String id = BaseUtil.retStr(params.get("id"));
        //获取当前时间
        Timestamp timestamp = DateUtil.getSqlTimestamp();
        Map<String, Object> treeParam =
                getTreeParamResult(new CommonTreeParam(BaseUtil.retStr(params.get("parentId")), "parentId", params));
        params.putAll(treeParam);
        if(BaseUtil.isEmpty(params.get("parentId")))
            params.put("parentId", treeParam.get(CommonTreeParam.ROOT_ID));
        if(BaseUtil.isEmpty(id)){
            id = StringUtil.getUuid(true);
            params.put("id", id);
            params.put("createUserId", params.get("userId"));
            params.put("createTime", DateUtil.getSqlTimestamp());
            params.put("editUserId", params.get("userId"));
            params.put("editTime", params.get("createTime"));
            int retVal = baseRoleInfoDao.insert(params);
            if(retVal == 0) {
                throw new BizException(BaseBizExceptionCode.ROLE_MANAGE_SAVE_ERROR,
                        "保存base_role_info失败");
            }
        }else{//修改
            Map<String, Object> dnMap = baseRoleInfoDao.findById(id);
            dnMap.putAll(params);
            dnMap.put("editUserId", params.get("userId"));
            dnMap.put("editTime", timestamp);
            int retVal = baseRoleInfoDao.update(dnMap);
            // 更新操作是否成功
            if(0 == retVal)
                throw new BizException(BaseBizExceptionCode.ROLE_MANAGE_UPDATE_ERROR,
                        "修改base_role_info失败");
        }
        JsonResult jsonResult = new JsonResult();
        return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
    }

    @Transactional
    @Override
    public Map<String, Object> logicDelete(List<String> ids) {
        Map<String, Object> result = new HashMap<>();
        int count = 0;
        for (String id : ids) {
            if(!BaseUtil.isEmpty(id)) {
                Map<String, Object> tpn = getDao().findById(id);
                int retVal = baseRoleInfoDao.logicDelete(tpn);
                if(retVal == 0)
                    throw new BizException(BaseBizExceptionCode.ROLE_MANAGE_DELETE_ERROR,
                            "删除base_role_info失败");
                count += retVal;
            }
        }
        result.put("count", count);
        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public PageBean listPageSelf(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        params.put("notParentId", "0");
        PageBean page = baseRoleInfoDao.listPage(params, pageNum, pageSize);
        return page;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> listAll(Map<String, Object> params) {
        params.put("notParentId", "0");
        List<Map<String, Object>> list = baseRoleInfoDao.getList("listBy", params);
        return list;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> roleTree(Map<String, Object> params) {
        return getChildrenById(new CommonTreeParam(BaseUtil.retIntNull(params.get("levels"))));
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> listRoleByParam(Map<String, Object> params) {
        params.put("id", params.get("employeeId"));
        return baseRoleInfoDao.getList("listByEmployeeId", params);
    }
}
