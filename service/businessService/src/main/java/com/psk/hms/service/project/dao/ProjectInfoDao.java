package com.psk.hms.service.project.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.Map;

/**
 * ProjectInfoDao
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
public interface ProjectInfoDao extends BaseDao {
    /**
	 * 逻辑删除
	 * @param params
	 * @return
	 */
	public int logicDelete(Map<String, Object> params);
}