package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.constant.base.BaseOperatorBizConstant;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseOperatorBizDao;
import com.psk.hms.service.base.service.BaseOperatorBizService;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.base.service.BaseOperatorPackageInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Service("baseOperatorBizService")
public class BaseOperatorBizServiceImpl extends BaseServiceImpl implements BaseOperatorBizService {

    @Autowired
    private BaseOperatorBizDao baseOperatorBizDao;

    @Override
    protected BaseDao getDao() {
        return baseOperatorBizDao;
    }

    @Autowired
    private BaseOperatorInfoService baseOperatorInfoService;
    @Autowired
    private BaseOperatorPackageInfoService baseOperatorPackageInfoService;

    @Transactional
    @Override
    public Map<String, Object> save(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        Map<String, Object> operatorBizParams = new HashMap<>();
        operatorBizParams.put(BaseOperatorBizConstant.BIZ_ID, params.get(BaseOperatorBizConstant.BIZ_ID));
        operatorBizParams.put(BaseOperatorBizConstant.BIZ_TYPE, params.get(BaseOperatorBizConstant.BIZ_TYPE));
        operatorBizParams.put(BaseOperatorBizConstant.PERMISSION_ID, params.get(BaseOperatorBizConstant.PERMISSION_ID));
        operatorBizParams.put(BaseOperatorBizConstant.PERMISSION_TYPE, params.get(BaseOperatorBizConstant.PERMISSION_TYPE));
        operatorBizParams.put(BaseOperatorBizConstant.IS_DELETE, params.get(Constant.IS_DELETE_NO));
        Map<String, Object> operatorBizMap = getDao().findBy(operatorBizParams);

        if(operatorBizMap != null)
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_MANAGE_SAVE_ERROR, "已经关联上此功能");
        operatorBizMap = new HashMap<String, Object>();
        String id= StringUtil.getUuid(true);
        operatorBizMap.put(BaseOperatorBizConstant.ID, id);
        operatorBizMap.put(BaseOperatorBizConstant.CREATE_TIME, now);
        operatorBizMap.put(BaseOperatorBizConstant.CREATE_USER_ID, params.get(Constant.CURRENT_USER_ID));
        operatorBizMap.put(BaseOperatorBizConstant.EDIT_TIME, now);
        operatorBizMap.put(BaseOperatorBizConstant.EDIT_USER_ID, params.get(Constant.CURRENT_USER_ID));
        operatorBizMap.put(BaseOperatorBizConstant.IS_DELETE, Constant.IS_DELETE_NO);
        operatorBizMap.put(BaseOperatorBizConstant.BIZ_ID, params.get(BaseOperatorBizConstant.BIZ_ID));
        operatorBizMap.put(BaseOperatorBizConstant.BIZ_TYPE, params.get(BaseOperatorBizConstant.BIZ_TYPE));
        Map<String, Object> permissionMap ;
        if(BaseOperatorBizConstant.PERMISSION_TYPE_1.equals(params.get(BaseOperatorBizConstant.PERMISSION_TYPE))) {
            permissionMap = (Map<String, Object>) baseOperatorInfoService.findById(params.get(BaseOperatorBizConstant.PERMISSION_ID).toString());
            if(permissionMap == null)
                throw new BizException(BaseBizExceptionCode.OPERATEBIZ_MANAGE_SAVE_ERROR, "没有找到功能");
        }else {
            permissionMap = (Map<String, Object>) baseOperatorPackageInfoService.findById(params.get(BaseOperatorBizConstant.PERMISSION_ID).toString());
            if(permissionMap == null)
                throw new BizException(BaseBizExceptionCode.OPERATEBIZ_MANAGE_SAVE_ERROR, "没有找到权限包");
        }

        operatorBizMap.put(BaseOperatorBizConstant.PERMISSION_ID, params.get(BaseOperatorBizConstant.PERMISSION_ID));
        operatorBizMap.put(BaseOperatorBizConstant.PERMISSION_TYPE, params.get(BaseOperatorBizConstant.PERMISSION_TYPE));

        try {
            getDao().insert(operatorBizMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_MANAGE_SAVE_ERROR, "保存失败");
        }

        return operatorBizMap;
    }

    @Transactional
    @Override
    public void delete(Map<String, Object> params) {
        Map<String, Object> operatorBizMap = (Map<String, Object>) getDao().findById((String)params.get(BaseOperatorBizConstant.ID));

        if(operatorBizMap == null)
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_MANAGE_DELETE_ERROR, "没有找到要修改的权限信息");

        Timestamp now = DateUtil.getSqlTimestamp();

        operatorBizMap.put(BaseOperatorBizConstant.EDIT_TIME, now);
        operatorBizMap.put(BaseOperatorBizConstant.EDIT_USER_ID, params.get(Constant.CURRENT_USER_ID));
        operatorBizMap.put(BaseOperatorBizConstant.IS_DELETE, Constant.IS_DELETE_YES);

        try {
            getDao().update(operatorBizMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_MANAGE_DELETE_ERROR, "删除失败");
        }
    }

    @Override
    public PageBean permisionPackageList(Map<String, Object> params, int pageNum, int pageSize) {
        String sqlId = null;
        switch ((String)params.get("bizType")) {
            case BaseOperatorBizConstant.BIZ_TYPE_1:
                sqlId = "listForOperatorPackageTeamType";
                break;
            case BaseOperatorBizConstant.BIZ_TYPE_2:
                sqlId = "listForOperatorPackageTeamInfo";
                break;
            case BaseOperatorBizConstant.BIZ_TYPE_3:
                sqlId = "listForOperatorPackageRoleInfo";
                break;
            case BaseOperatorBizConstant.BIZ_TYPE_4:
                sqlId = "listForOperatorPackageTeamRoleCustom";
                break;
            case BaseOperatorBizConstant.BIZ_TYPE_5:
                sqlId = "listForOperatorPackageCommonRole";
                break;
            case BaseOperatorBizConstant.BIZ_TYPE_6:
                sqlId = "listForOperatorPackageTeamEmployee";
                break;
            default:
                return null;
        }
        PageBean operatorMapList = baseOperatorPackageInfoService.listPage(sqlId, params, pageNum, pageSize);
        return operatorMapList;
    }
}
