package com.psk.hms.service.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.dao.LogOperatorDao;
import org.springframework.stereotype.Repository;


/**********
 * 功能日志dao接口实现
 * @author xiangjz
 * @date 2018年5月16日
 * @version 1.0
 *
 */
@Repository("logOperatorDao")
public class LogOperatorDaoImpl extends BaseDaoImpl implements LogOperatorDao {

	
}
