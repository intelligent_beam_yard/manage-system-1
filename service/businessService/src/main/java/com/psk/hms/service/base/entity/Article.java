package com.psk.hms.service.base.entity;

import java.io.Serializable;

public class Article implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String title;
	private String url;
	private String parentId;
	private String parentIds;
	private String isRootNode;
	private Integer childrenNum;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public String getIsRootNode() {
		return isRootNode;
	}
	public void setIsRootNode(String isRootNode) {
		this.isRootNode = isRootNode;
	}
	public Integer getChildrenNum() {
		return childrenNum;
	}
	public void setChildrenNum(Integer childrenNum) {
		this.childrenNum = childrenNum;
	}
}
