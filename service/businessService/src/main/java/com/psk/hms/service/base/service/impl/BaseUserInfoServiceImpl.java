package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.constant.base.BaseUserInfoConstant;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.constant.common.poi.ImportExcelConstant;
import com.psk.hms.base.entity.LayGridReturn;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.entity.poi.ExcelTemplateParam;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.exception.company.CompanyBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.rsa.IDEAUtil;
import com.psk.hms.base.util.rsa.MD5Util;
import com.psk.hms.base.util.security.Pbkdf2Util;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.constant.LoginConstant;
import com.psk.hms.service.base.constant.UserConstant;
import com.psk.hms.service.base.dao.BaseUserInfoDao;
import com.psk.hms.service.base.service.BaseUserInfoService;
import com.psk.hms.service.common.importexcel.ImportExcelHandler;
import com.psk.hms.service.company.service.CompanyEmployeeService;
import com.psk.hms.service.company.service.CompanyInfoService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.*;

@Service("baseUserInfoService")
public class BaseUserInfoServiceImpl extends BaseServiceImpl<Map<String, Object>> implements BaseUserInfoService {

    @Autowired
    private BaseUserInfoDao baseUserInfoDao;
    @Autowired
    private CompanyEmployeeService companyEmployeeService;
    @Autowired
    private ImportExcelHandler importExcelHandler;
    @Autowired
    private CompanyInfoService companyInfoService;

    @Override
    protected BaseDao getDao() {
        return baseUserInfoDao;
    }

    private Map<String, Object> data = null;

    @Override
    @Transactional
    public JsonResult addOrUpdate(Map<String, Object> params) {
        //获取记录id值
        String id = BaseUtil.retStr(params.get("id"));
        //获取当前时间
        Timestamp timestamp = DateUtil.getSqlTimestamp();
        params.put("loginCompanyId", params.get("companyId"));
        params.put("companyId", params.get("employeeCompanyId"));
        if (BaseUtil.isEmpty(id)) {
            Map<String, Object> registeredUser = register(params);
            //添加单位员工数据（包含companyId）
            Map<String, Object> companyEmployeeMap = new HashMap<String, Object>();
            companyEmployeeMap.putAll(params);
            companyEmployeeMap.remove("id");
            companyEmployeeMap.put("userId", registeredUser.get("id"));
            companyEmployeeMap.put("gender", params.get("sex"));
            companyEmployeeMap.put("avatar", params.get("headUrl"));
            companyEmployeeService.addOrUpdate(companyEmployeeMap);
        } else {//修改
            Map<String, Object> dnMap = baseUserInfoDao.findById(id);
            dnMap.putAll(params);
            dnMap.put("editUserId", params.get("userId"));
            dnMap.put("editTime", timestamp);
            int retVal = baseUserInfoDao.update(dnMap);
            // 更新操作是否成功
            if (0 == retVal)
                throw new BizException(BaseBizExceptionCode.USER_MANAGE_UPDATE_ERROR,
                        "修改base_user_info失败");
            //修改单位员工数据（包含companyId）
            Map<String, Object> employeeCondition = new HashMap<String, Object>();
            employeeCondition.put("userId", id);
            List<Map<String, Object>> employeeList = companyEmployeeService.listBy(employeeCondition);
            if (BaseUtil.isEmpty(employeeList))
                throw new BizException(CompanyBizExceptionCode.COMPANY_EMPLOYEE_SELECT_ERROR,
                        "数据异常，该用户没有员工数据");
            else if (employeeList.size() > 1)
                throw new BizException(CompanyBizExceptionCode.COMPANY_EMPLOYEE_SELECT_ERROR,
                        "数据异常，该用户有多个员工数据");
            String userEmployeeId = BaseUtil.retStr(employeeList.get(0).get("id"));
            Map<String, Object> companyEmployeeMap = new HashMap<String, Object>();
            companyEmployeeMap.putAll(params);
            companyEmployeeMap.put("id", userEmployeeId);
            companyEmployeeMap.put("userId", id);
            companyEmployeeMap.put("gender", params.get("sex"));
            companyEmployeeMap.put("avatar", params.get("headUrl"));
            companyEmployeeService.addOrUpdate(companyEmployeeMap);
        }
        JsonResult jsonResult = new JsonResult();
        return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public Map<String, Object> logicDelete(Map<String, Object> params) {
        Map<String, Object> result = new HashMap<>();
        int count = 0;
        List<String> list = (List<String>) params.get(BaseUserInfoConstant.IDS);
        for (String id : list) {
            if (!StringUtil.isEmpty(id)) {
                Map<String, Object> deviceGuardHistory = baseUserInfoDao.findById(id);
                //删除用户信息
                int retVal = baseUserInfoDao.logicDelete(deviceGuardHistory);
                if (retVal == 0) {
                    throw BizException.DB_UPDATE_RESULT_0;
                }
                //删除员工信息
                Map<String, Object> employeeCondition = new HashMap<String, Object>();
                employeeCondition.put("userId", id);
                List<Map<String, Object>> employeeList = companyEmployeeService.listBy(employeeCondition);
                List<String> employeeIds = new ArrayList<String>();
                if (!BaseUtil.isEmpty(employeeList)) {
                    for (Map<String, Object> employee : employeeList) {
                        employeeIds.add(BaseUtil.retStr(employee.get("id")));
                    }
                }
                companyEmployeeService.logicDelete(employeeIds);
                count += retVal;
            }
        }
        result.put("count", count);
        return result;
    }

    @Override
    @Transactional
    public Map<String, Object> changePwd(Map<String, Object> params) {
        String oldPwd = BaseUtil.retStr(params.get("oldPwd"));
        Map<String, Object> userInfoMap = new HashMap<String, Object>();
        userInfoMap.put("id", params.get("id"));
        Map<String, Object> user = baseUserInfoDao.findBy(userInfoMap);
        if (!BaseUtil.isEmpty(oldPwd)) {
            if (BaseUtil.isEmpty(user))
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户不存在");
            String saltStr = BaseUtil.retStr(user.get(BaseUserInfoConstant.SALT)); // 密码盐
            byte[] salt = Base64.decodeBase64(saltStr);
            byte[] encryptedPassword = null;
            try {
                encryptedPassword = Pbkdf2Util.getEncryptedPassword(oldPwd, salt);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
            userInfoMap.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
            Map<String, Object> userInfoMap1 = baseUserInfoDao.findBy(userInfoMap);
            if (BaseUtil.isEmpty(userInfoMap1)) {
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "原密码错误");
            }
        }
        params.remove(BaseUserInfoConstant.PASSWORD);
        // 原密码验证成功后，新密码加密
        byte[] salt = null;
        byte[] encryptedPassword = null;
        try {
            salt = Pbkdf2Util.generateSalt();
            encryptedPassword = Pbkdf2Util.getEncryptedPassword(BaseUtil.retStr(params.get("newPwd")), salt);
        } catch (Exception e) {
            e.printStackTrace();
        }// 密码盐
        params.put(BaseUserInfoConstant.EDIT_TIME, DateUtil.getSqlTimestamp());
        params.put(BaseUserInfoConstant.SALT, Base64.encodeBase64String(salt));
        params.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
        user.putAll(params);
        int rs = baseUserInfoDao.update(user);
        if (rs != 1) {
            throw BizException.DB_UPDATE_RESULT_0;
        }
        return params;
    }

    @Override
    @Transactional
    public Map<String, Object> resetPwd(Map<String, Object> params) {
        params.put("newPwd", MD5Util.MD5Encode("111111"));
        return changePwd(params);
    }


    @SuppressWarnings("unused")
    @Override
    public Map<String, Object> login(Map<String, Object> params) {
        Map<String, Object> userInfoParams = new HashMap<String, Object>();
        userInfoParams.put(BaseUserInfoConstant.USER_NAME, params.get(BaseUserInfoConstant.USER_NAME));
        Map<String, Object> userInfoMap = baseUserInfoDao.findBy(userInfoParams);
        if (userInfoMap == null) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户不存在");
        } else {
            // 验证用户状态 是否冻结..
            if (UserConstant.STATE_FROZEN.equals(userInfoMap.get(BaseUserInfoConstant.STATE))) {
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户被冻结");
            }
            // 3.密码错误次数超限
            int errorCount = (Integer) userInfoMap.get(BaseUserInfoConstant.ERROR_COUNT);
            int passErrorCount = LoginConstant.WEB_PWD_INPUT_ERROR_LIMIT;
            if (errorCount >= passErrorCount) {
                Date stopDate = (Date) userInfoMap.get("stopDate");
                int hourSpace = DateUtil.getDateHourSpace(stopDate, DateUtil.getDate());
                int passErrorHour = LoginConstant.PASSERRORHOUR;
                if (hourSpace < passErrorHour) {
                    throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR,
                            "超过最大密码错误次数，" + LoginConstant.PASSERRORHOUR + "小时不能登录");
                }
            }
            // 验证用户密码
            if (true) {
                String saltStr = (String) userInfoMap.get(BaseUserInfoConstant.SALT); // 密码盐
                byte[] salt = Base64.decodeBase64(saltStr);
                byte[] encryptedPassword = null;
                try {
                    encryptedPassword = Pbkdf2Util.getEncryptedPassword(params.get(BaseUserInfoConstant.PASSWORD).toString(), salt);
                } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                    e.printStackTrace();
                }
                params.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
                Map<String, Object> userInfoMap1 = baseUserInfoDao.findBy(params);
                if (userInfoMap1 == null) {
                    throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "密码错误");
                } else
                    return userInfoMap1;
                // 登录成功
            } else {
                // 验证用户密码错误次数
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "超过最大次数");
            }
        }
    }

    @Override
    public Map<String, Object> checkUser(Map<String, Object> params) {
        Map<String, Object> userInfoParams = new HashMap<String, Object>();
        userInfoParams.put(BaseUserInfoConstant.USER_NAME, (String) params.get(BaseUserInfoConstant.USER_NAME));
        Map<String, Object> user = baseUserInfoDao.findBy(userInfoParams);
        if (!StringUtil.isBlank1(user)) {
            // 4.验证密码
            String saltStr = (String) user.get(BaseUserInfoConstant.SALT); // 密码盐
            byte[] salt = Base64.decodeBase64(saltStr);
            byte[] encryptedPassword = null;
            try {
                encryptedPassword = Pbkdf2Util.getEncryptedPassword((String) params.get(BaseUserInfoConstant.PASSWORD), salt);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
            userInfoParams.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
            Map<String, Object> user1 = baseUserInfoDao.findBy(userInfoParams);
            if (StringUtil.isBlank1(user1)) {
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "密码错误");
            } else {
                return user1;
            }
        } else {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户名不存在");
        }
    }

    @Override
    @Transactional
    public Map<String, Object> register(Map<String, Object> params) {
        Map<String, Object> userParams = new HashMap<>();
        String userName = BaseUtil.retStr(params.get(BaseUserInfoConstant.USER_NAME));
        userParams.put(BaseUserInfoConstant.USER_NAME, userName);
        Map<String, Object> user = baseUserInfoDao.findBy(userParams);
        if (user != null) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户名：'" + userName + "' 数据重复");
        } else {
            params.put(BaseUserInfoConstant.ID, StringUtil.getUuid(true));
            params.put(BaseUserInfoConstant.CREATE_USER_ID, params.get("userId"));
            params.put(BaseUserInfoConstant.CREATE_TIME, DateUtil.getSqlTimestamp());
            params.put(BaseUserInfoConstant.EDIT_USER_ID, params.get("userId"));
            params.put(BaseUserInfoConstant.EDIT_TIME, params.get(BaseUserInfoConstant.CREATE_TIME));
            try {
                // 密码加密
                byte[] salt = Pbkdf2Util.generateSalt();// 密码盐
                byte[] encryptedPassword = Pbkdf2Util.getEncryptedPassword(BaseUtil.retStr(params.get(BaseUserInfoConstant.PASSWORD)), salt);
                params.put(BaseUserInfoConstant.SALT, Base64.encodeBase64String(salt));
                params.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
                params.put(BaseUserInfoConstant.SECRET_KEY, Base64.encodeBase64String(IDEAUtil.initKey()));
            } catch (Exception e) {
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "加密失败");
            }

            if (BaseUtil.isEmpty(params.get(BaseUserInfoConstant.AGE)))
                params.put(BaseUserInfoConstant.AGE, 0);

            int retVal = baseUserInfoDao.insert(params);
            if (retVal == 0)
                throw new BizException(BaseBizExceptionCode.USER_MANAGE_SAVE_ERROR,
                        "保存base_user_info失败");
            return params;
        }
    }

    /*---------------------------admin端-------------------------*/
    @SuppressWarnings("unchecked")
    public Map<String, Object> adminLogin(Map<String, Object> params) {
        // 1.获取用户
        Map<String, Object> userInfoParams = new HashMap<String, Object>();
        userInfoParams.put(BaseUserInfoConstant.USER_NAME, params.get(BaseUserInfoConstant.USER_NAME));
        Map<String, Object> userInfoMap = (HashMap<String, Object>) baseUserInfoDao.findBy(userInfoParams);
        if (null == userInfoMap) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户名不存在");
        }

        // 2.冻结账户
        String state = (String) userInfoMap.get(BaseUserInfoConstant.STATE);
        if (state.equals(UserConstant.STATE_FROZEN)) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "用户被冻结");
        }

        // 3.密码错误次数超限
        int errorCount = (Integer) userInfoMap.get(BaseUserInfoConstant.ERROR_COUNT);
        int passErrorCount = LoginConstant.WEB_PWD_INPUT_ERROR_LIMIT;
        if (errorCount >= passErrorCount) {
            Date stopDate = (Date) userInfoMap.get("stopDate");
            int hourSpace = DateUtil.getDateHourSpace(stopDate, DateUtil.getDate());
            int passErrorHour = LoginConstant.PASSERRORHOUR;
            if (hourSpace < passErrorHour) {
                throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR,
                        "超过最大密码错误次数，" + LoginConstant.PASSERRORHOUR + "小时不能登录");
            }
        }


        // 4.验证密码
        String saltStr = (String) userInfoMap.get(BaseUserInfoConstant.SALT); // 密码盐
        byte[] salt = Base64.decodeBase64(saltStr);
        String passStr = (String) userInfoMap.get(BaseUserInfoConstant.PASSWORD); // 密码
        byte[] encryptedPassword = Base64.decodeBase64(passStr);
        boolean bool = false;
        try {
            bool = Pbkdf2Util.authenticate((String) params.get("passwordForMD5"), encryptedPassword, salt);
            //bool = true;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        if (bool) {
            // 密码验证成功

            return userInfoMap;
        } else {
            // 密码验证失败
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "密码错误");
        }

    }

    /**
     * 修改密码
     *
     * @param params
     */
    @Transactional
    public void adminUpdatePassword(Map<String, Object> params) {
        Map<String, Object> userInfoMap = getDao().findById((String) params.get("currentUserId"));
        if (userInfoMap == null)
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_VALIDATE_ERROR, "用户没找到");
        userInfoMap.put(BaseUserInfoConstant.EDIT_USER_ID, params.get("currentUserId"));
        userInfoMap.put(BaseUserInfoConstant.EDIT_TIME, DateUtil.getSqlTimestamp());
        try {
            // 密码加密
            byte[] salt = Pbkdf2Util.generateSalt();// 密码盐
            byte[] encryptedPassword = Pbkdf2Util.getEncryptedPassword(params.get("newPasswordForMD5").toString(), salt);
            userInfoMap.put(BaseUserInfoConstant.SALT, Base64.encodeBase64String(salt));
            userInfoMap.put(BaseUserInfoConstant.PASSWORD, Base64.encodeBase64String(encryptedPassword));
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "加密失败");
        }

        try {
            getDao().update(userInfoMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "修改密码失败");
        }
    }


    /****  运营端用户管理  *******/

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void update(HashMap<String, Object> params) {

        Map<String, Object> userMap = (Map<String, Object>) baseUserInfoDao.findById((String) params.get("id"));

        if (userMap == null)
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "没有找到要修改的用户信息");

        Timestamp now = DateUtil.getSqlTimestamp();
        userMap.put("editTime", now);
        userMap.put("id", params.get("id"));
        userMap.put("editUserId", params.get("currentUserId"));
        userMap.put("userName", params.get("userName"));
        userMap.put("nickName", params.get("nickName"));
        userMap.put("remarks", params.get("remarks"));
        userMap.put("email", params.get("email"));
        userMap.put("phone", params.get("phone"));
        userMap.put("sex", params.get("sex"));
        userMap.put("idCard", params.get("idCard"));
        userMap.put("age", (StringUtil.isEmpty(params.get("age").toString()) ? 0 : params.get("age")));
        userMap.put("education", params.get("education"));
        try {
            baseUserInfoDao.update(userMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR, "用户信息修改失败");
        }
    }

    @Transactional
    @Override
    public JsonResult importExcel(Map<String, Object> params)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException, InstantiationException {
        MultipartFile importFile = (MultipartFile) params.get("importFile");
        String templateExcelType = BaseUtil.retStr(params.get("templateExcelType"));
        Map<String, Object> result =
                importExcelHandler.importExcelTemplate(new ExcelTemplateParam(importFile, templateExcelType));
        List<Map<String, Object>> dataList = (List<Map<String, Object>>) result.get("dataList");
        for (Map<String, Object> data : dataList) {
            //设置默认密码
            params.put(BaseUserInfoConstant.PASSWORD, MD5Util.MD5Encode(BaseUtil.retStr(data.get("userName")) + "123456"));
            data.putAll(params);
            data.put("employeeCompanyId", data.get("companyName" + ImportExcelConstant.COMPANY_ID_KEY));
            data.put("sex", data.get("sex" + ImportExcelConstant.ENUM_VALUE_KEY));
            addOrUpdate(data);
        }
        JsonResult jsonResult = new JsonResult();
        return jsonResult.success();
    }

    /**
     * 查询单个用户信息
     */
    public Map<String, Object> findByParam(Map<String, Object> param) {
        Map<String, Object> resultMap = baseUserInfoDao.findBy(param);
        if (BaseUtil.isEmpty(resultMap))
            throw new BizException(BaseBizExceptionCode.USER_LOGIN_ERROR,
                    "数据异常，用户不存在");
        String userId = BaseUtil.retStr(resultMap.get("id"));

        //查询用户员工
        Map<String, Object> employeeCondition = new HashMap<String, Object>();
        employeeCondition.put("userId", userId);
        List<Map<String, Object>> employeeList = companyEmployeeService.listBy(employeeCondition);
        if (BaseUtil.isEmpty(employeeList))
            throw new BizException(CompanyBizExceptionCode.COMPANY_EMPLOYEE_SELECT_ERROR,
                    "数据异常，该用户没有员工数据");
        else if (employeeList.size() > 1)
            throw new BizException(CompanyBizExceptionCode.COMPANY_EMPLOYEE_SELECT_ERROR,
                    "数据异常，该用户有多个员工数据");
        String userEmployeeId = BaseUtil.retStr(employeeList.get(0).get("id"));
        String employeeCompanyId = BaseUtil.retStr(employeeList.get(0).get("companyId"));
        resultMap.put("employeeId", userEmployeeId);

        //查询员工所属单位
        Map<String, Object> company = companyInfoService.findById(employeeCompanyId);
        if (BaseUtil.isEmpty(company))
            throw new BizException(CompanyBizExceptionCode.COMPANY_INFO_SELECT_ERROR,
                    "数据异常，单位不存在");
        resultMap.put("companyId", company.get("id"));
        resultMap.put("companyName", company.get("companyName"));

        return resultMap;
    }

    @Override
    public LayGridReturn listPageGridForAdmin(Map<String, Object> params) {
        params.put("orderByClause", "order by baseUserInfo.create_time desc");
        PageBean pageBean = getDao().listPage("listByForAdmin", params, BaseUtil.retInt(params.get(Constant.PAGE_NUM)), BaseUtil.retInt(params.get(Constant.PAGE_SIZE)));
        return new LayGridReturn(0, "", pageBean.getList(), pageBean.getTotal());
    }
}
