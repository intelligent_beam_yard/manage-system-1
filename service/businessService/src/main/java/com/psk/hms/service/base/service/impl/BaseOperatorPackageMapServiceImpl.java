package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.constant.base.BaseOperatorPackageMapConstant;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseOperatorPackageMapDao;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.base.service.BaseOperatorPackageInfoService;
import com.psk.hms.service.base.service.BaseOperatorPackageMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Service("baseOperatorPackageMapService")
public class BaseOperatorPackageMapServiceImpl extends BaseServiceImpl implements BaseOperatorPackageMapService {

    @Autowired
    private BaseOperatorPackageMapDao baseOperatorPackageMapDao;
    @Autowired
    private BaseOperatorPackageInfoService baseOperatorPackageInfoService;
    @Autowired
    private BaseOperatorInfoService baseOperatorInfoService;


    @Override
    protected BaseDao getDao() {
        return baseOperatorPackageMapDao;
    }

    @Override
    @Transactional
    public Map<String,Object> save(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();
        HashMap<String, Object> operatorPackageMap = new HashMap<String, Object>();
        Map packageMap=(Map) baseOperatorPackageInfoService.findById((String) params.get(BaseOperatorPackageMapConstant.PACKAGEID));
        if(packageMap==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "权限包不存在");
        }
        Map operatorMap=(Map) baseOperatorInfoService.findById((String) params.get(BaseOperatorPackageMapConstant.OPERATORID));
        if(operatorMap==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "功能不存在");
        }

        String id=StringUtil.getUuid(true);
        operatorPackageMap.put(BaseOperatorPackageMapConstant.ID, id);
        operatorPackageMap.put(BaseOperatorPackageMapConstant.CREATE_TIME, now);
        operatorPackageMap.put(BaseOperatorPackageMapConstant.CREATE_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.EDIT_TIME, now);
        operatorPackageMap.put(BaseOperatorPackageMapConstant.EDIT_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.IS_DELETE, Constant.IS_DELETE_NO);
        operatorPackageMap.put(BaseOperatorPackageMapConstant.REMARKS, params.get(BaseOperatorPackageMapConstant.REMARKS));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.OPERATORID, params.get(BaseOperatorPackageMapConstant.OPERATORID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.PACKAGEID, params.get(BaseOperatorPackageMapConstant.PACKAGEID));
        try {
            getDao().insert(operatorPackageMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "权限包关联失败");
        }
        return operatorPackageMap;
    }

    @Override
    @Transactional
    public void update(Map<String, Object> params) {
        @SuppressWarnings("unchecked")
        Map<String, Object> operatorPackageMap = (Map<String, Object>) getDao().findById((String) params.get(BaseOperatorPackageMapConstant.ID));

        if (operatorPackageMap == null)
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_UPDATE_ERROR, "没有找到要修改的权限包关联信息");

        Map packageMap=(Map) baseOperatorPackageInfoService.findById((String) params.get(BaseOperatorPackageMapConstant.PACKAGEID));
        if(packageMap==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "权限包不存在");
        }
        Map operatorMap=(Map) baseOperatorInfoService.findById((String) params.get(BaseOperatorPackageMapConstant.OPERATORID));
        if(operatorMap==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "功能不存在");
        }

        Timestamp now = DateUtil.getSqlTimestamp();
        operatorPackageMap.put(BaseOperatorPackageMapConstant.EDIT_TIME, now);
        operatorPackageMap.put(BaseOperatorPackageMapConstant.ID, params.get(BaseOperatorPackageMapConstant.ID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.EDIT_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.OPERATORID, params.get(BaseOperatorPackageMapConstant.OPERATORID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.PACKAGEID, params.get(BaseOperatorPackageMapConstant.PACKAGEID));
        operatorPackageMap.put(BaseOperatorPackageMapConstant.REMARKS, params.get(BaseOperatorPackageMapConstant.REMARKS));
        try {
            getDao().update(operatorPackageMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_UPDATE_ERROR, "权限包基本信息关联失败");
        }
    }

    @Override
    @Transactional
    public void delete(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        //删除功能
        @SuppressWarnings("unchecked")
        Map<String, Object> operatorInfoMap = (Map<String, Object>) getDao().findById((String) params.get("id"));

        operatorInfoMap.put(BaseOperatorPackageMapConstant.EDIT_TIME, now);
        operatorInfoMap.put(BaseOperatorPackageMapConstant.EDIT_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
        operatorInfoMap.put(BaseOperatorPackageMapConstant.IS_DELETE, BaseOperatorPackageMapConstant.IS_DELETE_YES);
        try {
            getDao().update(operatorInfoMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_DELETE_ERROR, "权限包关联删除失败");
        }
    }

}
