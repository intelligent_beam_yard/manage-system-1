package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.enums.CommonEnumType;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseImportExcelTemplateDao;
import com.psk.hms.service.base.service.BaseImportExcelTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @link BaseImportExcelTemplateServiceImpl 实现类
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-26
 * @version 1.0.0
 */
@Service
public class BaseImportExcelTemplateServiceImpl extends BaseServiceImpl<Map<String, Object>> implements BaseImportExcelTemplateService {

    @Autowired
    private BaseImportExcelTemplateDao baseImportExcelTemplateDao;
    
    @Override
	protected BaseDao getDao() {
		return baseImportExcelTemplateDao;
	}
    
    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public JsonResult batchSave(Map<String,Object> params) {
    	JsonResult jsonResult = new JsonResult();
    	Timestamp timestamp = DateUtil.getSqlTimestamp();
    	//获取模板节点id，如果有，视为修改，没有则视为新增
    	String templateId = BaseUtil.retStr(params.get("templateId"));
    	if(!BaseUtil.isEmpty(templateId)) {
    		//如果不为空，则先删除老数据，剩下的视为新增
    		Map<String, Object> deleteParam = new HashMap<String, Object>();
    		deleteParam.put("parentId", templateId);
    		baseImportExcelTemplateDao.delete("deleteByParam", deleteParam);
    		deleteParam = new HashMap<String, Object>();
    		deleteParam.put("id", templateId);
    		baseImportExcelTemplateDao.logicDelete(deleteParam);
    	}
    	//新增模板表
    	templateId = StringUtil.getUuid(true);
    	List<Map<String, Object>> dataList = (List<Map<String, Object>>) params.get("dataList");
    	if(!BaseUtil.isEmpty(dataList))
	    	for (Map<String, Object> data : dataList) {
				String isRoot = BaseUtil.retStr(data.get("isRoot"));
				if(CommonEnumType.YesOrNo.是.getValue().equals(isRoot)) {
					data.put("id", templateId);
					data.put("parentId", "0");
				} else {
					String dataId = StringUtil.getUuid(true);
					data.put("id", dataId);
					data.put("parentId", templateId);
				}
				data.put("createUserId", params.get("userId"));
				data.put("createTime", timestamp);
				data.put("editUserId", params.get("userId"));
				data.put("editTime", params.get("createTime"));
				int retVal = baseImportExcelTemplateDao.insert(data);
				if(retVal == 0) {
					throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_SAVE_ERROR,
							"保存base_import_excel_template失败");
				}
			}
    	return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG);
    }

    @SuppressWarnings("unchecked")
	@Transactional
	@Override
	public JsonResult addOrUpdate(Map<String,Object> params) {
		//获取记录id值
		String id = BaseUtil.retStr(params.get("id"));
		//获取当前时间
		Timestamp timestamp = DateUtil.getSqlTimestamp();		
		if(BaseUtil.isEmpty(id)){
			id = StringUtil.getUuid(true);
			params.put("id", id);
			params.put("createUserId", params.get("userId"));
			params.put("createTime", timestamp);
			params.put("editUserId", params.get("userId"));
			params.put("editTime", params.get("createTime"));
			int retVal = baseImportExcelTemplateDao.insert(params);
			if(retVal == 0) {
				throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_SAVE_ERROR,
						"保存base_import_excel_template失败");
			}
		}else{//修改
			Map<String, Object> dnMap = baseImportExcelTemplateDao.findById(id);
			dnMap.putAll(params);
			dnMap.put("editUserId", params.get("userId"));
			dnMap.put("editTime", timestamp);
			int retVal = baseImportExcelTemplateDao.update(dnMap);
			// 更新操作是否成功
			if(0 == retVal)
				throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_UPDATE_ERROR,
						"修改base_import_excel_template失败");
		}
		JsonResult jsonResult = new JsonResult();
		return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
	}
	
	@Transactional
	@Override
	public Map<String, Object> logicDelete(List<String> ids) {
		Map<String, Object> result = new HashMap<>(); 
		int count = 0;
		for (String id : ids) {
			if(!BaseUtil.isEmpty(id)) {
				Map<String, Object> tpn = this.findById(id);
				int retVal = baseImportExcelTemplateDao.logicDelete(tpn);
				if(retVal == 0)
					throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_DELETE_ERROR,
							"删除base_import_excel_template失败");
				count += retVal;
			}
		}
		result.put("count", count);
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public PageBean listPageSelf(Map<String, Object> params, Integer pageNum, Integer pageSize) {
		PageBean page = baseImportExcelTemplateDao.listPage(params, pageNum, pageSize);
		return page;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Map<String, Object>> listAll(Map<String, Object> params) {
		List<Map<String, Object>> list = baseImportExcelTemplateDao.getList("listBy", params);
		return list;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, Object> findTemplate(Map<String, Object> params) {
		String templateId = BaseUtil.retStr(params.get("templateId"));
		Map<String, Object> rootMap = baseImportExcelTemplateDao.findById(templateId);
		//查询子项
		Map<String, Object> condition = new HashMap<String, Object>();
		condition.put("parentId", templateId);
		condition.put("orderByClause", " order by baseImportExcelTemplate.sort ");
		List<Map<String, Object>> itemList = baseImportExcelTemplateDao.listBy(condition);
		rootMap.put("items", itemList);
//		List<Map<String, Object>> list = baseImportExcelTemplateDao.getList("listBy", params);
		return rootMap;
	}
    
}