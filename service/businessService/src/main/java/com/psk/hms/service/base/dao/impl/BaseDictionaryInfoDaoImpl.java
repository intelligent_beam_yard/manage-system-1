package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseDictionaryInfoDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * BaseDictionaryInfoDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@Repository("baseDictionaryInfoDao")
public class BaseDictionaryInfoDaoImpl extends BaseDaoImpl implements BaseDictionaryInfoDao {
	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}