package com.psk.hms.service.base.service;

import com.psk.hms.core.service.BaseService;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface BaseOperatorInfoService extends BaseService {

	/**
	 * 保存
	 * @param 
	 * @return
	 */
	Map<String, Object> save(Map<String, Object> params);
	
	
	Map<String, Object> findForView(String id);
	
	/**
	 * 修改
	 * @param 
	 * @return
	 */
	void update(Map<String, Object> params);
	
	/**
	 * 删除
	 * @param params ==> ids id字符串 逗号分隔
	 * 		  params ==> currentUserId 当前用户id
	 */
	void delete(Map<String, Object> params);
	
	/**
	 * 获取具备权限的urls
	 * 
	 * @param params : {
	 * 		teamTypeId团队类型id
	 * 		teamId团队id
	 * 		baseRoleId通用角色id
	 * 		teamEmployeeId团队人员id
	 * }
	 * @return
	 */
	List<Map<String, Object>> listByOperation(Map<String, Object> params);	
	
	/**
	 * 授权保存
	 * @param 
	 * @return
	 */
	void authSave(Map<String, Object> params);


	/**
	 * 权限包勾选
	 * @param params
	 * @return
	 */
	Map<String,Object> operatorPackage(Map<String, Object> params);


	/**
	 * 权限包取消勾选
	 * @param params
	 */
	void uncheckOperatorPackage(Map<String, Object> params);


	void treeAuthSave(Map<String, Object> params);


	void treeCancelSave(Map<String, Object> params);

	/**
	 * 验证是否有权限
	 * @param params
	 * @return
	 */
	boolean hasPrivilege(String operatorId, String employeeId);

}
