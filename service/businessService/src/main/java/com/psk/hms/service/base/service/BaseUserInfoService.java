package com.psk.hms.service.base.service;

import com.psk.hms.base.entity.LayGridReturn;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.core.service.BaseService;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xiangjz
 * @version 1.0.0
 * @date 2018年4月26日
 */
@SuppressWarnings("rawtypes")
public interface BaseUserInfoService extends BaseService<Map<String, Object>> {

    /**
     * 导入用户信息
     *
     * @param params
     * @return
     */
    JsonResult importExcel(Map<String, Object> params)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException, InstantiationException;

    /**
     * 新增或修改
     *
     * @param params
     * @return
     */
    JsonResult addOrUpdate(Map<String, Object> params);

    /**
     * 逻辑删除
     *
     * @param params
     * @return
     */
    Map<String, Object> logicDelete(Map<String, Object> params);

    /**
     * @param params 参数	用户名   原密码  新密码
     * @return
     */
    Map<String, Object> changePwd(Map<String, Object> params);

    /**
     * @param params 重置密码
     * @return
     */
    Map<String, Object> resetPwd(Map<String, Object> params);

    /**
     * 用户登录
     *
     * @param params
     * @return
     */
    Map<String, Object> login(Map<String, Object> params);

    /**
     * 校验用户是否存在
     *
     * @param params
     * @return
     */
    Map<String, Object> checkUser(Map<String, Object> params);

    /**
     * 注册
     *
     * @param params
     * @return
     */
    Map<String, Object> register(Map<String, Object> params);

    /**
     * 修改密码
     *
     * @param params
     */
    void adminUpdatePassword(Map<String, Object> params);

    /**
     * 更新用户
     *
     * @param params
     */
    void update(HashMap<String, Object> params);

    /**
     * 查询单条用户
     *
     * @param params
     * @return
     */
    Map<String, Object> findByParam(Map<String, Object> params);

    /**
     * admin端datagrid组件分页数据
     *
     * @param params
     * @return
     */
    LayGridReturn listPageGridForAdmin(Map<String, Object> params);
}
