package com.psk.hms.service.common.importexcel;

import com.psk.hms.base.constant.common.poi.ImportExcelConstant;
import com.psk.hms.base.entity.poi.ExcelTemplateParam;
import com.psk.hms.base.entity.poi.ValidateReturn;
import com.psk.hms.base.enums.CommonEnumType;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.POIUtil;
import com.psk.hms.base.util.ValidateUtil;
import com.psk.hms.service.base.service.BaseDictionaryInfoService;
import com.psk.hms.service.base.service.BaseImportExcelTemplateService;
import com.psk.hms.service.common.enums.EnumsLoader;
import com.psk.hms.service.company.service.CompanyInfoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @功能说明：通用导入excel业务数据接口
 * @author xiangjz
 * @创建日期： 2018-08-25
 */
@SuppressWarnings("unchecked")
@Component
public class ImportExcelHandler {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private BaseImportExcelTemplateService baseImportExcelTemplateService;
	@Autowired
	private BaseDictionaryInfoService baseDictionaryInfoService;
	@Autowired
	private CompanyInfoService companyInfoService;
	@Autowired
	private EnumsLoader enumsLoader;
	
	//----------------------通用导入  start----------------------
	
	/**
	 * 通用，根据excel模板导入业务数据
	 * @param etp
	 * @return
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws InstantiationException 
	 */
	public Map<String, Object> importExcelTemplate(ExcelTemplateParam etp) 
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, 
			NoSuchMethodException, SecurityException, IOException, InstantiationException {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		
		MultipartFile importExcel = etp.getImportFile();
		String templateType = etp.getTemplateExcelType();
		//查询模板配置表中的数据
		Map<String, Object> templateCondition = new HashMap<String, Object>();
		templateCondition.put("templateExcelType", templateType); //模板类型  枚举字典
		templateCondition.put("parentId", "0"); 
		//查父模板节点
		List<Map<String, Object>> parentTemplateDataList = 
				baseImportExcelTemplateService.listBy(templateCondition);
		if(BaseUtil.isEmpty(parentTemplateDataList))
			throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "导入模板父节点缺少数据配置");
		if(parentTemplateDataList.size() > 1)
 			throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "导入模板父节点配置重复");
		String parentId = BaseUtil.retStr(parentTemplateDataList.get(0).get("id"));
		//查模板配置数据
		templateCondition = new HashMap<String, Object>();
		templateCondition.put("parentId", parentId); 
		templateCondition.put("orderByClause", " order by baseImportExcelTemplate.sort ");
		List<Map<String, Object>> templateDataList = 
				baseImportExcelTemplateService.listBy(templateCondition);
		if(BaseUtil.isEmpty(templateDataList))
			throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "导入模板子节点缺少数据配置");
		
		int i = etp.getStartRow() - 1; //实际验证起始行
		int remindRow = etp.getStartRow(); //提示行
		int remindIndex = 0; //提示列
		List<Map<String, Object>> importList = POIUtil.readExcelContent(importExcel.getInputStream());
		for (Map<String, Object> line : importList) { // 循环数据列表
			if(i == etp.getStartRow() - 1) {
				//第一行为字段描述，验证模板有无被修改，错位，增减行等
				for (int index = 0; index < templateDataList.size(); index ++) { //循环模板配置，以之为准
					remindIndex = index + 1;
					Map<String, Object> templateField = templateDataList.get(index);
					String name = BaseUtil.retStr(templateField.get("name")); //字段名，展示用
					Object value = line.get("field"+index);
					//1、验证数据是否有错位
					if(!name.equals(BaseUtil.retStr(value)))
						throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列："+value+"字段有误，应当是'"+name+"'");
				}
			} else {
				Map<String, Object> item = new HashMap<String, Object>();
				for (int index = 0; index < templateDataList.size(); index ++) { //循环模板配置，以之为准
					remindIndex = index + 1;
					Map<String, Object> templateField = templateDataList.get(index);
					String fieldName = BaseUtil.retStr(templateField.get("fieldName")); //字段名称key
					String name = BaseUtil.retStr(templateField.get("name")); //字段名，展示用
					String dataSource = BaseUtil.retStr(templateField.get("dataSource")); //数据来源 枚举字典
					String validMethodType = BaseUtil.retStr(templateField.get("validMethodType")); //数据验证方法类型
					/*Integer sort = BaseUtil.retInt(templateField.get("sort")); //排序字段
					 */				
					Object value = line.get("field"+index);
					
					//2、验证是否是必填
					String isNecessary = BaseUtil.retStr(templateField.get("isNecessary")); //是否必填
					if(CommonEnumType.YesOrNo.是.getValue().equals(isNecessary)) {
						//验证是否必填
						if(BaseUtil.isEmpty(line.get("field"+index)))
							throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列：不能为空");
					}
					
					if(!BaseUtil.isEmpty(value)) {
						//3、验证数据格式
						String validMethod = BaseUtil.retStr(templateField.get("validMethod")); //是否验证参数值
						if(!BaseUtil.isEmpty(validMethod)) {
							@SuppressWarnings("rawtypes")
							Class ValidateUtilClass = ValidateUtil.class;
							Object o = ValidateUtilClass.newInstance();
							Method method = ValidateUtilClass.getMethod(validMethod, String.class);
							Object checkResult = method.invoke(o, BaseUtil.retStr(value));
							ValidateReturn validateReturn = (ValidateReturn) checkResult;
							if (!validateReturn.isPassed()) {
								throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列："+validateReturn.getMsg());
							}
						}
						
						//4、验证数据字典数据，反向查询code
						if(BaseUtil.isEmpty(dataSource)) {
							//业务来源数据，不处理
						} else if (CommonEnumType.ImportTemplateDataSourceType.枚举字典来源.getValue().equals(dataSource)) {
							//值为枚举字典的name，需要转换成value
							if(BaseUtil.isEmpty(validMethodType))
								throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列："+"参数配置异常，枚举字典未指定类名");
							//当数据来源是枚举时，这个字段值代表枚举类的名称
							Map<Object, Object> enumMap = 
									enumsLoader.getCommonEnumMap(validMethodType);
							if(BaseUtil.isEmpty(enumMap))
								throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列："+"枚举异常，未找到指定枚举类");
							
							boolean enumValueValid = false; //枚举值验证通过标志
							//比对名称
							for (Entry<Object, Object> entry : enumMap.entrySet()) {
								String enumValue = BaseUtil.retStr(entry.getValue());
								if(enumValue.equals(BaseUtil.retStr(value))) {
									enumValueValid = true;
									item.put(fieldName+ImportExcelConstant.ENUM_VALUE_KEY, BaseUtil.retStr(entry.getKey()));
									break;
								}
							}
							if(!enumValueValid)
								throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列："+"枚举异常，枚举中未找到对应值");
							
						} else if (CommonEnumType.ImportTemplateDataSourceType.数据字典来源.getValue().equals(dataSource)) {
							//value为数据字典的name，需要转换成code
							Map<String, Object> dictionaryCondition = new HashMap<String, Object>();
							dictionaryCondition.put("name", value);
							List<Map<String, Object>> dictionaryList =
									baseDictionaryInfoService.listBy(dictionaryCondition);
							if(!BaseUtil.isEmpty(dictionaryList)) {
								if(dictionaryList.size() > 1)
									throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列：找到name为'"+value+"'重复的数据字典");
								item.put(fieldName+ImportExcelConstant.DICTIONARY_CODE_KEY, BaseUtil.retStr(dictionaryList.get(0).get("code")));
							} else {
								throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列：没有找到name为'"+value+"'的数据字典数据");
							}
						} else if (CommonEnumType.ImportTemplateDataSourceType.单位名称来源.getValue().equals(dataSource)) {
							//value为数据字典的name，需要转换成code
							Map<String, Object> companyCondition = new HashMap<String, Object>();
							companyCondition.put("companyName", value);
							List<Map<String, Object>> companyList =
									companyInfoService.listBy(companyCondition);
							if(!BaseUtil.isEmpty(companyList)) {
								if(companyList.size() > 1)
									throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列：找到单位名称为'"+value+"'重复的单位");
								item.put(fieldName+ImportExcelConstant.COMPANY_ID_KEY, BaseUtil.retStr(companyList.get(0).get("id")));
							} else {
								throw new BizException(BaseBizExceptionCode.IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR, "第"+remindRow+"行, 第"+remindIndex+"列：没有找到单位名称为'"+value+"'的单位数据");
							}
						}
					}
					
					item.put(fieldName, value);
				}
				resultList.add(item);
			}
			remindRow ++;
			i++;
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dataList", resultList);
		return resultMap;
	}
	
	//----------------------通用导入  end----------------------
}
