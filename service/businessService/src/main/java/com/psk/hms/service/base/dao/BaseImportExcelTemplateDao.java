package com.psk.hms.service.base.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.Map;

/**
 * BaseImportExcelTemplateDao
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-26
 * @version 1.0.0
 */
public interface BaseImportExcelTemplateDao extends BaseDao {
    /**
	 * 逻辑删除
	 * @param params
	 * @return
	 */
	public int logicDelete(Map<String, Object> params);
}