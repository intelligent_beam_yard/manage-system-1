package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseUserInfoDao;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository("baseUserInfoDao")
public class BaseUserInfoDaoImpl extends BaseDaoImpl implements BaseUserInfoDao {

    @Override
    public <E> List<E> getList(String sqlId, Object args) {
        return super.getList("listBy", args);
    }

    @Override
    public <T> T getOneBySqlId(String sqlId, Object args) {
        return super.getOneBySqlId("findById", args);
    }


    @Override
    public int update(String sqlId, Object args) {
        return super.update("update", args);
    }

    @Override
    public int insert(String sqlId, Object args) {
        return super.insert("insert", args);
    }

    @Override
    public int delete(String sqlId, Object args) {
        return super.delete("delete", args);
    }

    @Override
    public int logicDelete(Map<String, Object> params) {
        return super.update("logicDelete", params);
    }

    @Override
    public Integer count(Map<String, Object> params) {
        return super.getOneBySqlId("countByParam", params);
    }

    @Override
    public int update(Object args) {
        // TODO Auto-generated method stub
        return super.update(args);
    }

    @Override
    public int insert(Object args) {
        // TODO Auto-generated method stub
        return super.insert(args);
    }

    @Override
    public int delete(Object args) {
        // TODO Auto-generated method stub
        return super.delete(args);
    }

    @Override
    public String getStatement(String sqlId) {
        // TODO Auto-generated method stub
        return super.getStatement(sqlId);
    }

    @Override
    public List<String> getUserIdListByEmplyeeId(Map<String, Object> params) {
        return super.getList("listUserId", params);
    }

    @Override
    public List<Map<String, Object>> listByForView(Map<String, Object> params) {
        return super.getList("listByForView", params);
    }

}
