package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseMenuInfoDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * 描述:
 * 作者: xiangjz
 * 时间: 2017年4月8日
 * 版本: 1.0
 *
 */
@Repository("baseMenuInfoDao")
public class BaseMenuInfoDaoImpl extends BaseDaoImpl implements BaseMenuInfoDao {

    @Override
    public int logicDelete(Map<String,Object> params) {
        return super.update("logicDelete", params);
    }
}
