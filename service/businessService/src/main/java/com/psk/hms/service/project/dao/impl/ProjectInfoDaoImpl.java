package com.psk.hms.service.project.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.project.dao.ProjectInfoDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * ProjectInfoDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@Repository("projectInfoDao")
public class ProjectInfoDaoImpl extends BaseDaoImpl implements ProjectInfoDao {
	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}