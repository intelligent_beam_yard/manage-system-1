package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.constant.base.BaseOperatorBizConstant;
import com.psk.hms.base.constant.base.BaseOperatorInfoConstant;
import com.psk.hms.base.constant.base.BaseOperatorPackageMapConstant;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.enums.CommonEnumType;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseOperatorBizDao;
import com.psk.hms.service.base.dao.BaseOperatorInfoDao;
import com.psk.hms.service.base.dao.BaseRoleEmployeeDao;
import com.psk.hms.service.base.service.*;
import com.psk.hms.service.company.service.CompanyEmployeeService;
import com.psk.hms.service.company.service.CompanyInfoService;
import com.psk.hms.service.company.service.CompanyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("baseOperatorInfoService")
public class BaseOperatorInfoServiceImpl extends BaseServiceImpl implements BaseOperatorInfoService {

    @Autowired
    private BaseOperatorPackageInfoService baseOperatorPackageInfoService;
    @Autowired
    private BaseOperatorInfoDao baseOperatorInfoDao;
    @Autowired
    private BaseOperatorInfoService baseOperatorInfoService;
    @Autowired
    private BaseOperatorBizService baseOperatorBizService;
    @Autowired
    private BaseOperatorBizDao baseOperatorBizDao;

    @Autowired
    private BaseOperatorPackageMapService baseOperatorPackageMapService;

    @Autowired
    private CompanyTypeService companyTypeService;
    @Autowired
    private CompanyInfoService companyInfoService;

    @Autowired
    private CompanyEmployeeService companyEmployeeService;
    @Autowired
    private BaseRoleEmployeeDao baseRoleEmployeeDao;
    @Autowired
    private BaseRoleEmployeeService baseRoleEmployeeService;
    @Autowired
    private BaseRoleInfoService baseRoleInfoService;

    @Override
    protected BaseDao getDao() {
        return baseOperatorInfoDao;
    }

    @Override
    @Transactional
    public Map<String, Object> save(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        Map<String, Object> oepratorParams = new HashMap<String, Object>();
        oepratorParams.put("url", params.get("url"));
        HashMap<String, Object> operatorInfoMap = (HashMap<String, Object>) baseOperatorInfoService.findBy(oepratorParams);
        if(operatorInfoMap != null){
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_SAVE_ERROR, "已经存在相同url的功能");
        }
        operatorInfoMap = new HashMap<String, Object>();
        String id=StringUtil.getUuid(true);
        operatorInfoMap.put(BaseOperatorInfoConstant.ID, id);
        operatorInfoMap.put(BaseOperatorInfoConstant.CREATE_TIME, now);
        operatorInfoMap.put(BaseOperatorInfoConstant.CREATE_USER_ID, params.get(BaseOperatorInfoConstant.CURRENT_USER_ID));
        operatorInfoMap.put(BaseOperatorInfoConstant.EDIT_TIME, now);
        operatorInfoMap.put(BaseOperatorInfoConstant.CREATE_USER_ID, params.get(BaseOperatorInfoConstant.CURRENT_USER_ID));
        operatorInfoMap.put(BaseOperatorInfoConstant.IS_DELETE, Constant.IS_DELETE_NO);
        operatorInfoMap.put(BaseOperatorInfoConstant.REMARKS, params.get(BaseOperatorInfoConstant.REMARKS));
        operatorInfoMap.put(BaseOperatorInfoConstant.NAME, params.get(BaseOperatorInfoConstant.NAME));
        operatorInfoMap.put(BaseOperatorInfoConstant.ONE_MANY, params.get(BaseOperatorInfoConstant.ONE_MANY));
        operatorInfoMap.put(BaseOperatorInfoConstant.RETURN_PARAM_KEYS, params.get(BaseOperatorInfoConstant.RETURN_PARAM_KEYS));
        operatorInfoMap.put(BaseOperatorInfoConstant.RETURN_URL, params.get(BaseOperatorInfoConstant.RETURN_URL));
        operatorInfoMap.put(BaseOperatorInfoConstant.ROW_FILTER, params.get(BaseOperatorInfoConstant.ROW_FILTER));
        operatorInfoMap.put(BaseOperatorInfoConstant.URL, params.get(BaseOperatorInfoConstant.URL));
        operatorInfoMap.put(BaseOperatorInfoConstant.MODULE_ID, params.get(BaseOperatorInfoConstant.MODULE_ID));
        operatorInfoMap.put(BaseOperatorInfoConstant.SPLIT_PAGE, params.get(BaseOperatorInfoConstant.SPLIT_PAGE));
        operatorInfoMap.put(BaseOperatorInfoConstant.FORM_TOKEN, params.get(BaseOperatorInfoConstant.FORM_TOKEN));
        operatorInfoMap.put(BaseOperatorInfoConstant.IP_BLACK, params.get(BaseOperatorInfoConstant.IP_BLACK));
        operatorInfoMap.put(BaseOperatorInfoConstant.PRIVILEGESS, params.get(BaseOperatorInfoConstant.PRIVILEGESS));
        operatorInfoMap.put(BaseOperatorInfoConstant.CSRF, params.get(BaseOperatorInfoConstant.CSRF));
        operatorInfoMap.put(BaseOperatorInfoConstant.REFERER, params.get(BaseOperatorInfoConstant.REFERER));
        operatorInfoMap.put(BaseOperatorInfoConstant.METHOD, params.get(BaseOperatorInfoConstant.METHOD));
        operatorInfoMap.put(BaseOperatorInfoConstant.SYS_LOG, params.get(BaseOperatorInfoConstant.SYS_LOG));
        operatorInfoMap.put(BaseOperatorInfoConstant.ENCTYPE, params.get(BaseOperatorInfoConstant.ENCTYPE));
        operatorInfoMap.put(BaseOperatorInfoConstant.APPLY_TO, params.get(BaseOperatorInfoConstant.APPLY_TO));
        operatorInfoMap.put(BaseOperatorInfoConstant.IS_EXAME, params.get(BaseOperatorInfoConstant.IS_EXAME));

        try {
            getDao().insert(operatorInfoMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_SAVE_ERROR, "保存失败");
        }

        return operatorInfoMap;
    }

    @Override
    public Map<String, Object> findForView(String id) {
        Map<String, Object> operatorInfoParams = new HashMap<String, Object>();
        operatorInfoParams.put("id", id);
        HashMap<String, Object> operatorInfoMap = (HashMap<String, Object>) baseOperatorInfoService.findBy(operatorInfoParams, "findForView");
        return operatorInfoMap;
    }

    @Override
    @Transactional
    public void update(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        Map<String, Object> operatorInfoMap = (Map<String, Object>) getDao().findById((String)params.get("id"));
        if(operatorInfoMap == null)
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_UPDATE_ERROR, "没有找到功能");

        Map<String, Object> anotherOperatorInfoParams = new HashMap<String, Object>();
        anotherOperatorInfoParams.put(BaseOperatorInfoConstant.URL, params.get(BaseOperatorInfoConstant.URL));
        HashMap<String, Object> anotherOperatorInfoMap = (HashMap<String, Object>) getDao().findBy(anotherOperatorInfoParams);

        if(anotherOperatorInfoMap != null){
            if(!anotherOperatorInfoMap.get(BaseOperatorInfoConstant.ID).equals(params.get(BaseOperatorInfoConstant.ID)))
                throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_UPDATE_ERROR, "已经存在相同url的功能");
        }

        operatorInfoMap.put(BaseOperatorInfoConstant.EDIT_TIME, now);
        operatorInfoMap.put(BaseOperatorInfoConstant.EDIT_USER_ID, params.get(Constant.CURRENT_USER_ID));
        operatorInfoMap.put(BaseOperatorInfoConstant.REMARKS, params.get(BaseOperatorInfoConstant.REMARKS));
        operatorInfoMap.put(BaseOperatorInfoConstant.NAME, params.get(BaseOperatorInfoConstant.NAME));
        operatorInfoMap.put(BaseOperatorInfoConstant.ONE_MANY, params.get(BaseOperatorInfoConstant.ONE_MANY));
        operatorInfoMap.put(BaseOperatorInfoConstant.RETURN_PARAM_KEYS, params.get(BaseOperatorInfoConstant.RETURN_PARAM_KEYS));
        operatorInfoMap.put(BaseOperatorInfoConstant.RETURN_URL, params.get(BaseOperatorInfoConstant.RETURN_URL));
        operatorInfoMap.put(BaseOperatorInfoConstant.ROW_FILTER, params.get(BaseOperatorInfoConstant.ROW_FILTER));
        operatorInfoMap.put(BaseOperatorInfoConstant.URL, params.get(BaseOperatorInfoConstant.URL));
        operatorInfoMap.put(BaseOperatorInfoConstant.MODULE_ID, params.get(BaseOperatorInfoConstant.MODULE_ID));
        operatorInfoMap.put(BaseOperatorInfoConstant.SPLIT_PAGE, params.get(BaseOperatorInfoConstant.SPLIT_PAGE));
        operatorInfoMap.put(BaseOperatorInfoConstant.FORM_TOKEN, params.get(BaseOperatorInfoConstant.FORM_TOKEN));
        operatorInfoMap.put(BaseOperatorInfoConstant.IP_BLACK, params.get(BaseOperatorInfoConstant.IP_BLACK));
        operatorInfoMap.put(BaseOperatorInfoConstant.PRIVILEGESS, params.get(BaseOperatorInfoConstant.PRIVILEGESS));
        operatorInfoMap.put(BaseOperatorInfoConstant.CSRF, params.get(BaseOperatorInfoConstant.CSRF));
        operatorInfoMap.put(BaseOperatorInfoConstant.REFERER, params.get(BaseOperatorInfoConstant.REFERER));
        operatorInfoMap.put(BaseOperatorInfoConstant.METHOD, params.get(BaseOperatorInfoConstant.METHOD));
        operatorInfoMap.put(BaseOperatorInfoConstant.SYS_LOG, params.get(BaseOperatorInfoConstant.SYS_LOG));
        operatorInfoMap.put(BaseOperatorInfoConstant.ENCTYPE, params.get(BaseOperatorInfoConstant.ENCTYPE));
        operatorInfoMap.put(BaseOperatorInfoConstant.APPLY_TO, params.get(BaseOperatorInfoConstant.APPLY_TO));
        operatorInfoMap.put(BaseOperatorInfoConstant.IS_EXAME, params.get(BaseOperatorInfoConstant.IS_EXAME));

        try {
            getDao().update(operatorInfoMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_UPDATE_ERROR, "修改失败");
        }
    }

    @Override
    @Transactional
    public void delete(Map<String, Object> params) {
        List<String> idsArr = (List<String>) params.get("ids");
        Timestamp now = DateUtil.getSqlTimestamp();

        for (String operatorId : idsArr) {
            //删除功能与权限包的关联
            Map<String, Object> operatorPackageMapParams = new HashMap<String, Object>();
            operatorPackageMapParams.put(BaseOperatorPackageMapConstant.OPERATORID, operatorId);
            List<Map<String, Object>> operatorPackageMapList = baseOperatorPackageMapService.listBy(operatorPackageMapParams);

            for(Map<String, Object> operatorPackageMap : operatorPackageMapList){
                operatorPackageMap.put("editTime", now);
                operatorPackageMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
                operatorPackageMap.put("isDelete", Constant.IS_DELETE_YES);
                try {
                    baseOperatorPackageMapService.update(operatorPackageMap);
                } catch (Exception e) {
                    log.error("", e);
                    throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_DELETE_ERROR, "功能删除失败");
                }
            }

            //删除功能业务关联
            Map<String, Object> operatorBizParams = new HashMap<String, Object>();
            operatorBizParams.put(BaseOperatorBizConstant.PERMISSION_ID, operatorId);
            operatorBizParams.put(BaseOperatorBizConstant.PERMISSION_TYPE, BaseOperatorBizConstant.PERMISSION_TYPE_1);
            List<Map<String, Object>> operatorBizMapList = baseOperatorBizService.listBy(operatorBizParams);

            for(Map<String, Object> operatorBizMap : operatorBizMapList){
                operatorBizMap.put("editTime", now);
                operatorBizMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
                operatorBizMap.put("isDelete", Constant.IS_DELETE_YES);
                try {
                    baseOperatorBizDao.update(operatorBizMap);
                } catch (Exception e) {
                    log.error("", e);
                    throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_DELETE_ERROR, "功能删除失败");
                }
            }

            //删除功能
            Map<String, Object> operatorInfoMap = (Map<String, Object>) getDao().findById(operatorId);
            if(operatorInfoMap == null)
                continue;

            operatorInfoMap.put("editTime", now);
            operatorInfoMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
            operatorInfoMap.put("isDelete", Constant.IS_DELETE_YES);
            try {
                getDao().update(operatorInfoMap);
                //删除缓存中的operators
                /*operatorDataCacher.deleteOneCacheOperator(operatorId);*/
            } catch (Exception e) {
                log.error("", e);
                throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_DELETE_ERROR, "功能删除失败");
            }
        }
    }

    @Override
    public List<Map<String, Object>> listByOperation(Map<String, Object> params) {
        String teamTypeId = BaseUtil.retStr(params.get("teamTypeId"));
        String teamId = BaseUtil.retStr(params.get("teamId"));
//		String baseRoleId = BaseUtil.retStr(params.get("baseRoleId"));
        String teamEmployeeId = BaseUtil.retStr(params.get("employeeId"));
        //通过teamEmployeeId找到对应的所有通用的角色baseRoleInfo
        Map<String, Object> baseRoleInfoMap = new HashMap<String, Object>();
        baseRoleInfoMap.put("employeeId", teamEmployeeId);
        baseRoleInfoMap.put("roleType", "1");
        List<Map<String, Object>> briList =
                baseRoleEmployeeDao.getList("listRoleBy", baseRoleInfoMap);
        params.put("briList", briList);
        //通过teamEmployeeId找到对应的所有自定义的角色teamRole
        Map<String, Object> teamRoleMap = new HashMap<String, Object>();
        teamRoleMap.put("employeeId", teamEmployeeId);
        teamRoleMap.put("roleType", "2");
        List<Map<String, Object>> trList =
                baseRoleEmployeeDao.getList("listRoleBy", teamRoleMap);
        params.put("trList", trList);

        if(!BaseUtil.isEmpty(teamTypeId) || !BaseUtil.isEmpty(teamId) ||
                !BaseUtil.isEmpty(briList) || !BaseUtil.isEmpty(trList) ||
                !BaseUtil.isEmpty(teamEmployeeId)) {
            params.put("validOperatorCondition", true);
        }
        return baseOperatorInfoService.listBy(params,"listByOperation");
    }

    @Override
    @Transactional
    public void authSave(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        //先删除后新增
        Map<String, Object> oldOperatorBizParams = new HashMap<String, Object>();
        oldOperatorBizParams.put("bizType", params.get("bizType"));
        oldOperatorBizParams.put("permissionId", params.get("permissionId"));
        oldOperatorBizParams.put("permissionType", params.get("permissionType"));
        List<Map<String, Object>> oldOperatorBizMapList = baseOperatorBizDao.listBy(oldOperatorBizParams);

        for(Map<String, Object> oldOperatorBizMap : oldOperatorBizMapList){
            oldOperatorBizMap.put("editTime", now);
            oldOperatorBizMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
            oldOperatorBizMap.put("isDelete", Constant.IS_DELETE_YES);

            try {
                baseOperatorBizDao.update(oldOperatorBizMap);
                // ------------------------ 新增缓存中的operatorBiz ------------------------
                /*operatorDataCacher.updateCachedOperatorBizForOperatorAuth(oldOperatorBizMap);*/
                // ------------------------ 新增缓存中的operatorBiz ------------------------
            } catch (Exception e) {
                throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_AUTH_ERROR, "功能授权失败");
            }
        }

        List<String> bizIds = (List<String>) params.get("bizIds");

        if(bizIds != null)
            for (String bizId : bizIds) {
                //新增功能关联
                Map<String, Object> operatorBizMap = new HashMap<String, Object>();

                operatorBizMap.put("id", StringUtil.getUuid(true));
                operatorBizMap.put("createTime", now);
                operatorBizMap.put("createUserId", params.get(Constant.CURRENT_USER_ID));
                operatorBizMap.put("editTime", now);
                operatorBizMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
                operatorBizMap.put("isDelete", Constant.IS_DELETE_NO);
                operatorBizMap.put("bizId", bizId);
                operatorBizMap.put("bizType", params.get("bizType"));
                operatorBizMap.put("permissionId", params.get("permissionId"));
                operatorBizMap.put("permissionType", params.get("permissionType"));
                try {
                    baseOperatorBizDao.insert(operatorBizMap);
                    // ------------------------ 新增缓存中的operatorBiz ------------------------
					/*operatorBizMap.put("insert", true);
					operatorDataCacher.updateCachedOperatorBizForOperatorAuth(operatorBizMap);*/
                    // ------------------------ 新增缓存中的operatorBiz ------------------------
                } catch (Exception e) {
                    throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_AUTH_ERROR, "功能授权失败");
                }
            }

    }

    /**
     * 权限包勾选
     */
    @Override
    public Map<String, Object> operatorPackage(Map<String, Object> params) {
        //根据权限包id和功能id查询是否已经关联
        @SuppressWarnings("unchecked")
        Map<String,Object> packageMap=(Map<String, Object>) baseOperatorPackageMapService.findBy(params);
        Map<String,Object> map=null;
        if(packageMap!=null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_DELETE_ERROR, "权限包已勾选");
        }else {
            map=baseOperatorPackageMapService.save(params);
        }
        return map;
    }

    /**
     * 权限包取消勾选
     */
    @Override
    public void uncheckOperatorPackage(Map<String, Object> params) {
        //根据权限包id和功能id查询是否已经关联
        @SuppressWarnings("unchecked")
        Map<String,Object> packageMap=(Map<String, Object>) baseOperatorPackageMapService.findBy(params);
        if(packageMap==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_DELETE_ERROR, "授权包尚未勾选");
        }else {
            Map<String,Object> map=new HashMap<>();
            map.put("id", packageMap.get("id"));
            map.put(BaseOperatorPackageMapConstant.CURRENT_USER_ID, packageMap.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
            baseOperatorPackageMapService.delete(map);
        }
    }

    @Override
    @Transactional
    public void treeAuthSave(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        Map<String, Object> oldOperatorBizParams = new HashMap<String, Object>();
        oldOperatorBizParams.put("bizId", params.get("bizId"));
        oldOperatorBizParams.put("bizType", params.get("bizType"));
        oldOperatorBizParams.put("permissionId", params.get("permissionId"));
        oldOperatorBizParams.put("permissionType", params.get("permissionType"));
        List<Map<String, Object>>oldOperatorBizMapList = null;
        oldOperatorBizMapList = baseOperatorBizDao.listBy(oldOperatorBizParams);

        if(oldOperatorBizMapList != null&&oldOperatorBizMapList.size()>0) {
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_AUTH_ERROR, "功能已经授权");
        }
        //新增功能关联
        Map<String, Object> operatorBizMap = new HashMap<String, Object>();

        operatorBizMap.put("id", StringUtil.getUuid(true));
        operatorBizMap.put("createTime", now);
        operatorBizMap.put("createUserId", params.get(Constant.CURRENT_USER_ID));
        operatorBizMap.put("editTime", now);
        operatorBizMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
        operatorBizMap.put("isDelete", Constant.IS_DELETE_NO);
        operatorBizMap.put("bizId", params.get("bizId"));
        operatorBizMap.put("bizType", params.get("bizType"));
        operatorBizMap.put("permissionId", params.get("permissionId"));
        operatorBizMap.put("permissionType", params.get("permissionType"));
        try {
            baseOperatorBizDao.insert(operatorBizMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_AUTH_ERROR, "功能授权失败");
        }
    }

    @Override
    @Transactional
    public void treeCancelSave(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();

        Map<String, Object> oldOperatorBizParams = new HashMap<String, Object>();
        oldOperatorBizParams.put("bizId", params.get("bizId"));
        oldOperatorBizParams.put("bizType", params.get("bizType"));
        oldOperatorBizParams.put("permissionId", params.get("permissionId"));
        oldOperatorBizParams.put("permissionType", params.get("permissionType"));
        List<Map<String, Object>>oldOperatorBizMapList=null;
        oldOperatorBizMapList = baseOperatorBizDao.listBy(oldOperatorBizParams);

        if(oldOperatorBizMapList != null&&oldOperatorBizMapList.size()==1) {
            //新增功能关联
            Map<String, Object> operatorBizMap = oldOperatorBizMapList.get(0);
            operatorBizMap.put("isDelete", Constant.IS_DELETE_YES);
            operatorBizMap.put("editTime", now);
            operatorBizMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
            try {
                baseOperatorBizDao.update(operatorBizMap);
            } catch (Exception e) {
                throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_AUTH_ERROR, "取消功能授权失败");
            }
        }else {
            throw new BizException(BaseBizExceptionCode.OPERATE_MANAGE_AUTH_ERROR, "功能尚未授权");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean hasPrivilege(String operatorId, String employeeId) {
        if (StringUtil.isBlank(operatorId) || StringUtil.isBlank(employeeId)) {
            return false;
        }

        /*if(operatorCacheOnYml.isOperatorCacheOn()) {
            //获取员工相关验证参数
            Map<String, Object> employeeValidParams = operatorDataCacher.getEmployeeBizAbout(employeeId);
            return operatorDataCacher.hasPrivilege(operatorId, BaseOperatorInfoConstant.APPLY_TO_4, employeeValidParams);
        } else {*/
            // 验证员工
            Map<String, Object> companyEmployeeMap = companyEmployeeService.findById(employeeId);
            if (BaseUtil.isEmpty(companyEmployeeMap))
                return false;
            // 验证单位
            Map<String, Object> companyInfoMap = companyInfoService.findById(BaseUtil.retStr(companyEmployeeMap.get("companyId")));
            if (BaseUtil.isEmpty(companyInfoMap))
                return false;
            // 单位类型
            Map<String, Object> companyTypeMap = companyTypeService.findById(BaseUtil.retStr(companyInfoMap.get("companyType")));
            if (BaseUtil.isEmpty(companyTypeMap))
                return false;

            // 验证单位类型权限
            Map<String, Object> baseOperatorBizParams = new HashMap<>();
            baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_ID, companyTypeMap.get(Constant.ID));
            baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_TYPE, CommonEnumType.OperatorBizType.单位类型.getValue());
            baseOperatorBizParams.put("operatorId", operatorId);
            List<Map<String, Object>> baseOperatorBizMapList = baseOperatorBizService.listBy(baseOperatorBizParams,
                    "listForAuth");
            if (!baseOperatorBizMapList.isEmpty()) {
                return true;
            }
            // 验证单位权限
            baseOperatorBizParams.clear();
            baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_ID, companyInfoMap.get(Constant.ID));
            baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_TYPE, CommonEnumType.OperatorBizType.单位.getValue());
            baseOperatorBizParams.put("operatorId", operatorId);
            baseOperatorBizMapList = baseOperatorBizService.listBy(baseOperatorBizParams, "listForAuth");
            if (!baseOperatorBizMapList.isEmpty()) {
                return true;
            }
            // 验证通用角色权限
            Map<String, Object> employeeRoleParams = new HashMap<>();
            employeeRoleParams.put("employeeId", employeeId);
            List<Map<String, Object>> teamEmployeeRoleMapList = baseRoleEmployeeService.listBy(employeeRoleParams);
            for (Map<String, Object> employeeRoleMap : teamEmployeeRoleMapList) {
                Map<String, Object> baseRoleInfoMap = (Map<String, Object>) baseRoleInfoService.findById(BaseUtil.retStr(employeeRoleMap.get("roleId")));
                if (BaseUtil.isEmpty(baseRoleInfoMap)) {
                    continue;
                }
                baseOperatorBizParams.clear();
                baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_ID, baseRoleInfoMap.get(Constant.ID));
                baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_TYPE, CommonEnumType.OperatorBizType.通用角色.getValue());
                baseOperatorBizParams.put("operatorId", operatorId);
                baseOperatorBizMapList = baseOperatorBizService.listBy(baseOperatorBizParams, "listForAuth");
                if (!baseOperatorBizMapList.isEmpty())
                    return true;
            }
            //验证单位人员
            baseOperatorBizParams.clear();
            baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_ID, employeeId);
            baseOperatorBizParams.put(BaseOperatorBizConstant.BIZ_TYPE, CommonEnumType.OperatorBizType.单位人员.getValue());
            baseOperatorBizParams.put("operatorId", operatorId);
            baseOperatorBizMapList = baseOperatorBizService.listBy(baseOperatorBizParams, "listForAuth");
            if (!baseOperatorBizMapList.isEmpty()) {
                return true;
            }
            return false;
        /*}*/
    }

}
