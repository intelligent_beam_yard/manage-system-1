package com.psk.hms.service.base.constant;


import com.psk.hms.base.constant.common.Constant;

/**
 * 用户常量类
 * @author xiangjz
 *
 */
public class UserConstant extends Constant {

	/**
	 * 用户状态 -- 正常 - 0 
	 */
	public static final String STATE_NORMAL = "1";
	/**
	 * 用户状态 -- 冻结 - 1 
	 */
	public static final String STATE_FROZEN = "2";
	/**
	 * 用户状态 -- 离职 - 3
	 */
	public static final String STATE_DIMISSION = "3";
}
