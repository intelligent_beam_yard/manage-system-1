package com.psk.hms.service.company.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.company.dao.CompanyInfoDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * CompanyInfoDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@Repository("companyInfoDao")
public class CompanyInfoDaoImpl extends BaseDaoImpl implements CompanyInfoDao {

	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}