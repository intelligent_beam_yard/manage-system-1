package com.psk.hms.service.base.service;

import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.core.service.BaseService;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface BaseRoleInfoService extends BaseService {

	/**
	 * 根据条件查询用户角色
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> listRoleByParam(Map<String,Object> params);

	/**
	 * 查询角色树
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> roleTree(Map<String,Object> params);

	/**
	 * 解绑员工角色
	 * @param params
	 * @return
	 */
	public JsonResult unBindEmployee(Map<String,Object> params);

	/**
	 * 绑定员工角色
	 * @param params
	 * @return
	 */
	public JsonResult bindEmployee(Map<String,Object> params);

	/**
	 * 添加、修改
	 * @param params
	 * @return
	 */
	public JsonResult addOrUpdate(Map<String,Object> params);


	/**
	 * 逻辑删除[<b>批量</b>]
	 *
	 * @param ids
	 * @return
	 */
	Map<String,Object> logicDelete(List<String> ids);

	/**
	 * 分页查询所有
	 * @param params
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	PageBean<Map<String, Object>> listPageSelf(Map<String, Object> params, Integer pageNum, Integer pageSize);

	/**
	 * 查询所有
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> listAll(Map<String, Object> params);
	
}
