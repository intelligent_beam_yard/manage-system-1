package com.psk.hms.service.common.enums;

import com.psk.hms.base.enums.CommonEnumTypeToList;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.redis.service.RedisService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


/**
 * @功能说明：加载的业务字典类
 * @author xiangjz
 * @创建日期： 2018-01-02
 */
@SuppressWarnings("unchecked")
@Component
public class EnumsLoader {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private RedisService redis;
	
	private final String ENUMNAME = "enums";

	/**
	 * 加载业务字典到内存中
	 * 
	 */
	public void loadCodeDictionary() {
		log.info("开始加载业务枚举......");
		//状态读取
		Map enums = new HashMap();
		enums.putAll(CommonEnumTypeToList.writeMemoryTempleStatus());
		redis.set("enums", enums);
		log.info("加载业务枚举完成......");
	}
	
	/**
	 * 枚举定义——获取相应值转换为list
	 * @return
	 */
	public List getAllCommonEnumList() {
		Map<String, Object> enumsMap = (Map) redis.get(ENUMNAME);
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		if(!BaseUtil.isEmpty(enumsMap)) {
			for (Entry<String, Object> entry : enumsMap.entrySet()) {
				String key = entry.getKey();
				if(key.endsWith("_list")) {
					Map<String, Object> item = new HashMap<String, Object>();
					key = key.substring(key.indexOf("_")+1, key.lastIndexOf("_"));
					item.put("enumName", key);
					item.put("enumValues", entry.getValue());
					resultList.add(item);
				}
			}
		}
		return resultList;
	}
	
	/**
	 * 枚举定义——获取相应值转换为list
	 * @param val
	 * @return
	 */
	public List<Object> getCommonEnumList(String val) {
	   return resourceAsList("comEnum_"+val+"_list");
	}
	
	/**
	 * 枚举定义——获取相应值转换为map
	 * 
	 * @param val
	 * @return
	 */
	public Map<Object, Object> getCommonEnumMap(String val) {
		return resourceAsMap("comEnum_"+val+"_map");
	}
	
	
	/**
	 * 获取资源
	 * @param key
	 * @return
	 */
	public Object  getObjectByKey(String key) {
		Map enums = (Map) redis.get(ENUMNAME);
		if (!enums.keySet().contains(key))
			return null;
		return enums.get(key);
	}
	
	/**
	 * 从内存中获取指定业务类型的业务字典集合
	 * @param key -业务字典类别
	 * @return -返回特定类别的值集合
	 */
	public List<Object> resourceAsList(String key) {
		Map enums = (Map) redis.get(ENUMNAME);
		if (!enums.keySet().contains(key))
			return new ArrayList<Object>();
		return (List<Object>) enums.get(key);

	}

	/**
	 * map获取
	 * @param key
	 * @return
	 */
	public Map<Object, Object> resourceAsMap(String key) {
		Map enums = (Map) redis.get(ENUMNAME);
		if (!enums.keySet().contains(key))
			return null;
		return (Map<Object, Object>) enums.get(key);

	}

	/**
	 * 从内存中获取指定系统参数的值
	 * @param key
	 * 		-系统字典key
	 * @return String
	 * 		-返回系统字典value
	 */
	public String getResourceAsString(String key) {
		Map enums = (Map) redis.get(ENUMNAME);
		if (!enums.keySet().contains("SYS_" + key))
			return null;
		return (String) enums.get("SYS_" + key);
	}


	/**
	 * 获取业务字典对应type,value的中文名称
	 * @param type
	 * 		-业务字典类型
	 * @param value
	 * 		-业务字典值
	 * @return
	 */
	public String getCnNameOfBusinessDictionary(String type, String value) {
		if (BaseUtil.isEmpty(value))
			return null;
		return null;
	}
}
