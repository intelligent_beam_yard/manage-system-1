package com.psk.hms.service.base.service;

import com.psk.hms.core.service.BaseService;

import java.util.Map;

@SuppressWarnings("rawtypes")
public interface BaseOperatorPackageInfoService extends BaseService {

	Map<String,Object> save(Map<String, Object> params);

	void update(Map<String, Object> params);

	void delete(Map<String, Object> params);

	Map<String, Object> permissionPack(Map<String, Object> params);

	void permissionUnPack(Map<String, Object> params);
}
