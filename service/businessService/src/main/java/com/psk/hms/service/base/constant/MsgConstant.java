package com.psk.hms.service.base.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * 消息常量类
 * @author xiangjz
 *
 */
public class MsgConstant/* extends Constant*/{
	//--------------------------------------下面是消息终端类型--------------------------------------
	/**
	 * 终端类型(站内) - 1
	 */
	public static final String MSG_ENDTYPE_INSIDE = "1";
	/**
	 * 终端类型(轻推) - 2
	 */
	public static final String MSG_ENDTYPE_QT = "2";
	/**
	 * 终端类型(微信) - 3
	 */
	public static final String MSG_ENDTYPE_WX = "3";
	
	
	
	
	//--------------------------------------下面是消息类型--------------------------------------
	/**
	 * 消息类型(文字消息) - 1
	 */
	public static final String MSG_TYPE_TEXT = "1";
	/**
	 * 消息类型(单图文消息) - 2
	 */
	public static final String MSG_TYPE_IMGTEXT_SINGLE = "2";
	/**
	 * 消息类型(多图文消息) - 3
	 */
	public static final String MSG_TYPE_IMGTEXT_MULTI = "3";
	
	/**
	 * 构造屏蔽的终端类型list
	 * @param endTypes
	 * @return
	 */
	public static List<String> generateNoEnds(String... endTypes){
		List<String> resultList = new ArrayList<String>();
		for (String end : endTypes) {  
			resultList.add(end);
        }
		return resultList;
	}
}
