package com.psk.hms.service.base.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.List;
import java.util.Map;


/**
 * 描述： 通用角色dao
 * 作者： xiangjz
 * 时间： 2018年5月4日
 * 版本： 1.0v
 *
 */
public interface BaseRoleInfoDao extends BaseDao {
	
	List<Map<String, Object>> listByEmployeeId(String employeeId);

	/**
	 * 逻辑删除
	 * @param params
	 * @return
	 */
	public int logicDelete(Map<String,Object> params);
}
