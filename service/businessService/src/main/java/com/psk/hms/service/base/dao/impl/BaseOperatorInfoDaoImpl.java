package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseOperatorInfoDao;
import org.springframework.stereotype.Repository;

/**
 * 描述:
 * 作者: xiangjz
 * 时间: 2017年4月8日
 * 版本: 1.0
 *
 */
@Repository("baseOperatorInfoDao")
public class BaseOperatorInfoDaoImpl extends BaseDaoImpl implements BaseOperatorInfoDao {

}
