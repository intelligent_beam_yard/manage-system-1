package com.psk.hms.service.service;


import com.psk.hms.core.service.BaseService;

import java.util.Map;


/******
 * 功能日志service接口
 * @author xiangjz
 * @date 2018年5月16日
 * @version 1.0
 *
 */
@SuppressWarnings("rawtypes")
public interface LogOperatorService extends BaseService {
	
	void save(Map<String, Object> params);

	void delete(Map<String, Object> params);
}
