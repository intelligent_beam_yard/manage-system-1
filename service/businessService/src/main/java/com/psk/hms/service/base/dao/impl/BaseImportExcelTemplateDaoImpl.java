package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseImportExcelTemplateDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * BaseImportExcelTemplateDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-26
 * @version 1.0.0
 */
@Repository("baseImportExcelTemplateDao")
public class BaseImportExcelTemplateDaoImpl extends BaseDaoImpl implements BaseImportExcelTemplateDao {
	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}