package com.psk.hms.service.company.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.company.dao.CompanyEmployeeDao;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * CompanyEmployeeDaoImpl
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-20
 * @version 1.0.0
 */
@Repository("companyEmployeeDao")
public class CompanyEmployeeDaoImpl extends BaseDaoImpl implements CompanyEmployeeDao {

	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}