package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.constant.base.BaseOperatorPackageInfoConstant;
import com.psk.hms.base.constant.base.BaseOperatorPackageMapConstant;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseOperatorPackageInfoDao;
import com.psk.hms.service.base.dao.BaseOperatorPackageMapDao;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.base.service.BaseOperatorPackageInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("baseOperatorPackageInfoService")
public class BaseOperatorPackageInfoServiceImpl extends BaseServiceImpl implements BaseOperatorPackageInfoService {

    @Autowired
    private BaseOperatorPackageInfoDao baseOperatorPackageInfoDao;
    @Autowired
    private BaseOperatorPackageMapDao baseOperatorPackageMapDao;
    @Autowired
    private BaseOperatorInfoService baseOperatorInfoService;

    @Override
    protected BaseDao getDao() {
        return baseOperatorPackageInfoDao;
    }

    @Override
    @Transactional
    public Map<String, Object> save(Map<String, Object> params) {
        Timestamp now = DateUtil.getSqlTimestamp();
        HashMap<String, Object> operatorPackageMap = new HashMap<String, Object>();
        Map<String,Object> args=new HashMap<String,Object>();
        args.put("code", params.get("code"));
        Map<String,Object> map=getDao().findBy(args);
        if(map!=null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "code已被占用");
        }

        String id=StringUtil.getUuid(true);
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.ID, id);
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.CREATE_TIME, now);
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.CREATE_USER_ID, params.get(BaseOperatorPackageInfoConstant.CURRENT_USER_ID));
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.EDIT_TIME, now);
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.EDIT_USER_ID, params.get(BaseOperatorPackageInfoConstant.CURRENT_USER_ID));
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.IS_DELETE, Constant.IS_DELETE_NO);
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.REMARKS, params.get(BaseOperatorPackageInfoConstant.REMARKS));
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.NAME, params.get(BaseOperatorPackageInfoConstant.NAME));
        operatorPackageMap.put(BaseOperatorPackageInfoConstant.CODE, params.get(BaseOperatorPackageInfoConstant.CODE));

        try {
            getDao().insert(operatorPackageMap);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "权限包基础信息保存失败");
        }
        return operatorPackageMap;
    }

    @Override
    @Transactional
    public void update(Map<String, Object> params) {
        @SuppressWarnings("unchecked")
        Map<String, Object> operatorInfo = (Map<String, Object>) getDao().findById((String) params.get(BaseOperatorPackageInfoConstant.ID));

        if (operatorInfo == null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_UPDATE_ERROR, "没有找到要修改的权限包基本信息");
        }

        Map<String,Object> args=new HashMap<String,Object>();
        args.put(BaseOperatorPackageInfoConstant.CODE, params.get(BaseOperatorPackageInfoConstant.CODE));
        Map<String,Object> map=getDao().findBy(args);
        if(map!=null&&!map.get(BaseOperatorPackageInfoConstant.ID).equals(params.get(BaseOperatorPackageInfoConstant.ID))) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_SAVE_ERROR, "code已被占用");
        }

        Timestamp now = DateUtil.getSqlTimestamp();
        operatorInfo.put(BaseOperatorPackageInfoConstant.EDIT_TIME, now);
        operatorInfo.put(BaseOperatorPackageInfoConstant.ID, params.get(BaseOperatorPackageInfoConstant.ID));
        operatorInfo.put(BaseOperatorPackageInfoConstant.EDIT_USER_ID, params.get(BaseOperatorPackageInfoConstant.CURRENT_USER_ID));
        operatorInfo.put(BaseOperatorPackageInfoConstant.NAME, params.get(BaseOperatorPackageInfoConstant.NAME));
        operatorInfo.put(BaseOperatorPackageInfoConstant.CODE, params.get(BaseOperatorPackageInfoConstant.CODE));
        operatorInfo.put(BaseOperatorPackageInfoConstant.REMARKS, params.get(BaseOperatorPackageInfoConstant.REMARKS));
        try {
            getDao().update(operatorInfo);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_UPDATE_ERROR, "权限包基本信息修改失败");
        }
    }

    @SuppressWarnings("unused")
    @Override
    @Transactional
    public void delete(Map<String, Object> params) {
        @SuppressWarnings("unchecked")
        List<String> idsArr = (List<String>) params.get("ids");
        Timestamp now = DateUtil.getSqlTimestamp();

        for (String operatorPackageId : idsArr) {
            //删除功能
            @SuppressWarnings("unchecked")
            Map<String, Object> operatorInfoMap = (Map<String, Object>) getDao().findById(operatorPackageId);
            if(operatorInfoMap == null)
                continue;
            operatorInfoMap.put(BaseOperatorPackageInfoConstant.EDIT_TIME, now);
            operatorInfoMap.put(BaseOperatorPackageInfoConstant.EDIT_USER_ID, params.get(BaseOperatorPackageInfoConstant.CURRENT_USER_ID));
            operatorInfoMap.put(BaseOperatorPackageInfoConstant.IS_DELETE, BaseOperatorPackageInfoConstant.IS_DELETE_YES);
            try {
                //删除权限包
                getDao().update(operatorInfoMap);

                Map<String, Object> condition = new HashMap<String, Object>();
                condition.put(BaseOperatorPackageMapConstant.PACKAGEID, operatorPackageId);
                List<Map<String, Object>> operatorPackageMapList = baseOperatorPackageMapDao.listBy(condition);
                //删除权限包关联的功能
                deleteAllByPackageId(operatorPackageId);

            } catch (Exception e) {
                throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_DELETE_ERROR, "权限包基础信息删除失败");
            }
        }

    }

    /**
     * 根据权限包id,删除权限包下所有功能关联，删除权限包的授权
     * @param packageId
     */
    private void deleteAllByPackageId(String packageId) {
        //删除权限包下所有功能
        try {
            baseOperatorPackageMapDao.update("deleteAllByPackageId",packageId);
        } catch (Exception e) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_PACKAGE_DELETE_ERROR, "删除权限包下所有功能的关联失败");
        }
    }

    @Override
    @Transactional
    public Map<String, Object> permissionPack(Map<String, Object> params) {
        //查询验证功能是否存在
        @SuppressWarnings("unchecked")
        Map<String,Object> operator=(Map<String, Object>) baseOperatorInfoService.findById((String) params.get(BaseOperatorPackageMapConstant.OPERATORID));
        if(operator==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_TO_PACKAGE_SAVE_ERROR, "功能不存在，加入权限包失败");
        }
        //查询功能是否在权限包内
        @SuppressWarnings("unchecked")
        Map<String,Object> operatorPackage=(Map<String, Object>) baseOperatorPackageMapDao.findBy(params);
        if(operatorPackage!=null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_TO_PACKAGE_SAVE_ERROR, "功能已存在权限包内，无需加入");
        }
        Map<String,Object> operatorMap=new HashMap<String,Object>();
        try {
            Timestamp now = DateUtil.getSqlTimestamp();
            String id=StringUtil.getUuid(true);
            operatorMap.put(BaseOperatorPackageMapConstant.ID, id);
            operatorMap.put(BaseOperatorPackageMapConstant.CREATE_TIME, now);
            operatorMap.put(BaseOperatorPackageMapConstant.CREATE_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
            operatorMap.put(BaseOperatorPackageMapConstant.EDIT_TIME, now);
            operatorMap.put(BaseOperatorPackageMapConstant.EDIT_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
            operatorMap.put(BaseOperatorPackageMapConstant.IS_DELETE, Constant.IS_DELETE_NO);
            operatorMap.put(BaseOperatorPackageMapConstant.OPERATORID, params.get(BaseOperatorPackageMapConstant.OPERATORID));
            operatorMap.put(BaseOperatorPackageMapConstant.PACKAGEID, params.get(BaseOperatorPackageMapConstant.PACKAGEID));
            //将功能id关联权限包
            baseOperatorPackageMapDao.insert(operatorMap);

        }catch (Exception e) {
            e.printStackTrace();
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_TO_PACKAGE_SAVE_ERROR, "功能加入权限包失败");
        }
        return operatorMap;
    }

    @Override
    public void permissionUnPack(Map<String, Object> params) {
        //查询功能是否在权限包内
        @SuppressWarnings("unchecked")
        Map<String,Object> operator=(Map<String, Object>) baseOperatorPackageMapDao.findById((String)params.get(BaseOperatorPackageMapConstant.ID));
        if(operator==null) {
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_TO_PACKAGE_DELETE_ERROR, "功能不存在权限包内，无需移除");
        }
        try {
            Timestamp now = DateUtil.getSqlTimestamp();
            operator.put(BaseOperatorPackageMapConstant.EDIT_TIME, now);
            operator.put(BaseOperatorPackageMapConstant.EDIT_USER_ID, params.get(BaseOperatorPackageMapConstant.CURRENT_USER_ID));
            operator.put(BaseOperatorPackageMapConstant.IS_DELETE, Constant.IS_DELETE_YES);
            //将功能id关联权限包
            baseOperatorPackageMapDao.update(operator);
        }catch (Exception e) {
            log.error(e);
            throw new BizException(BaseBizExceptionCode.OPERATEBIZ_TO_PACKAGE_DELETE_ERROR, "功能移除权限包失败");
        }
    }

}
