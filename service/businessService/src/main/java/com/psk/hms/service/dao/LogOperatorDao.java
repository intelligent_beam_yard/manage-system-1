package com.psk.hms.service.dao;


import com.psk.hms.core.dao.BaseDao;

/**********
 * 功能日志dao接口
 * @author xiangjz
 * @date 2018年5月16日
 * @version 1.0
 *
 */
public interface LogOperatorDao extends BaseDao {

}
