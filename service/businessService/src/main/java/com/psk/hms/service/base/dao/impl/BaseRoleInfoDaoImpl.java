package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseRoleInfoDao;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 描述:
 * 作者: xiangjz
 * 时间: 2018年5月4日
 * 版本: 1.0
 *
 */
@Repository("baseRoleInfoDao")
public class BaseRoleInfoDaoImpl extends BaseDaoImpl implements BaseRoleInfoDao {

	@Override
	public List<Map<String, Object>> listByEmployeeId(String employeeId) {
		return this.sqlSession.selectList(getStatement("listByEmployeeId"), employeeId);
	}

	@Override
	public int logicDelete(Map<String,Object> params) {
		return super.update("logicDelete", params);
	}
}
