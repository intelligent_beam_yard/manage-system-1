package com.psk.hms.service.base.service;

import com.psk.hms.base.entity.PageBean;
import com.psk.hms.core.service.BaseService;

import java.util.Map;


@SuppressWarnings("rawtypes")
public interface BaseOperatorBizService extends BaseService {
	
	Map<String, Object> save(Map<String, Object> params);
	
	void delete(Map<String, Object> params);

	PageBean permisionPackageList(Map<String, Object> params, int pageNum, int pageSize);
}
