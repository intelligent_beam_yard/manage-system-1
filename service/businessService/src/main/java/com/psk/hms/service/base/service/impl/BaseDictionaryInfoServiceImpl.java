package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.entity.CommonTreeParam;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseDictionaryInfoDao;
import com.psk.hms.service.base.service.BaseDictionaryInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @link BaseDictionaryInfoServiceImpl 实现类
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@Service
public class BaseDictionaryInfoServiceImpl extends BaseServiceImpl<Map<String, Object>> implements BaseDictionaryInfoService {

    @Autowired
    private BaseDictionaryInfoDao baseDictionaryInfoDao;
    
    @Override
	protected BaseDao getDao() {
		return baseDictionaryInfoDao;
	}

    @SuppressWarnings("unchecked")
	@Transactional
	@Override
	public JsonResult addOrUpdate(Map<String,Object> params) {
    	//查询是否有重复数据，不允许code或者name重复
		Map<String, Object> validCondition = new HashMap<String, Object>();
		validCondition.put("code", params.get("code"));
		validCondition.put("notId", params.get("id"));
		List<Map<String, Object>> validList = baseDictionaryInfoDao.listBy(validCondition);
		if(!BaseUtil.isEmpty(validList))
			throw new BizException(BaseBizExceptionCode.DICTIONARY_ERROR,
					"数据重复，不允许有相同code的数据字典");
		validCondition = new HashMap<String, Object>();
		validCondition.put("name", params.get("name"));
		validCondition.put("notId", params.get("id"));
		validList = baseDictionaryInfoDao.listBy(validCondition);
		if(!BaseUtil.isEmpty(validList))
			throw new BizException(BaseBizExceptionCode.DICTIONARY_ERROR,
					"数据重复，不允许有相同name的数据字典");
    	
		//获取记录id值
		String id = BaseUtil.retStr(params.get("id"));
		//获取当前时间
		Timestamp timestamp = DateUtil.getSqlTimestamp();
		Map<String, Object> treeParam = 
				getTreeParamResult(new CommonTreeParam(BaseUtil.retStr(params.get("parentId")), "parentId", params));
		params.putAll(treeParam);
		if(BaseUtil.isEmpty(params.get("parentId")))
			params.put("parentId", treeParam.get(CommonTreeParam.ROOT_ID));
		if(BaseUtil.isEmpty(id)){
			id = StringUtil.getUuid(true);
			params.put("id", id);
			params.put("createUserId", params.get("userId"));
			params.put("createTime", DateUtil.getSqlTimestamp());
			params.put("editUserId", params.get("userId"));
			params.put("editTime", params.get("createTime"));
			int retVal = baseDictionaryInfoDao.insert(params);
			if(retVal == 0) {
				throw new BizException(BaseBizExceptionCode.DICTIONARY_MANAGE_SAVE_ERROR,
						"保存base_dictionary_info失败");
			}
		}else{//修改
			Map<String, Object> dnMap = baseDictionaryInfoDao.findById(id);
			dnMap.putAll(params);
			dnMap.put("editUserId", params.get("userId"));
			dnMap.put("editTime", timestamp);
			int retVal = baseDictionaryInfoDao.update(dnMap);
			// 更新操作是否成功
			if(0 == retVal)
				throw new BizException(BaseBizExceptionCode.DICTIONARY_MANAGE_UPDATE_ERROR,
						"修改base_dictionary_info失败");
		}
		JsonResult jsonResult = new JsonResult();
		return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
	}
	
	@Transactional
	@Override
	public Map<String, Object> logicDelete(List<String> ids) {
		Map<String, Object> result = new HashMap<>(); 
		int count = 0;
		for (String id : ids) {
			if(!BaseUtil.isEmpty(id)) {
				Map<String, Object> tpn = this.findById(id);
				int retVal = baseDictionaryInfoDao.logicDelete(tpn);
				if(retVal == 0)
					throw new BizException(BaseBizExceptionCode.DICTIONARY_MANAGE_DELETE_ERROR,
							"删除base_dictionary_info失败");
				count += retVal;
			}
		}
		result.put("count", count);
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public PageBean listPageSelf(Map<String, Object> params, Integer pageNum, Integer pageSize) {
		PageBean page = baseDictionaryInfoDao.listPage(params, pageNum, pageSize);
		return page;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Map<String, Object>> listAll(Map<String, Object> params) {
		List<Map<String, Object>> list = baseDictionaryInfoDao.getList("listBy", params);
		return list;
	}

	@Override
	public Map<String, Object> test(Map<String, Object> params) {
		CommonTreeParam ctp = new CommonTreeParam(BaseUtil.retStr(params.get("parentId")), "parentId");
		Map<String, Object> treeParam = 
				getTreeParamResult(ctp);
		String parentIds = BaseUtil.retStr(treeParam.get("parentIds"));
		Integer levels = BaseUtil.retInt(treeParam.get("levels"));
		return treeParam;
	}
	
	@Override
	public List<Map<String, Object>> test2(Map<String, Object> params) {
		return getChildrenById(new CommonTreeParam(BaseUtil.retStr(params.get("parentId")), "parentId", BaseUtil.retInt(params.get("levels")), "levels"));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Map<String, Object>> dictionaryTree(Map<String, Object> params) {
		return getChildrenById(new CommonTreeParam(BaseUtil.retIntNull(params.get("levels"))));
	}
    
}