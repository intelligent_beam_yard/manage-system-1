package com.psk.hms.service.service.impl;

import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.dao.LogOperatorDao;
import com.psk.hms.service.service.LogOperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/******
 * 功能日志service接口实现类
 * @author xiangjz
 * @date 2018年5月16日
 * @version 1.0
 *
 */
@SuppressWarnings("rawtypes")
@Service("logOperatorService")
public class LogOperatorServiceImpl extends BaseServiceImpl implements LogOperatorService {
	@Autowired
	private LogOperatorDao logOperatorDao;

	@Override
	protected BaseDao getDao() {
		return logOperatorDao;
	}

	@Transactional
	public void save(Map<String, Object> params) {
		Timestamp now = DateUtil.getSqlTimestamp();
		
		Map<String, Object> logOperatorMap = new HashMap<String, Object>();
		logOperatorMap.put("id", StringUtil.getUuid(true));
		logOperatorMap.put("createTime", now);
		logOperatorMap.put("createUserId", params.get(Constant.CURRENT_USER_ID));
		logOperatorMap.put("editTime", now);
		logOperatorMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
		logOperatorMap.put("isDelete", Constant.IS_DELETE_NO);
		logOperatorMap.put("remarks", params.get("remarks"));
		logOperatorMap.put("controllerStartTime", DateUtil.getSqlTimestamp((long)params.get("controllerStartTime")));
		logOperatorMap.put("controllerEndTime", DateUtil.getSqlTimestamp((long)params.get("controllerEndTime")));
		logOperatorMap.put("controllerProcessingTime", params.get("controllerProcessingTime"));
		logOperatorMap.put("cookie", params.get("cookie"));
		logOperatorMap.put("ip", params.get("ip"));
		logOperatorMap.put("method", params.get("method"));
		logOperatorMap.put("referer", params.get("referer"));
		logOperatorMap.put("requestPath", params.get("requestPath"));
		logOperatorMap.put("userAgent", params.get("userAgent"));
		logOperatorMap.put("operatorId", params.get("operatorId"));
		logOperatorMap.put("accept", params.get("accept"));
		logOperatorMap.put("acceptEncoding", params.get("acceptEncoding"));
		logOperatorMap.put("acceptLanguage", params.get("acceptLanguage"));
		logOperatorMap.put("connection", params.get("connection"));
		logOperatorMap.put("host", params.get("host"));
		logOperatorMap.put("xrequestWith", params.get("xrequestWith"));
		logOperatorMap.put("userId", params.get("userId"));
		
		try {
			getDao().insert(logOperatorMap);
		} catch (Exception e) {
			throw new BizException(BaseBizExceptionCode.LOGOPERATE_MANAGE_SAVE_ERROR, "保存失败");
		}
	}

	
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public void delete(Map<String, Object> params){
		List<String> idsArr = (List<String>) params.get("ids");
		
		Timestamp now = DateUtil.getSqlTimestamp();
		
		for (String systemId : idsArr) {
			//删除
			Map<String, Object> systemInfoMap = (Map<String, Object>) getDao().findById(systemId);
			if(systemInfoMap == null)
				continue;
			
			systemInfoMap.put("editTime", now);
			systemInfoMap.put("editUserId", params.get(Constant.CURRENT_USER_ID));
			systemInfoMap.put("isDelete", Constant.IS_DELETE_YES);
			try {
				logOperatorDao.update(systemInfoMap);
			} catch (Exception e) {
				throw new BizException(BaseBizExceptionCode.LOGOPERATE_MANAGE_DELETE_ERROR, "日志删除失败");
			}
		}
	}
}
