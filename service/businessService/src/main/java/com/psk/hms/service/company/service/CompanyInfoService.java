package com.psk.hms.service.company.service;

import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.core.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * CompanyInfoService
 * 
 * @author jiangr
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
public interface CompanyInfoService extends BaseService<Map<String, Object>> {
    
    /**
	 * 添加、修改
	 * @param params
	 * @return
	 */
	public JsonResult addOrUpdate(Map<String, Object> params);

	
	/**
	 * 逻辑删除[<b>批量</b>]
	 * 
	 * @param ids
	 * @return
	 */
	Map<String,Object> logicDelete(List<String> ids);
	
	/**
	 * 分页查询所有
	 * @param params
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	PageBean<Map<String, Object>> listPage(Map<String, Object> params, Integer pageNum, Integer pageSize);
	
	/**
	 * 查询所有
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> listAll(Map<String, Object> params);
	
	/**
	 * 查询所有
	 * @param params
	 * @return
	 */
	List<Map<String, Object>> listAllCompany(String sqlId, Map<String, Object> params);
}