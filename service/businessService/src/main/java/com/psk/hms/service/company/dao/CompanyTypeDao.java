package com.psk.hms.service.company.dao;

import com.psk.hms.core.dao.BaseDao;

import java.util.Map;

/**
 * CompanyTypeDao
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-22
 * @version 1.0.0
 */
public interface CompanyTypeDao extends BaseDao {
    /**
	 * 逻辑删除
	 * @param params
	 * @return
	 */
	public int logicDelete(Map<String, Object> params);
}