package com.psk.hms.service.base.dao.impl;

import com.psk.hms.core.dao.impl.BaseDaoImpl;
import com.psk.hms.service.base.dao.BaseOperatorPackageInfoDao;
import org.springframework.stereotype.Repository;


@Repository("baseOperatorPackageInfoDao")
public class BaseOperatorPackageInfoDaoImpl extends BaseDaoImpl implements BaseOperatorPackageInfoDao {

}
