package com.psk.hms.service.base.service.impl;

import com.psk.hms.base.entity.CommonTreeParam;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.exception.company.CompanyBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.core.dao.BaseDao;
import com.psk.hms.core.service.impl.BaseServiceImpl;
import com.psk.hms.service.base.dao.BaseMenuInfoDao;
import com.psk.hms.service.base.dao.BaseRoleInfoDao;
import com.psk.hms.service.base.service.BaseMenuInfoService;
import com.psk.hms.service.company.dao.CompanyEmployeeDao;
import com.psk.hms.service.company.dao.CompanyInfoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @link BaseMenuInfoServiceImpl 实现类
 *
 * @author xiangjz
 * @editBy
 * @date 2018-08-20
 * @version 1.0.0
 */
@Service
public class BaseMenuInfoServiceImpl extends BaseServiceImpl<Map<String, Object>> implements BaseMenuInfoService {

    @Autowired
    private BaseMenuInfoDao baseMenuInfoDao;
    @Autowired
    private BaseRoleInfoDao baseRoleInfoDao;
    @Autowired
    private CompanyEmployeeDao companyEmployeeDao;
    @Autowired
    private CompanyInfoDao companyInfoDao;

    @Override
    protected BaseDao getDao() {
        return baseMenuInfoDao;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public JsonResult addOrUpdate(Map<String,Object> params) {
        //查询是否有重复数据，不允许code或者name重复
        Map<String, Object> validCondition = new HashMap<String, Object>();
        validCondition.put("name", params.get("name"));
        validCondition.put("notId", params.get("id"));
        List<Map<String, Object>> validList = baseMenuInfoDao.listBy(validCondition);
        if(!BaseUtil.isEmpty(validList))
            throw new BizException(BaseBizExceptionCode.MENU_MANAGE_ERROR,
                    "数据重复，不允许有相同name的菜单");

        validCondition.clear();
        validCondition.put("operatorId", params.get("operatorId"));
        validCondition.put("notId", params.get("id"));
        validList = baseMenuInfoDao.listBy(validCondition);
        if(!BaseUtil.isEmpty(validList))
            throw new BizException(BaseBizExceptionCode.MENU_MANAGE_ERROR,
                    "数据重复，不允许绑定相同的权限");

        //获取记录id值
        String id = BaseUtil.retStr(params.get("id"));
        //获取当前时间
        Timestamp timestamp = DateUtil.getSqlTimestamp();
        Map<String, Object> treeParam =
                getTreeParamResult(new CommonTreeParam(BaseUtil.retStr(params.get("parentId")), "parentId", params));
        params.putAll(treeParam);
        if(BaseUtil.isEmpty(params.get("parentId"))){
            params.put("parentId", treeParam.get(CommonTreeParam.ROOT_ID));
            params.put("parentIds", treeParam.get(CommonTreeParam.ROOT_ID));
        }

        if(BaseUtil.isEmpty(id)){
            id = StringUtil.getUuid(true);
            params.put("id", id);
            params.put("createUserId", params.get("userId"));
            params.put("createTime", timestamp);
            params.put("editUserId", params.get("userId"));
            params.put("editTime", params.get("createTime"));

            int retVal = baseMenuInfoDao.insert(params);
            if(retVal == 0) {
                throw new BizException(BaseBizExceptionCode.MENU_MANAGE_SAVE_ERROR,
                        "保存base_menu_info失败");
            }
        }else{//修改
            Map<String, Object> dnMap = baseMenuInfoDao.findById(id);
            dnMap.putAll(params);
            dnMap.put("editUserId", params.get("userId"));
            dnMap.put("editTime", timestamp);
            int retVal = baseMenuInfoDao.update(dnMap);
            // 更新操作是否成功
            if(0 == retVal)
                throw new BizException(BaseBizExceptionCode.MENU_MANAGE_UPDATE_ERROR,
                        "修改base_menu_info失败");
        }
        JsonResult jsonResult = new JsonResult();
        return jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG, id);
    }

    @Transactional
    @Override
    public Map<String, Object> logicDelete(List<String> ids) {
        Map<String, Object> result = new HashMap<>();
        int count = 0;
        for (String id : ids) {
            if(!BaseUtil.isEmpty(id)) {
                Map<String, Object> tpn = this.findById(id);
                int retVal = baseMenuInfoDao.logicDelete(tpn);
                if(retVal == 0)
                    throw new BizException(BaseBizExceptionCode.MENU_MANAGE_DELETE_ERROR,
                            "删除base_menu_info失败");
                count += retVal;
            }
        }
        result.put("count", count);
        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public PageBean listPageSelf(Map<String, Object> params, Integer pageNum, Integer pageSize) {
        PageBean page = baseMenuInfoDao.listPage(params, pageNum, pageSize);
        return page;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> listAll(Map<String, Object> params) {
        List<Map<String, Object>> list = baseMenuInfoDao.getList("listBy", params);
        return list;
    }

    /*@SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> menuTree(Map<String, Object> params) {
        *//**
         * 看是获取本单位的菜单树，还是其它单位的菜单树
         *//*
        boolean isSelf = (boolean) params.get("isSelf");
        CommonTreeParam commonTreeParam = new CommonTreeParam();
        Map<String, Object> sqlParam = new HashMap<String, Object>();
        String companyType = getCompanyType(BaseUtil.retStr(params.get("companyId")));
        if(isSelf) {
            commonTreeParam.setSqlId("listByOperator");

            sqlParam.put("orderByClause", " order by menu.sort asc ");
            String employeeId = BaseUtil.retStr(params.get("employeeId"));
            sqlParam.put("employeeId", employeeId);
            sqlParam.put("companyType", companyType);
            List<Map<String, Object>> roleList = null;
            if(!BaseUtil.isEmpty(employeeId)) {
                //通过employeeId找到对应的所有通用的角色baseRoleInfo
                Map<String, Object> baseRoleInfoMap = new HashMap<String, Object>();
                baseRoleInfoMap.put("id", employeeId);
                roleList = baseRoleInfoDao.getList("listByEmployeeId", baseRoleInfoMap);
            }
            sqlParam.put("roleList", roleList);
            if(!BaseUtil.isEmpty(employeeId) || !BaseUtil.isEmpty(companyType)) {
                sqlParam.put("validOperatorCondition", true);
            }
        } else {
            commonTreeParam.setSqlId("listByOperatorDataAuth");

            sqlParam.put("orderByClause", " order by menu.sort asc ");

            List<Map<String, Object>> dataAuthList = new ArrayList<Map<String, Object>>();
            //获取单位类型
            Map<String, Object> dataAuth = new HashMap<String, Object>();
            //如果切换到的不是登录者所在单位，则需要单独查询companyType
            dataAuth.put("operatorCode", companyType);
            dataAuth.put("dataAuthType", CommonEnumType.DataAuthType.单位类型.getValue());
            dataAuthList.add(dataAuth);
            dataAuth = new HashMap<String, Object>();
            String companyId = BaseUtil.retStr(params.get("companyId"));
            dataAuth.put("operatorCode", companyId);
            dataAuth.put("dataAuthType", CommonEnumType.DataAuthType.单位.getValue());
            dataAuthList.add(dataAuth);
            sqlParam.put("dataAuthList", dataAuthList);
        }
        commonTreeParam.setSqlParam(sqlParam);
        commonTreeParam.setLevels(BaseUtil.retIntNull(params.get("levels")));
        List<Map<String, Object>> listTree = getChildrenById(commonTreeParam);
        return listTree;
    }*/

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> menuTree(Map<String, Object> params) {
        /**
         * 看是获取本单位的菜单树，还是其它单位的菜单树
         */
        CommonTreeParam commonTreeParam = new CommonTreeParam();
        Map<String, Object> sqlParam = new HashMap<String, Object>();
        String companyType = getCompanyType(BaseUtil.retStr(params.get("companyId")));
        commonTreeParam.setSqlId("listByOperator");

        sqlParam.put("orderByClause", " order by menu.sort asc ");
        String employeeId = BaseUtil.retStr(params.get("employeeId"));
        sqlParam.put("employeeId", employeeId);
        sqlParam.put("companyType", companyType);
        List<Map<String, Object>> roleList = null;
        if(!BaseUtil.isEmpty(employeeId)) {
            //通过employeeId找到对应的所有通用的角色baseRoleInfo
            Map<String, Object> baseRoleInfoMap = new HashMap<String, Object>();
            baseRoleInfoMap.put("id", employeeId);
            roleList = baseRoleInfoDao.getList("listByEmployeeId", baseRoleInfoMap);
        }
        sqlParam.put("roleList", roleList);
        if(!BaseUtil.isEmpty(employeeId) || !BaseUtil.isEmpty(companyType)) {
            sqlParam.put("validOperatorCondition", true);
        }
        commonTreeParam.setSqlParam(sqlParam);
        commonTreeParam.setLevels(BaseUtil.retIntNull(params.get("levels")));
        List<Map<String, Object>> listTree = getChildrenById(commonTreeParam);
        return listTree;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public List<Map<String, Object>> menuTreeForAdmin(Map<String, Object> params) {
        params = BaseUtil.isEmpty(params) ? new HashMap<>() : params;
        /**
         * 看是获取本单位的菜单树，还是其它单位的菜单树
         */
        CommonTreeParam commonTreeParam = new CommonTreeParam();
        commonTreeParam.setSqlId("listForAdmin");
        Map<String, Object> sqlParam = new HashMap<String, Object>();
        sqlParam.put("orderByClause", " order by menu.sort asc ");
        commonTreeParam.setSqlParam(sqlParam);
        commonTreeParam.setLevels(BaseUtil.retIntNull(params.get("levels")));
        List<Map<String, Object>> listTree = getChildrenById(commonTreeParam);
        return listTree;
    }

    /**
     * 根据单位id查询单位类型
     * @param companyId
     * @return
     */
    private String getCompanyType(String companyId) {
        if(BaseUtil.isEmpty(companyId))
            throw new BizException(CompanyBizExceptionCode.COMPANY_EMPLOYEE_SELECT_ERROR,
                    "参数错误，单位id为空");
        Map<String, Object> companyInfo =
                companyInfoDao.findById(companyId);
        if(BaseUtil.isEmpty(companyInfo))
            throw new BizException(CompanyBizExceptionCode.COMPANY_INFO_MANAGE_ERROR,
                    "无法找到单位信息");
        String companyType = BaseUtil.retStr(companyInfo.get("companyType"));
        return companyType;
    }

}