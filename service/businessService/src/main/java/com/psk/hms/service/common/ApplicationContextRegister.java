package com.psk.hms.service.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * 这是用来在spring容器之外注入bean的类，可以用于强行注入因spring控制反转无法注入的类，比如坑爹的@ServerEndpoint
 * 我的websocket里面竟然无法注入bean  mmp
 * @author xiangjz
 *
 */
@Component 
@Lazy(false) 
public class ApplicationContextRegister implements ApplicationContextAware { 
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationContextRegister.class);   
	private static ApplicationContext APPLICATION_CONTEXT;   /**  * 设置spring上下文  *  * @param applicationContext spring上下文  * @throws BeansException  */
	
	@Override  
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException { 
		LOGGER.debug("ApplicationContext registed-->{}", applicationContext);  
		APPLICATION_CONTEXT = applicationContext;  
	} 
	public static ApplicationContext getApplicationContext() { 
		return APPLICATION_CONTEXT;  
	}
}

