package com.psk.hms.server.admin.api.controller.base;

import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.service.base.service.BaseMenuInfoService;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.token.util.TokenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * baseOperatorInfo控制器
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@RestController
@RequestMapping("/admin/api/base/baseOperatorInfo")
public class BaseOperatorInfoController extends BaseController {

    @Autowired
    private BaseOperatorInfoService baseOperatorInfoService;
    @Autowired
    private BaseMenuInfoService baseMenuInfoService;
    
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * 获取用户所有权限，菜单树
     * 
     * @param params
     * @return
     */
    @RequestMapping(value = "/listOperator", method = {RequestMethod.POST,RequestMethod.GET})
    public JsonResult listOperator(@RequestParam Map<String, Object> params, HttpServletRequest request){
    	params.putAll(TokenUtil.getCurrentTokenInfo(request));
    	JsonResult result = new JsonResult();
    	//获取companyId  看是否来自切换单位
    	String companyId = BaseUtil.retStr(params.get("companyId"));
    	String tokenCompanyId = TokenUtil.getCompanyId(request);
//    	String tokenCompanyId = "5210d0e412044c83bc3fb91d39bd8f4b";
    	boolean isSelf = false;
    	if(tokenCompanyId.equals(companyId))
    		isSelf = true;
    	else 
    		params.put("companyId", BaseUtil.isEmpty(companyId)?tokenCompanyId:companyId);
    	
		params.put("isSelf", isSelf);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//获取菜单树
		List<Map<String, Object>> menuTree = baseMenuInfoService.menuTree(params);
		resultMap.put("menu", menuTree);
//		//获取权限数据集合
//		List<Map<String, Object>> operatorList = baseOperatorInfoService.listByOperator(params);
//		resultMap.put("operator", operatorList);
		result.success(resultMap);
    	return result;
    }

	/**
	 * 根据条件查询单条记录，如果查出多条记录，直接报错
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/findByParam", method = {RequestMethod.POST,RequestMethod.GET})
	public JsonResult findByParam(@RequestParam Map<String, Object> params){
		JsonResult result = new JsonResult();
		Map<String, Object> resultMap = (Map<String, Object>) baseOperatorInfoService.findBy(params);
		result.success(resultMap);
		return result;
	}

	/**
	 * 条件查询所有
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/listAllBy", method = {RequestMethod.POST,RequestMethod.GET})
	public JsonResult listAllBy(@RequestParam Map<String, Object> params){
		JsonResult result = new JsonResult();
		List<Map<String, Object>> resultList = baseOperatorInfoService.listBy(params);
		result.success(resultList);
		return result;
	}
	
	/**
	 * 新增、修改读取表结构信息
	 * @param params
	 * @return
	 */
	/*@PostMapping("/addOrUpdate")
	public JsonResult addOrUpdate(@RequestBody Map<String ,Object> params, HttpServletRequest request){
		JsonResult jsonResult = new JsonResult();
		params.putAll(TokenUtil.getCurrentTokenInfo(request));
		baseOperatorInfoService.addOrUpdate(params);
		jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG);
		return jsonResult;
	}*/
	
	/**
	 * 删除团队读取表结构信息 [<b>软删除</b>]
	 * 
	 * @param params
	 * @return
	 */
	/*@SuppressWarnings("unchecked")
	@PostMapping("logicDelete")
	public JsonResult logicDelete(@RequestBody Map<String ,Object> params){
		// 参数校验
		Map<String, Object> validateResult = new ApiParameterValidator<>(params)
				.list("ids", String.class)
				.getResult();
		if (!validateResult.isEmpty()) {
			return new JsonResult().paramFailure(validateResult);
		}

		JsonResult result = new JsonResult();
		List<String> ids = (List<String>) params.get("ids");
		Map<String, Object> data = baseOperatorInfoService.logicDelete(ids);
		result.success(data);
		return result;
	}*/
    
}