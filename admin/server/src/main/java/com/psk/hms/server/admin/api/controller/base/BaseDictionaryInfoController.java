package com.psk.hms.server.admin.api.controller.base;

import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.validator.chain.ApiParameterValidator;
import com.psk.hms.service.base.service.BaseDictionaryInfoService;
import com.psk.hms.token.util.TokenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * baseDictionaryInfo控制器
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@RestController
@RequestMapping("/api/base/baseDictionaryInfo")
public class BaseDictionaryInfoController extends BaseController {

    @Autowired
    private BaseDictionaryInfoService baseDictionaryInfoService;
    
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * 测试1
     * 
     * @param params
     * @return
     */
    @RequestMapping(value = "/test", method = {RequestMethod.POST,RequestMethod.GET})
    public JsonResult test(@RequestParam Map<String, Object> params){
    	JsonResult result = new JsonResult();
		Map<String, Object> resultMap = baseDictionaryInfoService.test(params);
		result.success(resultMap);
    	return result;
    }
    
    /**
     * 测试2
     * 
     * @param params
     * @return
     */
    @RequestMapping(value = "/test2", method = {RequestMethod.POST,RequestMethod.GET})
    public JsonResult test2(@RequestParam Map<String, Object> params){
    	JsonResult result = new JsonResult();
		List<Map<String, Object>> resultMap = baseDictionaryInfoService.test2(params);
		result.success(resultMap);
    	return result;
    }
    
    /**
	 * 查询字典树
	 * 
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/dictionaryTree", method = {RequestMethod.POST,RequestMethod.GET})
	public JsonResult dictionaryTree(@RequestParam Map<String, Object> params){
		JsonResult result = new JsonResult();
		List<Map<String, Object>> dictionaryTree = baseDictionaryInfoService.dictionaryTree(params);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("dictionaryTree", dictionaryTree);
		result.success(resultMap);
		return result;
	}
    
    /**
	 * 查询所有读取表结构信息
	 * 
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/listAll", method = {RequestMethod.POST,RequestMethod.GET})
	public JsonResult listAll(@RequestParam Map<String, Object> params){
		JsonResult result = new JsonResult();
		List<Map<String, Object>> list = baseDictionaryInfoService.listAll(params);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		result.success(resultMap);
		return result;
	}

	/**
	 * 分页查询
	 * @param params
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "listPage", method = {RequestMethod.POST,RequestMethod.GET})
	public JsonResult listPage(@RequestBody Map<String, Object> params){
		// 参数校验
		Map<String, Object> validateResult = new ApiParameterValidator<>(params)
				.gte(Constant.PAGE_NUM, 1)
				.rangeClose(Constant.PAGE_SIZE, 1, 9999)
				.getResult();
		if (!validateResult.isEmpty()) {
			return new JsonResult().paramFailure(validateResult);
		}

		JsonResult jsonResult = new JsonResult();
		PageBean pageBean = baseDictionaryInfoService.listPage("listByForAdmin", params, BaseUtil.retInt(params.get(Constant.PAGE_NUM)), BaseUtil.retInt(params.get(Constant.PAGE_SIZE)));
		jsonResult.success(pageBean);
		return jsonResult;
	}
	
	/**
	 * 根据条件查询单条记录，如果查出多条记录，直接报错
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/findByParam", method = {RequestMethod.POST,RequestMethod.GET})
	public JsonResult findByParam(@RequestParam Map<String, Object> params){
		JsonResult result = new JsonResult();
		Map<String, Object> resultMap = baseDictionaryInfoService.findBy(params);
		result.success(resultMap);
		return result;
	}
	
	/**
	 * 新增、修改读取表结构信息
	 * @param params
	 * @return
	 */
	@PostMapping("/addOrUpdate")
	public JsonResult addOrUpdate(@RequestBody Map<String ,Object> params, HttpServletRequest request){
		JsonResult jsonResult = new JsonResult();
		params.putAll(TokenUtil.getCurrentTokenInfo(request));
		baseDictionaryInfoService.addOrUpdate(params);
		jsonResult.create(JsonResult.SUCCESS, JsonResult.SUCCESS_MSG);
		return jsonResult;
	}
	
	/**
	 * 删除团队读取表结构信息 [<b>软删除</b>]
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@PostMapping("logicDelete")
	public JsonResult logicDelete(@RequestBody Map<String ,Object> params){
		// 参数校验
		Map<String, Object> validateResult = new ApiParameterValidator<>(params)
				.list("ids", String.class)
				.getResult();
		if (!validateResult.isEmpty()) {
			return new JsonResult().paramFailure(validateResult);
		}

		JsonResult result = new JsonResult();
		List<String> ids = (List<String>) params.get("ids");
		Map<String, Object> data = baseDictionaryInfoService.logicDelete(ids);
		result.success(data);
		return result;
	}
    
}