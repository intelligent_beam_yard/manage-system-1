package com.psk.hms.server.admin.web.controller.index;

import com.psk.hms.server.admin.web.controller.common.WebController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
public class IndexController extends WebController {

    @RequestMapping("home")
    public ModelAndView index(HttpServletRequest request) {
        ModelAndView view = new ModelAndView("hms/layout/layout");
        return view;
    }
}
