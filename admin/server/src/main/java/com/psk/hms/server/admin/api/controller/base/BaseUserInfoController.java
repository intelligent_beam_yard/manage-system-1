package com.psk.hms.server.admin.api.controller.base;

import com.psk.hms.base.constant.base.BaseUserInfoConstant;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.entity.LayGridReturn;
import com.psk.hms.base.entity.PageBean;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.rsa.MD5Util;
import com.psk.hms.base.util.validator.chain.ApiParameterValidator;
import com.psk.hms.service.base.service.BaseUserInfoService;
import com.psk.hms.token.util.TokenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * baseUserInfo控制器
 *
 * @author xiangjz
 * @version 1.0.0
 * @editBy
 * @date 2018-08-19
 */
@RestController
@RequestMapping("/admin/api/base/baseUserInfo")
public class BaseUserInfoController extends BaseController {
    @Autowired
    private BaseUserInfoService baseUserInfoService;


    protected Log log = LogFactory.getLog(this.getClass());

    /**
     * 保存或更新
     *
     * @param params
     * @return
     */
    @RequestMapping("addOrUpdate")
    public JsonResult addOrUpdate(@RequestBody Map<String, Object> params, HttpServletRequest request) {
        // 参数校验
        Map<String, Object> validateResult = new ApiParameterValidator<>(params)
                .effectiveString(BaseUserInfoConstant.USER_NAME)
                .effectiveString(BaseUserInfoConstant.NICK_NAME)
                .effectiveString(BaseUserInfoConstant.PHONE)
                .numberStringIfExist(BaseUserInfoConstant.AGE)
                .getResult();
        if (!validateResult.isEmpty()) {
            return new JsonResult().paramFailure(validateResult);
        }

        JsonResult jsonResult = new JsonResult();
        params.putAll(TokenUtil.getCurrentTokenInfo(request));

        if (BaseUtil.isEmpty(params.get(BaseUserInfoConstant.PASSWORD)))
            params.put(BaseUserInfoConstant.PASSWORD, MD5Util.MD5Encode("123456"));

        params.put("employeeCompanyId", params.get("companyId"));
        baseUserInfoService.addOrUpdate(params);
        jsonResult.success();
        return jsonResult;
    }

    /**
     * 根据条件查询单条记录，如果查出多条记录，直接报错
     *
     * @param params
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @RequestMapping(value = "/findByParam", method = {RequestMethod.POST, RequestMethod.GET})
    public JsonResult findByParam(@RequestParam Map<String, Object> params) {
        JsonResult jsonResult = new JsonResult();
        Map<String, Object> result = baseUserInfoService.findByParam(params);
        result.remove("password");
        result.remove("salt");
        result.put("userId", result.get("id"));
        jsonResult.success(result);
        return jsonResult;
    }

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @RequestMapping(value = "listPage", method = {RequestMethod.POST, RequestMethod.GET})
    public LayGridReturn listPage(@RequestParam Map<String, Object> params) {
        // 参数校验
        Map<String, Object> validateResult = new ApiParameterValidator<>(params)
                .numberString(Constant.PAGE_NUM)
                .numberString(Constant.PAGE_SIZE)
                .getResult();
        if (!validateResult.isEmpty()) {
            return new LayGridReturn().failure(BaseUtil.retStr(validateResult.get("msg")));
        }
        return baseUserInfoService.listPageGridForAdmin(params);
    }

    /**
     * 逻辑删除
     *
     * @param params
     * @return
     */
    @PostMapping("logicDelete")
    public JsonResult logicDelete(@RequestBody Map<String, Object> params) {
        // 参数校验
        Map<String, Object> validateResult = new ApiParameterValidator<>(params)
                .list("ids", String.class)
                .getResult();
        if (!validateResult.isEmpty()) {
            return new JsonResult().paramFailure(validateResult);
        }

        JsonResult jsonResult = new JsonResult();
        Map<String, Object> data = baseUserInfoService.logicDelete(params);
        jsonResult.success(data);
        return jsonResult;
    }

    /**
     * 修改密码
     *
     * @param params
     * @return
     */
    @PostMapping("changePwd")
    public JsonResult changePwd(@RequestBody Map<String, Object> params) {
        JsonResult jsonResult = new JsonResult();
        baseUserInfoService.changePwd(params);
        jsonResult.success();
        return jsonResult;
    }

    /**
     * 重置密码
     *
     * @param params
     * @return
     */
    @PostMapping("resetPwd")
    public JsonResult resetPwd(@RequestBody Map<String, Object> params) {
        JsonResult jsonResult = new JsonResult();
        baseUserInfoService.resetPwd(params);
        jsonResult.success();
        return jsonResult;
    }

    /**
     * 导入
     *
     * @return
     * @throws IOException
     */
    @PostMapping("importExcel")
    public JsonResult importExcel(@RequestParam Map<String, Object> params, @RequestParam(value = "importFile", required = true) MultipartFile importFile, HttpServletRequest request) {
        JsonResult jsonResult = new JsonResult();
        try {
            params.putAll(TokenUtil.getCurrentTokenInfo(request));
            params.put("importFile", importFile);
            jsonResult = baseUserInfoService.importExcel(params);
        } catch (BizException e) {
            log.error("== 导入用户 exception:", e);
            jsonResult.create(e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("== 导入用户 exception:", e);
            jsonResult.failure();
        }
        return jsonResult;
    }

}
