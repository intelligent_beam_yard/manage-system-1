package com.psk.hms.server.admin.web.controller.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping(value = "/admin/web/login")
public class WebLoginController extends WebController {

    /**
     * 显示登录界面
     *
     * @return
     */
    @RequestMapping
    public ModelAndView login(HttpServletRequest req) {
        ModelAndView view = new ModelAndView();
        return setMVViewName(view, "hms/login/loginPage");
    }

}