package com.psk.hms.server.admin.api.controller.enums;

import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.validator.chain.ApiParameterValidator;
import com.psk.hms.service.common.enums.EnumsLoader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * 枚举控制器
 * 
 * @author xiangjz
 * @editBy 
 * @date 2018-08-19
 * @version 1.0.0
 */
@RestController
@RequestMapping("/admin/api/enums/systemEnums")
public class EnumsController extends BaseController {

    @Autowired
    private EnumsLoader enumsLoader;
    
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * 获取所有的枚举字典
     * 
     * @param params
     * @return
     */
    @RequestMapping(value = "/getEnumsByName", method = {RequestMethod.POST,RequestMethod.GET})
    public JsonResult getEnumsByName(@RequestParam Map<String, Object> params){
		// 校验参数
		Map<String, Object> validateResult = new ApiParameterValidator<String, Object>()
				.create(params)
				.effectiveString("enumName")
				.getResult();
		if(!validateResult.isEmpty()) {
			return new JsonResult().paramFailure(validateResult);
		}

    	JsonResult result = new JsonResult();
    	String enumName = BaseUtil.retStr(params.get("enumName"));
    	try {
    		List<Object> resultList = 
    				enumsLoader.getCommonEnumList(enumName);
    		result.success(resultList);
    	} catch (BizException e) {
    		log.error("== 获取"+enumName+"枚举字典 error:", e);
    		result.create(e.getCode(), e.getMsg());
    	} catch (Exception e) {
    		log.error("== 获取"+enumName+"枚举字典 error:", e);
    		result.failure();
    	}
    	return result;
    }
    
    /**
     * 获取所有的枚举字典
     * 
     * @param params
     * @return
     */
    @RequestMapping(value = "/getAllEnums", method = {RequestMethod.POST,RequestMethod.GET})
    public JsonResult getAllEnums(@RequestParam Map<String, Object> params){
    	JsonResult result = new JsonResult();
    	try {
    		List<Map<String, Object>> enums =
    				enumsLoader.getAllCommonEnumList();
    		result.success(enums);
    	} catch (BizException e) {
    		log.error("== 获取所有枚举字典 error:", e);
    		result.create(e.getCode(), e.getMsg());
    	} catch (Exception e) {
    		log.error("== 获取所有枚举字典 error:", e);
    		result.failure();
    	}
    	return result;
    }
    
}