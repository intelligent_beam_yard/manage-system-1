package com.psk.hms.server.admin.web.controller.common;

import com.psk.hms.base.util.BaseUtil;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


public class WebController {

    protected ModelAndView createSingleView(String path) {
        ModelAndView view = new ModelAndView();
        view.setViewName(path);
        return view;
    }

    protected ModelAndView createLayoutView(String path) {

        ModelAndView view = new ModelAndView("layout/layout");
        view.addObject("resource_path", "layout/resource.html");
        view.addObject("header_path", "layout/header.html");
        view.addObject("left_path", "layout/userLeft.html");
        view.addObject("content_path", path + ".html");
        return view;
    }

    protected ModelAndView createTreeLayoutView(String path, HttpServletRequest req) {
        ModelAndView view = new ModelAndView("layout/layout");
        view.addObject("resource_path", "layout/resource.html");
        view.addObject("header_path", "layout/header.html");
        view.addObject("left_path", BaseUtil.isEmpty(req.getSession().getAttribute("user")) ? "" : "layout/userLeft.html");
        view.addObject("content_path", path + ".html");
        view.addObject("pathInfo", req.getPathInfo());
        return view;
    }

    /**
     * 模板添加对象
     *
     * @param
     */
    public void addMVObject(ModelAndView mv, String key, Object value) {
        mv.addObject(key, BaseUtil.isEmpty(value) ? new Object() : value);
    }


    /**
     * 模板添加对象
     *
     * @param
     */
    public ModelAndView setMVViewName(ModelAndView mv, String viewName) {
        mv.setViewName(viewName);
        return mv;
    }

}
