package com.psk.hms.server.admin.config;

import com.psk.hms.redis.service.RedisService;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.base.service.BaseUserInfoService;
import com.psk.hms.service.service.LogOperatorService;
import com.psk.hms.token.constant.TokenConstant;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Resource;

/**
 * 网关配置相关
 * 需要什么拦截自行配置
 * @author xiangjz
 *
 */
@SuppressWarnings("deprecation")
@Configuration
public class PassportConfig extends WebMvcConfigurerAdapter {
	@Resource
	private BaseOperatorInfoService baseOperatorInfoService;
	@Resource
	private LogOperatorService logOperatorService;
	@Resource
	private BaseUserInfoService baseUserInfoService;
	@Resource
	private RedisService redisService;

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
				.allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
				//.allowedHeaders(TokenConstant.TOKEN)
				.exposedHeaders(TokenConstant.ACCESS_TOKEN, TokenConstant.AUTH_TOKEN)
				.allowCredentials(true).maxAge(3600);
	}
	
	@Override	
	public void addInterceptors(InterceptorRegistry registry) {
//		registry.addInterceptor(new GlobalInterceptor(baseOperatorInfoService, logOperatorService)).addPathPatterns("/**");
//		registry.addInterceptor(new TokenInterceptor(baseOperatorInfoService, redisService, logOperatorService)).addPathPatterns("/**");
//		registry.addInterceptor(new PermissionInterceptor(baseOperatorInfoService, baseUserInfoService, logOperatorService)).addPathPatterns("/**");
        super.addInterceptors(registry);
    } 
	
}