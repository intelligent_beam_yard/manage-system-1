package com.psk.hms.server.admin.api.controller.common;

import com.alibaba.fastjson.JSONObject;
import com.psk.hms.base.constant.common.Constant;
import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.enums.CommonEnumType;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.validator.chain.ApiParameterValidator;
import com.psk.hms.redis.service.RedisService;
import com.psk.hms.service.base.service.BaseUserInfoService;
import com.psk.hms.service.base.service.BaseUserLoginService;
import com.psk.hms.service.company.service.CompanyEmployeeService;
import com.psk.hms.service.company.service.CompanyInfoService;
import com.psk.hms.service.company.service.CompanyTypeService;
import com.psk.hms.token.constant.TokenConstant;
import com.psk.hms.token.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述: 登录controller
 * 作者: xiangjz
 * 时间: 2017年4月8日
 * 版本: 1.0
 *
 */
@RestController
@RequestMapping("/admin/api/login")
public class ApiLoginController extends BaseController {
	
	@Autowired
	private BaseUserInfoService baseUserInfoService;
	@Autowired
	private BaseUserLoginService baseUserLoginService;
	@Autowired
	private CompanyInfoService companyInfoService;
	@Autowired
	private CompanyEmployeeService companyEmployeeService;
	@Autowired
	private CompanyTypeService companyTypeService;
	@Autowired
	private RedisService redis;
	
	/**
	 * 登录
	 * @throws IOException 
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping("login")
	public JsonResult login(@RequestBody Map<String ,Object> params) {
		JsonResult jsonResult = new JsonResult();
        // 校验参数
        Map<String, Object> validateResult = new ApiParameterValidator<String, Object>()
                .create(params)
                .effectiveString("userName")
                .effectiveString("password")
                .getResult();
        if(!validateResult.isEmpty()) {
            return new JsonResult().paramFailure(validateResult);
        }

        Map<String, Object> result = baseUserLoginService.adminLogin(params);
        String userId = BaseUtil.retStr(result.get(Constant.ID));
        //查询当前单位的员工
        params = new HashMap<String, Object>();
        params.put("userId", userId);
        params.put("isCurrentCompany", CommonEnumType.YesOrNo.是.getValue());
        //获取单位信息
        Map<String, Object> currentEmployee = companyEmployeeService.findBy(params);
        Map<String, Object> company =
                companyInfoService.findById(BaseUtil.retStr(currentEmployee.get("companyId")));
        JSONObject json = new JSONObject();
        json.put("userId", userId);
        if(!BaseUtil.isEmpty(currentEmployee)) {
            json.put("employeeId", currentEmployee.get("id"));
            json.put("companyId", currentEmployee.get("companyId"));
        }
        String accessToken = JwtUtils.createJWT(json.toString(), "登录access-token", TokenConstant.JWT_MAX_TTL_MILLISECOND);

		//放入缓存
		redis.set(TokenConstant.ACCESS_TOKEN+"_"+accessToken, accessToken, TokenConstant.JWT_MAX_TTL_SECOND);

		Map<String, Object> data = new HashMap<>();
		data.put(TokenConstant.ACCESS_TOKEN, accessToken);

		//用户map
		Map<String, Object> user = (Map<String, Object>) baseUserInfoService.findById(userId);
		user.remove("password");
		user.remove("salt");
		user.remove("secretKey");
		user.remove("createTime");
		user.remove("editTime");
		user.remove("isDelete");
		user.remove("state");
		user.remove("version");
		user.remove("errorCount");
		user.put("userId", user.get("id"));
		user.remove("id");
		user.put("employeeId", currentEmployee.get("id"));
		user.put("projectId", company.get("projectId"));
		data.put("user", user);
		//单位map
		Map<String, Object> companyMap = new HashMap<String, Object>();

		if(!BaseUtil.isEmpty(company)) {
			companyMap.put("companyId", company.get("id"));
			companyMap.put("companyName", company.get("companyName"));
			companyMap.put("projectId", company.get("projectId"));
		}

		//查询单位类型
		Map<String, Object> companyTypeCondition = new HashMap<String, Object>();
		companyTypeCondition.put("code", company.get("companyType"));
		Map<String, Object> companyTypeMap = companyTypeService.findBy(companyTypeCondition);
		if(!BaseUtil.isEmpty(companyTypeMap)) {
			companyMap.put("companyType", companyTypeMap.get("code"));
			companyMap.put("companyTypeName", companyTypeMap.get("name"));
		}
		data.put("company", companyMap);

		//成功
		jsonResult.success(data);
		return jsonResult;
	}

	/**
	 * 注销
	 */
	@PostMapping("logout")
	public JsonResult logout(@RequestBody Map<String, Object> params) {
		// 参数校验
		Map<String, Object> validateResult = new ApiParameterValidator<>(params)
				.effectiveString(TokenConstant.ACCESS_TOKEN, "token必须")
				.getResult();
		if (!validateResult.isEmpty()) {
			return new JsonResult().paramFailure(validateResult);
		}
		String key = (String) params.get(TokenConstant.ACCESS_TOKEN);
		redis.remove(key);
		return new JsonResult().success();
	}
}
