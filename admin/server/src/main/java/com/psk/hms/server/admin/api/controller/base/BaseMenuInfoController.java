package com.psk.hms.server.admin.api.controller.base;

import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.validator.chain.ApiParameterValidator;
import com.psk.hms.service.base.service.BaseMenuInfoService;
import com.psk.hms.token.util.TokenUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * baseMenuInfo控制器
 *
 * @author xiangjz
 * @version 1.0.0
 * @editBy
 * @date 2018-08-20
 */
@RestController
@RequestMapping("/admin/api/base/baseMenuInfo")
public class BaseMenuInfoController extends BaseController {

    @Autowired
    private BaseMenuInfoService baseMenuInfoService;

    protected Log log = LogFactory.getLog(this.getClass());

    /**
     * 查询菜单树
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "menuTree", method = {RequestMethod.POST, RequestMethod.GET})
    public JsonResult menuTree(@RequestParam Map<String, Object> params) {
        JsonResult result = new JsonResult();
        List<Map<String, Object>> menuTree = baseMenuInfoService.menuTreeForAdmin(params);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("menuTree", menuTree);
        result.success(resultMap);
        return result;
    }


    @PostMapping(value = "menuTreeForAdmin")
    public List<Map<String, Object>> getMenuLayerTree(@RequestParam(required = false) Map<String, Object> params) {
        return baseMenuInfoService.menuTreeForAdmin(params);
    }

    /**
     * 根据条件查询单条记录，如果查出多条记录，直接报错
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/findByParam", method = {RequestMethod.POST, RequestMethod.GET})
    public JsonResult findByParam(@RequestParam Map<String, Object> params) {
        JsonResult result = new JsonResult();
        Map<String, Object> resultMap = baseMenuInfoService.findBy(params);
        result.success(resultMap);
        return result;
    }

    /**
     * 查询所有读取表结构信息
     *
     * @param params
     * @return
     */
    @RequestMapping(value = "/listAll", method = {RequestMethod.POST, RequestMethod.GET})
    public JsonResult listAll(@RequestParam Map<String, Object> params) {
        JsonResult result = new JsonResult();
        List<Map<String, Object>> list = baseMenuInfoService.listAll(params);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("list", list);
        result.success(resultMap);
        return result;
    }

    /**
     * 新增/修改
     *
     * @param params
     * @return
     */
    @RequestMapping("/addOrUpdate")
    public JsonResult addOrUpdate(@RequestBody Map<String, Object> params, HttpServletRequest request) {
        JsonResult jsonResult = new JsonResult();
        params.putAll(TokenUtil.getCurrentTokenInfo(request));
        baseMenuInfoService.addOrUpdate(params);
        return jsonResult.success();
    }

    /**
     * 删除团队读取表结构信息 [<b>软删除</b>]
     *
     * @param params
     * @return
     */
    @SuppressWarnings("unchecked")
    @PostMapping("logicDelete")
    public JsonResult logicDelete(@RequestBody Map<String, Object> params) {
        // 参数校验
        Map<String, Object> validateResult = new ApiParameterValidator<>(params)
                .list("ids", String.class)
                .getResult();
        if (!validateResult.isEmpty()) {
            return new JsonResult().paramFailure(validateResult);
        }

        JsonResult result = new JsonResult();
        List<String> ids = (List<String>) params.get("ids");
        Map<String, Object> data = baseMenuInfoService.logicDelete(ids);
        result.success(data);
        return result;
    }

}