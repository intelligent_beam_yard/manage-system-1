package com.psk.hms.server.admin.web.controller.base;

import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.server.admin.web.controller.common.WebController;
import com.psk.hms.service.base.service.BaseMenuInfoService;
import com.psk.hms.service.base.service.BaseUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/admin/web/base/baseUserInfo")
public class WebBaseUserInfoController extends WebController {

    @Autowired
    private BaseUserInfoService baseUserInfoService;

    @RequestMapping(value = "pageList")
    public ModelAndView pageList(HttpServletRequest req, @RequestParam Map<String, Object> params) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("hms/BaseUserInfo/BaseUserInfoList");
        return mv;
    }

    @RequestMapping(value = "pageEdit")
    public ModelAndView pageEdit(@RequestParam Map<String, Object> params) {
        /*// 参数校验
        Map<String, Object> validateResult = new ApiParameterValidator<>(params)
                .list("id", String.class)
                .getResult();
        if (!validateResult.isEmpty()) {
            return new JsonResult().paramFailure(validateResult);
        }*/

        ModelAndView mv = new ModelAndView();
        Map<String, Object> entity = baseUserInfoService.findById(BaseUtil.retStr(params.get("id")));
        mv.setViewName("hms/BaseUserInfo/BaseUserInfoEdit");
        mv.addObject("baseUserInfo", BaseUtil.isEmpty(entity) ? new HashMap<>() : entity);
        mv.addObject("isCheckOnly", params.get("isCheckOnly"));
        return mv;
    }
}