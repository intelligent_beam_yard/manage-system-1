package com.psk.hms.server.admin.init;

import com.psk.hms.redis.service.RedisService;
import com.psk.hms.service.common.enums.EnumsLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;



/**
 */
@Component  
@Order(value=2) 
public class InitRunner implements ApplicationRunner{
	
	@Autowired
	private RedisService redisService;
	
	@Autowired
	private EnumsLoader enumsLoader;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		enumsLoader.loadCodeDictionary();
	}

   

}
