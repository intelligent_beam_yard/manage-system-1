var isLoaded;
var hasBusinessRole;
var hasRole;
var departId;
var user;

//打开我的页面
function showMine(){
	Utils.createAjaxWindow({
		title : '账户信息',
		url : Utils.getRootPath() + "/userInfo/mine",
		btnArray : ['保存']
	});
}

function showIframePageContent(url, preHandleUrl, menuName, menuId){
	if($(".header-seperation").css('display')=='none'){
		$("#ssss").click();
	}
	if(!Utils.isEmpty(preHandleUrl)){
		/*var loading = layer.load(1);
		$.ajax({
    		type: 'post',
    		url: Utils.getRootPath()+"/"+preHandleUrl,
    		success: function(data, textStatus){
				layer.close(loading);
    			if(data.success){
    				url += '&userEntrpCode='+userEntrpCode+'&userName='+userName+'&userEntrpLevel='+userEntrpLevel+'&checkUserTypeCode='+checkUserTypeCode+'&personId='+userId+'&isBranchComp='+isBranchComp;
                		var iframe;
                      iframe = document.createElement('iframe');
                      iframe.setAttribute('src',url);
                      iframe.setAttribute('width','100%');
                      iframe.setAttribute('height','700');
                	  $('#searchListId').html(iframe);
    			}else{
    				Utils.showToastFail(data.msg);
    			}
    		},
    		error: function(){
				layer.close(loading);
    			Utils.showToastFail(loadErrorMsg);
    		}
    	});*/
	}else{
	  var iframe;
      iframe = document.createElement('iframe');
      iframe.setAttribute('src',url);
      iframe.setAttribute('width','100%');
      iframe.setAttribute('height','630');
      $(iframe).css('border','none');
      /* $('#page-content').html(iframe);*/
	  $('#tab-ul').find('li').removeClass('active');
	  //判断该菜单是否已存在，如果不存在tab，直接append，否则跳转到该tab并刷新iframe
	  var showId = 'menuId_'+menuId;
	  var closeId = "'"+showId+"'";
	  var aTagId = "'"+'a_tag_'+showId+"'";
	  if(!document.getElementById(showId)){
		  $('#tab-ul').append(
			  '<li id="'+showId+'" class="active">'+
				  '<a onclick="targetToTabPage('+aTagId+')" href="a_tag_'+showId+'" role="tab" data-toggle="tab">'+
					  menuName+
					  '&nbsp<i onclick="closeTab('+closeId+')" class="layui-icon layui-unselect layui-tab-close">ဆ</i>'+
				  '</a>'+
			  '</li>'
		  )
		  $('#tab-content').children('div').removeClass('active');
		  $('#tab-content').append(
			  '<div class="tab-pane active" id="a_tag_'+showId+'">'+
			  		iframe.outerHTML+
			  '</div>'
		  )
	  }else{
		  $('#'+showId).addClass('active');
		  //然后刷新对应的iframe
		  $('#tab-content').children('div').removeClass('active');
		  $('#a_tag_'+showId).addClass('active');
		  $('#a_tag_'+showId).find('iframe').get(0).contentWindow.location.reload(true);
	  }
	}
}

//点击tab标签切换到指定tab页
function targetToTabPage(id){
	$('#tab-content').children('div').removeClass('active');
	$('#'+id).addClass('active');
}

//点击叉叉关闭tab
function closeTab(showId){
	Utils.preventPopAndDefault(event);
	//先判断当前关闭tab是否是active，如果是，则指定该tab的前一个tab显示，如果前面没有了，就后面一个，如果都没有就不管了
	if($('#'+showId).hasClass('active')){
		if(!Utils.isEmpty($('#'+showId).prev('li').get(0))){
			$('#'+showId).prev().addClass('active');
			var prev_id = $('#'+showId).prev().attr('id');
			$('#a_tag_'+prev_id).addClass('active');
		}else{
			if($('#'+showId).after().get(0)){
				$('#'+showId).after().addClass('active');
				var prev_id = $('#'+showId).after().attr('id');
				$('#a_tag_'+prev_id).addClass('active');
			}
		}
	}
	$('#'+showId).remove();
	$('#a_tag_'+showId).remove();
}

function reloadIframe(){
	$('#tab-content').find('.active').find('iframe').get(0).contentWindow.location.reload(true);
}



function getMenuTreeData(param){
	var selector = param.selector;
	var data = param.data;

	var url = api.operator.menuOperator;
	httpClient.postData(url, {}, {
		onSuccess: function(d) {
			console.log(d.menu);
		}
	})

//	var index = loading();
	/*$.ajax({
		url: Utils.getRootPath() + "/menu/getAllMenuTree", 
		type : 'post',
		dataType: 'json',
		async:false,
		data : data,
		success: function(d){
//			Utils.closeLayerWindow(index);
			if(d.success){
				selector.empty();
				loadMenuTree(d.obj, selector)
			}else{
//				Utils.showToastFail(d.msg);
			}
		},
		error: function(d){
//			Utils.closeLayerWindow(index);
			Utils.showToastFail(d.msg);
		}
	});*/
	isLoaded = true;

}

function loadMenuTree(menus, selector, showIcon){
	$.each(menus, function(index,obj){
            var url = "javascript:;";
            if(!Utils.isEmpty(obj.url)){
                url = Utils.getRootPath();
                if(!Utils.isEmpty(obj.url) && obj.url.indexOf('/') != 0)
                    url += '/';
                url += obj.url;
                //本来审核审批申请的初审核定人员菜单跟复核复核和复核核定人员菜单一样的，但是实际业务流程初审核定跟初审经办的菜单名一样，所以这里手动改
                url = "javascript:showIframePageContent('"+url+"',null,'"+obj.name+"','"+obj.id+"')";
            }
            //如果有子菜单，就拼一个壳，如果没有，就拼菜单
            if(!Utils.isEmpty(obj.children) && obj.children.length > 0){
                //本来审核审批申请的初审核定人员菜单跟复核复核和复核核定人员菜单一样的，但是实际业务流程初审核定跟初审经办的菜单名一样，所以这里手动改
                var menu =  '<li>';
                if(showIcon)
                    menu += 	'<a id="ssss" href="javascript:;"> <i class="'+obj.icon+'"></i>&nbsp&nbsp <span class="title">'+obj.name+'</span> <span class=" arrow"></span> </a>';
                else
                    menu += 	'<a id="ssss" href="javascript:;">  <span class="title">'+obj.name+'</span> <span class=" arrow"></span> </a>';
					menu += 		'<ul class="sub-menu" id="menuTree_'+obj.id+'">'+
									'</ul>'+
							'</li>'
                selector.append(menu)
                var nextSelector = $('#menuTree_'+obj.id);
                loadMenuTree(obj.children, nextSelector);
            }else{
                if(showIcon)
                    selector.append(
                        '<li><a href="'+url+'"> <i class="'+obj.icon+'"></i>&nbsp&nbsp <span class="title">'+obj.name+'</span> </a></li>'
                    )
                else
                    selector.append(
                        '<li> <a href="'+url+'"> '+obj.name+' </a> </li>'
                    )
            }
	});
}

//重新加载主菜单
function reloadMenu(){
	getMenuTreeData({
		selector : $('#menuUl'),
		data : {departId : departId, hasRole : hasRole, hasBusinessRole : hasBusinessRole}
	})
	$.Webarch.init();
}

//注销
function logOut(){
	Utils.showConfirm('注销登录？', function(){
		Utils.setCookie('user', '');
		Utils.setCookie('websocketStatue','fail');
		window.location.href=Utils.getRootPath()+"/login/logOut";
	});
}

//上传头像或者企业logo
function uploadProfile(){
	var upload = layui.upload;
	   
	  //执行实例
	  var uploadInst = upload.render({
	    elem: '#test1' //绑定元素
	    ,url: Utils.getRootPath()+"/attachmentInfo/doAddAttachment" //上传接口
	    ,data:{objectType:0,attachType:0,objectId:1}
	    ,done: function(res){
	      //上传完毕回调
	    }
	    ,error: function(){
	      //请求异常回调
	    }
	  });
	
	var loading = layer.load(1);
	$.ajaxFileUpload({
		url : Utils.getRootPath()+"/attachmentInfo/doAddAttachment",
		secureuri : false,
		fileElementId : $(thiz).attr('id'),//对应input标签的ID，用于获取input中的文件
		type:"post",
		dataType:'json',
		data:{objectType:0,attachType:0,objectId:1},
		success : function(data) {
			layer.close(loading);
			$(thiz).val('');
			isTest = false;
			if(data.success){
				alldate();
			}else{
				Utils.showToastFail(data.msg);
			}
		},
		error:function(d){
			isTest = false;
			layer.close(loading);
			$(thiz).val('');
			Utils.showToastFail('网络错误');
		}
	});
}