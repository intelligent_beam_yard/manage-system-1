var menus;
var selector;
$(function() {
	selector = $('#treegrid');
	getMenuTreeData(selector);
})

function getMenuTreeData(selector){
	Utils.treegrid({
		url : api.menu.menuTree,
		fields : [ [{
			field : 'name',
			title : '菜单名称',
			width:'20%'
		}, {
			field : 'url',
			title : '跳转地址',
			width:'20%'
		},{
			field : 'remark',
			title : '备注',
			width:'30%',
		},{
			field : 'icon',
			title : '图标',
			width:'10%',
			align : "center",
			formatter : function(v){
				return '<i class="'+v+'"></i>';
			}
		},{
			field : 'opt',
			title : '操作',
			width:'20%',
			align : "center",
			formatter : function(value, row, index) {
				var href = '';
     			if(!row || !row.id){
     				return;
     			}
     			var id = "'"+row.id+"'";
     			// 修改按钮
     			href += '<a class="layui-btn layui-btn-xs" javascript=":;" onclick="edit('+id+')" lay-event="edit">编辑</a>&nbsp';
				// 删除按钮
				href += '<a class="layui-btn layui-btn-danger layui-btn-xs " javascript=":;" onclick="delInLogic('+id+')" lay-event="del">删除</a>';
     			return href;
			}
		} ] ]
	})

}

//新增页面
function add() {
	FrameUtil.createCommonWindow({
		title : '新增信息', 
		url : web.menu.editPage,
        onSuccess : function(){
//			getMenuTreeData($('#nestable'));
			selector.treegrid('reload');
		}
	});
}

//编辑页面
function edit(id) {
    FrameUtil.createCommonWindow({
		title : '编辑信息', 
		url : web.menu.editPage+"?id=" + id,
        onSuccess : function(){
//			getMenuTreeData($('#nestable'));
			selector.treegrid('reload');
		}
	});
}

//提交删除
function delInLogic(id) {
    FrameUtil.showConfirm('确定删除？', function(){
		httpClient.postData(api.menu.delete, {
			ids : [id]
		}, {
            notReloadLayerTable : true,
            onSuccess : function(d){
                selector.treegrid('reload');
            }
		})
	});
}