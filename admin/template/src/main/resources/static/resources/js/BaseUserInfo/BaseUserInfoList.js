$(function () {
    $('#searchForm').find('select').bind("change", function () {
        loadDateGrid();
    })
    $('#searchForm').find('input').bind("input", function () {
        loadDateGrid();
    })

    //加载数据网格
    loadDateGrid();
})

function loadDateGrid() {
    Utils.layerDatagrid({
            //这里是datagrid参数
            url: api.baseUserInfo.listPage,
            fields: [
                /*{field: 'id', title: 'ID', width: '5%', sort: true, fixed: 'left'}*/
                {
                    field: 'state', title: '是否可用', width: '10%', align: 'center'
                    , templet: function (data) {
                        if (!Utils.isEmpty(data.state)) {
                            /*var usableName = data.state == 1 ? '禁用' : '启动';
                            if (data.state == 1)
                                return '<a class="layui-btn layui-btn-danger layui-btn-xs" javascript=":;" onclick="forbidden(' + data.id + ', 0)">禁用</a>&nbsp';
                            else
                                return '<a class="layui-btn layui-btn-xs" javascript=":;" onclick="forbidden(' + data.id + ', 1)">启用</a>&nbsp';*/

                            return enums.getNameByValue("YesOrNo", data.state);
                        }
                    }
                }
                , {field: 'userName', title: '用户名', width: '10%'}
                , {field: 'nickName', title: '姓名', width: '10%', sort: true}
                , {field: 'email', title: '邮箱', width: '12%', sort: true}
                , {field: 'phone', title: '手机号', width: '12%'}
                , {
                    field: 'sex', title: '性别', width: '10%'
                    , templet: function (data) {
                        return enums.getNameByValue("Gender", data.sex);
                    }
                }
                , {field: 'idCard', title: '身份证', width: '20%'}
                , {field: 'age', title: '年龄', width: '10%'}
                , {fixed: 'right', width: 200, align: 'left', toolbar: '#toolbar'} // 这里的toolbar值是模板元素的选择器
            ]
            ,
            requestParam: getRequestParam()
        },
        //这里是tool工具栏方法
        {
            edit: edit,
            del: delInLogic,
            detail: detail
        },
        //这里是tool工具栏其它附加方法
        [
            detail //这里只是举个例子，传function数组
        ]);
}

//新增页面
function add() {
    FrameUtil.createCommonWindow({
        title: '新增用户',
        url: web.baseUserInfo.editPage
    });
}

//编辑页面
function edit(data) {
    var id = data.id;
    FrameUtil.createCommonWindow({
        title: '编辑用户',
        url: web.baseUserInfo.editPage + "?id=" + id
    });
}

//查询页面
function detail(data) {
    var id = data.id;
    FrameUtil.createWindowWithoutBtn({
        title: '用户查询',
        url: web.baseUserInfo.editPage + "?id=" + id + "&isCheckOnly=true"
    });
}

//提交逻辑删除
function delInLogic(data) {
    var id = data.id;
    httpClient.postData(api.baseUserInfo.delete, {
        ids: [id]
    })
}

//获取请求查询条件参数
function getRequestParam() {
    return Utils.serializeObject($("#searchForm").ajaxForm());
}

/*
//修改用户是否可用状态
function forbidden(id, usable) {
    httpClient.postData(Utils.getRootPath() + "/userInfo/setUsable", {
        id: id,
        usable: usable
    });
}*/
