var isLoaded;
$(function(){
	var user = storage.getItem("user");
	var company = storage.getItem("company");
	if(!Utils.isEmpty(user) && !Utils.isEmpty(company)){
        $("userNickName").val(user.nickName);
        $("companyName").val(company.companyName);
	}

	$("[data-toggle='tooltip']").tooltip();

	//渲染菜单
	isLoaded = $('#isLoaded').val();
	/**
	 * 该js异步请求菜单并渲染的方法只允许，在加载主页面的时候，执行一次
	 */
	if(!Utils.isEmpty(isLoaded)){
		//加载左侧菜单
		getMenuTreeData({
			selector : $('#menuUl')
		})
		$('#isLoaded').val('');
	}
})

function getMenuTreeData(param){
	var selector = param.selector;
	var data = param.data;

	var url = api.operator.menuOperator;
	httpClient.postData(url, {}, {
		onSuccess: function(d) {
			console.log(d.data.menu);
            selector.empty();
            loadMenuTree(d.data.menu, selector);
            $.Webarch.init();
		}
	})

//	var index = loading();
	/*$.ajax({
		url: Utils.getRootPath() + "/menu/getAllMenuTree", 
		type : 'post',
		dataType: 'json',
		async:false,
		data : data,
		success: function(d){
//			Utils.closeLayerWindow(index);
			if(d.success){
				selector.empty();
				loadMenuTree(d.obj, selector)
			}else{
//				Utils.showToastFail(d.msg);
			}
		},
		error: function(d){
//			Utils.closeLayerWindow(index);
			Utils.showToastFail(d.msg);
		}
	});*/
	isLoaded = true;

}