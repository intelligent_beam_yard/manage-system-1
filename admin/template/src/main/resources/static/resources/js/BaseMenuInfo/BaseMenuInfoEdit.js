$(function() {
	
	$.fn.combotree.defaults.loadFilter = $.fn.tree.defaults.loadFilter; //这里要加上，否则combotree不会生效
	
	if(!Utils.isEmpty(isCheckOnly)){
		//查看详情页面，将所有空间置为disabled
		$('input').attr('disabled','disabled');
		$('select').attr('disabled','disabled');
		$('textarea').attr('disabled','disabled');
	}
	
	var parentId = $("#parentId").val();
	
	Utils.combotree({
		url : api.menu.menuTree,
		selfId : $("#id").val(),
		parentDom : $("#parentId")
	})

    loadOperators($('#operatorId'));
})

/**
 * 加载缓存选项
 * @param $dom
 */
function loadOperators($dom) {
    var applyToEnumValue = '1'; //admin菜单
    var operatorId = $('#operatorIdValue').val();
    httpClient.getData(api.operator.listAllBy, {
        applyTo: applyToEnumValue
    },{
        onSuccess : function(d){
            //存储后端枚举相关
            $dom.empty();
            $.each(d.data, function(index,obj){
                if(obj.id == operatorId){
                    $dom.append(
                        "<option value='"+obj.id+"' selected>"+obj.name+"</option>"
                    )
                }else{
                    $dom.append(
                        "<option value='"+obj.id+"'>"+obj.name+"</option>"
                    )
                }
            })
            $dom.select2();
        }
    });
}

