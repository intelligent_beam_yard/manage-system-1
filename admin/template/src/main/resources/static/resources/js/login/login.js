$(function(){
	if(!Utils.isEmpty(Utils.getCookie('userName'))){
		$('#rememberMe').attr("checked","true");
		$('#login_username').val(Utils.getCookie('userName'));
	}
	
	$('input').blur(function(){
		$(this).css('color', '#000');
	})
	
	$('input').focus(function(){
		$(this).css('color', '#000');
	})
	//解决session失效后，在iframe中重定向到登录页而不是最顶层iframe的问题
	if (window != top) 
		top.location.href = location.href; 
})

function keyLogin(){  
    if (event.keyCode==13)   //回车键的键值为13  
         document.getElementById("userLogin").click(); //调用登录按钮的登录事件  
}

$(function(){
	
	
	$('#userLogin').click(function(){
//		Utils.preventPopAndDefault(event);
		userLogin();
	});
	$('#toLog').click(function(){
		add();
	});
	$('#resetPassword').click(function(){
		resetPassword();
	});
	
})


//注册页面
function add() {
	Utils.createAjaxWindow({
		title : '重庆市两江新区建设行业企业资质审批（告知承诺）管理系统', 
		url : Utils.getRootPath() + "/cngz/register",
		btnArray : ['提交','关闭']
	});
	
}

//忘记密码页面
function resetPassword() {
	//第二种弹出框
	Utils.createAjaxWindow({
		title : '重庆市两江新区建设行业企业资质审批（告知承诺）管理系统', 
		url : Utils.getRootPath() + "/cngz/resetPassword",
		btnArray : ['保存','关闭']
	});
	
}


function userLogin(){
	var url = api.login.login;
	httpClient.postData(url, {
		userName: $('#login_username').val(),
		password: $.md5($('#login_pass').val())
	},{
		onSuccess : function(d){
			//存储登录凭证相关
			storage.setItem("access-token", d.data['access-token']);
			storage.setItem("company", d.data.company);
			storage.setItem("user", d.data.user);

			//存储后端枚举相关
            loadEnums();
		}
	});
}

function loadEnums(){
    var url = api.enums.allEnums;
    httpClient.postData(url, {},{
        onSuccess : function(d){
            //存储后端枚举相关
			var enumDataList = d.data;
			var enumStorage = {};
            for (var i = 0; i < enumDataList.length; i++) {
                var enumData = enumDataList[i];
                enumStorage[enumData.enumName] = enumData.enumValues;
            }
            storage.setItem("enums", enumStorage);
            window.location.href = Utils.getRootPath()+"/home";
        }
    });
}