/**
 * 工具脚本
 */
var httpClient = function() {
	return {
        /**
         * post提交数据
         * @param url
         * @param param
         * @param otherParam
         */
        postData : function(url, param, otherParam){
            var type = 'post';
            var dataType = 'json';
            if(!otherParam) {
                otherParam = {};
            }
            this.ajaxHandler({
                url: url,
                param: param,
                otherParam: otherParam,
                type: type,
                dataType: dataType,
                onSuccess: otherParam.onSuccess,
                onFailure: otherParam.onFailure,
                parentIndex: otherParam.parentIndex,
                datagridSelector: otherParam.datagridSelector ? otherParam.datagridSelector : 'datagrid',
                reloadTableGrid: param.reloadTableGrid ? param.reloadTableGrid : true
            })
        },
        /**
         * get提交数据
         * @param url
         * @param param
         * @param otherParam
         */
        getData : function(url, param, otherParam){
            var type = 'get';
            var dataType = 'json';
            if(!otherParam) {
                otherParam = {};
            }
            this.ajaxHandler({
                url: url,
                param: param,
                otherParam: otherParam,
                type: type,
                dataType: dataType,
                onSuccess: otherParam.onSuccess,
                onFailure: otherParam.onFailure,
                parentIndex: otherParam.parentIndex,
                datagridSelector: otherParam.datagridSelector ? otherParam.datagridSelector : 'datagrid',
                reloadTableGrid: param.reloadTableGrid ? param.reloadTableGrid : true
            })
        },

        ajaxHandler: function(param){
            var index = FrameUtil.loading();
            var ajaxOption = {
                url: param.url,
                type : param.type,
                dataType: param.dataType,
                headers: {
                    'access-token': storage.getItem("access-token")
                },
                success: function(d){
                    FrameUtil.closeLayerWindow(index);
                    if(d.code == Code.success){
                        if(!Utils.isEmpty(param.otherParam)){
                            if(param.reloadTableGrid)
                                reloadLayerTable(param.datagridSelector);
                            if(!Utils.isEmpty(param.onSuccess))
                                param.onSuccess(d);
                        }
                    } else if(d.code == Code.paramsFailure){
                        var msgJson = d.data;
                        if(!Utils.isEmpty(msgJson)){
                            var msg = "";
                            Object.keys(msgJson).forEach(function(key){
                                msg += msgJson[key] + ', ';
                            });
                            if(msg.length > 0)
                                msg = msg.substring(0, msg.lastIndexOf(","));

                            showbsMsg(msg, 'error', true, {parentIndex : param.parentIndex});
                        }
                    } else {
                        if(!Utils.isEmpty(param.onFailure))
                            param.onFailure(d);
                        showbsMsg(d.msg, 'error', true, {parentIndex : param.parentIndex});
                    }
                },
                error: function(result){
                    FrameUtil.closeLayerWindow(index);
                    if(!Utils.isEmpty(param.onFailure))
                        param.onFailure(result.responseJSON);
                    showbsMsg(result.responseJSON.msg, 'error');
                }
            }
            if(param.type.toLowerCase() == 'post') {
                ajaxOption['contentType'] = "application/json;charset=UTF-8";
                ajaxOption['data'] = JSON.stringify(param.param);
            } else {
                ajaxOption['data'] = param.param;
            }
            $.ajax(ajaxOption);
        },

        multipartAjaxSubmit : function (form,arg) {
            var index1 = parent.layer.load(1);
            form.ajaxSubmit({
                type: "POST",
                url:form.attr('action'),
                data:{'arg':arg},
                dataType: "json",
                success: function(result){
                    if(result.success){
                        parent.$('#tt').datagrid('reload');
                        layer.msg(result.msg, {icon: 1});
                        parent.layer.close(index1);// 如果设定了yes回调，需进行手工关闭
                        var index = parent.layer.getFrameIndex(window.name); // 先得到当前iframe层的索引
                        parent.layer.close(index);
                    }else{
                        parent.layer.close(index1);
                        layer.msg(result.msg, {icon: 2});
                    }
                },
                error: function(result){
                    layer.msg(result.responseJSON.msg, {icon: 2});
                }
            });
        },
        /**
         * 公共文件（图片）上传方法
         */
        commonImgUploader : function (dom, url) {
            var index1 = layer.load(1);
            $(dom).parent('form').ajaxSubmit({
                type: "POST",
                url:url,
                dataType: "json",
                success: function(result){
                    if(result.success){
                        console.log(result.obj);
                        $(dom).parent('form').parent().find('img[up="img"]').attr('src',result.obj);
                        layer.msg("成功", {icon: 1});
                        layer.close(index1);
                    }else{
                        layer.close(index1);
                        layer.msg(result.msg, {icon: 2});
                    }
                },
                error: function(result){
                    layer.close(index1);
                    layer.msg(result.responseJSON.msg, {icon: 2});
                }
            });
        }
	};
}();