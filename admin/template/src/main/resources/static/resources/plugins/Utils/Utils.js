//统一监听登录session失效重定向
$.ajaxSetup({
    type: 'POST',
    contentType: "application/x-www-form-urlencoded;charset=utf-8",
    complete: function (xhr, status) {
        var sessionStatus = xhr.getResponseHeader('sessionstatus');
        if (sessionStatus == 'timeout') {
            // var top = getTopWinow();
            // var yes = confirm('由于您长时间没有操作, session已过期, 请重新登录.');
            // if (yes) {
            alert('登录失效,请重新登录！');
            window.parent.location.href = Utils.getRootPath() + '/home';
            // }
        }
    }
});

var totalFontAndBgc = 'background-color:#f0ffff;color:black;font-weight:bold;';


var loadErrorMsg = '网络错误';

$(function () {
    // 将所有标有dicType和dicValue的控件加载数据字典数据
    var dicInputs = document.getElementsByTagName('select');
    for (var i = 0; i < dicInputs.length; i++) {

        if ($(dicInputs[i]).attr('dicType')) {
            var dicType = $(dicInputs[i]).attr('dicType');
            var dicId = $(dicInputs[i]).attr('dicValue');
            getSelectedDicListByTableName($(dicInputs[i]), dicType, dicId);
        } else if ($(dicInputs[i]).attr('enumType')) {
            var enumName = $(dicInputs[i]).attr('enumType');
            var enumId = $(dicInputs[i]).attr('enumValue');
            var enumEmpty = $(dicInputs[i]).attr('enumEmpty');
            var enumFilter = $(dicInputs[i]).attr('enumFilter');
            // 调用枚举字典加载数据方法
            Utils.getSelectedEnumListByEnumName({
                dom: $(dicInputs[i]),
                enumName: enumName,
                enumId: enumId,
                enumEmpty: enumEmpty,
                enumFilter: enumFilter
            });
        }
    }
})

var mylocalStorage = window.localStorage;

/**
 * 工具脚本
 */
var Utils = function () {
    return {
        /**
         * @requires jQuery
         *
         * 将form表单元素的值序列化成对象
         *
         * @returns object
         */
        serializeObject: function (form) {
            var o = {};
            $.each(form.serializeArray(), function (index) {
                var val = this['value'];
                //解决字符串传入后台参数date类型不匹配的问题
                if (!Utils.isEmpty(val)) {
                    if (Utils.isStrDate(val))
                        val = Utils.stringToDate(val);
                }
                if (o[this['name']]) {
                    o[this['name']] = o[this['name']] + "," + val;
                } else {
                    o[this['name']] = val;
                }
            });
            return o;
        },
        // 字符串转日期
        stringToDate: function (fDate) {
            var fullDate = fDate.split("-");
            return new Date(fullDate[0], fullDate[1] - 1, fullDate[2], 0, 0, 0);
        },
        // 验证字符串是否是日期格式
        isStrDate: function (dateString) {
            if (dateString.trim() == "") return true;
            var r = dateString.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
            if (r == null) {
                return false;
            }
            var d = new Date(r[1], r[3] - 1, r[4]);
            var num = (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4]);
            if (num == 0) {
            }
            return (num != 0);
        },
        //指定时间增加m个自然月
        addMoth: function (date, m) {
            var ds = date.split('-'), _d = ds[2] - 0;
            var nextM = new Date(ds[0], ds[1] + m, 0);
            var max = nextM.getDate();
            d = new Date(ds[0], ds[1] - 1 + m, _d > max ? max : _d - 1);
            return d.toLocaleDateString().match(/\d+/g).join('-');
        },
        getDaysByDateString: function (startDate, endDate) {
            var diffDate = endDate - startDate + 1 * 24 * 60 * 60 * 1000;
            var days = diffDate / (1 * 24 * 60 * 60 * 1000);
            return Math.floor(days);
        },
        /**
         * 设置cookie * name 标识 * value 值 * options { * 'path': '访问路径', * 'domain' :
		 * '域名', * 'expire' : 过期时间 }
         */
        setCookie: function (name, value, options) {
            var options = options ? options : {},
                path = options.path ? options.path : '/',
                domain = options.domain ? options.domain : document.domain,
                time = options.expire ? (new Date().getTime() + options.expire * 1000) : '',
                expire = new Date(time).toUTCString();
            document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + ";expires=" + expire +
                ";domain=" + domain + ";path=" + path;
        },
        // 获取cookie
        getCookie: function (name) {
            var arr,
                reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg)) {
                return unescape(arr[2]);
            }
            return null;

        },
        // 移除cookie
        removeCookie: function (name) {
            var val = this.getCookie(name);
            if (val != null) {
                this.setCookie(name, val, {
                    expire: -1
                })
            }
        },

        /**
         * 阻止冒泡和默认事件
         */
        preventPopAndDefault: function (e) {
            e.stopPropagation();
            e.preventDefault();
        },

        /**
         * 获取两个时间点之差（自然月+天） type = '1';
         * 如果返回月数，则整数部分按照号数相等节点计算，相隔几个月算几个月，小数部分看号数多余部分，多余部分除以30 type = '0';
         * 如果返回天数，直接返回自然天数
         */
        getDateLast: function (startdate, enddate, type) {
            if (typeof startdate == 'string') {
                startdate = new Date(startdate.replace(/-/g, "\/"));// 时间转换
            } else {
                startdate = new Date(startdate);// 时间转换
            }
            if (typeof enddate == 'string') {
                enddate = new Date(enddate.replace(/-/g, "\/"));// 时间转换
            } else {
                enddate = new Date(enddate);// 时间转换
            }
            if (type == '0') {
                var mil = Math.abs(Date.parse(startdate) - Date.parse(enddate));
                return isNaN(mil) ? 0 : (mil / 1000 / 60 / 60 / 24) + 1;
            } else if (type == '1') {
                var month = 0;
                var day = 0;
                if (enddate.getDate() >= startdate.getDate()) {
                    month = (enddate.getFullYear() - startdate.getFullYear()) * 12
                        + enddate.getMonth() - startdate.getMonth();
                    day = enddate.getDate() - startdate.getDate();
                } else {
                    var startDateTemp = new Date(startdate);
                    var endDateTemp = new Date(enddate);
                    if (startdate.getDate() == getlastDay(startDateTemp)) {
                        if (enddate.getDate() == getlastDay(endDateTemp)) {
                            month = (enddate.getFullYear() - startdate.getFullYear()) * 12
                                + enddate.getMonth() - startdate.getMonth();
                            day = 0;
                        } else {
                            month = (enddate.getFullYear() - startdate.getFullYear()) * 12
                                + enddate.getMonth() - startdate.getMonth() - 1;
                            var lastmonthday = getlastmonthDat(endDateTemp);
                            day = lastmonthday - startdate.getDate() + enddate.getDate();
                        }
                    } else {
                        if (enddate.getDate() == getlastDay(endDateTemp)) {
                            month = (enddate.getFullYear() - startdate.getFullYear()) * 12
                                + enddate.getMonth() - startdate.getMonth();
                            day = 0;
                        } else {
                            month = (enddate.getFullYear() - startdate.getFullYear()) * 12
                                + enddate.getMonth() - startdate.getMonth() - 1;

                            var lastmonthday = getlastmonthDat(endDateTemp);
                            day = lastmonthday - startdate.getDate() + enddate.getDate();

                        }
                    }
                }
                return (month + (day / 30)).toFixed(2);
            } else {
                return 0;
            }
        },
        /**
         * 加载layui时间控件
         */
        getLayerDate: function (option) {
            var laydate = layui.laydate;
            // 执行一个laydate实例
            option.show = true;
            laydate.render(option);
        },
        /**
         * 获取两个时间之差
         */
        compareDate: function (date1, date2) {
            if (typeof date1 == 'string') {
                date1 = new Date(date1.replace(/-/g, "\/"));// 时间转换
            } else {
                date1 = new Date(date1);// 时间转换
            }
            if (typeof date2 == 'string') {
                date2 = new Date(date2.replace(/-/g, "\/"));// 时间转换
            } else {
                date2 = new Date(date2);// 时间转换
            }
            return Date.parse(date1) - Date.parse(date2);
        },
        /**
         * 获取两个时间之差并根据值返回文字信息
         */
        getTimeDiffStr: function (date1, date2) {
            var dd = date1;
            if (typeof date1 == 'string') {
                date1 = new Date(date1.replace(/-/g, "\/"));// 时间转换
            } else {
                date1 = new Date(date1);// 时间转换
            }
            if (typeof date2 == 'string') {
                date2 = new Date(date2.replace(/-/g, "\/"));// 时间转换
            } else {
                date2 = new Date(date2);// 时间转换
            }
            var timeDiff = Math.abs(date1 - date2);
            var retStr = '';
            if (timeDiff / 1000 <= 60) {
                retStr = '1分钟前';
            } else if (timeDiff / (1000 * 60 * 60) < 1) {
                retStr = Math.round(timeDiff / (1000 * 60)) + '分钟前';
            } else if (timeDiff / (1000 * 60 * 60 * 24) < 1) {
                retStr = Math.round(timeDiff / (1000 * 60 * 60)) + '小时前';
            } else if (timeDiff / (1000 * 60 * 60 * 24 * 30) < 1) {
                retStr = "约" + Math.round(timeDiff / (1000 * 60 * 60 * 24)) + '天前';
            } else if (timeDiff / (1000 * 60 * 60 * 24 * 365) < 1) {
                retStr = "约" + Math.round(timeDiff / (1000 * 60 * 60 * 24 * 30)) + '个月前';
            } else if (timeDiff / (1000 * 60 * 60 * 24 * 365) >= 1) {
                retStr = "约" + Math.round(timeDiff / (1000 * 60 * 60 * 24 * 365)) + '年前';
            } else {
                retStr = dd;
            }
            return retStr;
        },
        /**
         * 统一刷新数据网格
         */
        reloadLayerTable: function (selector) {
            layui.use('table', function () {
                var table = layui.table;
                var selector = isEmpty(selector) ? 'datagrid' : selector;
                table.reload(selector, {});
            });
        },
        /**
         * 统一上传附件
         */
        uploadFile: function (baseParam) {
            var elem = baseParam.elem; // 指定点击的target
            var url = baseParam.url; // 上传请求url
            var data = baseParam.data; // 参数
            var accept = baseParam.accept; // 附件类型
            // 可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
            var exts = baseParam.exts; // 后缀限制 你设置 exts: 'zip|rar|7z'
            // 即代表只允许上传压缩格式的文件。如果 accept
            // 未设定，那么限制的就是图片的文件格式
            var size = baseParam.size; // 文件大小，单位为kb 默认不限制
            var multiple = baseParam.multiple; // 是否多文件上传
            var number = baseParam.number; // 多文件上传时的数量
            var choose = baseParam.choose; // 选择文件时的回调
            var before = baseParam.before; // 上传前的回调
            var done = baseParam.done; // 请求成功的回调
            var error = baseParam.error; // 请求异常的回调
            layui.use('upload', function () {
                var upload = layui.upload;
                var uploadInst = upload.render({
                    elem: elem // 绑定元素
                    , url: url // 上传接口
                    , data: data
                    , accept: accept
                    , exts: exts
                    , size: size
                    , multiple: multiple
                    , number: number
                    , choose: function (data) {
                        if (!isEmpty(choose))
                            choose(data);
                    }
                    , before: function (data) {
                        if (!isEmpty(before))
                            before(data);
                    }
                    , done: function (data) {
                        if (!isEmpty(done))
                            done(data);
                    }
                    , error: function (data) {
                        if (!isEmpty(error))
                            error(data);
                    }
                });
            });
        },
        /**
         * 统一加载数据网格
         * @param baseParam
         * @param funcParam
         * @param otherFuncParamList 其它方法数组
         */
        layerDatagrid: function (baseParam, funcParam, otherFuncParamList) {
            var selector = isEmpty(baseParam.selector) ? 'datagrid' : baseParam.selector;
            var url = baseParam.url;
            var type = baseParam.type ? baseParam.type : 'post';
            var fields = baseParam.fields;
            var height = isEmpty(baseParam.height) ? 500 : baseParam.height;
            var requestParam = baseParam.requestParam;
            var page = isEmpty(baseParam.page) ? {
                theme: '#22262e'  // 数据网格背景色
            } : baseParam.page;
            var done = baseParam.done;
            layui.use('table', function () {
                var table = layui.table;

                table.render({
                    elem: '#' + selector
                    , height: height
                    // ,even: true //各行换色
                    , url: url // 数据接口
                    , method: type
                    ,request: {
                        pageName: 'pageNum' //页码的参数名称，默认：page
                        ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
                    }
                    , page: page // 开启分页
                    , where: requestParam
                    , cols: [fields]  // 字段
                    , loading: true // 加载转圈圈
                    , text: '数据加载中...' // 加载转圈圈
                    , done: function (res, curr, count) {
                        if (Utils.isEmpty(res.code != 200)) {
                            $('#' + selector).next('div').find('.layui-none').text(res.msg);
                        } else if (Utils.isEmpty(res.data)) {
                            $('#' + selector).next('div').find('.layui-none').text('暂无数据');
                        }
                        if (!isEmpty(done))
                            done(res);
                    }
                });

                // 监听工具条
                table.on('tool(' + selector + ')', function (obj) { // 注：tool是工具条事件名，test是table原始容器的属性
                    // lay-filter="对应的值"
                    var data = obj.data; // 获得当前行数据
                    var layEvent = obj.event; // 获得 lay-event 对应的值（也可以是表头的
                    // event
                    // 参数对应的值）
                    var tr = obj.tr; // 获得当前行 tr 的DOM对象
                    if (layEvent === 'edit') { // 编辑
                        funcParam.edit(data);
                    } else if (layEvent === 'del') { // 删除
                        layer.confirm('确定删除？', {icon: 3, title: '提示'}, function (index) {
                            // obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                            // del(data.id); //真删除
                            funcParam.del(data);
                            FrameUtil.closeLayerWindow(index);
                        });
                    }
                    else if (layEvent === 'checkOut') { // 删除
                        layer.confirm('确定注销？', {icon: 3, title: '提示'}, function (index) {
                            // obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                            // del(data.id); //真删除
                            funcParam.checkOut(data);
                            FrameUtil.closeLayerWindow(index);
                        });
                    } else if (layEvent === 'detail') { // 查看
                        funcParam.detail(data);
                    } else {
                        if(otherFuncParamList){
                            for (var func in otherFuncParamList) {
                                func(data);
                            }
                        }
                    }
                });
            });
        },
        /**
         * 统一加载tree
         */
        treegrid: function (baseParam) {
            var selector = isEmpty(baseParam.selector) ? $('#treegrid') : baseParam.selector;
            var url = baseParam.url;
            var fields = baseParam.fields;
// var height = isEmpty(baseParam.height)?455:baseParam.height;
            var requestParam = Utils.isEmpty(baseParam.requestParam) ? {} : baseParam.requestParam;
            var onDrop = baseParam.onDrop;
            var onLoadSuccess = Utils.isEmpty(baseParam.onLoadSuccess) ? function (data) {
                selector.treegrid("clearSelections");
            } : baseParam.onLoadSuccess;
            selector.treegrid({
                idField: 'id',
                treeField: 'name',
                url: url,
                method: 'post',
                queryParams: requestParam,
                loadMsg: '数据加载中,请稍候...',
                rownumbers: true,
                singleSelect: true,
                animate: true,
                columns: fields,
                onLoadSuccess: onLoadSuccess,
                onDrop: onDrop,
                onBeforeSelect: function (row) {
                    var selectedRow = selector.treegrid("getSelected");
                    if (!selectedRow) {
                        return true;
                    }
                    if (row.id == selectedRow.id) {
                        selector.treegrid("clearSelections");
                        return false;
                    }
                }
            });
        },
        /**
         * 统一加载combotree
         */
        combotree: function (baseParam) {
            var selector = isEmpty(baseParam.selector) ? $('#combotree') : baseParam.selector;
            var url = baseParam.url;
// var fields = baseParam.fields;
            var width = isEmpty(baseParam.width) ? 397 : baseParam.width;
            var height = isEmpty(baseParam.height) ? 32 : baseParam.height;
            var selfId = isEmpty(baseParam.selfId) ? '' : baseParam.selfId;
            var parentDom = isEmpty(baseParam.parentDom) ? '' : baseParam.parentDom;
            var requestParam = baseParam.requestParam;
            selector.combotree({
                url: url,
                idField: 'id',
                textField: 'name',
                method: 'post',
                checkbox: true,
                width: 397,
                height: 32,
                onSelect: function (d) {
                    if (d) {
                        if (d.id == selfId) {
                            FrameUtil.showToastFail('不能选择自己为父级元素');
                            selector.combotree('clear');
                            parentDom.val('');
                        } else {
                            parentDom.val(d.id);
                        }
                    }
                }
            });
        },
        /**
         * 判断是否为空
         */
        isEmpty: function (obj) {
            if (undefined == obj || null == obj || obj == "" || obj == "undefined") {
                return true;
            } else {
                return false;
            }
        },
        /**
         * 获取项目根路径
         */
        getRootPath: function () {
            /*
             * var pathHref = window.location.href; var projectUrl = '';
             *
             * var pathName = window.document.location.pathname; var projectName =
             * pathName .substring(0, pathName.substr(1).indexOf('/') + 1);
             *
             * projectUrl = pathHref.substring(0, pathHref.indexOf(projectName) +
             * projectName.length); return (projectUrl);
             */
            return '';
        },
        /**
         * JS 取消冒泡事件 兼容火狐IE
         */
        stopPropagation: function (e) {
            if (e && e.stopPropagation) {
                // W3C取消冒泡事件
                // e.preventDefault();
                e.stopPropagation();
            } else {
                // IE取消冒泡事件
                window.event.cancelBubble = true;
            }
        },
        /**
         * 判断字符长度
         */
        getByteLen: function (val) {
            var len = 0;
            for (var i = 0; i < val.length; i++) {
                var length = val.charCodeAt(i);
                if (length >= 0 && length <= 128) {
                    len += 1;
                } else {
                    len += 2;
                }
            }
            return len;
        },
        /**
         * 设置宽度和高度自适应
         */
        resizedGrid: function (div_id) {
            var height = $(window).height() - 88;
            $("#" + div_id).datagrid('resize', {
                height: height
            });
        },
        /**
         * 设置宽度和高度自适应
         */
        resizedTreeLayoutGrid: function (div_id) {
            var height = $(window).height() - 88;
            var width = $(window).width() - 200;
            if ($("#maincont")) {
                width = $("#maincont").width() - 200;
            }
            $("#" + div_id).datagrid('resize', {
                width: width,
                height: height
            });
        },
        /*
         * 让datagrid在无数据的情况下显示横向滚动条
         * 注意此方法要结合datagrid的onLoadSuccess方法使用，需要判断数据为0的情况下调用此方法。 onLoadSuccess:
         * function(data){ if (data.total == 0) { scrollShow("dg",data.total); } }
         */
        scrollShow: function (datagridId, total) {
            if (total == 0) {
                var datagrid = $("#" + datagridId);
                var width = datagrid.prev(".datagrid-view2").find(".datagrid-header-row").width();
                datagrid.prev(".datagrid-view2")
                    .children(".datagrid-body")
                    .html("<div style='width:" + width + "px;border:solid 0px;height:1px;'></div>");
            }
        },
        /**
         * 加载枚举字典
         * @param params
         * @returns {boolean}
         */
        getSelectedEnumListByEnumName: function (params) {
            //拼接的jquery对象
            var dom = params.dom;
            if(this.isEmpty(dom))
                return false;

            //枚举名称
            var enumName = params.enumName;
            if(this.isEmpty(enumName))
                return false;

            //选中的枚举值
            var selectedId = params.selectedId;
            //首行加载不限
            var enumEmpty = params.enumEmpty;
            //不加载的枚举值
            var enumFilter = params.enumFilter;

            if (enumEmpty) {
                dom.append(
                    "<option value=''>--不限--</option>"
                )
            }

            var enumList = enums.getByEnumName(enumName);
            if(this.isEmpty(enumList))
                return false;

            $.each(enumList, function (index, obj) {
                if (isEmpty(enumFilter) || !in_array(enumFilter.split(','), obj.value)) {
                    if (obj.value + "" == selectedId) {
                        dom.append(
                            "<option value='" + obj.value + "' selected>" + obj.name + "</option>"
                        )
                    } else {
                        if (obj.value == '000') {

                            dom.append(
                                "<optgroup label='" + obj.name + "' style='font-weight:bold;'>" + obj.name + "</optgroup>"
                            );
                        } else {
                            dom.append(
                                "<option value='" + obj.value + "'>" + obj.name + "</option>"
                            );
                        }
                    }
                }
            })
            dom.select2();
        },
    };
}();

var errCssSpan = {
    'border-color': '#CC0033',
    'border-width': '1px',
    'border-style': 'solid'
};
var errCssSpan2 = {
    'border-color': '#CC0033',
    'border-width': '2px',
    'border-style': 'solid'
};

function removeRedBorder($dom) {
    $dom.css('border-color', '');
    $dom.css('border-width', '');
    $dom.css('border-style', '');
}

String.prototype.replaceAll = function (reallyDo, replaceWith, ignoreCase) {
    if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
        return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi" : "g")),
            replaceWith);
    } else {
        return this.replace(reallyDo, replaceWith);
    }
};

Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, // month
        "d+": this.getDate(), // day
        "h+": this.getHours(), // hour
        "m+": this.getMinutes(), // minute
        "s+": this.getSeconds(), // second
        "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
        "S": this.getMilliseconds() // millisecond
    };
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};

function in_array(arr, obj) {
    var i = arr.length;
    while (i--) {
        if (arr[i] === obj) {
            return true;
        }
    }
    return false;
}

function commonInputValidate(ip) {
    ip.parent().find('span').remove();
    var validateResult = true;
    var isMarked = '0';
    if (ip.attr('rq') == 'numOnly') {
        if (checkNumOnly(ip[0])) {
            ip.parent().find('span').remove();
            removeRedBorder(ip);
        } else {
            validateResult = false;
        }
    }
    return validateResult;
}

function commonFormValidate(form) {
    var spanArray = form.get(0).getElementsByTagName('span');
    for (var i = 0; i < spanArray.length; i++) {
        if ($(spanArray[i]).hasClass('help-block')) {
            $(spanArray[i]).css('display', 'none');
        }
    }
    var isMarked = '0';
    var validateResult = true;
    var inputArray = form.get(0).getElementsByTagName('input');
    var areaArray = form.get(0).getElementsByTagName('textarea');
    var selectArray = form.get(0).getElementsByTagName('select');
    for (var i = 0; i < inputArray.length; i++) {
        removeRedBorder($(inputArray[i]));
        if ($(inputArray[i]).attr('rq') == '*') {
            if (isEmpty($(inputArray[i]).val())) {
                if ($(inputArray[i]).next('div').children('span').children('span')) {
                    $(inputArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
// $(inputArray[i]).next('div').prepend('<span class="help-block"
// style="top:0px;position:absolute;width:200px"><font
// color="#CC0033">该项为必填*<font></span>')
                } else if ($(inputArray[i]).next('div').children('span[class="help-block"]') || $(inputArray[i]).next('span').hasClass('help-block')) {
                    $(inputArray[i]).next('span').next('div').prepend('<span class="help-block" style="top:0px;position:absolute;width:200px"><font color="#CC0033">该项为必填*<font></span>')
                } else {
                    $(inputArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
                }
                $(inputArray[i]).css(errCssSpan);
                if (isMarked == '0') {
                    isMarked = '1';
                    validateResult = false;
                }
            }
        } else {
            if ($(inputArray[i]).attr('rq') == 'tel') {
                if (!checkTel(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'telWithoutNull') {
                if (!checkTelWithoutNull(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'ltel') {
                if (!checkLTel(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'ltelWithoutNull') {
                if (!checkLTelWithoutNull(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'email') {
                if (!checkEmail(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'wordOnly') {
                if (!checkWordOnly(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'numOnly') {
                if (!checkNumOnly(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'positiveNumOnly') {
                if (!checkPositiveNumOnly(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'price') {
                if (!checkPrice(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'url') {
                if (!checkURL(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'IDCard') {
                if (!checkIDCard(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'douNum') {
                if (!checkDoubleNumber(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'nullDouNum') {
                if (!checkNullDoubleNumber(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'UserName') {
                if (!checkUserName(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'mobile') {
                if (!checkMobile(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'rpwd') {
                if (!checkPassword(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            }
        }
    }
    for (var i = 0; i < areaArray.length; i++) {
        removeRedBorder($(areaArray[i]));
        if ($(areaArray[i]).attr('rq') == '*') {
            if (isEmpty($(areaArray[i]).val())) {
                if ($(areaArray[i]).next('div').children('span').children('span')) {
                    $(areaArray[i]).next('div').prepend('<span class="help-block" style="top:0px;position:absolute;width:200px"><font color="#CC0033">该项为必填*<font></span>')
                } else {
                    $(areaArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
                }
                $(areaArray[i]).css(errCssSpan);
                if (isMarked == '0') {
                    isMarked = '1';
                    validateResult = false;
                }
            }
        }
    }
    for (var i = 0; i < selectArray.length; i++) {
        removeRedBorder($(selectArray[i]));
        if ($(selectArray[i]).attr('rq') == '*') {
            if (isEmpty($(selectArray[i]).val())) {
                $(selectArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必选*<font></span>')
                $(selectArray[i]).css(errCssSpan);
                if (isMarked == '0') {
                    isMarked = '1';
                    validateResult = false;
                }
            }
        }
    }

    return validateResult;
}

// 加载loading遮罩层
function loading(type) {
    if (isEmpty(type))
        return layer.load();
    else
        return layer.load(type);
}

// 关闭弹出层
function closeLayerWindow(index) {
    layer.close(index);
}

// 提示框
function showbsMsg(msg, type, showCloseButton, otherParam) {
    var type = isEmpty(type) ? 'success' : type;
    var msg;
    if (isEmpty(msg)) {
        msg = type == 'success' ? '操作成功' : '操作失败';
    }
// var showCloseButton = isEmpty(showCloseButton)?false:true;
    var msger = parent.Messenger();
    if (!isEmpty(otherParam)) {
        var parentIndex = otherParam.parentIndex;
        if (parentIndex == 0) parentIndex = '0';
        if (!isEmpty(parentIndex)) {
            if (parentIndex == '0')
                msger = Messenger();
            else if (parentIndex == 1)
                msger = parent.Messenger();
            else if (parentIndex == 2)
                msger = parent.parent.Messenger();
        }
    }
    msger.post({
        message: msg,
        type: type,
        showCloseButton: true
    });
}

// 重新加载layer数据网格
function reloadLayerTable(selector) {
    layui.use('table', function () {
        var table = layui.table;
        var selector = isEmpty(selector) ? 'datagrid' : selector;
        table.reload(selector, {});
    });
}

/**
 * 验证手机号：移动电话
 *
 * @param object
 */
function checkVfMobile(object) {
    var phone = object.value;
    var patrn = /^(\+?0?86\-?)?1[345789]\d{9}$/;
    // 判断phone是否为空
    if (phone.length != 0) {
        if (0 <= phone.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(phone)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证电子邮箱
 *
 * @param object
 */
function checkVfEmail(object) {
    var email = object.value;
    var patrn = /^([a-z0-9A-Z]+[_|-|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
    if (email.length != 0) {
        if (0 <= email.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">邮箱不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(email)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的邮箱！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证邮政编码
 *
 * @param object
 */
function checkVfZip(object) {
    var zip = object.value;
    var patrn = /^[0-9][0-9]{5}$/;
    if (zip.length != 0) {
        if (0 <= zip.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">邮政编码不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(zip)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的邮政编码！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证区号
 *
 * @param object
 */
function checkVfQh(object) {
    var qh = object.value;
    var patrn = /^[0-9]{3,4}$/;
    if (qh.length != 0) {
        if (0 <= qh.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">区号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(qh)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的区号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证座机号
 *
 * @param object
 */
function checkVfTel(object) {
    var tel = object.value;
    var patrn = /^[0-9]{7,8}$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">电话不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的固定电话！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证分机号
 */
function checkVfDisTel(object) {
    var tel = object.value;
    var patrn = /^[0-9]{3,4}$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">分机号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的分机号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

function isEmpty(obj) {
    if (obj == undefined || obj == null || obj == "" || obj == "undefined") {
        return true;
    } else {
        return false;
    }
}

function trimStr(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

function trimBrStr(str) {
    return str.replace(/\n/g, "");
}

function checkTel(object) {
    var phone = object.value;
    var patrn = /^(\+?0?86\-?)?1[345789]\d{9}$/;
    if (phone.length == 0) {
// object.placeholder = "手机号不能为空！";
// $(object).after('<span class="help-block"><font
// color="#CC0033">手机号不能为空！<font></span>')
// $(object).css(errCssSpan);
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "手机号不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的手机号！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}


function checkPwd(object) {
    var phone = object.value;
// var patrn =
// /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)(?![a-zA-z\d]+$)(?![a-zA-z!@#$%^&*]+$)(?![\d!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]+$/;
// var patrn = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,}$/;
// var patrn =
// /^(?!\d+$)(?![a-zA-Z]+$)(?![a-zA-Z-`=\\;',./~!@#$%^&*()_+|{}:"<>?]+$)(?![0-9-`=\\;',./~!@#$%^&*()_+|{}:"<>?]+$)/;
    var patrn = /^((?!\d+$)(?![a-zA-Z]+$)[a-zA-Z\d@#$%^&_+].{7,})+$/;
    if (phone.length == 0) {
// object.placeholder = "固定电话不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">密码不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">密码不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">密码至少8位，必有字母和数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkLTel(object) {
    var phone = object.value;
    var patrn = /^((\d{3,4}\-)|)\d{7,8}(|([-\u8f6c]{1}\d{1,5}))$/;
    if (phone.length == 0) {
// object.placeholder = "固定电话不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">固定电话不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">固定电话不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的固定电话！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkEmail(object) {
    var phone = object.value;
    var patrn = /^([a-z0-9A-Z]+[_|-|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
    if (phone.length == 0) {
// object.placeholder = "邮箱不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
        $(object).css(errCssSpan);
        return false;
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "手机号不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">邮箱不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的手机号！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的邮箱！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkWordOnly(object) {
    var phone = object.value;
    var patrn = /^[\u4E00-\u9FA5]{1,}$/;
    if (phone.length == 0) {
// object.placeholder = "不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">文本不能为空！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">文本不能有空格！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的文字！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkNumOnly(object) {
    var phone = object.value;
    var patrn = /^\d{1,}$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        object.setCustomValidity('请输入正确的11位电话号码');
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkPositiveNumOnly(object) {
    var phone = object.value;
    var patrn = /^\+?[1-9]\d*$/;
    ;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">整数不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">整数不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的正整数！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkPrice(object) {
    var phone = object.value;
    if (phone.indexOf(',') > 0)
        phone = phone.replace(/,/g, '');
// var patrn = /^\d+(?:\.\d{1,2})?$/;
    var patrn = /^(-)?\d{0,14}(\.\d{1,2})?$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkFourDotNum(object) {
    var phone = object.value;
    var patrn = /^(-)?0$|^(-)?[1-9]\d{0,15}$|^(-)?[1-9]\d{0,15}\.{1}\d{1,4}$|^(-)?0\.{1}\d{1,4}$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">小数不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">小数不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">至少4位小数！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkPriceNegativeAllow(object) {
    var phone = object.value;
    var patrn = /^(-)?\d+(\.\d+)?$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}


function checkTelWithoutNull(object) {
    var phone = object.value;
    var patrn = /^(\+?0?86\-?)?1[345789]\d{9}$/;
    if (phone.length == 0) {
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "手机号不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的手机号！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkLTelWithoutNull(object) {
    var phone = object.value;
    var patrn = /^((\d{3,4}\-)|)\d{7,8}(|([-\u8f6c]{1}\d{1,5}))$/;
    if (phone.length == 0) {
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">固定电话不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的固定电话！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkURL(object) {
    var phone = object.value;
    var qqq = "((http|ftp|https)://)(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,6})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,4})*(/[a-zA-Z0-9\&%_\./-~-]*)?";
// var patrn = "^((https|http|ftp|rtsp|mms)?://)"
// + "?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?"
// //ftp的user@
// + "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
// + "|" // 允许IP和DOMAIN（域名）
// + "([0-9a-zA-Z_!~*'()-]+\.)*" // 域名- www.
// + "([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\." // 二级域名
// + "[a-zA-Z]{2,6})" // first level domain- .com or .museum
// + "(:[0-9]{1,4})?" // 端口- :80
// + "((/?)|"
// + "(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";

    var re = new RegExp(qqq);
    var result = re.test(phone);
    if (phone.length == 0) {
        /*$(object).after('<span class="help-block"><font color="#CC0033">请求地址不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;*/
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">请求地址不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!result) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的请求地址！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证项目名称
function checkProjectName(object) {
    var phone = object.value;
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = /^.{5,}$/;
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">项目名不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
        $(object).after('<span class="help-block"><font color="#CC0033">项目名不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (phone.length < 5) {
        $(object).after('<span class="help-block"><font color="#CC0033">5个字符以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
        $(object).after('<span class="help-block"><font color="#CC0033">必填5位以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证文本框
function checkTextArea(object) {
    var phone = object.value;
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = /^.{10,}$/;
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (phone.length < 5) {
        $(object).after('<span class="help-block"><font color="#CC0033">10个字符以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
        $(object).after('<span class="help-block"><font color="#CC0033">必填10位以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证15位整数加2位小数
function checkjva(object) {
    var phone = object.value;
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = /^(-)?\d{0,14}(\.\d{1,2})?$/;
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空!<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
        $(object).after('<span class="help-block"><font color="#CC0033">输入类型不匹配或长度超过限制<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证输入文本最小长度
function checkMinimumLength(minimumLength, object) {
    var phone = trimBrStr(object.value);
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = '.{' + minimumLength + ',}';
    var reg = new RegExp(patrn);
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } /*
		 * else if (0 <= phone.indexOf(' ')) { $(object).after('<span
		 * class="help-block"><font color="#CC0033">内容不能有空格！<font></span>')
		 * $(object).css(errCssSpan); return false; }
		 */ else if (Utils.isEmpty(phone.replace(/ /g, ''))) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能都是空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!phone.match(reg)) {
        $(object).after('<span class="help-block"><font color="#CC0033">必填' + minimumLength + '位以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}


// 给li下拉选项添加点击事件
function clickLi(obj) {
    // $(".entrpCodeInput").val($(obj).attr("code"));
    $(obj).parent('.entrpDiv').prev().find('.entrpCodeInput').val($(obj).attr("code"));
}

// 给li下拉选项添加点击事件
function clickNameLi(obj) {
    // $(".entrpNameInput").val($(obj).attr("punitName"));//获得选中的名称
    $(obj).parent('.entrpDiv').prev().find('.entrpNameInput').val($(obj).attr("punitName"));// 获得选中的名称
    // $(".entrpNameCode").val($(obj).attr("code"));//获得选中的代码
    $(obj).parent('.entrpDiv').prev().find('.entrpNameCode').val($(obj).attr("code"));// 获得选中的代码
    var allParentsNode = $(obj).parents();
    allParentsNode.each(function () {
        if ($(this).is('form')) {
            var idStr = $(this).attr('id');
            if (idStr == 'searchFormId') {
                searchmore();
            }
            if (idStr == 'searchFormId_children') {
                searchmore_chaldren();
            }
            if (idStr == 'searchFormId2') {
                searchmore2();
            }
        }
    })

}

// 填报月份页面上显示处理
// strs为时间字符串
function writeDeal(date, type) {

    var strs = "" + date + "";
    strs = strs.split("-");
    var year = strs[0];
    var month = strs[1];
    var day = strs[2];
// if(month==10&&day<21){
// month=9;
// }
    if (type == '月报' && (month == 1 && day < 9)) {
        year = year - 1;
        month = 12;
    }
    else if (type == '月报' && (month != 1 && day < 9)) {
        month = month - 1;
    }
    if (!isEmpty(month)) {
        var time;
        if (type == '月报') {
            time = year + "年" + (month - 0) + "月";
        } else if (type == '半年报') {
            if (month == 7) {
                time = year + "年半年报调整";
            }
        } else if (type == '年报') {
            if (month == 1 || month == 2) {
                time = (year - 1) + "年年报调整";
            }
        }
        return time;
    } else {
        return year + "年合计";
    }
}

// 处理报送期
function dealtime(date) {
    if (!isEmpty(date)) {
        var strs = "" + date + "";
        strs = strs.split("-");
        var year = strs[0];
        var month = strs[1];
        var day = strs[2];
        if (month == 1 && day < 9) {
            year = year - 1;
            month = 12;
        }
        else if (month != 1 && day < 9) {
            month = month - 1;
        }
        time = year + "-" + month;

        return time;
    } else {
        return '';
    }
}

/**
 * 选中年份时间
 *
 * @param aaa
 */
function chooseYear(aaa) {
    $(aaa).datetimepicker({
        startView: 'decade',
        minView: 'decade',
        format: 'yyyy',
        maxViewMode: 2,
        minViewMode: 2,
        startDate: -Infinity,
        endDate: year,
        autoclose: true
    });
}

function loadNumInput() {
    $(".intValue").bind('input propertychange', function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
        }
        var value = $(this).val();
        var re = /^[+-]?[1-9]?[0-9]*$/;
        if (!re.test(value) && value != "" && value != "0" && value != "-" && value != "+") {
            $(this).val(oldValue);
        }
    }).click(function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
            var t = $(this).val();
            $(this).val("").focus().val(t);
        }
    }).keydown(function () {
        oldValue = $(this).val();
    }).blur(function () {
        var val = $(this).val();
        if (val == "" || val == "-" || val == "+") {
            $(this).val("0");
        }
        $(this).css("textAlign", "right");
    });


    $(".doubleValue").bind('input propertychange', function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
        }
        var value = $(this).val();
        var re = /^[+-]?[1-9]?[0-9]*$/;
        var re2 = /^[+-]?[1-9]?[0-9]*\.[0-9]*$/;
        if (!re2.test(value) && !re.test(value) && value != "" && value != "0" && value != "-" && value != "+") {
            $(this).val(oldValue);
        }
    }).click(function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
            var t = $(this).val();
            $(this).val("").focus().val(t);
        }
    }).keydown(function () {
        oldValue = $(this).val();
    }).blur(function () {
        var val = $(this).val();
        if (val == "" || val == "0" || val == "-" || val == "+") {
            $(this).val("0.0");
        }
        $(this).css("textAlign", "right");
    });

}

/**
 * 验证是否可修改
 *
 * @param nowDate：系统当前时间
 * @param fillDate：填报时间
 * @param recordType：填报类型
 */
function checkEditStatus(nowDate, fillDate, recordType) {
    // 判断recordType类型
// if(recordType == "半年报"|| recordType == "年报"){
// return true;
// }
    var showNowDate = writeDeal(nowDate, recordType);
    var showFillDate = writeDeal(fillDate, recordType);
    // 判断是否 相等
    console.log(nowDate + "---" + fillDate);
    console.log(showNowDate + "-" + showFillDate);
    if (showNowDate != showFillDate) {
        return false;
    } else {
        return true;
    }


}

// 将数字添加千分符
function toThousands(num) {
    var result = '0', counter = 0;
    if (!isEmpty(num)) {
        /*
         * if(!isNaN(num)) num = parseFloat(num).toFixed(2);
         */
        num = (num || 0).toString();
        if (num.indexOf(',') > 0)
            num = num.replace(/,/g, '');
        var isBelowZero = false;
        if (num.indexOf('-') == 0) {
            isBelowZero = true;
            num = num.substring(1);
        }
        var re = /\d{1,3}(?=(\d{3})+$)/g;
        var n1 = num.replace(/^(\d+)((\.\d+)?)$/, function (s, s1, s2) {
            return s1.replace(re, "$&,") + s2;
        });
        if (isBelowZero)
            n1 = '-' + n1;
        return n1;
    }
    return result;
}

// 将数字去除千分符
function removeThousands(num) {
    return isEmpty(num) ? '0' : num.replace(/,/g, '');
}

function isNum(val) {
    var reg = /^[0-9]+.?[0-9]*$/;
    if (reg.test(val)) {
        return true;
    }
    return false;
}

function getFloat(num) {
    if (!Utils.isEmpty(num) && !isNaN(num)) {
        return parseFloat(num).toFixed(2);
    } else if (isNaN(num)) {
        return num;
    }
    else {
        return 0.0;
    }
}

/**
 * 获取合计行在datgrid中的行数
 *
 * @param yearNum
 *            当前合计行年份
 * @param rows
 *            数据集
 * @param index
 *            当前合计索引
 */
function getTotalRowNum(yearNum, rows, index) {
    if (index == 0) {
        return 0;
    }
    for (var i = 0; i < rows.length; i++) {
        var yearStr = writeDeal(rows[i].TIME, rows[i].RECORDTYPENAME);
        if (yearStr.indexOf(yearNum) >= 0) {
            index = i;
            break;
        }
    }
    return index;
}

// 方法2：填报类型字段名称不同
function getTotalRowNum1(yearNum, rows, index) {
    if (index == 0) {
        return 0;
    }
    for (var i = 0; i <= rows.length; i++) {
        var yearStr = writeDeal(rows[i].TIME, rows[i].WRITETYPE);
        if (yearStr.indexOf(yearNum) >= 0) {
            index = i;
            break;
        }
    }
    return index;
}

function isContainsChineseWord(str) {
    var result = false;
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 255) {
            result = true;
            break;
        }
    }
    return result;
}

//验证字母/数字/下划线
function checkUserName(object) {
    var tel = object.value;
    var patrn = /^[a-z0-9_-]{5,20}$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入5-20为字母、数字、下划线<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证身份证
function checkIDCard(object) {
    var tel = object.value;
    var patrn = /^\d{15}(\d\d[0-9xX])?$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的身份证号码<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证可为空小数或整数
function checkNullDoubleNumber(object) {
    var tel = object.value;
    var patrn = /^(\d+)(\.\d+)?$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的小数或整数<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

//验证非空小数或整数
function checkDoubleNumber(object) {
    var tel = object.value;
    var patrn = /^(\d+)(\.\d+)?$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的小数或整数<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证小数
/**
 * 验证手机号：移动电话
 *
 * @param object
 */
function checkMobile(object) {
    var phone = object.value;
    var patrn = /^1\d{10}$/;
    // 判断phone是否为空
    if (phone.length != 0) {
        if (0 <= phone.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(phone)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证确认密码和密码是否一致
function checkPassword(object) {
    var rpwd = object.value;
    var pwd = $('#pwd').val();

    if (rpwd != pwd) {
        $(object).after('<span class="help-block"><font color="#CC0033">两次输入的密码不一致！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}