var web = '/admin/web';

/**
 * admin端url跳转地址
 */
var web = function() {
	return {
		// 登录
		login: {
			login: web + "/login", //登录页
		},
		//菜单
		menu: {
            editPage: web + "/base/baseMenuInfo/pageEdit", //admin端所有菜单维护
		},
		//权限
		operator: {
		},
        //用户
        baseUserInfo: {
            editPage: web + "/base/baseUserInfo/pageEdit", //admin端所有菜单维护
        },
	};
}();