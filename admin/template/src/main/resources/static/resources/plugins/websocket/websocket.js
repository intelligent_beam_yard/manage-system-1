//处理方式枚举
var handleWay = {
	'tipOnly' : '1001',
	'undo_audit' : '2001',
	'undo_audAudit' : '2002',
	'notice' : '2003',
	'apply_1' : '3001',
	'logOut' : '4001'
}

$(function(){
	//websocket测试----------------------------------------------------
	var websocket = null;  
    //判断当前浏览器是否支持WebSocket  
    if ('WebSocket' in window) {  
    	var websocketStatue = Utils.getCookie('websocketStatue');
    	var userStr = Utils.getCookie('user');
    	if(!Utils.isEmpty(userStr)){
    		var user = eval('('+userStr+')');
//    		websocket = new WebSocket("ws://10.65.52.42:8080/websocket/client"+user.id);  //本地
    		websocket = new WebSocket("ws://jsj.liangjiang.gov.cn/websocket/client"+user.id);  //正式环境
//    		websocket = new WebSocket("ws://222.180.171.161:45/websocket/client"+user.id);  //测试环境
    		//连接发生错误的回调方法  
		   websocket.onerror = function () {  
				 //如果连接失败，则清除所有连接状态
			   	Utils.setCookie('websocketStatue','fail');
//		        setMessageInnerHTML("实时消息连接发生错误");  
		   };  
		  
		    //连接成功建立的回调方法  
		    websocket.onopen = function () {  
		    	//将首次连接成功状态，塞入本地缓存
		    	Utils.setCookie('websocketStatue','success');
//		    	Utils.showToastSuccess("实时消息连接成功");
		    }  
		  
		   //接收到消息的回调方法  
		    websocket.onmessage = function (event) {  
		    	if(!Utils.isEmpty(event.data)){
		    		var websocketReturn = eval("("+event.data+")");
		    		if(websocketReturn.success){
		    			var attributes = websocketReturn.attributes;
		    			var websocketReturnHandleWay = attributes.handleWay;
		    			console.log(websocketReturnHandleWay);
		    			if(websocketReturnHandleWay == handleWay.tipOnly){
		    				Utils.showToastSuccess(websocketReturn.msg); 
		    			}else if(websocketReturnHandleWay == handleWay.undo_audit
		    					|| websocketReturnHandleWay == handleWay.notice){
		    				showAuditPage(1, 11);
//		    				var msg = '<a style="color:#fff" href="javascript:showAuditPage()">'+websocketReturn.msg+'</a>'
		    				var msg = websocketReturn.msg;
		    				Utils.showToastSuccess(msg); 
		    			}else if(websocketReturnHandleWay == handleWay.undo_audAudit){
		    				showAuditPage(9, 14);
		    				Utils.showToastSuccess(websocketReturn.msg); 
		    			}else if(websocketReturnHandleWay == handleWay.apply_1){
		    				showIframePageContent('/realEstateApply/fillHomePage',null,'资质申请',7);
		    				Utils.showToastSuccess(websocketReturn.msg); 
		    			}else if(websocketReturnHandleWay == handleWay.logOut){
		    				Utils.showAlert(websocketReturn.msg, function(){
		    					Utils.setCookie('user', '');
		    					Utils.setCookie('websocketStatue','fail');
		    					window.location.href=Utils.getRootPath()+"/login/logOut";
		    				}, 4); 
		    			}
		    		}
		    	}
		   }  
		 
		    //连接关闭的回调方法  
		    websocket.onclose = function () {  
//		       setMessageInnerHTML("实时消息连接关闭");  
		    }  
		  
		    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。  
		    window.onbeforeunload = function () {  
		        closeWebSocket();  
		    } 
    	}
   } else {  
       alert('当前浏览器 Not support websocket')  
   }  
  
    //将消息显示在网页上  
    function setMessageInnerHTML(innerHTML) {  
       Utils.showToastFail(innerHTML);
   }  
  
   //关闭WebSocket连接  
    function closeWebSocket() {  
        websocket.close();  
    }  
 
    $('#sendBtn').click(function(){
    	Utils.preventPopAndDefault(event);
        var message = document.getElementById('text').value;  
        websocket.send(message); 
    })
  //websocket测试----------------------------------------------------
})

//展示审核列表
function showAuditPage(objectType, menuId){
	var title;
	if(objectType == '1')
		title = '房地产开发资质审核';
	else if(objectType == '9')
		title = '动态复查';
	showIframePageContent('/auditInfo/pageList?objectType='+objectType,null,title,menuId);
}
