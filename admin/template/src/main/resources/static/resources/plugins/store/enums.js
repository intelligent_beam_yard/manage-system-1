
/**
 * 后端枚举
 */
var enums = function() {
	return {
        //读取指定枚举
        getByEnumName: function(enumName) {
            var enumData = eval('('+localStorage.getItem("enums")+')');
            return enumData[enumName];
        },
		//根据枚举名和值，获取name
		getNameByValue: function(enumName, value) {
            if(!value)
                return "";

        	var enumObj = this.getByEnumName(enumName);
        	if(!enumObj)
        	    return "";
        	var retVal = "";
            for (var i = 0; i < enumObj.length; i++) {
				if(enumObj[i].value == value){
                    retVal = enumObj[i].name;
                    break;
                }
            }

            return retVal;
		}
	};
}();