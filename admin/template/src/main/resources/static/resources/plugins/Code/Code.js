
/**
 * 返回码
 */
var Code = function() {
	return {
		success: 10000000,  //成功
		commonFailure: 19999999,  //系统异常
		paramsFailure: 19999998,  //参数异常
	};
}();