var loadErrorMsg = '网络错误';

$(function () {
    layui.use('layer', function () {

    });
    // 注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function () {
        var element = layui.element;

        // …
    });
    layui.use('laydate', function () {
    });
})

/**
 * 工具脚本
 */
var FrameUtil = function () {
    return {
        /**
         * 返回后台列表的window height
         */
        createWindowWithoutBtn: function (param) {
            var lay = isEmpty(param.parentOpen) ? layer : parent.layer;
            var dataGridSelector = param.dataGridSelector ? param.dataGridSelector : "datagrid";

            var width = !isEmpty(param.width) ? param.width + 'px' : '900px';
            var height = !isEmpty(param.height) ? param.height + 'px' : '550px';
            var title = param.title;
            var url = param.url;

            var index = lay.open({
                resize: true,
                title: title,
                area: [width, height],
                offset: '10px',
                type: 2,
                anim: 2,
                content: url,
                btn: [],
                yes: function (index, layero) {
                    layer.alert('点击确定', {icon: 1});
                }, btn2: function (index, layero) {
                    layer.alert('关闭');
                }/*
					 * ,btn3: function(index, layero){ //按钮【按钮三】的回调 }
					 */
                , cancel: function () {
                    // 右上角关闭回调
                }
            });
            if (param.fullScreen)
                lay.full(index);
        },
        /**
         * 返回后台列表的window height
         */
        createAjaxWindow: function (param) {
            var width = !isEmpty(param.width) ? param.width + 'px' : '900px';
            var height = !isEmpty(param.height) ? param.height + 'px' : '550px';
            var title = param.title;
            var url = param.url;
            var btnArray = param.btnArray; // ['保存', '关闭']
            /*
             * var layerWindowJson = { resize : true, title : title, area:
             * [width, height], type : 2, content: url, offset: '10px', btn:
             * btnArray }
             *
             * if(!isEmpty(btnArray)){ for(var i = 1; i <= btnArray.length; i
             * ++){ var btnIndex = i.toString(); console.log(btnIndex) var
             * btnName; if(btnIndex == '1'){ btnName = 'yes'; }else{ btnName =
             * 'btn'+btnIndex; } layerWindowJson[btnName] = function(index,
             * layero){ console.log(btnIndex) console.log(index) var body =
             * $("iframe").contents().find("body"); var submitBtn =
             * body.find("button[sb='nbBtn_"+btnIndex+"']"); submitBtn.click(); } } }
             *
             * var index = layer.open(layerWindowJson);
             */
            var index = layer.open({
                resize: true,
                title: title,
                area: [width, height],
                type: 2,
                anim: 2,
                content: url,
                offset: '10px',
                scrollbar: false,
                btn: btnArray,
                yes: function (index, layero) {
                    var body = $("iframe").contents().find("body");
                    var submitBtn = body.find("button[sb='nbBtn_1']");
                    submitBtn.click();
                },
                btn2: function (index, layero) {
                    var body = $("iframe").contents().find("body");
                    var submitBtn = body.find("button[sb='nbBtn_2']");
                    submitBtn.click();
                    return false;
                },
                btn3: function (index, layero) {
                    var body = $("iframe").contents().find("body");
                    var submitBtn = body.find("button[sb='nbBtn_3']");
                    submitBtn.click();
                    return false;
                },
                btn4: function (index, layero) {
                    var body = $("iframe").contents().find("body");
                    var submitBtn = body.find("button[sb='nbBtn_4']");
                    submitBtn.click();
                    return false;
                }
            });
            if (param.fullScreen)
                layer.full(index);
        },
        /**
         * 返回后台列表的window height
         */
        createCommonWindow: function (param) {
            var lay = isEmpty(param.parentOpen) ? layui.layer : parent.layui.layer;
            var dataGridSelector = param.dataGridSelector ? param.dataGridSelector : "datagrid";
            var reloadTableGrid = param.reloadTableGrid ? param.reloadTableGrid : true;
            var width = !isEmpty(param.width) ? param.width + 'px' : '900px';
            var height = !isEmpty(param.height) ? param.height + 'px' : '550px';
            var title = param.title;
            var url = param.url;
            var saveBtnWord = !isEmpty(param.saveBtnWord) ? param.saveBtnWord : '保存';

            var that = this;

            var index = lay.open({
                resize: true,
                title: title,
                area: [width, height],
                offset: '10px',
                type: 2,
                anim: 2,
                content: url,
                btn: [saveBtnWord, '关闭'],
                yes: function (index, layero) {
                    layer.alert('保存');
                }, btn2: function (index, layero) {
                    layer.alert('关闭');
                }
                , cancel: function () {
                    // 右上角关闭回调
                },
                yes: function (index, layero) {
//					  var iframe = isEmpty(param.parentOpen)?$("iframe"):$("iframe:last");
                    var form = $("iframe").contents().find("form[action != '']");
                    var result = commonFormValidate(form);
                    if (result) {
                        var url = form.attr('action');
                        httpClient.postData(url, Utils.serializeObject(form), {
                            onSuccess: function (d) {
                                if(reloadTableGrid) {
                                    layui.use('table', function () {
                                        var table = layui.table;
                                        table.reload(
                                            dataGridSelector, {}
                                        );
                                    });
                                }
                                that.closeLayerWindow(index);
                                if(!isEmpty(param.onSuccess))
                                    param.onSuccess();
                            }
                        });
                    } else {
                        showbsMsg('您有未通过验证的选项', 'error');
                        return false;
                    }
                },
                btn2: function (index, layero) {
                    layui.use('table', function () {
                        var table = layui.table;
                        table.reload(
                            dataGridSelector, {}
                        );

                    });
                    that.closeLayerWindow(index);
                }
            });
            if (param.fullScreen)
                lay.full(index);
        },
        multipartAjaxSubmit: function (form, arg) {
            var index1 = parent.layer.load(1);
            form.ajaxSubmit({
                type: "POST",
                url: form.attr('action'),
                data: {'arg': arg},
                dataType: "json",
                success: function (result) {
                    if (result.success) {
                        parent.$('#tt').datagrid('reload');
                        layer.msg(result.msg, {icon: 1});
                        parent.layer.close(index1);// 如果设定了yes回调，需进行手工关闭
                        var index = parent.layer.getFrameIndex(window.name); // 先得到当前iframe层的索引
                        parent.layer.close(index);
                    } else {
                        parent.layer.close(index1);
                        layer.msg(result.msg, {icon: 2});
                    }
                },
                error: function (result) {
                    layer.msg(result.responseJSON.msg, {icon: 2});
                }
            });
        },
        /**
         * 公共文件（图片）上传方法
         */
        commonImgUploader: function (dom, url) {
            var index1 = layer.load(1);
            $(dom).parent('form').ajaxSubmit({
                type: "POST",
                url: url,
                dataType: "json",
                success: function (result) {
                    if (result.success) {
                        console.log(result.obj);
                        $(dom).parent('form').parent().find('img[up="img"]').attr('src', result.obj);
                        layer.msg("成功", {icon: 1});
                        layer.close(index1);
                    } else {
                        layer.close(index1);
                        layer.msg(result.msg, {icon: 2});
                    }
                },
                error: function (result) {
                    layer.close(index1);
                    layer.msg(result.responseJSON.msg, {icon: 2});
                }
            });
        },
        /**
         * alert 提示框
         */
        showAlert: function (cont, func, type) {
            var type = isEmpty(type) ? 3 : type
            layer.alert(cont, {icon: type}, function (index) {
                if (!isEmpty(func))
                    func();
            }); // 这时如果你也还想执行yes回调，可以放在第三个参数中。
        },
        /**
         * 确认/取消 操作框
         */
        showConfirm: function (cont, func1, func2) {
            layer.confirm(cont, {icon: 3, title: '提示'}, function (index) {
                if (!isEmpty(func1))
                    func1();
                layer.close(index);
            }, function (index2) {
                if (!isEmpty(func2))
                    func2();
                layer.close(index2);
            });
        },
        /**
         * 输入框 操作框
         */
        showPrompt: function (cont, title, width, height, func1) {
            width = width ? width + "px" : '800px';
            height = height ? height + "px" : '800px';
            layer.prompt({
                formType: 2,
                value: cont,
                title: title,
                area: [width, height] // 自定义文本域宽高
            }, function (value, index, elem) {
                func1();
                layer.close(index);
            });
        },
        /**
         * 简单提示
         */
        showTips: function (cont, jQueryDom) {
            layer.tips(cont, jQueryDom);
        },
        /**
         * 统一tip层(操作成功)
         */
        showToastSuccess: function (msg, showCloseButton, otherParam) {
// layer.msg(content, {icon: 1});

            var type = 'success';
            var msg;
            if (isEmpty(msg)) {
                msg = type == 'success' ? '操作成功' : '操作失败';
            }
// var showCloseButton = isEmpty(showCloseButton)?false:true;
            var msger = Messenger();
            if (!isEmpty(otherParam)) {
                var parentIndex = otherParam.parentIndex;
                if (isEmpty(parentIndex))
                    msger = Messenger();
                else if (parentIndex == 1)
                    msger = parent.Messenger();
                else if (parentIndex == 2)
                    msger = parent.parent.Messenger();
            }
            msger.post({
                message: msg,
                type: type,
                showCloseButton: true
            });
        },
        /**
         * 统一tip层(服务端报错，操作失败)
         */
        showToastFail: function (msg, showCloseButton) {
// layer.msg(content, {icon: 1});

            var type = 'error';
            var msg;
            if (isEmpty(msg)) {
                msg = type == 'success' ? '操作成功' : '操作失败';
            }
// var showCloseButton = isEmpty(showCloseButton)?false:true;
            Messenger().post({
                message: msg,
                type: type,
                showCloseButton: true
            });
        },
        /**
         * 统一加载层
         */
        loading: function (type) {
            var index = null;
            // layui.use('layer', function(){
            var layer = layui.layer;
            if (!Utils.isEmpty(layer)) {
                if (Utils.isEmpty(type))
                    index = layer.load();
                else
                    index = layer.load(type);
            }
            // })
            return index;
        },
        /**
         * 统一刷新数据网格
         */
        reloadLayerTable: function (selector) {
            layui.use('table', function () {
                var table = layui.table;
                var selector = isEmpty(selector) ? 'datagrid' : selector;
                table.reload(selector, {});
            });
        },
        /**
         * 统一上传附件
         */
        uploadFile: function (baseParam) {
            var elem = baseParam.elem; // 指定点击的target
            var url = baseParam.url; // 上传请求url
            var data = baseParam.data; // 参数
            var accept = baseParam.accept; // 附件类型
            // 可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
            var exts = baseParam.exts; // 后缀限制 你设置 exts: 'zip|rar|7z'
            // 即代表只允许上传压缩格式的文件。如果 accept
            // 未设定，那么限制的就是图片的文件格式
            var size = baseParam.size; // 文件大小，单位为kb 默认不限制
            var multiple = baseParam.multiple; // 是否多文件上传
            var number = baseParam.number; // 多文件上传时的数量
            var choose = baseParam.choose; // 选择文件时的回调
            var before = baseParam.before; // 上传前的回调
            var done = baseParam.done; // 请求成功的回调
            var error = baseParam.error; // 请求异常的回调
            layui.use('upload', function () {
                var upload = layui.upload;
                var uploadInst = upload.render({
                    elem: elem // 绑定元素
                    , url: url // 上传接口
                    , data: data
                    , accept: accept
                    , exts: exts
                    , size: size
                    , multiple: multiple
                    , number: number
                    , choose: function (data) {
                        if (!isEmpty(choose))
                            choose(data);
                    }
                    , before: function (data) {
                        if (!isEmpty(before))
                            before(data);
                    }
                    , done: function (data) {
                        if (!isEmpty(done))
                            done(data);
                    }
                    , error: function (data) {
                        if (!isEmpty(error))
                            error(data);
                    }
                });
            });
        },
        /**
         * 统一关闭弹出层
         */
        closeLayerWindow: function (index) {
            layui.use('layer', function () {
                var layer = layui.layer;
                if(index) {
                    layer.close(index)
                } else {
                    layer.closeAll('loading');
                }
            })
        },
        /**
         * 获取选择日期（生日）的控件
         */
        getBirthDateChoose: function (dom, format) {
            var options = {
                elem: dom,
                format: format, // 日期格式
                istime: false,

                // min: laydate.now(), //最小日期
                max: laydate.now(), // 最大日期
                zIndex: 99999999, // css z-index
                choose: function (dates) { // 选择好日期的回调
                    $(dom).val(dates);
                }
            }

            layui.laydate(options);
        },
        /**
         * JS 取消冒泡事件 兼容火狐IE
         */
        stopPropagation: function (e) {
            if (e && e.stopPropagation) {
                // W3C取消冒泡事件
                // e.preventDefault();
                e.stopPropagation();
            } else {
                // IE取消冒泡事件
                window.event.cancelBubble = true;
            }
        },
    };
}();

var errCssSpan = {
    'border-color': '#CC0033',
    'border-width': '1px',
    'border-style': 'solid'
};
var errCssSpan2 = {
    'border-color': '#CC0033',
    'border-width': '2px',
    'border-style': 'solid'
};

function removeRedBorder($dom) {
    $dom.css('border-color', '');
    $dom.css('border-width', '');
    $dom.css('border-style', '');
}

String.prototype.replaceAll = function (reallyDo, replaceWith, ignoreCase) {
    if (!RegExp.prototype.isPrototypeOf(reallyDo)) {
        return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi" : "g")),
            replaceWith);
    } else {
        return this.replace(reallyDo, replaceWith);
    }
};

Date.prototype.format = function (format) {
    var o = {
        "M+": this.getMonth() + 1, // month
        "d+": this.getDate(), // day
        "h+": this.getHours(), // hour
        "m+": this.getMinutes(), // minute
        "s+": this.getSeconds(), // second
        "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
        "S": this.getMilliseconds() // millisecond
    };
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};

function commonFormValidate(form) {
    var spanArray = form.get(0).getElementsByTagName('span');
    for (var i = 0; i < spanArray.length; i++) {
        if ($(spanArray[i]).hasClass('help-block')) {
            $(spanArray[i]).css('display', 'none');
        }
    }
    var isMarked = '0';
    var validateResult = true;
    var inputArray = form.get(0).getElementsByTagName('input');
    var areaArray = form.get(0).getElementsByTagName('textarea');
    var selectArray = form.get(0).getElementsByTagName('select');
    for (var i = 0; i < inputArray.length; i++) {
        removeRedBorder($(inputArray[i]));
        if ($(inputArray[i]).attr('rq') == '*') {
            if (isEmpty($(inputArray[i]).val())) {
                if ($(inputArray[i]).next('div').children('span').children('span')) {
                    $(inputArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
                } else if ($(inputArray[i]).next('div').children('span[class="help-block"]') || $(inputArray[i]).next('span').hasClass('help-block')) {
                    $(inputArray[i]).next('span').next('div').prepend('<span class="help-block" style="top:0px;position:absolute;width:200px"><font color="#CC0033">该项为必填*<font></span>')
                } else {
                    $(inputArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
                }
                $(inputArray[i]).css(errCssSpan);
                if (isMarked == '0') {
                    isMarked = '1';
                    validateResult = false;
                }
            }
        } else {
            if ($(inputArray[i]).attr('rq') == 'tel') {
                if (!checkTel(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'telWithoutNull') {
                if (!checkTelWithoutNull(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'ltel') {
                if (!checkLTel(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'ltelWithoutNull') {
                if (!checkLTelWithoutNull(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'email') {
                if (!checkEmail(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'wordOnly') {
                if (!checkWordOnly(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'numOnly') {
                if (!checkNumOnly(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'positiveNumOnly') {
                if (!checkPositiveNumOnly(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'price') {
                if (!checkPrice(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'url') {
                if (!checkURL(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'IDCard') {
                if (!checkIDCard(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'douNum') {
                if (!checkDoubleNumber(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'nullDouNum') {
                if (!checkNullDoubleNumber(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'UserName') {
                if (!checkUserName(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'mobile') {
                if (!checkMobile(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            } else if ($(inputArray[i]).attr('rq') == 'rpwd') {
                if (!checkPassword(inputArray[i])) {
                    if (isMarked == '0') {
                        isMarked = '1';
                        validateResult = false;
                    }
                }
            }
        }
    }
    for (var i = 0; i < areaArray.length; i++) {
        removeRedBorder($(areaArray[i]));
        if ($(areaArray[i]).attr('rq') == '*') {
            if (isEmpty($(areaArray[i]).val())) {
                if ($(areaArray[i]).next('div').children('span').children('span')) {
                    $(areaArray[i]).next('div').prepend('<span class="help-block" style="top:0px;position:absolute;width:200px"><font color="#CC0033">该项为必填*<font></span>')
                } else {
                    $(areaArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
                }
                $(areaArray[i]).css(errCssSpan);
                if (isMarked == '0') {
                    isMarked = '1';
                    validateResult = false;
                }
            }
        }
    }
    for (var i = 0; i < selectArray.length; i++) {
        removeRedBorder($(selectArray[i]));
        if ($(selectArray[i]).attr('rq') == '*') {
            if (isEmpty($(selectArray[i]).val())) {
                $(selectArray[i]).after('<span class="help-block"><font color="#CC0033">该项为必选*<font></span>')
                $(selectArray[i]).css(errCssSpan);
                if (isMarked == '0') {
                    isMarked = '1';
                    validateResult = false;
                }
            }
        }
    }

    return validateResult;
}

// 加载loading遮罩层
function loading(type) {
    if (isEmpty(type))
        return layer.load();
    else
        return layer.load(type);
}

// 关闭弹出层
function closeLayerWindow(index) {
    layer.close(index);
}

// 提示框
function showbsMsg(msg, type, showCloseButton, otherParam) {
    var type = isEmpty(type) ? 'success' : type;
    var msg;
    if (isEmpty(msg)) {
        msg = type == 'success' ? '操作成功' : '操作失败';
    }
// var showCloseButton = isEmpty(showCloseButton)?false:true;
    var msger = parent.Messenger();
    if (!isEmpty(otherParam)) {
        var parentIndex = otherParam.parentIndex;
        if (parentIndex == 0) parentIndex = '0';
        if (!isEmpty(parentIndex)) {
            if (parentIndex == '0')
                msger = Messenger();
            else if (parentIndex == 1)
                msger = parent.Messenger();
            else if (parentIndex == 2)
                msger = parent.parent.Messenger();
        }
    }
    msger.post({
        message: msg,
        type: type,
        showCloseButton: true
    });
}

// 重新加载layer数据网格
function reloadLayerTable(selector) {
    layui.use('table', function () {
        var table = layui.table;
        var selector = isEmpty(selector) ? 'datagrid' : selector;
        table.reload(selector, {});
    });
}

/**
 * 验证手机号：移动电话
 *
 * @param object
 */
function checkVfMobile(object) {
    var phone = object.value;
    var patrn = /^(\+?0?86\-?)?1[345789]\d{9}$/;
    // 判断phone是否为空
    if (phone.length != 0) {
        if (0 <= phone.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(phone)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证电子邮箱
 *
 * @param object
 */
function checkVfEmail(object) {
    var email = object.value;
    var patrn = /^([a-z0-9A-Z]+[_|-|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
    if (email.length != 0) {
        if (0 <= email.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">邮箱不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(email)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的邮箱！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证邮政编码
 *
 * @param object
 */
function checkVfZip(object) {
    var zip = object.value;
    var patrn = /^[0-9][0-9]{5}$/;
    if (zip.length != 0) {
        if (0 <= zip.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">邮政编码不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(zip)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的邮政编码！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证区号
 *
 * @param object
 */
function checkVfQh(object) {
    var qh = object.value;
    var patrn = /^[0-9]{3,4}$/;
    if (qh.length != 0) {
        if (0 <= qh.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">区号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(qh)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的区号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证座机号
 *
 * @param object
 */
function checkVfTel(object) {
    var tel = object.value;
    var patrn = /^[0-9]{7,8}$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">电话不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的固定电话！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

/**
 * 验证分机号
 */
function checkVfDisTel(object) {
    var tel = object.value;
    var patrn = /^[0-9]{3,4}$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">分机号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的分机号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

function isEmpty(obj) {
    if (obj == undefined || obj == null || obj == "" || obj == "undefined") {
        return true;
    } else {
        return false;
    }
}

function trimStr(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

function trimBrStr(str) {
    return str.replace(/\n/g, "");
}

function checkTel(object) {
    var phone = object.value;
    var patrn = /^(\+?0?86\-?)?1[345789]\d{9}$/;
    if (phone.length == 0) {
// object.placeholder = "手机号不能为空！";
// $(object).after('<span class="help-block"><font
// color="#CC0033">手机号不能为空！<font></span>')
// $(object).css(errCssSpan);
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "手机号不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的手机号！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}


function checkPwd(object) {
    var phone = object.value;
// var patrn =
// /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)(?![a-zA-z\d]+$)(?![a-zA-z!@#$%^&*]+$)(?![\d!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]+$/;
// var patrn = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,}$/;
// var patrn =
// /^(?!\d+$)(?![a-zA-Z]+$)(?![a-zA-Z-`=\\;',./~!@#$%^&*()_+|{}:"<>?]+$)(?![0-9-`=\\;',./~!@#$%^&*()_+|{}:"<>?]+$)/;
    var patrn = /^((?!\d+$)(?![a-zA-Z]+$)[a-zA-Z\d@#$%^&_+].{7,})+$/;
    if (phone.length == 0) {
// object.placeholder = "固定电话不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">密码不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">密码不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">密码至少8位，必有字母和数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkLTel(object) {
    var phone = object.value;
    var patrn = /^((\d{3,4}\-)|)\d{7,8}(|([-\u8f6c]{1}\d{1,5}))$/;
    if (phone.length == 0) {
// object.placeholder = "固定电话不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">固定电话不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">固定电话不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的固定电话！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkEmail(object) {
    var phone = object.value;
    var patrn = /^([a-z0-9A-Z]+[_|-|\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\.)+[a-zA-Z]{2,}$/;
    if (phone.length == 0) {
// object.placeholder = "邮箱不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>')
        $(object).css(errCssSpan);
        return false;
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "手机号不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">邮箱不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的手机号！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的邮箱！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkWordOnly(object) {
    var phone = object.value;
    var patrn = /^[\u4E00-\u9FA5]{1,}$/;
    if (phone.length == 0) {
// object.placeholder = "不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">文本不能为空！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">文本不能有空格！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的文字！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkNumOnly(object) {
    var phone = object.value;
    var patrn = /^\d{1,}$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        object.setCustomValidity('请输入正确的11位电话号码');
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkPositiveNumOnly(object) {
    var phone = object.value;
    var patrn = /^\+?[1-9]\d*$/;
    ;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">整数不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">整数不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的正整数！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkPrice(object) {
    var phone = object.value;
    if (phone.indexOf(',') > 0)
        phone = phone.replace(/,/g, '');
// var patrn = /^\d+(?:\.\d{1,2})?$/;
    var patrn = /^(-)?\d{0,14}(\.\d{1,2})?$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkFourDotNum(object) {
    var phone = object.value;
    var patrn = /^(-)?0$|^(-)?[1-9]\d{0,15}$|^(-)?[1-9]\d{0,15}\.{1}\d{1,4}$|^(-)?0\.{1}\d{1,4}$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">小数不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">小数不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">至少4位小数！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkPriceNegativeAllow(object) {
    var phone = object.value;
    var patrn = /^(-)?\d+(\.\d+)?$/;
    if (phone.length == 0) {
// object.placeholder = "数字不能为空！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的文字！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的数字！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}


function checkTelWithoutNull(object) {
    var phone = object.value;
    var patrn = /^(\+?0?86\-?)?1[345789]\d{9}$/;
    if (phone.length == 0) {
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "手机号不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的手机号！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkLTelWithoutNull(object) {
    var phone = object.value;
    var patrn = /^((\d{3,4}\-)|)\d{7,8}(|([-\u8f6c]{1}\d{1,5}))$/;
    if (phone.length == 0) {
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">固定电话不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的固定电话！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

function checkURL(object) {
    var phone = object.value;
    var qqq = "((http|ftp|https)://)(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,6})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,4})*(/[a-zA-Z0-9\&%_\./-~-]*)?";
// var patrn = "^((https|http|ftp|rtsp|mms)?://)"
// + "?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?"
// //ftp的user@
// + "(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
// + "|" // 允许IP和DOMAIN（域名）
// + "([0-9a-zA-Z_!~*'()-]+\.)*" // 域名- www.
// + "([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\." // 二级域名
// + "[a-zA-Z]{2,6})" // first level domain- .com or .museum
// + "(:[0-9]{1,4})?" // 端口- :80
// + "((/?)|"
// + "(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";

    var re = new RegExp(qqq);
    var result = re.test(phone);
    if (phone.length == 0) {
        /*$(object).after('<span class="help-block"><font color="#CC0033">请求地址不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;*/
        return true;
    } else if (0 <= phone.indexOf(' ')) {
// object.placeholder = "固定电话不能有空格！";
        $(object).after('<span class="help-block"><font color="#CC0033">请求地址不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!result) {
// object.placeholder = "请输入正确的固定电话！";
        $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的请求地址！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证项目名称
function checkProjectName(object) {
    var phone = object.value;
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = /^.{5,}$/;
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">项目名不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
        $(object).after('<span class="help-block"><font color="#CC0033">项目名不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (phone.length < 5) {
        $(object).after('<span class="help-block"><font color="#CC0033">5个字符以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
        $(object).after('<span class="help-block"><font color="#CC0033">必填5位以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证文本框
function checkTextArea(object) {
    var phone = object.value;
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = /^.{10,}$/;
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (phone.length < 5) {
        $(object).after('<span class="help-block"><font color="#CC0033">10个字符以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
        $(object).after('<span class="help-block"><font color="#CC0033">必填10位以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证15位整数加2位小数
function checkjva(object) {
    var phone = object.value;
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = /^(-)?\d{0,14}(\.\d{1,2})?$/;
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">数字不能为空!<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (0 <= phone.indexOf(' ')) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能有空格！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!patrn.exec(phone)) {
        $(object).after('<span class="help-block"><font color="#CC0033">输入类型不匹配或长度超过限制<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}

// 验证输入文本最小长度
function checkMinimumLength(minimumLength, object) {
    var phone = trimBrStr(object.value);
// var patrn = /^[\u4e00-\u9fa5]{5,}$|^.{10,}$/;
    var patrn = '.{' + minimumLength + ',}';
    var reg = new RegExp(patrn);
    if (phone.length == 0) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能为空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } /*
		 * else if (0 <= phone.indexOf(' ')) { $(object).after('<span
		 * class="help-block"><font color="#CC0033">内容不能有空格！<font></span>')
		 * $(object).css(errCssSpan); return false; }
		 */ else if (Utils.isEmpty(phone.replace(/ /g, ''))) {
        $(object).after('<span class="help-block"><font color="#CC0033">内容不能都是空！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else if (!phone.match(reg)) {
        $(object).after('<span class="help-block"><font color="#CC0033">必填' + minimumLength + '位以上！<font></span>')
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}


// 给li下拉选项添加点击事件
function clickLi(obj) {
    // $(".entrpCodeInput").val($(obj).attr("code"));
    $(obj).parent('.entrpDiv').prev().find('.entrpCodeInput').val($(obj).attr("code"));
}

// 给li下拉选项添加点击事件
function clickNameLi(obj) {
    // $(".entrpNameInput").val($(obj).attr("punitName"));//获得选中的名称
    $(obj).parent('.entrpDiv').prev().find('.entrpNameInput').val($(obj).attr("punitName"));// 获得选中的名称
    // $(".entrpNameCode").val($(obj).attr("code"));//获得选中的代码
    $(obj).parent('.entrpDiv').prev().find('.entrpNameCode').val($(obj).attr("code"));// 获得选中的代码
    var allParentsNode = $(obj).parents();
    allParentsNode.each(function () {
        if ($(this).is('form')) {
            var idStr = $(this).attr('id');
            if (idStr == 'searchFormId') {
                searchmore();
            }
            if (idStr == 'searchFormId_children') {
                searchmore_chaldren();
            }
            if (idStr == 'searchFormId2') {
                searchmore2();
            }
        }
    })

}

// 填报月份页面上显示处理
// strs为时间字符串
function writeDeal(date, type) {

    var strs = "" + date + "";
    strs = strs.split("-");
    var year = strs[0];
    var month = strs[1];
    var day = strs[2];
// if(month==10&&day<21){
// month=9;
// }
    if (type == '月报' && (month == 1 && day < 9)) {
        year = year - 1;
        month = 12;
    }
    else if (type == '月报' && (month != 1 && day < 9)) {
        month = month - 1;
    }
    if (!isEmpty(month)) {
        var time;
        if (type == '月报') {
            time = year + "年" + (month - 0) + "月";
        } else if (type == '半年报') {
            if (month == 7) {
                time = year + "年半年报调整";
            }
        } else if (type == '年报') {
            if (month == 1 || month == 2) {
                time = (year - 1) + "年年报调整";
            }
        }
        return time;
    } else {
        return year + "年合计";
    }
}

// 处理报送期
function dealtime(date) {
    if (!isEmpty(date)) {
        var strs = "" + date + "";
        strs = strs.split("-");
        var year = strs[0];
        var month = strs[1];
        var day = strs[2];
        if (month == 1 && day < 9) {
            year = year - 1;
            month = 12;
        }
        else if (month != 1 && day < 9) {
            month = month - 1;
        }
        time = year + "-" + month;

        return time;
    } else {
        return '';
    }
}

/**
 * 选中年份时间
 *
 * @param aaa
 */
function chooseYear(aaa) {
    $(aaa).datetimepicker({
        startView: 'decade',
        minView: 'decade',
        format: 'yyyy',
        maxViewMode: 2,
        minViewMode: 2,
        startDate: -Infinity,
        endDate: year,
        autoclose: true
    });
}

function loadNumInput() {
    $(".intValue").bind('input propertychange', function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
        }
        var value = $(this).val();
        var re = /^[+-]?[1-9]?[0-9]*$/;
        if (!re.test(value) && value != "" && value != "0" && value != "-" && value != "+") {
            $(this).val(oldValue);
        }
    }).click(function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
            var t = $(this).val();
            $(this).val("").focus().val(t);
        }
    }).keydown(function () {
        oldValue = $(this).val();
    }).blur(function () {
        var val = $(this).val();
        if (val == "" || val == "-" || val == "+") {
            $(this).val("0");
        }
        $(this).css("textAlign", "right");
    });


    $(".doubleValue").bind('input propertychange', function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
        }
        var value = $(this).val();
        var re = /^[+-]?[1-9]?[0-9]*$/;
        var re2 = /^[+-]?[1-9]?[0-9]*\.[0-9]*$/;
        if (!re2.test(value) && !re.test(value) && value != "" && value != "0" && value != "-" && value != "+") {
            $(this).val(oldValue);
        }
    }).click(function () {
        if ($(this).css("textAlign") == "right") {
            $(this).css("textAlign", "left");
            var t = $(this).val();
            $(this).val("").focus().val(t);
        }
    }).keydown(function () {
        oldValue = $(this).val();
    }).blur(function () {
        var val = $(this).val();
        if (val == "" || val == "0" || val == "-" || val == "+") {
            $(this).val("0.0");
        }
        $(this).css("textAlign", "right");
    });

}

/**
 * 验证是否可修改
 *
 * @param nowDate：系统当前时间
 * @param fillDate：填报时间
 * @param recordType：填报类型
 */
function checkEditStatus(nowDate, fillDate, recordType) {
    // 判断recordType类型
// if(recordType == "半年报"|| recordType == "年报"){
// return true;
// }
    var showNowDate = writeDeal(nowDate, recordType);
    var showFillDate = writeDeal(fillDate, recordType);
    // 判断是否 相等
    console.log(nowDate + "---" + fillDate);
    console.log(showNowDate + "-" + showFillDate);
    if (showNowDate != showFillDate) {
        return false;
    } else {
        return true;
    }


}

// 将数字添加千分符
function toThousands(num) {
    var result = '0', counter = 0;
    if (!isEmpty(num)) {
        /*
         * if(!isNaN(num)) num = parseFloat(num).toFixed(2);
         */
        num = (num || 0).toString();
        if (num.indexOf(',') > 0)
            num = num.replace(/,/g, '');
        var isBelowZero = false;
        if (num.indexOf('-') == 0) {
            isBelowZero = true;
            num = num.substring(1);
        }
        var re = /\d{1,3}(?=(\d{3})+$)/g;
        var n1 = num.replace(/^(\d+)((\.\d+)?)$/, function (s, s1, s2) {
            return s1.replace(re, "$&,") + s2;
        });
        if (isBelowZero)
            n1 = '-' + n1;
        return n1;
    }
    return result;
}

// 将数字去除千分符
function removeThousands(num) {
    return isEmpty(num) ? '0' : num.replace(/,/g, '');
}

function isNum(val) {
    var reg = /^[0-9]+.?[0-9]*$/;
    if (reg.test(val)) {
        return true;
    }
    return false;
}

function getFloat(num) {
    if (!Utils.isEmpty(num) && !isNaN(num)) {
        return parseFloat(num).toFixed(2);
    } else if (isNaN(num)) {
        return num;
    }
    else {
        return 0.0;
    }
}

/**
 * 获取合计行在datgrid中的行数
 *
 * @param yearNum
 *            当前合计行年份
 * @param rows
 *            数据集
 * @param index
 *            当前合计索引
 */
function getTotalRowNum(yearNum, rows, index) {
    if (index == 0) {
        return 0;
    }
    for (var i = 0; i < rows.length; i++) {
        var yearStr = writeDeal(rows[i].TIME, rows[i].RECORDTYPENAME);
        if (yearStr.indexOf(yearNum) >= 0) {
            index = i;
            break;
        }
    }
    return index;
}

// 方法2：填报类型字段名称不同
function getTotalRowNum1(yearNum, rows, index) {
    if (index == 0) {
        return 0;
    }
    for (var i = 0; i <= rows.length; i++) {
        var yearStr = writeDeal(rows[i].TIME, rows[i].WRITETYPE);
        if (yearStr.indexOf(yearNum) >= 0) {
            index = i;
            break;
        }
    }
    return index;
}

function isContainsChineseWord(str) {
    var result = false;
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 255) {
            result = true;
            break;
        }
    }
    return result;
}

//验证字母/数字/下划线
function checkUserName(object) {
    var tel = object.value;
    var patrn = /^[a-z0-9_-]{5,20}$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入5-20为字母、数字、下划线<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证身份证
function checkIDCard(object) {
    var tel = object.value;
    var patrn = /^\d{15}(\d\d[0-9xX])?$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的身份证号码<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证可为空小数或整数
function checkNullDoubleNumber(object) {
    var tel = object.value;
    var patrn = /^(\d+)(\.\d+)?$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的小数或整数<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        return true;
    }
}

//验证非空小数或整数
function checkDoubleNumber(object) {
    var tel = object.value;
    var patrn = /^(\d+)(\.\d+)?$/;
    if (tel.length != 0) {
        if (0 <= tel.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">中间不可有空格<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(tel)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的小数或整数<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证小数
/**
 * 验证手机号：移动电话
 *
 * @param object
 */
function checkMobile(object) {
    var phone = object.value;
    var patrn = /^1\d{10}$/;
    // 判断phone是否为空
    if (phone.length != 0) {
        if (0 <= phone.indexOf(' ')) {
            $(object).after('<span class="help-block"><font color="#CC0033">手机号不能有空格！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else if (!patrn.exec(phone)) {
            $(object).after('<span class="help-block"><font color="#CC0033">请输入正确的手机号！<font></span>');
            $(object).css(errCssSpan);
            return false;
        } else {
            object.placeholder = '';
            return true;
        }
    } else {
        $(object).after('<span class="help-block"><font color="#CC0033">该项为必填*<font></span>');
        $(object).css(errCssSpan);
        return false;
    }
}

//验证确认密码和密码是否一致
function checkPassword(object) {
    var rpwd = object.value;
    var pwd = $('#pwd').val();

    if (rpwd != pwd) {
        $(object).after('<span class="help-block"><font color="#CC0033">两次输入的密码不一致！<font></span>');
        $(object).css(errCssSpan);
        return false;
    } else {
        object.placeholder = '';
        return true;
    }
}