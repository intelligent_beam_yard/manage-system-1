var api = '/admin/api';

/**
 * 接口地址
 */
var api = function() {
	return {
		// 登录
		login: {
			login: api + "/login/login", //登录
			logOut: api + "/login/logOut", //注销
		},
		// 枚举
		enums: {
			allEnums: api + "/enums/systemEnums/getAllEnums", //获取所有枚举
			getByName: api + "/enums/systemEnums/getEnumsByName", //根据名称获取枚举对象
		},
		//菜单
		menu: {
            menuTree: api + "/base/baseMenuInfo/menuTreeForAdmin", //admin端所有菜单维护
            addOrUpdate: api + "/base/baseMenuInfo/addOrUpdate", //菜单新增/修改
            delete: api + "/base/baseMenuInfo/logicDelete", //菜单删除
		},
		//权限
		operator: {
			menuOperator: api + "/base/baseOperatorInfo/listOperator", //根据权限获取菜单和权限数据
			listAllBy: api + "/base/baseOperatorInfo/listAllBy", //查询所有
		},
        //用户
        baseUserInfo: {
            listPage: api + "/base/baseUserInfo/listPage", //分页查询
            addOrUpdate: api + "/base/baseUserInfo/addOrUpdate", //新增/修改
            delete: api + "/base/baseUserInfo/logicDelete", //删除
            findByParam: api + "/base/baseUserInfo/findByParam", //单查
        },
	};
}();