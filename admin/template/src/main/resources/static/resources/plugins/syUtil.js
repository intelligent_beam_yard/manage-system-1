
/**
 * @requires jQuery
 * 
 * 将form表单元素的值序列化成对象
 * 
 * @returns object
 */
serializeObject = function(form) {
	var o = {};
	$.each(form.serializeArray(), function(index) {
		var val = this['value'];
		//解决字符串传入后台参数date类型不匹配的问题
		if(!Utils.isEmpty(val)){
			if(Utils.isStrDate(val))
				val = Utils.stringToDate(val);
		}
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + "," + val;
		} else {
			o[this['name']] = val;
		}
	});
	return o;
};

