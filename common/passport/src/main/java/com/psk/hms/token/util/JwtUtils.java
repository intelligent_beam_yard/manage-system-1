package com.psk.hms.token.util;

import com.alibaba.fastjson.JSONObject;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.token.constant.TokenConstant;
import com.psk.hms.token.entity.CheckResult;
import io.jsonwebtoken.*;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

/**
 * jwt加密和解密的工具类 
 * 创建时间 2018年04月23日
 */
public class JwtUtils {
	
	public static String createJWT(String id, String subject, long ttlMillis) {
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		SecretKey secretKey = generalKey();
		JwtBuilder builder = Jwts.builder()
				.setId(id)
				.setSubject(subject)   // 主题
				.setIssuer("轻筑")     // 签发者
				.setIssuedAt(now)      // 签发时间
				.signWith(signatureAlgorithm, secretKey); // 签名算法以及密匙
		if (ttlMillis >= 0) {
			long expMillis = nowMillis + ttlMillis;
			Date expDate = new Date(expMillis);
			builder.setExpiration(expDate); // 过期时间
		}
		return builder.compact();
	}
	/**
	 * 验证JWT
	 * @param jwtStr
	 * @return
	 */
	public static CheckResult validateJWT(String jwtStr) {
		CheckResult checkResult = new CheckResult();
		Claims claims = null;
		try {
			claims = parseJWT(jwtStr);
			checkResult.setSuccess(true);
			checkResult.setClaims(claims);
		} catch (ExpiredJwtException e) {
			checkResult.setErrCode(BaseBizExceptionCode.TOKEN_VALIDATE_EXPIRED_ERROR);
			checkResult.setSuccess(false);
		} catch (SignatureException e) {
			checkResult.setErrCode(BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR);
			checkResult.setSuccess(false);
		} catch (Exception e) {
			checkResult.setErrCode(BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR);
			checkResult.setSuccess(false);
		}
		return checkResult;
	}
	public static SecretKey generalKey() {
		byte[] encodedKey = Base64.decode(TokenConstant.JWT_SECERT);
	    SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
	    return key;
	}
	
	/**
	 * 
	 * 解析JWT字符串
	 * @param jwt
	 * @return
	 * @throws Exception
	 */
	public static Claims parseJWT(String jwt) throws Exception {
		SecretKey secretKey = generalKey();
		return Jwts.parser()
			.setSigningKey(secretKey)
			.parseClaimsJws(jwt)
			.getBody();
	}
	public static void main(String[] args) throws InterruptedException {
		//小明失效 10s
		JSONObject json = new JSONObject();
		json.put("userId", "03a44ba0aa4e4905bea726d4da976ba5");
		json.put("employeeId", "3abcbfd1bf0248dfa4f0c922d10a299d");
		json.put("teamId", "91c834e001594796a81972ea6fc91713");
		String token = JwtUtils.createJWT(json.toString(), "登录token", TokenConstant.JWT_MAX_TTL_MILLISECOND);
		
		System.out.println(token);
	}
}
