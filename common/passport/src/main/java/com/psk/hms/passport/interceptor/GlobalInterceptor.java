package com.psk.hms.passport.interceptor;

import com.psk.hms.base.constant.base.BaseOperatorInfoConstant;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.passport.interceptor.constant.WebConstant;
import com.psk.hms.passport.util.WebUtil;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.service.LogOperatorService;
import com.psk.hms.token.util.TokenUtil;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;


/**
 * 描述： 全局拦截器  
 * 1 全局变量的设置
 * 2 日志记录开始和结束操作
 * 作者： xiangjz
 * 时间： 2018年4月2日
 * 版本： 1.0v
 */
public class GlobalInterceptor extends Interceptor{
	
	private BaseOperatorInfoService baseOperatorInfoService;
	private LogOperatorService logOperatorService;

	public GlobalInterceptor(
			BaseOperatorInfoService baseOperatorInfoService,
			LogOperatorService logOperatorService
	) {
		this.baseOperatorInfoService = baseOperatorInfoService;
		this.logOperatorService = logOperatorService;
	}
	

	@SuppressWarnings("unchecked")
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception exception) throws Exception {
		if(log.isDebugEnabled()) log.debug("请求处理完毕，计算耗时");
		
		Map<String, Object> logOperatorMap = (Map<String, Object>) request.getSession().getAttribute(WebConstant.REQUEST_LOG_OPERATOR_KEY);
		
		if(logOperatorMap == null)
			return;
		
		// 结束时间
		long endtime = DateUtil.getDateByTime();
		logOperatorMap.put("controllerEndTime", DateUtil.getSqlTimestamp(endtime));
		
		// 总耗时
		Long controllerProcessingTime = endtime - ((Timestamp)logOperatorMap.get("controllerStartTime")).getTime();
		logOperatorMap.put("controllerProcessingTime", controllerProcessingTime);
		
		if(log.isDebugEnabled()) log.debug("操作日志保存");
		
		Map<String, Object> operatorParams = new HashMap<String, Object>();
		operatorParams.put("url", request.getRequestURI());

		Map<String, Object> operatorMap = (Map<String, Object>) baseOperatorInfoService.findBy(operatorParams);
		
		if(BaseOperatorInfoConstant.SYS_LOG_YES.equals(operatorMap.get("sysLog"))){  //需要记录日志
			logOperatorMap.put("sysLog", BaseOperatorInfoConstant.SYS_LOG_YES);
			logOperatorMap.put("operatorId", operatorMap.get("id"));
			
			try {
				logOperatorMap.put("userId", TokenUtil.getUserId(request));
			} catch (Exception e) {
				log.warn("操作日志保存记录的时候没有从token里面获取到userId");
			}

			logOperatorService.save(logOperatorMap);

			//messageProducer.sendMessage(JSONObject.toJSONString(logOperatorMap));
		}
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView) throws Exception {

	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {

		if(log.isDebugEnabled()) log.debug("初始化访问系统功能日志");
		Map<String, Object> logOperatorMap = initLogOperator(request);
		Timestamp startTime = DateUtil.getSqlTimestamp();
		logOperatorMap.put("controllerStartTime", DateUtil.getSqlTimestamp(startTime)); // 开始时间
		request.getSession().setAttribute(WebConstant.REQUEST_LOG_OPERATOR_KEY, logOperatorMap);

		return true;
	}
	
	/**
	 * 创建日志对象,并初始化一些属性值
	 * @param request
	 * @return
	 */
	public Map<String, Object> initLogOperator(HttpServletRequest request){
		
		String requestPath = WebUtil.getRequestURIWithParam(request);
		String ip = WebUtil.getIpAddr(request);
		String referer = request.getHeader("Referer"); 
		String userAgent = request.getHeader("User-Agent");
		String cookie = request.getHeader("Cookie");
		String method = request.getMethod();
		String xrequestedWith = request.getHeader("X-Requested-With");
		String host = request.getHeader("Host");
		String acceptLanguage = request.getHeader("Accept-Language");
		String acceptEncoding = request.getHeader("Accept-Encoding");
		String accept = request.getHeader("Accept");
		String connection = request.getHeader("Connection");

		Map<String, Object> logOperator = new HashMap<String, Object>();
		
		logOperator.put("id", StringUtil.getUuid(true));
		logOperator.put("cookie", cookie);
		logOperator.put("ip", ip);
		logOperator.put("method", method);
		logOperator.put("referer", referer);
		logOperator.put("requestPath", requestPath);
		logOperator.put("userAgent", userAgent);
		logOperator.put("accept", accept);
		logOperator.put("acceptEncoding", acceptEncoding);
		logOperator.put("acceptLanguage", acceptLanguage);
		logOperator.put("connection", connection);
		logOperator.put("host", host);
		logOperator.put("xrequestWith", xrequestedWith);

		return logOperator;
	}
}
