package com.psk.hms.token.controller;

import com.alibaba.fastjson.JSONObject;
import com.psk.hms.base.constant.common.RedisConstant;
import com.psk.hms.base.controller.BaseController;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.security.Pbkdf2Util;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.redis.service.RedisService;
import com.psk.hms.token.constant.TokenConstant;
import com.psk.hms.token.entity.CheckResult;
import com.psk.hms.token.util.JwtUtils;
import com.psk.hms.token.util.TokenUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

/**
 * Token控制器
 * 
 * 
 * @author xiangjz
 * @date 2018年4月25日
 * @version 1.0.0
 *
 */
@RestController
@RequestMapping("/token")
public class TokenController extends BaseController{

	@Autowired
	private RedisService redis;

	/**
	 * 刷新token
	 * 
	 * @param token
	 *            待刷新的token
	 * @return
	 */
	@GetMapping("/refresh")
	public JsonResult refresh(@RequestParam String token) {
		JsonResult result = new JsonResult();
		// 进行参数校验
		if (StringUtil.isEmpty(token))
			throw new BizException(BaseBizExceptionCode.TOKEN_VALIDATE_NULL_ERROR, "Path paramerters 'token' are required");

		// 检测token是否合法
		CheckResult tokenResult = JwtUtils.validateJWT(token);
		if (tokenResult.isSuccess()) {
			JSONObject tokenData = JSONObject.parseObject(tokenResult.getClaims().getId());
			// 生成新的token
			String newToken = JwtUtils.createJWT(tokenData.toJSONString(), "登录token", TokenConstant.JWT_TTL_MILLISECOND);
			//放入缓存
			redis.set(TokenConstant.ACCESS_TOKEN+"_"+token, token, TokenConstant.JWT_MAX_TTL_SECOND);

			Map<String, Object> data = new HashMap<>();
			data.put(TokenConstant.ACCESS_TOKEN, newToken);
			result.success(data);
		} else {
			switch (tokenResult.getErrCode()) {
			// 签名验证不通过
			case BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR:
				throw new BizException(tokenResult.getErrCode(), "Invalid token");
			default:
				result.failure();
			}
		}
		return result;
	}
	
	/**
	 * 生成token
	 * 
	 * @param userId 用户id
	 * 
	 * @return
	 */
	/*@GetMapping("/create")
	public JsonResult create(@RequestParam String userId) {
		JsonResult result = new JsonResult();
		JSONObject tokenJson = new JSONObject();
		tokenJson.put("userId", userId);
		String token = JwtUtils.createJWT(tokenJson.toJSONString(), "测试", TokenConstant.JWT_TTL_MILLISECOND);
		return result.success(token);
	}*/
	
	/**
	 * 是否过期
	 * 
	 * @param token
	 * 
	 * @return
	 */
	@GetMapping("/isExpire")
	public JsonResult isExpire(@RequestParam String token) {
		JsonResult result = new JsonResult();
		// 进行参数校验
		if (StringUtil.isEmpty(token))
			throw new BizException(BaseBizExceptionCode.TOKEN_VALIDATE_NULL_ERROR, "token不存在");

		// 检测token是否合法
		CheckResult tokenResult = JwtUtils.validateJWT(token);
		Map<String, Object> data = new HashMap<String, Object>();
		if (tokenResult.isSuccess()) {
			data.put("isExpire", TokenConstant.IS_EXPIRE_NO);
			result.success(data);
		} else {
			data.put("isExpire", TokenConstant.IS_EXPIRE_YES);
			result.success(data);
		}
		return result;
	}
	
	/**
	 * 解析access-token并返回有效的验证thirdAccessCode 和 signature
	 * thirdAccessCode 有效时长5分钟
	 * @return
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	@RequestMapping("/getAccessTokenCode")
	public JsonResult getAccessTokenCode(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException, InvalidKeySpecException{
		JsonResult jsonResult = new JsonResult();
		Map<String, Object> analyzeResult = TokenUtil.getCurrentTokenInfo(request);
		JSONObject analyzeResultJson = new JSONObject(analyzeResult);
		
		byte[] signature = Pbkdf2Util.generateSalt();
		String accessTokenCode = Base64.encodeBase64String(Pbkdf2Util.getEncryptedPassword(analyzeResultJson.toString(), signature)).replaceAll("/", "");
		
		//加入缓存  key=accessTokenCode  value=accessTokenCode+signature
		Map<String, Object> setParams = new HashMap<>();
		setParams.put(RedisConstant.KEY, accessTokenCode);
		setParams.put(RedisConstant.VALUE, accessTokenCode + Base64.encodeBase64String(signature) + "###" + analyzeResultJson.toJSONString());
		setParams.put(RedisConstant.EXPIRE_TIME, (long)5 * 60);

		redis.set(accessTokenCode, accessTokenCode + Base64.encodeBase64String(signature) + "###" + analyzeResultJson.toJSONString(), (long)5 * 60);

		Map<String, Object> data = new HashMap<>();
		data.put("accessTokenCode", accessTokenCode);
		data.put("signature", signature);
		jsonResult.success(data);
		return jsonResult;
	}	
}
