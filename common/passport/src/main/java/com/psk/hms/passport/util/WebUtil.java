package com.psk.hms.passport.util;

import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.passport.interceptor.constant.WebConstant;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class WebUtil {

	/**
	 * 获取完整请求路径(含内容路径及请求参数)
	 * 
	 * @param request
	 * @return
	 */
	public static String getRequestURIWithParam(HttpServletRequest request) {
		return request.getRequestURI() + (request.getQueryString() == null ? "" : "?" + request.getQueryString());
	}
	
	/**
	 * 获取客户端IP地址
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	/**
	 * 效验Referer有效性
	 * 
	 * @return
	 */
	public static boolean authReferer(HttpServletRequest request) {
		String referer = request.getHeader("Referer");
		if (null != referer && !referer.trim().equals("")) {
			referer = referer.toLowerCase();
			String domainStr = WebConstant.DOMAIN;
			String[] domainArr = domainStr.split(",");
			for (String domain : domainArr) {
				if (referer.startsWith(domain.trim())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 获取请求参数
	 * 
	 * @param request
	 * @param name
	 * @return
	 */
	public static String getParam(HttpServletRequest request, String name) {
		String value = request.getParameter(name);
		if (StringUtil.isNotBlank(value)) {
			try {
				return URLDecoder.decode(value, StringUtil.ENCODING).trim();
			} catch (UnsupportedEncodingException e) {
				return value;
			}
		}
		return value;
	}
}
