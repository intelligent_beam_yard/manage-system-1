package com.psk.hms.passport.interceptor.constant;
/**
 * 描述: 基础web 常量类
 * 作者: xiangjz
 * 时间: 2018年5月17日
 * 版本: 1.0
 *
 */
public class WebConstant {
	
	/**
	 * 当前操作日志request key
	 */
	public static final String REQUEST_LOG_OPERATOR_KEY = "request_log_operator";
	
	/**
	 * 登录地址记录   用户验证referrer
	 */
	public static final String DOMAIN = "http://127.0.0.1, http://localhost, http://10.73.1.205, http://39.108.223.205";

	/**
	 * 表单重复提交验证key
	 */
	public static final String REQUEST_FORMTOKEN = "formToken";
	
}