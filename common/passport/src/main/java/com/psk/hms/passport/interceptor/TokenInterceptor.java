package com.psk.hms.passport.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.redis.service.RedisService;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.service.LogOperatorService;
import com.psk.hms.token.constant.TokenConstant;
import com.psk.hms.token.entity.CheckResult;
import com.psk.hms.token.util.JwtUtils;
import io.jsonwebtoken.Claims;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述: token拦截器
 * 作者： xiangjz
 * 时间： 2018年5月17日
 * 版本： 1.0v
 */
public class TokenInterceptor extends Interceptor {

	private BaseOperatorInfoService baseOperatorInfoService;
	private RedisService redisService;
	private LogOperatorService logOperatorService;

	public TokenInterceptor(
			BaseOperatorInfoService baseOperatorInfoService,
			RedisService redisService,
			LogOperatorService logOperatorService
	) {
		this.baseOperatorInfoService = baseOperatorInfoService;
		this.redisService = redisService;
		this.logOperatorService = logOperatorService;
	}
	
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception exception) throws Exception {
		
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView) throws Exception {

	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		if (object instanceof HandlerMethod) {
			String tokenType = getTokenType(request);
			
			if(TokenConstant.ACCESS_TOKEN.equals(tokenType)){  //accessToken类型
				String uri = request.getRequestURI();
				
				if(urlNotValidate(uri,baseOperatorInfoService))
					return true;
				
				log.debug("获取accessToken");
				String accessToken = request.getHeader(TokenConstant.ACCESS_TOKEN);
				
				
				// * token的获取以缓存存储为主  原因是jwt不能注销   所以托管到redis来做中间层管理  
				// * redis存的token和jwt的有效时间是一致的  当然会有非常小的时间偏差   这种情况下以两者皆有效来当成true处理
				Object accessTokenObject = redisService.get(TokenConstant.ACCESS_TOKEN+"_"+accessToken);

				if(accessTokenObject != null){
					//默认  两小时有效期   5分钟的濒临期    5分钟的宽限期
					CheckResult checkResult = JwtUtils.validateJWT(accessToken);
					
					if(checkResult.isSuccess()){
						Claims claims = checkResult.getClaims();
						
						long nowMillis = System.currentTimeMillis();
						
						if(TokenConstant.JWT_EXTEND_TTL_MILLISECOND < claims.getExpiration().getTime() - nowMillis && claims.getExpiration().getTime() - nowMillis <= TokenConstant.JWT_PREDEATH_TTL_MILLISECOND + TokenConstant.JWT_EXTEND_TTL_MILLISECOND){
							log.debug("accessToken快失效了,刷新accessToken");
							// 在濒临时间之内  刷新accessToken
							//判断缓存是否有已经刷新过的accessToken
							Object refreshAccessTokenObject = redisService.get(TokenConstant.REFRESH_ACCESS_TOKEN+"_"+accessToken);

							//在这之前已经刷新过accessToken  但是由于并发高的原因  此accessToken是旧accessToken
							if(refreshAccessTokenObject != null){
								String refreshedAccessToken = (String) refreshAccessTokenObject;
								//把已经刷新的accessToken放到response的header  返回给前端
								response.setHeader(TokenConstant.ACCESS_TOKEN, refreshedAccessToken);
								return true;
							}else{
								//刷新accessToken
								JSONObject accessTokenData = JSONObject.parseObject(claims.getId());
								String newAccessToken;
								synchronized(TokenInterceptor.class){
									log.debug("并发测试, 刷新accessToken == start");
									// 生成新的token
									newAccessToken = JwtUtils.createJWT(accessTokenData.toJSONString(), "登录accessToken", TokenConstant.JWT_MAX_TTL_MILLISECOND);
									log.debug("并发测试, 刷新accessToken == end");
								}
								if(!StringUtil.isBlank1(newAccessToken)){
									//把刷新的accessToken放到response的header  返回给前端
									response.setHeader(TokenConstant.ACCESS_TOKEN, newAccessToken);
									
									//放入缓存    有效时间   = accessToken的最大有效截至时间 - 当前时间 
									redisService.set(TokenConstant.REFRESH_ACCESS_TOKEN+"_"+accessToken, newAccessToken, (claims.getExpiration().getTime() - nowMillis) / 1000);

									redisService.set(TokenConstant.ACCESS_TOKEN+"_"+newAccessToken, newAccessToken, TokenConstant.JWT_MAX_TTL_SECOND);

									return true;
								}else{
									print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_ACCESSMANAGE_REFRESH_ERROR, "刷新accessToken失败"));
									return false;
								}
							}
						}else if(0 < claims.getExpiration().getTime() - nowMillis && claims.getExpiration().getTime() - nowMillis <= TokenConstant.JWT_EXTEND_TTL_MILLISECOND){
							log.debug("accessToken已经在宽限期内,快失效了");
							//判断缓存是否有已经刷新过的token
							Object refreshAccessTokenObject = redisService.get(TokenConstant.REFRESH_ACCESS_TOKEN+"_"+accessToken);

							//在这之前已经刷新过token  但是由于并发高的原因  此token是旧token
							if(refreshAccessTokenObject != null){
								String refreshedAccessToken = (String) refreshAccessTokenObject;
								//把已经刷新的accessToken放到response的header  返回给前端
								response.setHeader(TokenConstant.ACCESS_TOKEN, refreshedAccessToken);
								return true;
							}else{
								//刷新accessToken
								JSONObject accessTokenData = JSONObject.parseObject(claims.getId());
								String newAccessToken;
								synchronized(TokenInterceptor.class){
									log.debug("并发测试, 刷新accessToken == start");
									// 生成新的accessToken
									newAccessToken = JwtUtils.createJWT(accessTokenData.toJSONString(), "登录accessToken", TokenConstant.JWT_MAX_TTL_MILLISECOND);
									log.debug("并发测试, 刷新accessToken == end");
								}
								if(!StringUtil.isBlank1(newAccessToken)){
									//把刷新的accessToken放到response的header  返回给前端
									response.setHeader(TokenConstant.ACCESS_TOKEN, newAccessToken);
									//放入缓存   有效时间   = token的最大有效截至时间 - 当前时间 
									redisService.set(TokenConstant.REFRESH_ACCESS_TOKEN+"_"+accessToken, newAccessToken, (claims.getExpiration().getTime() - nowMillis) / 1000);

									//accessToken放入缓存
									redisService.set(TokenConstant.ACCESS_TOKEN+"_"+newAccessToken, newAccessToken, TokenConstant.JWT_MAX_TTL_SECOND);
									return true;
								}else{
									print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_ACCESSMANAGE_REFRESH_ERROR, "刷新accessToken失败"));
									return false;
								}
							}
						}else{
							log.debug("accessToken签名验证成功");
							return true;
						}
					}else{
						int error = checkResult.getErrCode();
						if(BaseBizExceptionCode.TOKEN_VALIDATE_EXPIRED_ERROR == error){
							log.debug("accessToken签名过期");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_EXPIRED_ERROR, "accessToken签名过期"));
							return false;
						}else if(BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR == error){
							log.debug("accessToken签名验证失败");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR, "accessToken签名验证失败"));
							return false;
						}else{
							log.debug("accessToken不存在");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_NULL_ERROR, "accessToken不存在"));
							return false;
						}
					}
				}else{
					log.debug("accessToken不存在");
					print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_NULL_ERROR, "accessToken不存在"));
					return false;
				}
			}else if(TokenConstant.AUTH_TOKEN.equals(tokenType)){    //authToken类型
				
				log.debug("获取authToken");
				String authToken = request.getHeader(TokenConstant.AUTH_TOKEN);
				
				
				// * token的获取以缓存存储为主  原因是jwt不能注销   所以托管到redis来做中间层管理  
				// * redis存的token和jwt的有效时间是一致的  当然会有非常小的时间偏差   这种情况下以两者皆有效来当成true处理
				Object authTokenObject = redisService.get(TokenConstant.AUTH_TOKEN+"_"+authToken);

				if(authTokenObject != null){
					//默认  两小时有效期   5分钟的濒临期    5分钟的宽限期
					CheckResult checkResult = JwtUtils.validateJWT(authToken);
					
					if(checkResult.isSuccess()){
						Claims claims = checkResult.getClaims();
						
						long nowMillis = System.currentTimeMillis();
						
						if(TokenConstant.JWT_EXTEND_TTL_MILLISECOND < claims.getExpiration().getTime() - nowMillis && claims.getExpiration().getTime() - nowMillis <= TokenConstant.JWT_PREDEATH_TTL_MILLISECOND + TokenConstant.JWT_EXTEND_TTL_MILLISECOND){
							log.debug("authToken快失效了,刷新authToken");
							// 在濒临时间之内  刷新authToken
							//判断缓存是否有已经刷新过的authToken
							Object refreshAuthTokenObject = redisService.get(TokenConstant.REFRESH_AUTH_TOKEN+"_"+authToken);

							//在这之前已经刷新过accessToken  但是由于并发高的原因  此accessToken是旧accessToken
							if(refreshAuthTokenObject != null){
								String refreshedAuthToken = (String) refreshAuthTokenObject;
								//把已经刷新的authToken放到response的header  返回给前端
								response.setHeader(TokenConstant.AUTH_TOKEN, refreshedAuthToken);
								return true;
							}else{
								//刷新authToken
								JSONObject authTokenData = JSONObject.parseObject(claims.getId());
								String newAuthToken;
								synchronized(TokenInterceptor.class){
									log.debug("并发测试, 刷新authToken == start");
									// 生成新的token
									newAuthToken = JwtUtils.createJWT(authTokenData.toJSONString(), "登录authToken", TokenConstant.JWT_MAX_TTL_MILLISECOND);
									log.debug("并发测试, 刷新authToken == end");
								}
								if(!StringUtil.isBlank1(newAuthToken)){
									//把刷新的authToken放到response的header  返回给前端
									response.setHeader(TokenConstant.AUTH_TOKEN, newAuthToken);
									//放入缓存    有效时间   = authToken的最大有效截至时间 - 当前时间 
									redisService.set(TokenConstant.REFRESH_AUTH_TOKEN+"_"+authToken, newAuthToken, (claims.getExpiration().getTime() - nowMillis) / 1000);

									//authToken放入缓存
									redisService.set(TokenConstant.AUTH_TOKEN+"_"+newAuthToken, newAuthToken, TokenConstant.JWT_MAX_TTL_SECOND);

									return true;
								}else{
									print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_AUTHMANAGE_REFRESH_ERROR, "刷新authToken失败"));
									return false;
								}
							}
						}else if(0 < claims.getExpiration().getTime() - nowMillis && claims.getExpiration().getTime() - nowMillis <= TokenConstant.JWT_EXTEND_TTL_MILLISECOND){
							log.debug("authToken已经在宽限期内,快失效了");
							//判断缓存是否有已经刷新过的token
							Object refreshAuthTokenObject = redisService.get(TokenConstant.REFRESH_AUTH_TOKEN+"_"+authToken);

							//在这之前已经刷新过token  但是由于并发高的原因  此token是旧token
							if(refreshAuthTokenObject != null){
								String refreshedAuthToken = (String) refreshAuthTokenObject;
								//把已经刷新的authToken放到response的header  返回给前端
								response.setHeader(TokenConstant.AUTH_TOKEN, refreshedAuthToken);
								return true;
							}else{
								//刷新authToken
								JSONObject authTokenData = JSONObject.parseObject(claims.getId());
								String newAuthToken;
								synchronized(TokenInterceptor.class){
									log.debug("并发测试, 刷新authToken == start");
									// 生成新的authToken
									newAuthToken = JwtUtils.createJWT(authTokenData.toJSONString(), "登录authToken", TokenConstant.JWT_MAX_TTL_MILLISECOND);
									log.debug("并发测试, 刷新authToken == end");
								}
								if(!StringUtil.isBlank1(newAuthToken)){
									//把刷新的authToken放到response的header  返回给前端
									response.setHeader(TokenConstant.AUTH_TOKEN, newAuthToken);
									//放入缓存   有效时间   = token的最大有效截至时间 - 当前时间 
									redisService.set(TokenConstant.REFRESH_AUTH_TOKEN+"_"+authToken, newAuthToken, (claims.getExpiration().getTime() - nowMillis) / 1000);

									//accessToken放入缓存
									redisService.set(TokenConstant.AUTH_TOKEN+"_"+newAuthToken, newAuthToken, TokenConstant.JWT_MAX_TTL_SECOND);
									return true;
								}else{
									print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_AUTHMANAGE_REFRESH_ERROR, "刷新authToken失败"));
									return false;
								}
							}
						}else{
							log.debug("authToken签名验证成功");
							return true;
						}
					}else{
						int error = checkResult.getErrCode();
						if(BaseBizExceptionCode.TOKEN_VALIDATE_EXPIRED_ERROR == error){
							log.debug("authToken签名过期");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_EXPIRED_ERROR, "authToken签名过期"));
							return false;
						}else if(BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR == error){
							log.debug("authToken签名验证失败");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_FAIL_ERROR, "authToken签名验证失败"));
							return false;
						}else{
							log.debug("authToken不存在");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_NULL_ERROR, "authToken不存在"));
							return false;
						}
					}
				}else{
					log.debug("authToken不存在");
					print(request, response, new JsonResult().create(BaseBizExceptionCode.TOKEN_VALIDATE_NULL_ERROR, "authToken不存在"));
					return false;
				}
			}else{                   //header里面没有任何token  放过，到permission那一步再来验证
				return true;
			}
			
			
		}else{
			return true;
		}
	}
}
