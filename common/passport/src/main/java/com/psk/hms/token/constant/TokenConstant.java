package com.psk.hms.token.constant;
/**
 * token常量类
 * 创建时间  2018年04月23日
 */
public class TokenConstant {
	
    /**
	 * token
	 */
	public static final String ACCESS_TOKEN = "access-token";	// 普通登录的token
	public static final String AUTH_TOKEN = "auth-token";	    // 授权验证的authToken
	public static final String REFRESH_ACCESS_TOKEN = "refresh-access-token";	// 刷新access-token标记  用于缓存
	public static final String REFRESH_AUTH_TOKEN = "refresh-auth-token";	// 刷新auth-token标记  用于缓存
	
	public static final String IS_EXPIRE_YES = "1";	// token过期了
	public static final String IS_EXPIRE_NO = "0";	// token没有过期
	//public static final String TOKEN_REDIS_PREFIX = "login_user_token_id";	// Redis Token前缀
	//public static final String WX_REDIS_PREFIX = "login_wx_user_token_id";	// Redis Token前缀
	//public static final String QT_REDIS_PREFIX = "login_qt_user_token_id";	// Redis Token前缀
	
	/**
	 * JWT
	 */
	public static final String JWT_SECERT = "8677df7fc3a34e26a61c034d5ec8245d";			    //密匙
	public static final long JWT_TTL_MILLISECOND = 2 * 60 * 60 * 1000; 				    //token有效时长  2小时(毫秒)
	public static final long JWT_TTL_SECOND = 2 * 60 * 60; 						            //token有效时长  2小时(秒)

	public static final long JWT_EXTEND_TTL_MILLISECOND = 5 * 60 * 1000; 				    //token宽限时长  5分钟(毫秒)
	public static final long JWT_EXTEND_TTL_SECOND = 5 * 60; 				                //token宽限时长  5分钟(秒)
	
	public static final long JWT_PREDEATH_TTL_MILLISECOND = 5 * 60 * 1000; 				    //token宽限时长  5分钟(毫秒)
	public static final long JWT_PREDEATH_TTL_SECOND = 5 * 60; 				                //token宽限时长  5分钟(秒)
	
	public static final long JWT_MAX_TTL_SECOND = JWT_TTL_SECOND + JWT_EXTEND_TTL_SECOND; 			                //token最长有效时长  = 有效时长 + 宽限时长(毫秒)
	public static final long JWT_MAX_TTL_MILLISECOND = JWT_TTL_MILLISECOND + JWT_EXTEND_TTL_MILLISECOND; 			//token最长有效时长  = 有效时长 + 宽限时长(秒)
	
	public static final long JWT_MIN_TTL_SECOND = JWT_TTL_SECOND - JWT_PREDEATH_TTL_SECOND; 			            //token预死时长  = 有效时长 - 预死时长(毫秒)
	public static final long JWT_MIN_TTL_MILLISECOND = JWT_TTL_MILLISECOND - JWT_PREDEATH_TTL_MILLISECOND; 			//token预死时长  = 有效时长 - 预死时长(秒)
	
	/********************************** access-token 相关************************************/
	public static final String JWT_USER_ID_KEY = "userId";   //由于生成token的jwt id 是由多个参数组合成json而成  "userId" 就是其中之一
	public static final String JWT_COMPANY_ID_KEY = "companyId";   //由于生成token的jwt id 是由多个参数组合成json而成  "companyId" 就是其中之一
	public static final String JWT_EMPLOYEE_ID_KEY = "employeeId";   //由于生成token的jwt id 是由多个参数组合成json而成  "employeeId" 就是其中之一

	/********************************** auth-token 相关************************************/
	public static final String JWT_APP_ID_KEY = "appId";   //由于生成auth-token的jwt id 是由多个参数组合成json而成  "appId" 就是其中之一
	public static final String JWT_APP_SECRET_KEY = "appSecret";   //由于生成auth-token的jwt id 是由多个参数组合成json而成  "appSecret" 就是其中之一
}
