package com.psk.hms.passport.interceptor;

import com.psk.hms.base.constant.base.BaseOperatorInfoConstant;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.DateUtil;
import com.psk.hms.base.util.rsa.IDEAUtil;
import com.psk.hms.base.util.string.StringUtil;
import com.psk.hms.passport.util.WebUtil;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.service.base.service.BaseUserInfoService;
import com.psk.hms.service.service.LogOperatorService;
import com.psk.hms.token.constant.TokenConstant;
import com.psk.hms.token.util.TokenUtil;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述: 权限认证拦截器
 * 1.处理权限验证
 * 2.处理全局异常
 * 3.处理权限相关的工具类方法
 * 作者： xiangjz
 * 时间： 2018年5月10日
 * 版本： 1.0v
 */
public class PermissionInterceptor extends Interceptor {

	private BaseOperatorInfoService baseOperatorInfoService;
	private BaseUserInfoService baseUserInfoService;
	private LogOperatorService logOperatorService;

	public PermissionInterceptor(
			BaseOperatorInfoService baseOperatorInfoService,
			BaseUserInfoService baseUserInfoService,
			LogOperatorService logOperatorService
	) {
		this.baseOperatorInfoService = baseOperatorInfoService;
		this.baseUserInfoService = baseUserInfoService;
		this.logOperatorService = logOperatorService;
	}

	
	
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception exception) throws Exception {
		
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView) throws Exception {

	}

	@SuppressWarnings("unchecked")
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		if (object instanceof HandlerMethod) {
			if(log.isDebugEnabled()) log.debug("获取uri");
			String uri = request.getRequestURI();
			
			String tokenType = getTokenType(request);
			
			if(TokenConstant.ACCESS_TOKEN.equals(tokenType)){  //accessToken类型
				
				
				Map<String, Object> operatorParams = new HashMap<String, Object>();
				operatorParams.put("url", uri);
				
				Map<String, Object> operatorMap = (Map<String, Object>) baseOperatorInfoService.findBy(operatorParams);
				
				if(operatorMap == null){
					
					//* 这一块先直接返回true  
					// * 但是这是不符合业务逻辑的    
					// * 由于前期mobile调后台数据很多operator都没有配置，所以直接放开， 后期这一块的operator都需要配置起来，然后该验证就验证，验证不通过的就返回false
					 
					if(urlNotValidate(uri,baseOperatorInfoService))
						return true;
					
					
					print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, "没有找到"+uri+"功能"));
					return false;
					
					//return true;
				}else{
					if(urlNotValidate(uri,baseOperatorInfoService))
						return true;

					Map<String, Object> userMap = (Map<String, Object>) baseUserInfoService.findById(TokenUtil.getUserId(request));
				
					if (BaseOperatorInfoConstant.PRIVILEGES_YES.equals(operatorMap.get("privilegess"))) {
						if(log.isDebugEnabled()) log.debug("需要权限验证!");
						
						if (userMap == null) {
							if(log.isDebugEnabled()) log.debug("权限认证过滤器检测:未登录!");
							StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：未登录");
							print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
							return false;
						}
						
						try {
	                        if (!baseOperatorInfoService.hasPrivilege(BaseUtil.retStr(operatorMap.get("id")), TokenUtil.getEmployeeId(request))) {// 权限验证

								if(log.isDebugEnabled()) log.debug("权限验证失败，没有权限!");
								StringBuffer msgBuffer = new StringBuffer("权限验证失败，您没有操作权限");
								print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
								return false;
							}
							
						} catch (Exception e) {
							log.error(e);
							return false;
						}
						
					}else{
						return true;
					}
					
					if(!BaseOperatorInfoConstant.METHOD_NOT_LIMIT.equals(operatorMap.get("method"))){
						if(log.isDebugEnabled()) log.debug("method校验");
						
						String method = request.getMethod().toLowerCase();
						if((BaseOperatorInfoConstant.METHOD_GET.equals(operatorMap.get("method")) && !"get".equals(method))
								|| (BaseOperatorInfoConstant.METHOD_POST.equals(operatorMap.get("method")) && !"post".equals(method))){
							StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：请求方法错误。method校验失败，operator.method=").append(operatorMap.get("method")).append("，request.method=").append(method);
							if(log.isDebugEnabled()) log.debug(msgBuffer.toString());
							print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
							return false;
						}
					}
					
					if(!BaseOperatorInfoConstant.ENCTYPE_NOT_LIMIT.equals(operatorMap.get("enctype"))){
						if(log.isDebugEnabled()) log.debug("enctype校验");
						
						String contentType = request.getContentType().toLowerCase();
						if((BaseOperatorInfoConstant.ENCTYPE_APPLICATION_OR_XWWWFORMURLENCODED.equals(operatorMap.get("enctype")) && contentType.indexOf("application/x-www-form-urlencoded") == -1)
								|| (BaseOperatorInfoConstant.ENCTYPE_MULTIPART_OR_FORMDATA.equals(operatorMap.get("enctype")) && contentType.indexOf("multipart/form-data") == -1)
								|| (BaseOperatorInfoConstant.ENCTYPE_TEXT_OR_PLAIN.equals(operatorMap.get("enctype")) && contentType.indexOf("text/plain") == -1)
								){
							StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：请求编码错误。enctype校验失败，operator.enctype=").append(operatorMap.get("enctype")).append("，request.contentType=").append(contentType);
							if(log.isDebugEnabled()) log.debug(msgBuffer.toString());
							print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
							return false;
						}
					}
					
					if(userMap != null){  // 理论上csrf安全涉及到的是后台数据更新和删除操作URL安全问题，所以不可能存在用户未登录情况
						if(BaseOperatorInfoConstant.CSRF_YES.equals(operatorMap.get("csrf"))){
							if(log.isDebugEnabled()) log.debug("csrf校验");
							
							String csrfToken = request.getHeader("csrfToken");
							if(StringUtil.isBlank(csrfToken)){
								csrfToken = request.getParameter("csrfToken");
							}
							if(StringUtil.isBlank(csrfToken)){
								StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：csrf校验失败，当前请求没有提交csrfToken参数");
								if(log.isDebugEnabled()) log.debug(msgBuffer.toString());
								print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
								return false;
							}
							
							String csrfTokenDecrypt = IDEAUtil.decrypt((String)userMap.get("secretKey"), csrfToken); // 使用用户私有密钥，不易伪造，可以定期更新密钥
							long csrfTokenTime = Long.parseLong(csrfTokenDecrypt.split(".#.")[1]);
							Date start = DateUtil.getDate();
							start.setTime(csrfTokenTime); // csrfToken生成时间
							int minute = DateUtil.getDateMinuteSpace(start, DateUtil.getDate()); // 已经生成多少分钟
							if(minute > 30){   // 先写死
								StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：超过30分钟，视为无效CsrfToken，需要刷新页面重新提交");
								if(log.isDebugEnabled()) log.debug(msgBuffer.toString());
								print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
								return false;
							}
							
							boolean referer = WebUtil.authReferer(request);
							if(!referer){
								StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：referer校验失败");
								if(log.isDebugEnabled()) log.debug(msgBuffer.toString());
								print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
								return false;
							}
						}
					}
					
					if(BaseOperatorInfoConstant.REFERER_YES.equals(operatorMap.get("referer"))){
						if(log.isDebugEnabled()) log.debug("referer校验");
						
						boolean referer = WebUtil.authReferer(request);
						if(!referer){
							StringBuffer msgBuffer = new StringBuffer("权限认证过滤器检测：referer校验失败");
							if(log.isDebugEnabled()) log.debug(msgBuffer.toString());
							print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, msgBuffer.toString()));
							return false;
						}
					}
				}
				return true;
			}else if(TokenConstant.AUTH_TOKEN.equals(tokenType)){   //authToken类型
				return true;
			}else{                          //token没有传
				if(urlNotValidate(uri,baseOperatorInfoService))
					return true;
				
				print(request, response, new JsonResult().create(BaseBizExceptionCode.OPERATE_VALIDATE_ERROR, "没有找到"+uri+"功能"));
				return false;
			}
		}else{
			return true;
		}
	}
	
	
}
