package com.psk.hms.passport.interceptor;

import com.alibaba.fastjson.JSON;
import com.psk.hms.base.parameter.JsonResult;
import com.psk.hms.service.base.service.BaseOperatorInfoService;
import com.psk.hms.token.constant.TokenConstant;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class Interceptor implements HandlerInterceptor {

	protected static final Log log = LogFactory.getLog(Interceptor.class);

	@SuppressWarnings("unchecked")
	public boolean urlNotValidate(String uri, BaseOperatorInfoService baseOperatorInfoService){

		Map<String, Object> operatorParams=new HashMap<String, Object>();
		operatorParams.put("url", uri);

		Map<String, Object> operatorMap = (Map<String, Object>) baseOperatorInfoService.findBy(operatorParams);
		if(operatorMap!=null&&operatorMap.get("isExame").equals("0")) {
			return true;
		}
		return false;
		
		
//		if(
//				//--------------------------------api---------------------------------
//				uri.startsWith("/api/login")
//				|| uri.equals("/error")                   //通用错误接口
//				|| uri.startsWith("/craneInsert/")        //塔吊外部接口
//				|| uri.startsWith("/device/crane/")        //塔吊外部接口
//				|| uri.startsWith("/device/elevator/")     //升降机外部接
//				|| uri.startsWith("/device/environment/")  //环境监测外部接口
//				|| uri.startsWith("/device/unloading/")    //卸料平台外部接口
//				|| uri.startsWith("/device/guard/")     //门禁外部接口
//				|| uri.equals("/api/team/invite/validate")    //验证邀请码
//				|| uri.equals("/api/team/invite/confirm")     //验证手机邀请码
//				|| uri.equals("/api/team/invite/register")    //邀请并注册
//				|| uri.equals("/api/team/invite/info")        //获取邀请码信息
//				|| uri.equals("/api/device/distribution/map")	// 人员分布-大屏使用
//				|| uri.startsWith("/log/api/")        //日志接口
//				|| uri.startsWith("/api/thirdParty/wx/scan/")        //閭�璇穒nfo
//				|| uri.equals("/api/team/employee/invite/qrCode")        //获取团队邀请二维码
//				|| uri.equals("/api/team/employee/visitor/qrCode")        //获取免登二维码
//				|| uri.startsWith("/api/shortCode")        //短码
//				|| uri.startsWith("/api/thirdParty/qt/nudge/")        //轻推获取jssdk签名接口
//				//|| uri.startsWith("/baseUserInfo/getCurrentTeam")        //获取当前团队					  
//				// || uri.startsWith("/baseUserInfo/addIntoTeam")        //鍔犲叆鍥㈤槦
//				// || uri.equals("/baseUserInfo/changeCurrentTeam")        //鍔犲叆鍥㈤槦
//				|| uri.startsWith("/api/base/interface/")        //接口扫描
//				|| uri.startsWith("/api/monitor/diagnose/status")        // 应用监控
//				|| uri.startsWith("/token/")                // token
//				|| uri.startsWith("/api/noVerify/")                // 涓嶉獙璇佺殑controller
//				|| uri.contains("/Modular")
//				|| uri.startsWith("/position_sdk/")
//				|| uri.contains("sendMsg")
//				|| uri.startsWith("/device/monitor/interface/")
//				//--------------------------------open api---------------------------------
//				|| uri.equals("/open/authorize/getAuthToken")
//				|| uri.startsWith("/open/monitor/interface/")
//				//--------------------------------admin---------------------------------
//				|| uri.equals("/admin/login/validate")
//				|| uri.equals("/admin/login/listTeam")
//				|| uri.equals("/admin/login/chooseTeam")
//				|| uri.startsWith("/common")
//				|| uri.equals("/admin/login/index")
//				|| uri.equals("/admin/login/logout")
//				|| uri.equals("/admin/authImg/index")
//				|| uri.equals("/admin/ueditor/index")
//				|| uri.equals("/admin/developer/timer/appMonitor/diagnose/status")
//				|| uri.startsWith("/admin/project/team/teamEmployee/invite/qrCode/")
//
//				|| uri.equals("/api/inspect/equipInfo/findByParam") 
//				|| uri.startsWith("/api/thirdParty/everhigh")
//				|| uri.startsWith("/admin/monitor/interface/")
//				//--------------------------------message---------------------------------
//				|| uri.startsWith("/webSocketMsgSenderController")
//		){
//			return true;
//			
//		}
//		return false;
	}
	
	
	public void print(HttpServletRequest request, HttpServletResponse response, JsonResult message) {
		try {
			response.setStatus(HttpStatus.OK.value());
			response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
			response.setHeader("Cache-Control", "no-cache, must-revalidate");
			response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
	        response.setHeader("Access-Control-Allow-Credentials", "true");
	        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	        response.setHeader("Access-Control-Max-Age", "3600");
	        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
			PrintWriter writer = response.getWriter();
			writer.write(JSON.toJSONString(message));
			writer.flush();
			writer.close();
		} catch (IOException e) {
			log.error("print error", e);
		}
	}
	
	/**
	 * 获取token类型
	 * 如果header两种token都传了?，程序会遍历出第一个并返回结果
	 * @param request
	 * @return "accessToken" or "authToken" or null
	 */
	public String getTokenType(HttpServletRequest request){
		String tokenType = null;
		
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
	        String key = (String) headerNames.nextElement();
	        
	        if(TokenConstant.ACCESS_TOKEN.equals(key)){
	        	tokenType = TokenConstant.ACCESS_TOKEN;
	        	break;
	        }else if(TokenConstant.AUTH_TOKEN.equals(key)){
	        	tokenType = TokenConstant.AUTH_TOKEN;
	        	break;
	        }
	    }
		
		return tokenType;
	}
}
