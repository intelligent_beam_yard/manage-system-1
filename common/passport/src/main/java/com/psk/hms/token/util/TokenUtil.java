package com.psk.hms.token.util;

import com.alibaba.fastjson.JSONObject;
import com.psk.hms.base.exception.BizException;
import com.psk.hms.token.constant.TokenConstant;
import com.psk.hms.token.entity.CheckResult;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * jwt加密和解密的工具类 
 * 创建时间 2018年04月23日
 */
public class TokenUtil {
	
	public static String getUserId(HttpServletRequest request) {
		String token = request.getHeader(TokenConstant.ACCESS_TOKEN);
		CheckResult checkResult = JwtUtils.validateJWT(token);
		if(checkResult.isSuccess()){
			JSONObject tokenData = JSONObject.parseObject(checkResult.getClaims().getId());
			return tokenData.getString(TokenConstant.JWT_USER_ID_KEY);
		}else{
			throw new BizException(checkResult.getErrCode(), "token验证失败");
		}
	}
	
	public static String getCompanyId(HttpServletRequest request) {
		String token = request.getHeader(TokenConstant.ACCESS_TOKEN);
		CheckResult checkResult = JwtUtils.validateJWT(token);
		if(checkResult.isSuccess()){
			JSONObject tokenData = JSONObject.parseObject(checkResult.getClaims().getId());
			return tokenData.getString(TokenConstant.JWT_COMPANY_ID_KEY);
		}else{
			throw new BizException(checkResult.getErrCode(), "token验证失败");
		}
		
	}
	
	public static String getEmployeeId(HttpServletRequest request) {
		String token = request.getHeader(TokenConstant.ACCESS_TOKEN);
		CheckResult checkResult = JwtUtils.validateJWT(token);
		if(checkResult.isSuccess()){
			JSONObject tokenData = JSONObject.parseObject(checkResult.getClaims().getId());
			return tokenData.getString(TokenConstant.JWT_EMPLOYEE_ID_KEY);
		}else{
			throw new BizException(checkResult.getErrCode(), "token验证失败");
		}
		
	}
	
	public static Map<String, Object> getCurrentTokenInfo(HttpServletRequest request) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(TokenConstant.JWT_USER_ID_KEY, getUserId(request));
		resultMap.put(TokenConstant.JWT_COMPANY_ID_KEY, getCompanyId(request));
		resultMap.put(TokenConstant.JWT_EMPLOYEE_ID_KEY, getEmployeeId(request));
		return resultMap;
	}
}
