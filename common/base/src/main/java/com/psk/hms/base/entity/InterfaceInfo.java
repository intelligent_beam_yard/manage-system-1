package com.psk.hms.base.entity;

import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;
import java.util.Set;

public class InterfaceInfo {

	private Set<String> url;

	private Set<RequestMethod> requestMethods;

	private Map<String, String> parameters;

	private String classPath;

	private String method;

	public Set<String> getUrl() {
		return url;
	}

	public void setUrl(Set<String> url) {
		this.url = url;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public Set<RequestMethod> getRequestMethods() {
		return requestMethods;
	}

	public void setRequestMethods(Set<RequestMethod> requestMethods) {
		this.requestMethods = requestMethods;
	}

	public String getClassPath() {
		return classPath;
	}

	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

}
