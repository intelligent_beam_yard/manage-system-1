package com.psk.hms.base.util.validator.field;

import com.psk.hms.base.util.validator.result.MapValidateResult;

public class AlternativeKeyGenerator<K, V> implements AbsentMsgGeneratable<K, V> {

	@Override
	public String getDefaultAbsentMsg(K key) {
		return "Validated field";
	}
	
	@Override
	public MapValidateResult<K> getDefaultAbsentResult(K key) {
		return new MapValidateResult<>(key).success();
	}

}
