package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

public class TeamKnowledgeConstant extends Constant {
	 /**
     *  数据表字段名称 - title
     */
    public static final String TITLE = "title";
    /**
     *  数据表字段名称 - teamId
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - userId
     */
    public static final String USER_ID = "userId";
    /**
     *  数据表字段名称 - parentId
     */
    public static final String PARENT_ID = "parentId";
    /**
     *  数据表字段名称 - parentIds
     */
    public static final String PARENT_IDS = "parentIds";
    /**
     *  数据表字段名称 - content
     */
    public static final String CONTENT = "content";
    /**
     *  数据表字段名称 - url
     */
    public static final String URL = "url";
    /**
     *  数据表字段名称 - files
     */
    public static final String FILES = "files";
    /**
     *  数据表字段名称 - contentType
     */
    public static final String CONTENT_TYPE = "contentType";
    /**
     *  数据表字段名称 - modelId
     */
    public static final String MODEL_ID = "modelId";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
}
