package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队成员信息常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamEmployeeConstant extends Constant {    
    /**
     *  数据表字段名称 - teamId
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - userId
     */
    public static final String USER_ID = "userId";
    /**
     *  数据表字段名称 - nickName
     */
    public static final String NICK_NAME = "nickName";
    /**
     *  数据表字段名称 - companyId
     */
    public static final String COMPANY_ID = "companyId";
    /**
     *  数据表字段名称 - score
     */
    public static final String SCORE = "score";
    /**
     *  数据表字段名称 - isCurrentTeam
     */
    public static final String IS_CURRENT_TEAM = "isCurrentTeam";
    /**
     *  数据表字段名称 - orgId
     */
    public static final String ORG_ID = "orgId";
    /**
     *  数据表字段名称 - isProtect
     */
    public static final String IS_PROTECT = "isProtect";
    /**
     *  数据表字段名称 - contact
     */
    public static final String CONTACT = "contact";
    
    
    /**
     *  业务逻辑 - 当前团队 - 1(是)
     */
    public static final String IS_CURRENT_TEAM_YES = "1";
    /**
     *  业务逻辑 - 当前团队 - 0(是)
     */
    public static final String IS_CURRENT_TEAM_NO = "0";
    /**
     *  业务逻辑 - 性别 - 1(女)
     */
    public static final String SEX_FEMALE = "1";
    /**
     *  业务逻辑 - 性别 - 0(男)
     */
    public static final String SEX_MALE = "0";
    /**
     *  业务逻辑 - 隐私保护 - 1(开启)
     */
    public static final String IS_PROTECT_YES = "1";
    /**
     *  业务逻辑 - 隐私保护 - 0(关闭)
     */
    public static final String IS_PROTECT_NO = "0";
    
    
    /**
     *  业务逻辑 - 参数 - roleId
     */
    public static final String ROLE_ID = "roleId";
    /**
     *  业务逻辑 - 参数 - teamJobId
     */
    public static final String TEAM_JOB_ID = "teamJobId";
    /**
     *  业务逻辑 - 参数 - organization
     */
    public static final String ORGANIZATION = "organization";
    /**
     *  业务逻辑 - 参数 - jobs
     */
    public static final String JOBS = "jobs";
    /**
     *  业务逻辑 - 参数 - roles
     */
    public static final String ROLES = "roles";
    /**
     *  业务逻辑 - 参数 - jobIds
     */
    public static final String JOB_IDS = "jobIds";
    /**
     *  业务逻辑 - 参数 - roleIds
     */
    public static final String ROLE_IDS = "roleIds";
    /**
     *  业务逻辑 - 参数 - exist
     */
    public static final String EXIST = "exist";
    /**
     *  业务逻辑 - 参数 - jobId
     */
    public static final String JOB_ID = "jobId";
    /**
     *  业务逻辑 - 参数 - employeeId
     */
    public static final String EMPLOYEE_ID = "employeeId";
    /**
     *  业务逻辑 - 参数 - teamRoleId
     */
    public static final String TEAM_ROLE_ID = "teamRoleId";
    /**
     *  业务逻辑 - 参数 - teamEmployeeId
     */
    public static final String TEAM_EMPLOYEE_ID = "teamEmployeeId";
    /**
     *  业务逻辑 - 参数 - roleType
     */
    public static final String ROLE_TYPE = "roleType";
    /**
     *  业务逻辑 - 参数 - count
     */
    public static final String COUNT = "count";
    /**
     *  业务逻辑 - 参数 - isExpand
     */
    public static final String IS_EXPAND = "isExpand";
    /**
     *  业务逻辑 - 参数 - filterKey
     */
    public static final String FILTER_KEY = "filterKey";
    /**
     *  业务逻辑 - 参数 - name
     */
    public static final String NAME = "name";
    /**
     *  业务逻辑 - 参数 - roleNameList
     */
    public static final String ROLE_NAME_LIST = "roleNameList";
    /**
     *  业务逻辑 - 参数 - employeeList
     */
    public static final String EMPLOYEE_LIST = "employeeList";
    /**
     *  业务逻辑 - 参数 - orgIds
     */
    public static final String ORG_IDS = "orgIds";
    /**
     *  业务逻辑 - 参数 - isEquals
     */
    public static final String IS_EQUALS = "isEquals";
    /**
     *  业务逻辑 - 参数 - passwordForMD5
     */
    public static final String PASSWORD_FOR_MD5 = "passwordForMD5";
    /**
     *  业务逻辑 - 参数 - teamName
     */
    public static final String TEAM_NAME = "teamName";
    /**
     *  业务逻辑 - 参数 - userName
     */
    public static final String USER_NAME = "userName";
    /**
     *  业务逻辑 - 参数 - companyName
     */
    public static final String COMPANY_NAME = "companyName";
    /**
     *  业务逻辑 - 参数 - orgName
     */
    public static final String ORG_NAME = "orgName";
    /**
     *  业务逻辑 - 参数 - jobNames
     */
    public static final String JOB_NAMES = "jobNames";
    /**
     *  业务逻辑 - 参数 - roleName
     */
    public static final String ROLE_NAME = "roleName";
    /**
     *  业务逻辑 - 参数 - teamVisiteId
     */
    public static final String TEAM_VISITE_ID = "teamVisiteId";
    /**
     *  业务逻辑 - 参数 - otherStr1
     */
    public static final String OTHER_STR1 = "otherStr1";
    public static final String OTHER_STR2 = "otherStr2";
    public static final String OTHER_STR3 = "otherStr3";
    //隐私保护
    public static final String OTHER_STR4 = "protectStr";
    
    public static final String OTHER_STR5 = "otherStr5";
    public static final String OTHER_STR6 = "otherStr6";
    public static final String OTHER_STR7 = "otherStr7";
    public static final String OTHER_STR8 = "otherStr8";
    public static final String OTHER_STR9 = "otherStr9";
    public static final String OTHER_STR10 = "otherStr10";
   
}
