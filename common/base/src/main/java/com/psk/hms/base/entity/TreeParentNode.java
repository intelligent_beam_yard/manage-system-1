package com.psk.hms.base.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 树状节点
 * 
 * @author xiangjz
 * @date 2018年6月28日
 * @version 1.0.0
 *
 */
public class TreeParentNode extends TreeNode {
	private static final long serialVersionUID = 1L;

	private List<TreeNode> children;
	
	public TreeParentNode() {
		super();
		this.children = new ArrayList<>();
	}
	
	public TreeParentNode(String id, String name) {
		super(id, name);
		this.children = new ArrayList<>();
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	
	public boolean addNode(TreeNode node) {
		return this.children.add(node);
	}
	
	public boolean hasNode(TreeNode node) {
		return this.children.contains(node);
	}

}
