package com.psk.hms.base.exception.company;

/**
 * <p>
 * COMPANY 业务异常类
 * </p>
 * 模块异常码 : 23000001 ~ 23999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class CompanyBizExceptionCode {
	
	/***
	 * 单位模块
	 * 23010000~23019999
	 */
	//单位模块-单位信息-增 
	public static final int COMPANY_INFO_SAVE_ERROR = 23010101;
	//单位模块-单位信息-删
	public static final int COMPANY_INFO_DELETE_ERROR = 23010102;
	//单位模块-单位信息-查
	public static final int COMPANY_INFO_SELECT_ERROR = 23010103;
	//单位模块-单位信息-改
	public static final int COMPANY_INFO_UPDATE_ERROR = 23010104;
	//单位模块-单位信息-通用异常
	public static final int COMPANY_INFO_MANAGE_ERROR = 23010199;
	
	//单位模块-单位成员-增
	public static final int COMPANY_EMPLOYEE_SAVE_ERROR = 23010201;
	//单位模块-单位成员-删
	public static final int COMPANY_EMPLOYEE_DELETE_ERROR = 23010202;
	//单位模块-单位成员-查
	public static final int COMPANY_EMPLOYEE_SELECT_ERROR = 23010203;
	//单位模块-单位成员-改
	public static final int COMPANY_EMPLOYEE_UPDATE_ERROR = 23010204;
	//单位模块-单位成员-通用异常
	public static final int COMPANY_EMPLOYEE_MANAGE_ERROR = 23010299;

	//单位模块-单位类型-增
	public static final int COMPANY_TYPE_SAVE_ERROR = 23010301;
	//单位模块-单位类型-删
	public static final int COMPANY_TYPE_DELETE_ERROR = 23010302;
	//单位模块-单位类型-查
	public static final int COMPANY_TYPE_SELECT_ERROR = 23010303;
	//单位模块-单位类型-改
	public static final int COMPANY_TYPE_UPDATE_ERROR = 23010304;
	//单位模块-单位成员-通用异常
	public static final int COMPANY_TYPE_MANAGE_ERROR = 23010399;
	
	
	//单位模块-通用异常
	public static final int COMPANY_MODULE_ERROR = 23019999;
}
