package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础角色常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseRoleInfoConstant extends Constant {
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - parentId
     */
    public static final String PARENT_ID = "parentId";
    /**
     *  数据表字段名称 - parentIds
     */
    public static final String PARENT_IDS = "parentIds";
    /**
     *  数据表字段名称 - isParent
     */
    public static final String IS_PARENT = "isParent";
    /**
     *  数据表字段名称 - levels
     */
    public static final String LEVELS = "levels";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
    /**
     *  数据表字段名称 - images
     */
    public static final String IMAGES = "images";
    /**
     *  数据表字段名称 - ISSHOW
     */
    public static final String ISSHOW = "isShow";
    /**
     *  数据表字段名称 - teamTypeId
     */
    public static final String TEAM_TYPE_ID = "teamTypeId";
    /**
     *  数据表字段名称 - isShow
     */
    public static final String IS_SHOW = "isShow";
    /**
     *  数据表字段名称 - isShow-是
     */
    public static final String IS_SHOW_YES = "1";
	/**
	 *  数据表字段名称 - isShow-否
	 */
	public static final String IS_SHOW_NO = "0";

}