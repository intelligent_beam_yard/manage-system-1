package com.psk.hms.base.exception.thirdParty;

/**
 * <p>
 * thirdParty 业务异常类
 * </p>
 * 模块异常码 : 30000001 ~ 39999999 <br/>
 *
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class ThirdPartyBizExceptionCode{

	/***
	 * 轻推模块
	 * 30010000~30019999
	 */
	//轻推模块-接口-签名
	public static final int QT_API_SIGNTURE_ERROR = 30010101;
	//轻推模块-接口-通用异常
	public static final int QT_API_ERROR = 30010199;

	/***
	 * 品茗模块
	 * 30020000~30029999
	 */
	//品茗模块-品茗管理-增
	public static final int PM_MANAGE_SAVE_ERROR = 30020101;
	//品茗模块-品茗管理-删
	public static final int PM_MANAGE_DELETE_ERROR = 30020102;
	//品茗模块-品茗管理-查
	public static final int PM_MANAGE_SELECT_ERROR = 30020103;
	//品茗模块-品茗管理-改
	public static final int PM_MANAGE_UPDATE_ERROR = 30020104;
	//品茗模块-品茗管理-通用异常
	public static final int PM_API_ERROR = 30020199;

	//品茗模块-验证-参数错误
	public static final int PM_VALIDATE_PARAMETER_ERROR = 30020201;
	//品茗模块-验证-缺少参数
	public static final int PM_VALIDATE_LESSPARAMETER_ERROR = 30020202;
	//品茗模块-品茗管理-通用异常
	public static final int PM_VALIDATE_ERROR = 30020299;

	/***
	 * 恒高模块
	 * 30030000~30039999
	 */
	// 恒高-查询服务器地址
	public static final int EVERHIGH_QUERY_ADDRESS = 30030101;
	public static final int EVERHIGH_ERROR = 30030199;
	// 恒高服务超时
	public static final int EVERHIGH_SERVER_TIMEOUT = 30030201;

	//第三方-通用异常
	public static final int ERROR = 39999999;

}
