package com.psk.hms.base.parameter.validator.field.impl;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

public class RangeFieldValidator<K, V, T extends Comparable<T>> extends CompareFieldValidator<K, V, T> {

	private T leftBorder;
	private T rightBorder;
	private boolean includeBorder;

	public RangeFieldValidator(K key, T leftBorder, T rightBorder, boolean includeBorder, boolean keyRequired) {
		super(key, leftBorder.getClass(), keyRequired);
		this.leftBorder = leftBorder;
		this.rightBorder = rightBorder;
		this.includeBorder = includeBorder;
		this.errorMsg = this.getDefaultErrorMsg(key);
	}

	public RangeFieldValidator(K key, T leftBorder, T rightBorder, boolean includeBorder, boolean keyRequired,
			String errorMsg) {
		super(key, leftBorder.getClass(), keyRequired, errorMsg);
		this.leftBorder = leftBorder;
		this.rightBorder = rightBorder;
		this.includeBorder = includeBorder;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> result = super.checkEntity(key, value);
		if (!result.isPass()) {
			return result;
		}
		T tValue = (T) value;
		boolean inRange = false;
		if (this.includeBorder) {
			inRange = this.inOrOnBorder(tValue);
		} else {
			inRange = this.inBorder(tValue);
		}
		if (!inRange) {
			result.failure(errorMsg);
		}
		return result;
	}

	private boolean inBorder(T value) {
		return this.isAboveBorder(leftBorder, value) && this.isUnderBorder(rightBorder, value);
	}

	private boolean inOrOnBorder(T value) {
		return this.inBorder(value) || this.isEqualBorder(leftBorder, value) || this.isEqualBorder(rightBorder, value);
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为").append(this.getType().getSimpleName()).append("类型，大于");
		if (this.includeBorder) {
			sb.append("等于");
		}
		sb.append(leftBorder).append("且小于");
		if (this.includeBorder) {
			sb.append("等于");
		}
		sb.append(rightBorder);
		return sb.toString();
	}

	public T getLeftBorder() {
		return leftBorder;
	}

	public T getRightBorder() {
		return rightBorder;
	}
}
