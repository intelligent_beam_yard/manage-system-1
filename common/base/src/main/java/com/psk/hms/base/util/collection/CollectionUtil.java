package com.psk.hms.base.util.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 集合比对工具类
 * 
 * @author xiangjz
 * @date 2018年11月19日
 * @version 1.0.0
 *
 */
public class CollectionUtil {

	private CollectionUtil() {
	}

	/**
	 * 双向列表比对。比对两个列表的元素，并返回双向比对结果。双向比对会比对 leftList => rightList 的差异和
	 * rightList => leftList 的差异。结果集将会包含leftList的特有元素列表（leftResult），rightList
	 * 的特有元素（rightResult）以及共有元素的元素列表（bothResult）。未初始化的列表参数将会抛出NPE。
	 * 时间复杂度 O(m + n) ,其中m,n是两个列表的长度。
	 * 
	 * @param leftList 源列表1，对应结果集中的leftResult和leftResultCount
	 * @param rightList 源列表2，对应结果集中的rightResult和rightResultCount
	 * @return 比对结果 {@link CompareResult}
	 */
	public static <T> CompareResult<List<T>> duplexListDiffer(List<T> leftList, List<T> rightList) {
		if (leftList == null || rightList == null) {
			throw new NullPointerException("Source lists cannot be null");
		}
		CompareResult<List<T>> result = new CompareResult<List<T>>();
		List<T> minResult = new ArrayList<T>();
		List<T> maxResult = new ArrayList<T>();
		List<T> bothResult = new ArrayList<T>();
		List<T> maxList = leftList;
		List<T> minList = rightList;
		result.setLeftResult(maxResult);
		result.setRightResult(minResult);
		result.setBothResult(bothResult);
		// 调整列表顺序，降低比较次数
		if (rightList.size() > leftList.size()) {
			maxList = rightList;
			minList = leftList;
			result.setLeftResult(minResult);
			result.setRightResult(maxResult);
		}
		Map<T, Integer> countMap = new HashMap<T, Integer>(maxList.size());
		for (T t : maxList) {
			countMap.put(t, 1);
		}
		for (T t : minList) {
			Integer count = countMap.get(t);
			if (count == null) {
				minResult.add(t);
				continue;
			}
			countMap.put(t, ++count);
		}
		for (Map.Entry<T, Integer> itemCount : countMap.entrySet()) {
			if (1 == itemCount.getValue()) {
				maxResult.add(itemCount.getKey());
				continue;
			}
			bothResult.add(itemCount.getKey());
		}
		// 设置结果集大小
		result.setLeftResultSize(result.getLeftResult().size());
		result.setRightResultSize(result.getRightResult().size());
		result.setBothResultSize(result.getBothResult().size());
		return result;
	}

	/**
	 * 双向排除比对。排除特定元素后进行双向比对，并返回比对结果。比对前会根据exclusiveList对源列表的数据
	 * 进行筛选，排除leftList和rightList中包含的exclusiveList中的元素，然后在进行双向比对。
	 * 时间复杂度 O(m + n + l) 其中m,n,l为三个列表的长度
	 * 
	 * @param leftList 源列表1，对应结果集中的leftResult和leftResultCount
	 * @param rightList 源列表2，对应结果集中的rightResult和rightResultCount
	 * @param exclusiveList 排除元素列表
	 * @return 比对结果 {@link CompareResult}
	 */
	public static <T> CompareResult<List<T>> duplexListDiffer(List<T> leftList, List<T> rightList,
			List<T> exclusiveList) {
		leftList = listDiffer(leftList, exclusiveList);
		rightList = listDiffer(rightList, exclusiveList);
		return duplexListDiffer(leftList, rightList);
	}
	
	/**
	 * 单向列表比对。比对leftList列表与rightList的元素，并返回比对结果。但向比对仅会比对 leftList => rightList
	 * 的差异并返回leftList特有元素的列表结果。
	 * 
	 * @param leftList 源列表1
	 * @param rightList 源列表2
	 * @return
	 */
	public static <T> List<T> listDiffer(List<T> leftList, List<T> rightList) {
		CompareResult<List<T>> result = duplexListDiffer(leftList, rightList);
		return result.getLeftResult();
	}

}
