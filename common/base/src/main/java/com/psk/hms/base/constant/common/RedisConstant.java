package com.psk.hms.base.constant.common;
/***********
 * 短信模板类型
 * @author xiangjz
 * @date 2018年5月2日
 * @version 1.0.0
 *
 */
public class RedisConstant {
	
	/**********
	 * key
	 */
	public static final String KEY = "key";
	/**********
	 * keys
	 */
	public static final String KEYS = "keys";
	/**********
	 * value
	 */
	public static final String VALUE = "value";
	/**********
	 * expireTime
	 */
	public static final String EXPIRE_TIME = "expireTime";
	
	/**********
	 * 下划线
	 */
	public static final String SPLIT_UNDERLINE = "_";
	/**********
	 * 注册KEY
	 */
	public static final String REDIS_USER_REGISTER_KEY = "register";
	/**********
	 * 修改密码
	 */
	public static final String REDIS_USER_CHANGEPWD_KEY = "changePwd";
	
	/**
	 * 邀请加入团队
	 */
	public static final String REDIS_TEAM_INVITE = "teamInvite";
	
	/**
	 * 邀请确认验证码
	 */
	public static final String REDIS_TEAM_INVITE_VALIDATE = "teamInviteValidate";
	
	/**
	 * 邀请确认验证码
	 */
	public static final String REDIS_TEAM_INVITE_REGISTER = "teamInviteRegister";
	
	/**
	 * 短码
	 */
	public static final String REDIS_SHORT_CODE = "baseShortCode";
	
	/**
	 * 轻推access_token
	 */
	public static final String REDIS_NUDGE_ACCESS_TOKEN = "nudge_access_token";
	
	/**
	 * 轻推ticket
	 */
	public static final String REDIS_NUDGE_TICKET = "nudge_ticket";
	
	/**
	 * 团队参观者邀请权限
	 */
	public static final String REDIS_TEAM_VISITOR_PERMISSION = "teamVisitorPermission";
	
	/**
	 * 权限比对
	 */
	public static final String REDIS_OPT_DIFFER = "teamOptDiffer";
}
