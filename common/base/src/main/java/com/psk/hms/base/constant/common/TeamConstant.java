package com.psk.hms.base.constant.common;

public class TeamConstant {
	/**
	 * 是否是当前团队 是-0
	 */
	public static final String IS_CURRENT_TEAM_YES = "1";
	/**
	 * 是否是当前团队 否-1
	 */
	public static final String IS_CURRENT_TEAM_NO = "0";
	
}
