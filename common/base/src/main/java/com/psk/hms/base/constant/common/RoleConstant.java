package com.psk.hms.base.constant.common;

/**
 *角色常量
 *@author 创建者：xiangjz
 *@date 创建时间：2018年5月17日 下午6:57:05
 *@version 1.0
 *@parameter 
*/
public class RoleConstant {

	/**
	 * 团队角色类型  通用团队类型  -- 1
	 */
	public static final String ROLE_TYPE_COMMON = "1";
	/**
	 * 团队角色类型  自定义团队类型  -- 2
	 */
	public static final String ROLE_TYPE_CUSTOM = "2";
}
