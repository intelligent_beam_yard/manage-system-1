package com.psk.hms.base.constant.common;

public class BaseRoleConstant {

	/**
	 * 是否展示节点  否-0
	 */
	public static final String IS_SHOW_NO = "0";
	/**
	 * 是否展示节点  是-1
	 */
	public static final String IS_SHOW_YES = "1";
	
}
