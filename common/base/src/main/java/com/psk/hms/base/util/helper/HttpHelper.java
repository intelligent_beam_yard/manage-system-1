package com.psk.hms.base.util.helper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class HttpHelper {

	private static final String MAIL_LOCK = "exchange_mail_lock";

	public static String LO_PATH = null;
	public static String DIR_PATH = null;
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 接口调用 GET
	 */
	public static String httpURLConectionGET(String urls) {
		StringBuilder sb = new StringBuilder();
		try {
			URL url = new URL(urls); // 把字符串转换为URL请求地址
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();// 打开连接
			connection.connect();// 连接会话
			// 获取输入流
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {// 循环读取流
				sb.append(line);
			}
			br.close();// 关闭流
			connection.disconnect();// 断开连接
			System.out.println(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("失败!");
		}
		return sb.toString();
	}

	/**
	 * 接口调用 POST
	 */
	public static void httpURLConnectionPOST(String postUrl) {
		try {
			URL url = new URL(postUrl);

			// 将url 以 open方法返回的urlConnection 连接强转为HttpURLConnection连接
			// (标识一个url所引用的远程对象连接)
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();// 此时cnnection只是为一个连接对象,待连接中

			// 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
			connection.setDoOutput(true);

			// 设置连接输入流为true
			connection.setDoInput(true);

			// 设置请求方式为post
			connection.setRequestMethod("POST");

			// post请求缓存设为false
			connection.setUseCaches(false);

			// 设置该HttpURLConnection实例是否自动执行重定向
			connection.setInstanceFollowRedirects(true);

			// 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
			// application/x-javascript text/xml->xml数据
			// application/x-javascript->json对象
			// application/x-www-form-urlencoded->表单数据
			// ;charset=utf-8 必须要，不然妙兜那边会出现乱码【★★★★★】
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

			// 建立连接
			// (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
			connection.connect();

			// 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)
			DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());

			String app_key = "app_key=" + URLEncoder.encode("4f7bf8c8260124e6e9c6bf094951a111", "utf-8"); // 已修改【改为错误数据，以免信息泄露】
			String agt_num = "&agt_num=" + URLEncoder.encode("10111", "utf-8"); // 已修改【改为错误数据，以免信息泄露】
			String pid = "&pid=" + URLEncoder.encode("BLZXA150401111", "utf-8"); // 已修改【改为错误数据，以免信息泄露】
			String departid = "&departid=" + URLEncoder.encode("10007111", "utf-8"); // 已修改【改为错误数据，以免信息泄露】
			String install_lock_name = "&install_lock_name=" + URLEncoder.encode("南天大门", "utf-8");
			String install_address = "&install_address=" + URLEncoder.encode("北京育新", "utf-8");
			String install_gps = "&install_gps=" + URLEncoder.encode("116.350888,40.011001", "utf-8");
			String install_work = "&install_work=" + URLEncoder.encode("小李", "utf-8");
			String install_telete = "&install_telete=" + URLEncoder.encode("13000000000", "utf-8");
			String intall_comm = "&intall_comm=" + URLEncoder.encode("一切正常", "utf-8");

			// 格式 parm = aaa=111&bbb=222&ccc=333&ddd=444
			String parm = app_key + agt_num + pid + departid + install_lock_name + install_address + install_gps
					+ install_work + install_telete + intall_comm;

			// 将参数输出到连接
			dataout.writeBytes(parm);

			// 输出完成后刷新并关闭流
			dataout.flush();
			dataout.close(); // 重要且易忽略步骤 (关闭流,切记!)

			// System.out.println(connection.getResponseCode());

			// 连接发起请求,处理服务器响应 (从连接获取到输入流并包装为bufferedReader)
			BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;
			StringBuilder sb = new StringBuilder(); // 用来存储响应数据

			// 循环读取流,若不到结尾处
			while ((line = bf.readLine()) != null) {
				// sb.append(bf.readLine());
				sb.append(line).append(System.getProperty("line.separator"));
			}
			bf.close(); // 重要且易忽略步骤 (关闭流,切记!)
			connection.disconnect(); // 销毁连接
			System.out.println(sb.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 接口调用 POST
	 */
	public static String httpURLConnectionsPOST(String postUrl, String params) {
		String result = "";
		try {
			URL url = new URL(postUrl);

			// 将url 以 open方法返回的urlConnection 连接强转为HttpURLConnection连接
			// (标识一个url所引用的远程对象连接)
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();// 此时cnnection只是为一个连接对象,待连接中

			// 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
			connection.setDoOutput(true);

			// 设置连接输入流为true
			connection.setDoInput(true);

			// 设置请求方式为post
			connection.setRequestMethod("POST");

			// post请求缓存设为false
			connection.setUseCaches(false);

			// 设置该HttpURLConnection实例是否自动执行重定向
			connection.setInstanceFollowRedirects(true);

			// 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
			// application/x-javascript text/xml->xml数据
			// application/x-javascript->json对象
			// application/x-www-form-urlencoded->表单数据
			// ;charset=utf-8 必须要，不然妙兜那边会出现乱码【★★★★★】
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

			// 建立连接
			// (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
			connection.connect();

			// 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)
			// DataOutputStream dataout = new DataOutputStream();
			PrintWriter out = new PrintWriter(connection.getOutputStream());

			// 将参数输出到连接
			out.println(params);

			// 输出完成后刷新并关闭流
			out.flush();
			out.close(); // 重要且易忽略步骤 (关闭流,切记!)

			// System.out.println(connection.getResponseCode());

			// 连接发起请求,处理服务器响应 (从连接获取到输入流并包装为bufferedReader)
			BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;
			StringBuilder sb = new StringBuilder(); // 用来存储响应数据

			// 循环读取流,若不到结尾处
			while ((line = bf.readLine()) != null) {
				// sb.append(bf.readLine());
				sb.append(line).append(System.getProperty("line.separator"));
			}
			bf.close(); // 重要且易忽略步骤 (关闭流,切记!)
			connection.disconnect(); // 销毁连接
			result = sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 通过文件url列表获取临时文件列表
	 * 
	 * @param urlList
	 *            文件url列表
	 * @param tempPath
	 *            临时文件夹的绝对路径
	 * @return 临时文件列表
	 */
	public static List<String> getTempFileListfromUrlList(String tempPath, List<String> urlList) {
		File tempFiles = new File(tempPath);
		if (!tempFiles.exists()) {
			tempFiles.mkdirs();
		}
		List<String> fileNameList = new ArrayList<String>();
		for (int i = 0; i < urlList.size(); i++) {

			String urlPath = urlList.get(i);
			InputStream is = null;
			FileOutputStream fos = null;
			try {
				URL url = new URL(urlPath); // 把字符串转换为URL请求地址
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();// 打开连接
				connection.connect();// 连接会话
				is = connection.getInputStream();
				int d = -1;
				String tempFile = tempPath + "/" + urlPath.substring(urlPath.lastIndexOf("/") + 1);
				while (new File(tempFile).exists()) {
					tempFile = tempFile.substring(0, tempFile.lastIndexOf(".")) + "temp"
							+ tempFile.substring(tempFile.lastIndexOf("."), tempFile.length());
				}
				fos = new FileOutputStream(tempFile);
				while ((d = is.read()) != -1) {
					fos.write(d);
				}
				fileNameList.add(tempFile);
			} catch (Exception e) {
				e.printStackTrace();
				print(e);
			} finally {
				try {
					if (fos != null)
						fos.close();
					if (is != null)
						is.close();
				} catch (IOException e) {
					e.printStackTrace();
					print(e);
				}
			}

		}

		return fileNameList;
	}

	/**
	 * http发送json请求专用,postData先用json对象封装了
	 * 
	 * @param url
	 *            发送的地址
	 * @param postData
	 *            发送的数据
	 * @return 返回的json字符串
	 * @throws IOException
	 */
	public static String postUrlReturnString(String url, String postData) throws IOException {
		HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
		conn.addRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Accept", "application/json");
		conn.addRequestProperty("Content-Type", "application/json");
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.connect();
		DataOutputStream out = new DataOutputStream(conn.getOutputStream());
		if (postData != null)
			out.write(postData.getBytes());
		out.flush();
		out.close();
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		StringBuilder resultBuilder = new StringBuilder();
		String line = "";
		while (!StringUtils.isEmpty(line = br.readLine())) {
			resultBuilder.append(line);
		}
		conn.disconnect();
		return resultBuilder.toString();
	}

	public static void print(Exception e) {
		synchronized (MAIL_LOCK) {
			File dir = new File(DIR_PATH);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			try (PrintWriter pw = new PrintWriter(new FileOutputStream(LO_PATH, true))) {
				e.printStackTrace(pw);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}

	public static void print(String str) {
		synchronized (MAIL_LOCK) {
			File dir = new File(DIR_PATH);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			try (PrintWriter pw = new PrintWriter(new FileOutputStream(LO_PATH, true))) {
				pw.println(sdf.format(new Date()) + "     " + str);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}

}
