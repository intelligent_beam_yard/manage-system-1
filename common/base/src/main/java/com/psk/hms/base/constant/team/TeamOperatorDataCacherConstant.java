package com.psk.hms.base.constant.team;

/**
 * 权限数据缓存常量(团队自定义)
 * @author xiangjz
 */
public class TeamOperatorDataCacherConstant {
	// 权限数据，自定义屏蔽的权限集合
	public static final String TEAM_OPERATOR_DATA_DENIED = "teamOperatorDataDenied";
	// 权限数据，自定义通过的权限集合
	public static final String TEAM_OPERATOR_DATA_PASSED = "teamOperatorDataPassed";
	
	// 这里是授权直接通过的，不用进行其它验证
	public static final String TEAM_OPERATOR_AUTH_PASS = "passed";
	// 这里是未授权的，需要进行其它验证
	public static final String TEAM_OPERATOR_AUTH_UNAUTH = "unAuth";
	// 这里是授权直接不通过的，需要进行其它验证，在移除了这里的bizIds之后验证
	public static final String TEAM_OPERATOR_AUTH_DENY = "denied";
}
