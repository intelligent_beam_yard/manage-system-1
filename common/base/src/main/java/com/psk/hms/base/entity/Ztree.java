package com.psk.hms.base.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 描述: 树节点实体
 * 作者: xiangjz
 * 时间: 2017年4月29日
 * 版本: 1.0
 *
 */
public class Ztree implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 节点id
	 */
	private String id;

	/**
	 * 节点名称
	 */
	private String name;
	/**
	 * 节点名称
	 */
	private String title;
	/**
	 * 节点名称
	 */
	private String label;
	/**
	 * 节点排序
	 */
	private int sort;
	
	/**
	 * code
	 */
	private String code;
	/**
	 * 关联字段id
	 */
	private String relevanceId;
	/**
	 * 是否授权
	 */
	private boolean isAuthorise;

	/**
	 * 是否显示
	 */
	private boolean isShow;


	/**
	 * 是否上级节点
	 */
	private boolean isParent;

	/**
	 * 是否选中
	 */
	private boolean checked;

	/**
	 * 是否选中
	 */
	private boolean nocheck;
	
	/**
	 * 节点图标
	 */
	private String icon;
	
	/**
	 * 子节点数据
	 */
	private List<Ztree> children;
	
	/**
	 * 文件相对路径前缀
	 */
	public static final String ICON_URL_PREFIX = "/js/ztree/css/zTreeStyle/img/diy/";
	
	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public boolean isAuthorise() {
		return isAuthorise;
	}

	public void setAuthorise(boolean isAuthorise) {
		this.isAuthorise = isAuthorise;
	}

	public String getRelevanceId() {
		return relevanceId;
	}

	public void setRelevanceId(String relevanceId) {
		this.relevanceId = relevanceId;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public boolean getIsShow() {
		return isShow;
	}
	
	public void setIsShow(boolean isShow) {
		this.isShow = isShow;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isNocheck() {
		return nocheck;
	}

	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<Ztree> getChildren() {
		return children;
	}

	public void setChildren(List<Ztree> children) {
		this.children = children;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

}
