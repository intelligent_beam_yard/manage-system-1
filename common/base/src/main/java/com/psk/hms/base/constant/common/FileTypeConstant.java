package com.psk.hms.base.constant.common;


/**
 * 描述： 文件类型code常量类
 * 作者： xiangjz
 * 时间： 2018年6月5日
 * 版本： 1.0v
 */
public class FileTypeConstant {

	
	
	/**
	 * 通用：模板
	 */
	public static final String FILETYPE_BASE_TEMPLATE ="BASE_TEMPLATE";
	
	
	/**
	 * 劳务-图片：考勤
	 */
	public static final String FILETYPE_LABOR_PICTURE_CHECKING ="LABOR_PICTURE_CHECKING";
	/**
	 * 劳务-图片：头像
	 */
	public static final String FILETYPE_LABOR_PICTURE_HEADPORTRAIT ="LABOR_PICTURE_HEADPORTRAIT";
	/**
	 * 劳务-图片：指纹
	 */
	public static final String FILETYPE_LABOR_PICTURE_FINGERPRINT ="LABOR_PICTURE_FINGERPRINT";
	/**
	 * 劳务-图片：奖惩
	 */
	
	public static final String FILETYPE_LABOR_PICTURE_VOLATION ="1531303384691";
	/**
	 * 劳务-图片：资格证书
	 */
	
	public static final String FILETYPE_LABOR_PICTURE_QUALIFICATION ="1532502217360";
	
	/**
	 * 团队：知识库
	 */
	public static final String FILETYPE_BASE_KNOWLEDGE ="BASE_KNOWLEDGE";
	
	
	/**
	 * 团队-团队成员：团队成员头像
	 */
	public static final String FILETYPE_TEAM_EMPLOYEE_AVATAR ="TEAM_EMPLOYEE_AVATAR";
	
	/**
	 * 团队：团队动态
	 */
	public static final String FILETYPE_DYNAMIC_ATTACH ="DYNAMIC_ATTACH";
	/**
	 * 团队：团队公告
	 */
	public static final String FILETYPE_PUBLIC_NEWS ="PUBLIC_NEWS";
	/**
	 * 团队：党建
	 */
	public static final String FILETYPE_PARTY_NEWS ="PARTY_NEWS";
	/**
	 * 团队：广告
	 */
	public static final String FILETYPE_BASE_ADS ="BASE_ADS";
	/**
	 * 基础-权限-用户：用户头像
	 */
	public static final String FILETYPE_USER_HEAD ="USER_HEAD";

	/**
	 * 基础-资源管理-文件-类型：知识库
	 */
	public static final String FILETYPE_FILE_TYPE_1 ="1531362920433";
	
	/**
	 * 设备
	 */
	public static final String FILETYPE_EQUIPMENT ="EQUIPMENT";
	/**
	 * 设备:封面
	 */
	public static final String FILETYPE_COVER ="1530685291935";
	/**
	 * 设备:视频监控截取
	 */
	public static final String FILETYPE_MONITOR_CUTTING ="1541743782234";
	
	/**
	 * 设备-文件-图片:人员分布图
	 */
	public static final String FILETYPE_EQUIPMENT_FILE_PICTURE_DISTRIBUTION ="1532505115073";
	/**
	 * 设备-文件-图片:视频封面
	 */
	public static final String FILETYPE_EQUIPMENT_FILE_PICTURE_POSTER ="QZ_EQUIPMENT_FILE_POSTER";
	
	/**
	 * 项目-巡检：安全巡检附件
	 */
	public static final String FILETYPE_SAFETY_INSPECT ="SAFETY_INSPECT";
	/**
	 * 项目-巡检：安全整改附件
	 */
	public static final String FILETYPE_SAFETY_INSPECT_RECT ="SAFETY_INSPECT_RECT";
	/**
	 * 项目-巡检：质量巡检附件
	 */
	public static final String FILETYPE_QUALITY_INSPECT ="QUALITY_INSPECT";
	/**
	 * 项目-巡检：质量整改附件
	 */
	public static final String FILETYPE_QUALITY_INSPECT_RECT ="QUALITY_INSPECT_RECT";
	
	/**
	 * 项目-巡检：设备巡检附件
	 */
	public static final String FILETYPE_EQUIP_INSPECT ="EQUIP_INSPECT";
	/**
	 * 项目-巡检：设备台账
	 */
	public static final String FILETYPE_EQUIP_INFO ="EQUIP_INFO";
	/**
	 * 项目-巡检：设备人员
	 */
	public static final String FILETYPE_EQUIP_PERSON ="EQUIP_PERSON";
	
	/**
	 * 项目-质量交底
	 */
	public static final String PROJECT_DISCLOSE ="DISCLOSE";
	
	/**
	 * 项目-质量交底
	 */
	public static final String BUILDER_DIARY ="BUILDER_DIARY";

	/**
	 * 项目-工资公示
	 */
	public static final String PROJECT_WAGES_PUBLICITY ="WAGES_PUBLICITY";
	
	
	
}
