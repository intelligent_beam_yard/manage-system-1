package com.psk.hms.base.parameter;

import java.lang.reflect.Method;

/**
 * 描述: 代码翻译转换参数 作者: xiangjz 时间: 2018年5月9日 版本: 1.0
 *
 */
public class TranslateParam {

	/**
	 * 字典表code
	 */
	private String dictCode;

	/**
	 * 需要被转换的属性名
	 */
	private String sourcePropertyName;

	/**
	 * 转换后增加的属性名
	 */
	private String targetPropertyName;

	private Method readMethod;

	/**
	 * 默认构造方法
	 * 
	 * @param dictId
	 *            - 字典表ID
	 * @param sourcePropertyName
	 *            - 需要被转换的属性名
	 * @param targetPropertyName
	 *            - 转换后增加的属性名
	 * @update:
	 */
	public TranslateParam(String dictCode, String sourcePropertyName, String targetPropertyName) {
		super();
		this.dictCode = dictCode;
		this.setSourcePropertyName(sourcePropertyName);
		this.targetPropertyName = targetPropertyName;
	}

	public String getTargetPropertyName() {
		return targetPropertyName;
	}

	public void setTargetPropertyName(String targetPropertyName) {
		this.targetPropertyName = targetPropertyName;
	}

	public void setReadMethod(Method readMethod) {
		this.readMethod = readMethod;
	}

	public Method getReadMethod() {
		return readMethod;
	}

	public void setSourcePropertyName(String sourcePropertyName) {
		this.sourcePropertyName = sourcePropertyName;
	}

	public String getSourcePropertyName() {
		return sourcePropertyName;
	}

	public String getDictCode() {
		return dictCode;
	}

	public void setDictCode(String dictCode) {
		this.dictCode = dictCode;
	}
}
