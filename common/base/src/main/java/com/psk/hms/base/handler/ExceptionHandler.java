package com.psk.hms.base.handler;

import com.psk.hms.base.exception.BizException;
import com.psk.hms.base.exception.EverHighTimeoutException;
import com.psk.hms.base.exception.base.BaseBizExceptionCode;
import com.psk.hms.base.exception.thirdParty.ThirdPartyBizExceptionCode;
import com.psk.hms.base.parameter.JsonResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 描述： 异常处理器
 * 作者： xiangjz
 * 时间： 2018年9月28日
 * 版本： 2.0v
 */
@ControllerAdvice
public class ExceptionHandler {

    private static final Log log = LogFactory.getLog(ExceptionHandler.class);

    //restTemplate连接异常
    @org.springframework.web.bind.annotation.ExceptionHandler(RestClientException.class)
    @ResponseBody
    public JsonResult globelRestClientExceptionResult(HttpServletRequest request, RestClientException e) {
        log.error(e);
        return new JsonResult().create(BaseBizExceptionCode.CONNECT_ERROR, "连接异常");
    }

    //业务异常
    @org.springframework.web.bind.annotation.ExceptionHandler(BizException.class)
    @ResponseBody
    public JsonResult globelBizExceptionResult(HttpServletRequest request, BizException e) {
        log.error(e);
        return new JsonResult().create(e.getCode(), e.getMsg());
    }

    //运行时异常
    @org.springframework.web.bind.annotation.ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public JsonResult globelRuntimeExceptionResult(HttpServletRequest request, RuntimeException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //空指针异常
    @org.springframework.web.bind.annotation.ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public JsonResult globelNullPointerExceptionResult(HttpServletRequest request, NullPointerException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //类型转换异常
    @org.springframework.web.bind.annotation.ExceptionHandler(ClassCastException.class)
    @ResponseBody
    public JsonResult globelClassCastExceptionResult(HttpServletRequest request, ClassCastException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //IO异常
    @org.springframework.web.bind.annotation.ExceptionHandler(IOException.class)
    @ResponseBody
    public JsonResult globelIOExceptionResult(HttpServletRequest request, IOException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //未知方法异常
    @org.springframework.web.bind.annotation.ExceptionHandler(NoSuchMethodException.class)
    @ResponseBody
    public JsonResult globelNoSuchMethodExceptionResult(HttpServletRequest request, NoSuchMethodException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //数组越界异常
    @org.springframework.web.bind.annotation.ExceptionHandler(IndexOutOfBoundsException.class)
    @ResponseBody
    public JsonResult globelIndexOutOfBoundsExceptionResult(HttpServletRequest request, IndexOutOfBoundsException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //400
    @org.springframework.web.bind.annotation.ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseBody
    public JsonResult globelHttpMessageNotReadableExceptionResult(HttpServletRequest request, HttpMessageNotReadableException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //400
    @org.springframework.web.bind.annotation.ExceptionHandler(TypeMismatchException.class)
    @ResponseBody
    public JsonResult globelTypeMismatchExceptionResult(HttpServletRequest request, TypeMismatchException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //400
    @org.springframework.web.bind.annotation.ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    public JsonResult globelMissingServletRequestParameterExceptionResult(HttpServletRequest request, MissingServletRequestParameterException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //405
    @org.springframework.web.bind.annotation.ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public JsonResult globelHttpRequestMethodNotSupportedExceptionResult(HttpServletRequest request, HttpRequestMethodNotSupportedException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //406
    @org.springframework.web.bind.annotation.ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    @ResponseBody
    public JsonResult globelHttpMediaTypeNotAcceptableExceptionResult(HttpServletRequest request, HttpMediaTypeNotAcceptableException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //500
    @org.springframework.web.bind.annotation.ExceptionHandler(ConversionNotSupportedException.class)
    @ResponseBody
    public JsonResult globelConversionNotSupportedExceptionResult(HttpServletRequest request, ConversionNotSupportedException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //500
    @org.springframework.web.bind.annotation.ExceptionHandler(HttpMessageNotWritableException.class)
    @ResponseBody
    public JsonResult globelHttpMessageNotWritableExceptionResult(HttpServletRequest request, HttpMessageNotWritableException e) {
        log.error(e);
        return new JsonResult().failure(e);
    }

    //系统异常(最高大上的异常   我居然没捕捉到)
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public JsonResult globelExceptionResult(HttpServletRequest request, Exception e) {
        log.error(e);
        return new JsonResult().failure(JsonResult.ERROR_MSG);
    }

    //文件上传超过最大异常
    @org.springframework.web.bind.annotation.ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseBody
    public JsonResult uploadExceptionResult(HttpServletRequest request, MaxUploadSizeExceededException e) {
        log.error(e);
        return new JsonResult().create(BaseBizExceptionCode.FILE_MANAGE_UPLOAD_ERROR, "单个文件不得超过20M, 单次上传文件总共不得超过200M");
    }

    // 恒高服务超时异常
    @org.springframework.web.bind.annotation.ExceptionHandler(EverHighTimeoutException.class)
    @ResponseBody
    public JsonResult everhighTimeoutExceptionResult(HttpServletRequest request, EverHighTimeoutException e) {
        log.error("", e);
        return new JsonResult().
                create(ThirdPartyBizExceptionCode.EVERHIGH_SERVER_TIMEOUT, "服务超时");
    }
}
