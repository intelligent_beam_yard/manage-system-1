package com.psk.hms.base.util.validator.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.psk.hms.base.util.validator.field.MapValidatable;
import com.psk.hms.base.util.validator.result.MapValidateResult;

/**
 * 默认的字段校验执行器。此执行器将会一次调用校验链中的校验器，无论校验是否通过。并将校验不匹配
 * 的结果依次写入结果集中。
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public class DefaultMapValidatorExecutor<K, V> implements MapValidatorExecutable<K, V> {

	@Override
	public Map<String, Object> execute(Map<K, V> map, List<MapValidatable<K, V>> validatorList) {
		Map<String, Object> result = new HashMap<>();
		for (MapValidatable<K, V> validator : validatorList) {
			MapValidateResult<K> mapValidateResult = validator.check(map);
			if(!mapValidateResult.isPass()) {
				result.put(mapValidateResult.getKey().toString(), mapValidateResult.getErrorMsg());
			}
		}
		return result;
	}

}
