package com.psk.hms.base.util.validator.field;

import com.psk.hms.base.util.validator.field.impl.TypeFieldValidator;

public abstract class CompareIfExistFieldValidator<K, V, T extends Comparable<T>> extends TypeFieldValidator<K, V> {

	public CompareIfExistFieldValidator(K key, Class<?> type, boolean keyRequired) {
		super(key, type, keyRequired);
		this.errorMsg = getDefaultErrorMsg(key);
	}

	public CompareIfExistFieldValidator(K key, Class<?> type, boolean keyRequired, String errorMsg) {
		super(key, type, keyRequired, errorMsg);
	}

	protected boolean isEqualBorder(T border, T value) {
		return border == null ? false : border.compareTo(value) == 0;
	}

	protected boolean isAboveBorder(T border, T value) {
		return border == null ? false : border.compareTo(value) < 0;
	}

	protected boolean isUnderBorder(T border, T value) {
		return border == null ? false : border.compareTo(value) > 0;
	}

}
