package com.psk.hms.base.constant.team;


import com.psk.hms.base.constant.common.Constant;

/**
 * 团队成员职务关联常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamJobEmployeeConstant extends Constant {
    /**
     *  数据表字段名称 - jobId
     */
    public static final String JOB_ID = "jobId";
    /**
     *  数据表字段名称 - employeeId
     */
    public static final String EMPLOYEE_ID = "employeeId";
}
