package com.psk.hms.base.parameter.validator.field;

/**
 * 校验错误消息生成能力接口。实现此接口便可以生成自定义的校验不匹配返回消息
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public interface ErrorMsgGeneratable<K, V> {

	/**
	 * 获取默认的校验不匹配消息。当用户未指定校验不匹配消息时，通过此方法来生成一个
	 * 默认的校验不匹配消息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public String getDefaultErrorMsg(K key);
	
}
