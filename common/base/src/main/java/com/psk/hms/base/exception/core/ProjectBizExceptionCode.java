package com.psk.hms.base.exception.core;

/**
 * <p>
 * project 业务异常类
 * </p>
 * 模块异常码 : 23000001 ~ 23999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class ProjectBizExceptionCode {
	
	/***
	 * 项目模块
	 * 23010000~23019999
	 */
	//项目模块-项目信息-增 
	public static final int PROJECT_INFO_SAVE_ERROR = 23010101;
	//项目模块-项目信息-删
	public static final int PROJECT_INFO_DELETE_ERROR = 23010102;
	//项目模块-项目信息-查
	public static final int PROJECT_INFO_SELECT_ERROR = 23010103;
	//项目模块-项目信息-改
	public static final int PROJECT_INFO_UPDATE_ERROR = 23010104;
	//项目模块-项目信息-通用异常
	public static final int PROJECT_INFO_MANAGE_ERROR = 23010199;
	
	//项目模块-项目全景-增
	public static final int PROJECT_PANORAMA_SAVE_ERROR = 23010201;
	//项目模块-项目全景-删
	public static final int PROJECT_PANORAMA_DELETE_ERROR = 23010202;
	//项目模块-项目全景-查
	public static final int PROJECT_PANORAMA_SELECT_ERROR = 23010203;
	//项目模块-项目全景-改
	public static final int PROJECT_PANORAMA_UPDATE_ERROR = 23010204;
	//项目模块-项目全景-通用异常
	public static final int PROJECT_PANORAMA_MANAGE_ERROR = 23010299;
	
	//项目模块-项目进度-增
	public static final int PROJECT_PROGRESS_SAVE_ERROR = 23010301;
	//项目模块-项目进度-删
	public static final int PROJECT_PROGRESS_DELETE_ERROR = 23010302;
	//项目模块-项目进度-查
	public static final int PROJECT_PROGRESS_SELECT_ERROR = 23010303;
	//项目模块-项目进度-改
	public static final int PROJECT_PROGRESS_UPDATE_ERROR = 23010304;
	//项目模块-项目进度-通用异常
	public static final int PROJECT_PROGRESS_MANAGE_ERROR = 23010399;
	
	//项目模块-项目班组-增
	public static final int PROJECT_WORKTEAM_SAVE_ERROR = 23010401;
	//项目模块-项目班组-删
	public static final int PROJECT_WORKTEAM_DELETE_ERROR = 23010402;
	//项目模块-项目班组-查
	public static final int PROJECT_WORKTEAM_SELECT_ERROR = 23010403;
	//项目模块-项目班组-改
	public static final int PROJECT_WORKTEAM_UPDATE_ERROR = 23010404;
	//项目模块-项目班组-通用异常
	public static final int PROJECT_WORKTEAM_MANAGE_ERROR = 23010499;
	
	//项目模块-项目工种-增
	public static final int PROJECT_WORKTYPE_SAVE_ERROR = 23010501;
	//项目模块-项目工种-删
	public static final int PROJECT_WORKTYPE_DELETE_ERROR = 23010502;
	//项目模块-项目工种-查
	public static final int PROJECT_WORKTYPE_SELECT_ERROR = 23010503;
	//项目模块-项目工种-改
	public static final int PROJECT_WORKTYPE_UPDATE_ERROR = 23010504;
	//项目模块-项目工种-通用异常
	public static final int PROJECT_WORKTYPE_MANAGE_ERROR = 23010599;
	
	//项目模块-项目每日任务-增
	public static final int PROJECT_DAILY_TASK_SAVE_ERROR = 23010601;
	//项目模块-项目每日任务-删
	public static final int PROJECT_DAILY_TASK_DELETE_ERROR = 23010602;
	//项目模块-项目每日任务-查
	public static final int PROJECT_DAILY_TASK_SELECT_ERROR = 23010603;
	//项目模块-项目每日任务-改
	public static final int PROJECT_DAILY_TASK_UPDATE_ERROR = 23010604;
	//项目模块-项目每日任务-通用异常
	public static final int PROJECT_DAILY_TASK_MANAGE_ERROR = 23010699;
	
	//项目模块-项目值班人员-增
	public static final int PROJECT_DUTY_STAFF_SAVE_ERROR = 23010701;
	//项目模块-项目值班人员-删
	public static final int PROJECT_DUTY_STAFF_DELETE_ERROR = 23010702;
	//项目模块-项目值班人员-查
	public static final int PROJECT_DUTY_STAFF_SELECT_ERROR = 23010703;
	//项目模块-项目值班人员-改
	public static final int PROJECT_DUTY_STAFF_UPDATE_ERROR = 23010704;
	//项目模块-项目值班人员-通用异常
	public static final int PROJECT_DUTY_STAFF_MANAGE_ERROR = 23010799;
	
	//项目模块-项目值班信息-增
	public static final int PROJECT_DUTY_SAVE_ERROR = 23010801;
	//项目模块-项目值班信息-删
	public static final int PROJECT_DUTY_DELETE_ERROR = 23010802;
	//项目模块-项目值班信息-查
	public static final int PROJECT_DUTY_SELECT_ERROR = 23010803;
	//项目模块-项目值班信息-改
	public static final int PROJECT_DUTY_UPDATE_ERROR = 23010804;
	//项目模块-项目值班信息-通用异常
	public static final int PROJECT_DUTY_MANAGE_ERROR = 23010899;
	
	//项目模块-项目值班类型-增
	public static final int PROJECT_DUTY_ITEM_SAVE_ERROR = 23010901;
	//项目模块-项目值班类型-删
	public static final int PROJECT_DUTY_ITEM_DELETE_ERROR = 23010902;
	//项目模块-项目值班类型-查
	public static final int PROJECT_DUTY_ITEM_SELECT_ERROR = 23010903;
	//项目模块-项目值班类型-改
	public static final int PROJECT_DUTY_ITEM_UPDATE_ERROR = 23010904;
	//项目模块-项目值班类型-通用异常
	public static final int PROJECT_DUTY_ITEM_MANAGE_ERROR = 23010999;
	
	//项目模块-项目总值班信息-增
	public static final int PROJECT_DUTY_GENERAL_SAVE_ERROR = 23011001;
	//项目模块-项目总值班信息-删
	public static final int PROJECT_DUTY_GENERAL_DELETE_ERROR = 23011002;
	//项目模块-项目总值班信息-查
	public static final int PROJECT_DUTY_GENERAL_SELECT_ERROR = 23011003;
	//项目模块-项目总值班信息-改
	public static final int PROJECT_DUTY_GENERAL_UPDATE_ERROR = 23011004;
	//项目模块-项目总值班信息-通用异常
	public static final int PROJECT_DUTY_GENERAL_MANAGE_ERROR = 23011099;
	
	//项目模块-测量实体-增
	public static final int PROJECT_MEASURE_ENTITY_SAVE_EEEOR = 23011101;
	//项目模块-测量实体-删
	public static final int PROJECT_MEASURE_ENTITY_DELETE_EEEOR = 23011102;
	//项目模块-测量实体-查
	public static final int PROJECT_MEASURE_ENTITY_SELECT_EEEOR = 23011103;
	//项目模块-测量实体-改
	public static final int PROJECT_MEASURE_ENTITY_UPDATE_EEEOR = 23011104;
	//项目模块-测量实体-通用异常
	public static final int PROJECT_MEASURE_ENTITY_EEEOR = 23011199;
	
	//项目模块-测量检查项-增
	public static final int PROJECT_MEASURE_ITEM_SAVE_EEEOR = 23011201;
	//项目模块-测量检查项-删
	public static final int PROJECT_MEASURE_ITEM_DELETE_EEEOR = 23011202;
	//项目模块-测量检查项-查
	public static final int PROJECT_MEASURE_ITEM_SELECT_EEEOR = 23011203;
	//项目模块-测量检查项-改
	public static final int PROJECT_MEASURE_ITEM_UPDATE_EEEOR = 23011204;
	//项目模块-测量检查项-通用异常
	public static final int PROJECT_MEASURE_ITEM_EEEOR = 23011299;
	
	//项目模块-测量记录-增
	public static final int PROJECT_MEASURE_RECORD_SAVE_EEEOR = 23011301;
	//项目模块-测量记录-删
	public static final int PROJECT_MEASURE_RECORD_DELETE_EEEOR = 23011302;
	//项目模块-测量记录-查
	public static final int PROJECT_MEASURE_RECORD_SELECT_EEEOR = 23011303;
	//项目模块-测量记录-改
	public static final int PROJECT_MEASURE_RECORD_UPDATE_EEEOR = 23011304;
	//项目模块-测量记录-通用异常
	public static final int PROJECT_MEASURE_RECORD_EEEOR = 23011399;
	
	//项目模块-测量点-增
	public static final int PROJECT_MEASURE_POINT_SAVE_EEEOR = 23011401;
	//项目模块-测量点-删
	public static final int PROJECT_MEASURE_POINT_DELETE_EEEOR = 23011402;
	//项目模块-测量点-查
	public static final int PROJECT_MEASURE_POINT_SELECT_EEEOR = 23011403;
	//项目模块-测量点-改
	public static final int PROJECT_MEASURE_POINT_UPDATE_EEEOR = 23011404;
	//项目模块-测量点-通用异常
	public static final int PROJECT_MEASURE_POINT_EEEOR = 23011499;
	
	//项目模块-测量点-增
	public static final int PROJECT_MEASURE_ITEM_TARGET_SAVE_EEEOR = 23011501;
	//项目模块-测量点-删
	public static final int PROJECT_MEASURE_ITEM_TARGET_DELETE_EEEOR = 23011502;
	//项目模块-测量点-查
	public static final int PROJECT_MEASURE_ITEM_TARGET_SELECT_EEEOR = 23011503;
	//项目模块-测量点-改
	public static final int PROJECT_MEASURE_ITEM_TARGET_UPDATE_EEEOR = 23011504;
	//项目模块-测量点-通用异常
	public static final int PROJECT_MEASURE_ITEM_TARGET_EEEOR = 23011599;
	
	//项目模块-项目要闻-增
	public static final int PROJECT_NEWS_SAVE_ERROR = 23010105;
	//项目模块-项目要闻-删
	public static final int PROJECT_NEWS_DELETE_ERROR = 23010105;
	//项目模块-项目要闻-查
	public static final int PROJECT_NEWS_SELECT_ERROR = 23010105;
	//项目模块-项目要闻-改
	public static final int PROJECT_NEWS_UPDATE_ERROR = 23010105;
	//项目模块-项目要闻-通用异常
	public static final int PROJECT_NEWS_ERROR = 23010105;
	
	//项目模块-大屏免登配置-增
	public static final int PROJECT_BIGSCREENAUTHCONFIG_SAVE_ERROR = 23011601;
	//项目模块-大屏免登配置-删
	public static final int PROJECT_BIGSCREENAUTHCONFIG_DELETE_ERROR = 23011602;
	//项目模块-大屏免登配置-查
	public static final int PROJECT_BIGSCREENAUTHCONFIG_SELECT_ERROR = 23011603;
	//项目模块-大屏免登配置-改
	public static final int PROJECT_BIGSCREENAUTHCONFIG_UPDATE_ERROR = 23011604;
	//项目模块-大屏免登配置-通用异常
	public static final int PROJECT_BIGSCREENAUTHCONFIG_ERROR = 23011605;
	
	//项目模块-交底-增
	public static final int PROJECT_DISCLOSE_SAVE_ERROR = 23011701;
	//项目模块-交底-删
	public static final int PROJECT_DISCLOSE_DELETE_ERROR = 23011702;
	//项目模块-交底-查
	public static final int PROJECT_DISCLOSE_SELECT_ERROR = 23011703;
	//项目模块-交底-改
	public static final int PROJECT_DISCLOSE_UPDATE_ERROR = 23011704;
	//项目模块-交底-通用异常
	public static final int PROJECT_DISCLOSE_ERROR = 23011705;
	
	//项目模块-工资公示-增
	public static final int PROJECT_WAGES_PUBLICITY_SAVE_ERROR = 23011801;
	//项目模块-工资公示-删
	public static final int PROJECT_WAGES_PUBLICITY_DELETE_ERROR = 23011802;
	//项目模块-工资公示-查
	public static final int PROJECT_WAGES_PUBLICITY_SELECT_ERROR = 23011803;
	//项目模块-工资公示-改
	public static final int PROJECT_WAGES_PUBLICITY_UPDATE_ERROR = 23011804;
	//项目模块-工资公示-通用异常
	public static final int PROJECT_WAGES_PUBLICITY_ERROR = 23011805;
	
	//项目模块-通用异常
	public static final int PROJECT_MODULE_ERROR = 23019999;
}
