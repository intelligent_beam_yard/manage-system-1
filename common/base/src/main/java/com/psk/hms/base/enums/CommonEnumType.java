package com.psk.hms.base.enums;

import com.psk.hms.base.util.BaseUtil;

/**
 * 
 * @功能说明：枚举定义{ }
 * 
 * @作者：xiangjz @创建日期： 2018-01-02
 */
public class CommonEnumType {

	private static SystemResourceUtil util = SystemResourceUtil.getInstance();

	/**
	 * 根据key获取名称
	 * 
	 * @param val
	 * @return
	 */
	public static String getEnumNameByKey(String val, String keys) {

		Object key = util.resourceAsMap(val).get(keys);
		if (BaseUtil.isEmpty(key))
			return "";
		return (String) key;
	}
	
	/**
	 * 数据权限类型:
	 */
	public enum DataAuthType {
		单位类型("1001"), 单位("2001");
		private String value;
		
		public String getValue() {
			return value;
		}

		DataAuthType(String value) {
			this.value = value;
		}
	}
	
	/**
	 * excel模板类型:
	 */
	public enum TemplateExcelType {
		用户导入("0001");
		private String value;
		
		public String getValue() {
			return value;
		}
		
		TemplateExcelType(String value) {
			this.value = value;
		}
	}
	
	/**
	 * excel模板字段数据来源类型 （没用了，用的数据字典暂时）:
	 */
	public enum ImportTemplateDataSourceType {
		业务来源("0000"), 数据字典来源("0001"), 枚举字典来源("0002"), 单位类型来源("0003"), 单位名称来源("0004");
		private String value;
		
		public String getValue() {
			return value;
		}
		
		ImportTemplateDataSourceType(String value) {
			this.value = value;
		}
	}
	
	/**
	 * 权限类型(应用于):
	 */
	public enum ApplyTo {
		mobile菜单("0"), admin菜单("1"), 功能接口("2"), mobile路由("3"), admin按钮("4");
		private String value;
		
		public String getValue() {
			return value;
		}

		ApplyTo(String value) {
			this.value = value;
		}
	}

	/**
	 * 附件类型:
	 */
	public enum AttachType {
		测试附件类型0("0");
		private String value;

		public String getValue() {
			return value;
		}

		AttachType(String value) {
			this.value = value;
		}
	}
	
	/**
	 * 未删除/已删除
	 */
	public enum IsDelete {
		未删除("0"), 已删除("1");
		private String value;

		public String getValue() {
			return value;
		}

		IsDelete(String value) {
			this.value = value;
		}
	}

	/**
	 * 性别
	 */
	public enum Gender {
		女("f"), 男("m"), 人妖("b"), 未知("u");
		private String value;

		public String getValue() {
			return value;
		}

		Gender(String value) {
			this.value = value;
		}
	}

	/**
	 * 是/否
	 */
	public enum YesOrNo {
		否("0"), 是("1");
		private String value;

		public String getValue() {
			return value;
		}

		YesOrNo(String value) {
			this.value = value;
		}
	}

	/**
	 * 权限业务对象类型
	 */
	public enum OperatorBizType {
		通用角色("1001"), 单位类型("2001"), 单位("2002"), 单位人员("2003");
		private String value;

		public String getValue() {
			return value;
		}

		OperatorBizType(String value) {
			this.value = value;
		}
	}
	
}