package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队授权业务关联常量类
 * @author xiangjz
 * @date 2018年8月28日
 * @version 1.0.0
 *
 */
public class TeamAuthBizConstant extends Constant {
    /**
     *  数据库字段-团队授权id
     */
    public static final String TEAM_AUTH_ID = "teamAuthId";
    /**
     *  数据库字段-团队授权关联的业务对象id，比如角色、组织机构、工种等等，具体类型根据biz_type来决定
     */
    public static final String BIZ_ID = "bizId";
    /**
     *  数据库字段-团队授权关联的业务对象类型(参考字典)
     */
    public static final String BIZ_TYPE = "bizType"; 
    /**
     * 关联类型1-角色 
     */
    public static final String BIZ_TYPE_1 = "1";
}
