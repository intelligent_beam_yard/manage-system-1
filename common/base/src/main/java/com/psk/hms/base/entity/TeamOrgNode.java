package com.psk.hms.base.entity;

import java.io.Serializable;

/**
 * 团队组织机构树
 * 
 * @author xiangjz
 * @date 2018年5月14日
 * @version 1.0.0
 *
 */
public class TeamOrgNode implements Serializable {
	private static final long serialVersionUID = 1L;

	// 组织ID
	private String id;
	// 组织名称
	private String name;
	// 排序
	private int sort;
	// 组织ID
	private String parentId;
	// 组织列表ID
	private String[] parentIds;

	public TeamOrgNode() {
	}

	public TeamOrgNode(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String[] getParentIds() {
		return parentIds;
	}

	public void setParentIds(String[] parentIds) {
		this.parentIds = parentIds;
	}

}
