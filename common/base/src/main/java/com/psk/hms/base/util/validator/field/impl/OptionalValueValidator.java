package com.psk.hms.base.util.validator.field.impl;

import com.psk.hms.base.util.validator.field.AbstractMapEntityValidator;
import com.psk.hms.base.util.validator.result.MapValidateResult;

/**
 * 可选值校验 如:性别为 `男` 或 `女`
 *
 * @author xiangjz
 */
public class OptionalValueValidator<K, V> extends AbstractMapEntityValidator<K, V> {

	protected Object[] values = new Object[] {};

	public OptionalValueValidator(K key, boolean keyRequired, String errorMsg, Object... values) {
		super(key, keyRequired, errorMsg);
		// fix errMsg
		if (errorMsg == null || "".equals(errorMsg)) {
			this.errorMsg = getDefaultErrorMsg(key);
		}
		if (values.length != 0)
			this.values = values;
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		int cnt = 0;
		for (Object v : values) {
			if (v.equals(value)) {
				++cnt;
			}
		}
		if (cnt <= 0)
			return new MapValidateResult<>(key).failure(errorMsg);
		else
			return new MapValidateResult<>(key).success();
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		if (values.length == 0) {
			sb.append("'").append(key).append("'没有传入可选值校验");
		} else
			sb.append("'").append(key).append("'应为").append(values).append("中的一个值");
		return sb.toString();
	}

}
