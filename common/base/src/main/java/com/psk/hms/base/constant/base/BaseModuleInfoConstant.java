package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础菜单常量类
 * @version 1.0.0
 *2018年11月30日 10:35:12
 */
public class BaseModuleInfoConstant extends Constant {

	/**
	 * 菜单业务关联images
	 */
	public static final String IMAGES = "images";
	/**
	 * 菜单业务关联levels
	 */
	public static final String LEVELS = "levels";
	/**
	 * 菜单业务关联operator_id
	 */
	public static final String OPERATOR_ID = "operatorId";
	/**
	 * 菜单业务关联parent_id
	 */
	public static final String PARENT_ID = "parentId";
	/**
	 * 菜单业务关联team_type_id
	 */
	public static final String TEAM_TYPE_ID = "teamTypeId";
	/**
	 * 菜单业务关联is_parent
	 */
	public static final String IS_PARENT = "isParent";
	/**
	 * 菜单业务关联name
	 */
	public static final String NAME = "name";
	/**
	 * 菜单业务关联sort
	 */
	public static final String SORT = "sort";
	

}
