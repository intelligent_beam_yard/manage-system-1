package com.psk.hms.base.entity;

import java.io.Serializable;
import java.util.Map;

/**
 * 描述: 拖拽数节点bean
 * 作者: xiangjz
 * 时间: 2018年11月21日
 * 版本: 1.0
 *
 */
public class DragTreeNodeBean implements Serializable{

	private static final long serialVersionUID = 1L;

	//节点id
	private String id;
	//节点id字段名
	private String idFieldName = "id";
	
	//目标的父节点id
	private String destParentId;
	//父节点id字段名
	private String parentIdFieldName = "parentId";
	
	//父节点ids字段名
	private String parentIdsFieldName = "parentIds";
	
	//是否父字段名
	private String isParentFieldName = "isParent";
	
	//层次字段名
	private String levelsFieldName = "levels";
	
	//目标左排序
	private int destLeftSort;
	//目标右排序
	private int destRightSort;
	//排序字段名
	private String sortFieldName = "sort";
	//其它限制条件
	private Map<String, Object> otherCondition;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdFieldName() {
		return idFieldName;
	}
	public void setIdFieldName(String idFieldName) {
		this.idFieldName = idFieldName;
	}
	public String getDestParentId() {
		return destParentId;
	}
	public void setDestParentId(String destParentId) {
		this.destParentId = destParentId;
	}
	public String getParentIdFieldName() {
		return parentIdFieldName;
	}
	public void setParentIdFieldName(String parentIdFieldName) {
		this.parentIdFieldName = parentIdFieldName;
	}
	public String getParentIdsFieldName() {
		return parentIdsFieldName;
	}
	public void setParentIdsFieldName(String parentIdsFieldName) {
		this.parentIdsFieldName = parentIdsFieldName;
	}
	public String getIsParentFieldName() {
		return isParentFieldName;
	}
	public void setIsParentFieldName(String isParentFieldName) {
		this.isParentFieldName = isParentFieldName;
	}
	public String getLevelsFieldName() {
		return levelsFieldName;
	}
	public void setLevelsFieldName(String levelsFieldName) {
		this.levelsFieldName = levelsFieldName;
	}
	public int getDestLeftSort() {
		return destLeftSort;
	}
	public void setDestLeftSort(int destLeftSort) {
		this.destLeftSort = destLeftSort;
	}
	public int getDestRightSort() {
		return destRightSort;
	}
	public void setDestRightSort(int destRightSort) {
		this.destRightSort = destRightSort;
	}
	public String getSortFieldName() {
		return sortFieldName;
	}
	public void setSortFieldName(String sortFieldName) {
		this.sortFieldName = sortFieldName;
	}
	public Map<String, Object> getOtherCondition() {
		return otherCondition;
	}
	public void setOtherCondition(Map<String, Object> otherCondition) {
		this.otherCondition = otherCondition;
	}

	
}
