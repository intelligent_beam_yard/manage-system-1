package com.psk.hms.base.util.validator.field;

import com.psk.hms.base.util.validator.result.MapValidateResult;

/**
 * 字段缺失校验结果生成能力接口。实现此接口便可以生成自定义的字段缺失校验结果
 * 
 * @author xiangjz
 * @date 2018年12月4日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public interface AbsentMsgGeneratable<K, V> {

	/**
	 * 获取默认的字段缺失返回结果。校验时通过此方法来生成一个默认的字段缺失返回结果。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public String getDefaultAbsentMsg(K key);
	
	/**
	 * 获取默认的字段缺失返回结果。校验时通过此方法来生成一个默认的字段缺失返回结果。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public MapValidateResult<K> getDefaultAbsentResult(K key);
	
}
