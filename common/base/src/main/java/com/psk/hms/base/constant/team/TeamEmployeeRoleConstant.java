package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队公司常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamEmployeeRoleConstant extends Constant {
    /**
     *  数据表字段名称 - teamEmployeeId
     */
    public static final String TEAM_EMPLOYEE_ID = "teamEmployeeId";
    /**
     *  数据表字段名称 - teamRoleId
     */
    public static final String TEAM_ROLE_ID = "teamRoleId";
    /**
     *  数据表字段名称 - roleType
     */
    public static final String ROLE_TYPE = "roleType";
    
    /**
     *  业务逻辑 - 角色类型 - 1（通用）
     */
    public static final String ROLE_TYPE_COMMON = "1";
    /**
     *  业务逻辑 - 角色类型 - 0（自定义）
     */
    public static final String ROLE_TYPE_CUSTOM = "0";
}
