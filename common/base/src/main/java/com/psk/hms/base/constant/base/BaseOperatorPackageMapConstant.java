package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 功能权限包关联常量类
 * @version 1.0.0
 *
 */
public class BaseOperatorPackageMapConstant extends Constant {
    /**
     *  数据表字段名称 - operator_id
     */
    public static final String OPERATORID = "operatorId";
    
    /**
     *  数据表字段名称 - package_id
     */
    public static final String PACKAGEID = "packageId";
   
}
