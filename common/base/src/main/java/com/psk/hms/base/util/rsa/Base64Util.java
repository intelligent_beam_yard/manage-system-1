package com.psk.hms.base.util.rsa;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import com.psk.hms.base.util.string.StringUtil;

import sun.misc.BASE64Decoder;

/**
 * 描述: base64工具类
 * 作者: xiangjz
 * 时间: 2017年8月27日
 * 版本: 1.0
 *
 */
@SuppressWarnings("restriction")
public class Base64Util {
	
	/**
	 * Url Base64编码
	 * 
	 * @param data
	 *            待编码数据
	 * @return String 编码数据
	 * @throws Exception
	 */
	
	
	public static String encode(String data) {
		String str = null;
		try {
			// 执行编码
			byte[] b = Base64.encodeBase64URLSafe(data.getBytes(StringUtil.ENCODING));
			str = new String(b, StringUtil.ENCODING);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}

	
	public static String decode(String data) {
		
		String str = null;
		try {
			// 执行解码
			byte[] b = Base64.decodeBase64(data.getBytes(StringUtil.ENCODING));
			str = new String(b, StringUtil.ENCODING);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	
	  //base64字符串转化成图片  
    public static Map<String,Object> GenerateImage(String imgStr, String fileName){   
    	//对字节数组字符串进行Base64解码并生成图片  
        if (imgStr == null) //图像数据为空  
            return null;  
        
        try {  
        	// -------------- 直接转流，不生成临时文件，直接转multipartfile  by xiangjz
            MultipartFile multipartFile = base64ToMultipart(imgStr, fileName);
            Map<String,Object> rs = new HashMap<>();
            InputStream t = multipartFile.getInputStream();
            long size = multipartFile.getSize();
            rs.put("inputStream", t);
            rs.put("size", size);
            rs.put("file", multipartFile);
        	
            // --------------- 不采用临时文件的方式，避免服务器文件过多，但是如果是前端压缩成blob之后传来的文件流再转成的base64，会失去原文件名等信息     (以下注释 by xiangjz 2019-01-16)
        	/*BASE64Decoder decoder = new BASE64Decoder();  
            //Base64解码  
            byte[] b = decoder.decodeBuffer(imgStr);  
            for(int i=0;i<b.length;++i)  
            {  
                if(b[i]<0)  
                {//调整异常数据  
                    b[i]+=256;  
                }  
            }  
            //生成jpeg图片  
            String imgFilePath = (StringUtil.isEmpty(name) || "blob".equals(name))?StringUtil.getUuid(true):name;//新生成的图片  
            String dir = imgFilePath.substring(0, imgFilePath.lastIndexOf("/")==-1?1:imgFilePath.lastIndexOf("/"));
            File f=new File(dir);
            if(!f.exists()){
            	f.mkdirs();
            }
            File file = new File(imgFilePath);
            OutputStream out = new FileOutputStream(imgFilePath);      
       //     InputStream in = new FileInputStream(imgFilePath);      
            out.write(b);  
            out.flush();  
            Map<String,Object> rs = new HashMap<>();
            InputStream t = new FileInputStream(file);
            long size = file.length();
            rs.put("inputStream", t);
            rs.put("size", size);
            File2MultipartFile tt = new File2MultipartFile(file);
            rs.put("file", tt);
          //  FileInputStream input = new FileInputStream(file); 
           // MultipartFile multipartFile = new MockMultipartFile("file", file.getName(),"text/plain", input);
            out.close();
        //    t.close();
            file.deleteOnExit();
         //   file.delete();
*/            return rs;  
        } catch (Exception e) {  
        	e.printStackTrace();
            return null;  
        }  
    } 
    
    /********
     * base64转文件存放指定路径
     * @param imgStr 文件base64编码
     * @param name		文件名称
     * @param dir	文件目录
     * @return
     */
    public static Map<String,Object> GenerateImage(String imgStr, String fileName, String dir){   
    	//对字节数组字符串进行Base64解码并生成图片  
    	if (imgStr == null) //图像数据为空  
    		return null;  
    	try {  
    		// -------------- 直接转流，不生成临时文件，直接转multipartfile  by xiangjz
            MultipartFile multipartFile = base64ToMultipart(imgStr, fileName);
            Map<String,Object> rs = new HashMap<>();
            InputStream t = multipartFile.getInputStream();
            long size = multipartFile.getSize();
            rs.put("inputStream", t);
            rs.put("size", size);
            rs.put("file", multipartFile);
    		
        	// --------------- 不采用临时文件的方式，避免服务器文件过多，但是如果是前端压缩成blob之后传来的文件流再转成的base64，会失去原文件名等信息     (以下注释 by xiangjz 2019-01-16)
            /*BASE64Decoder decoder = new BASE64Decoder();  
    		//Base64解码  
    		byte[] b = decoder.decodeBuffer(imgStr);  
    		for(int i=0;i<b.length;++i)  
    		{  
    			if(b[i]<0)  
    			{//调整异常数据  
    				b[i]+=256;  
    			}  
    		}  
    		//生成jpeg图片  
    		String imgFilePath = dir+name;//新生成的图片  
    		File f=new File(dir);
    		if(!f.exists()){
    			f.mkdirs();
    		}
    		File file = new File(imgFilePath);
    		OutputStream out = new FileOutputStream(imgFilePath);      
    		out.write(b);  
    		out.flush();  
    		Map<String,Object> rs = new HashMap<>();
    		InputStream t = new FileInputStream(file);
    		long size = file.length();
    		rs.put("inputStream", t);
    		rs.put("size", size);
    		File2MultipartFile tt = new File2MultipartFile(file);
    		rs.put("file", tt);
    		//  FileInputStream input = new FileInputStream(file); 
    		// MultipartFile multipartFile = new MockMultipartFile("file", file.getName(),"text/plain", input);
    		out.close();
    		t.close();
    		file.delete();*/
    		return rs;  
    	} catch (Exception e) {  
    		e.printStackTrace();
    		return null;  
    	}  
    } 
    
    
    /**
     * 文件转base64字符串
     * @param file
     * @return
     */
    public static String fileToBase64(MultipartFile file) {
        String base64 = null;
        InputStream in = null;
        try {
            in = file.getInputStream();
            byte[] bytes = new byte[in.available()];
            in.read(bytes);
            base64 = java.util.Base64.getEncoder().encodeToString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return base64;
    }

    public static MultipartFile base64ToMultipart(String base64, String fileName) {
        try {
        	String decodeBase64 = base64.contains("base64,") 
        						? base64.substring(base64.indexOf("base64,")+"base64,".length())
        						: base64;
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] b = new byte[0];
            b = decoder.decodeBuffer(decodeBase64);
            for(int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            return new BASE64DecodedMultipartFile(b, base64, fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /*public static void main(String[] args) {
    	GenerateImage("/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAGrAfQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCJR19aQjt3p4GBUbferc5CxAMCiUU+EfKKSVcilca2CHgH1HNcv4lGIyfb+ldPD1Nc14m4hY+1XDcU9hdAGLRM+grVQZA+tZGgEmzBNbScCiRMNgPSgdKUnjFAwBUlDW6Ui9ac3SmqOaEIkHSlxxQBxQSAtEdhjW4FNAz2pwy1SBcChiGgYHWlxS4pKQBQO1KKSgaFJCqDu2nIx+lcJ8R77zJLGzB+VV80gdBnt+HP513B5IJPQg/qK8s8b3Bn8RyDOVjjRR+Wf60pbGlPc509KaTSmkrI3HjBHI5qzFbuVyO9VAasx3LRjbnOa3p26kyvYWSNxntioWjKHnvWjHskXnJ9sVHcQ4PC8VvKnfVGanqZ5FIakZNuQeDTW4Nc8o2NUxhFB60480h4NZsYnNFGeaWkAlIaXFFACUYopaAACjBzSgEnijBp2AbSigjmkqQNLSdSl0rU7e8iZgYmBYA43DuK9002/ttVtI721fdFKM89Qf7voMV89jrXp/wv1Hdb3mmuTlSJkz+R/pVJiZ6CKcBzSKOP1p4HPWquSGKXv0pR6UYpMY3FBH+fypaKQDMYpvWn+tIRimIjAxSmnYpMdaYEMkec4qt5zRNyeKvVXuIdynAoYCo6SLkEfnSkCs2GV4ZSpPH/ANetJWDjIoYFSdcoRWVLHtbgVsy9/rVaWEOvSiL1IlEyzgimkYFSMuwlT600jitb3MGRbaKUjmimI2ScCmIMsaa0m47alhHrUllpO1OkGVpE6UrHioKWxFH96uc8S82759K6VV5Ncx4l4tn+laQJnsP0Ef6EO1ao471kaA2bJeewrWJokTHYd2pQOKaOlPHTNSMRh8tMHBBzUjH5arkMTQBPu44poBahF45qUKAKAEVcKKf2pRjFIaAEIpOgpxPFNzSAKQ04g460hFA0RnG3v6/qK8i8UIy+ILvcc5fI+mOK9amYIpLcBeSa8e1uZ59UnkcYJPT2pv4TSnuZppKU0lYG4tLTaU00BbguGjIC/jzWg1wJAS7KSBjjpWKD6VcgjYqWPA9O5rrpVOhlOFyaRUXcQhZ2Py4qm6FSd4wfStBygkDBj5hPr936e9NmiVULEEu3P/66qcb6gmZZFBp7Kc0mK5WtTQZS4pSADxzS7eeaTQxtBFTQW0tzKI4ULMT0FdVp3ga+uQrSIfoKQ1Fs5DFKELMODXpg8BBAu9VQdwOaZJ4XW1XKKrge1NWKcGkeepbtjcKY6HOSMV1N7p8aAsvDDtiuduldXweAK3srGGtymabTz1xTDXPJGgCur+Hs7Q+LYAp4kR0I9eK5Qda6jwAufGNmfTcf0NJAe1IcnG0jtUqgZpiDBx175qVRiqZImM0U4AE0hFACYpBS4o4FADaTFOxSUgG4pPWn4NJjrTQhmKafTtUhFNIzTAoXNsD8wHPtRbEj5TV0jIqqV2yE0DGyjg/WoVOBgjvVh8MtVpOMc8UWJe5DNbiQ5HWqMiMn3hnnFbERG3kcVUuFWQHGBzVXJkkZ/wAv96ipxaA0VVzPlZKg5zVlCBnFVg1So3WgWhej5FKaiibA/CpDUlIegzXL+KRi2k+ldVFXLeK8fZn+lVT3JnsVtBYfYl56qK191YegORYj3H+Fa4YnvVyREVoWFftUq9KrLUyMagpD3PHFIoyOlBNOUcUgFA4pec9aQkYoDYFADu1L2pM5pe1IAPSmjrSmhRzQMdjimHmnkVE57DJNAFO/fdA6j+7z/n8a8cvpTNdySN1Lc17BfA/ZJjjBK4HHuK8al+8c9e9VLRGlIjNJRRWBuFFFFIBwNTee+eDUGKeB0q4trYLFu2V5nIGOe5q1eMSWCKAq8Ejr+J7022IUYP3j29RUjxec+2IFiOoHb8P8a7Ir3TBv3jLfGML1ppzjirt1EkRKhgz+oIq3ovhzU9cuEisbV2HeRlO0fjXPNWZrHUyFXnABLHoBXT6D4H1PW2V2Qwwf3m716R4d+GVlpirNekXFzwcfwqa7eK0it0CRxhF5wAKwlUsbwp9zldC8E2GkKNsQd+7nqa6L7KigAIo+g6VeEYznHp/Sk2D0rFybN0rGY9uMk4rJu7VecL0ro5Ez0FZ1xEeQMHNCbG3dHAazpQkBdV2sOnFcHqdsysQ4ww/XrXsl5aq6EEc1w2vaZgMxz6g/nXVTnc56kOp5y2Q+e9NNWbqFopSCB3wagHIOaGYjBwa7T4aW5m8TeZj5YoXYn8MVxfevUPhVbqttqFwe7qg9ehz/AEqUD2PRlBBGO4qQc1Goz369fapRgmmxdAxzSdTS0negBO9GKUUCgBp4pMU6k70ANpD0p2KTHFADDRS4opiG496glAAznNTnpUcgytAFY8qahYDIqU/KSKrXLFUwvWmtxMheYncFNNiidjk5xS20Bkfkd6uuoRNvSm2KwIMKBtooUcdTRU3ZWhlhqmjbAqmJPSp43JHtWpzpmhCeKlzxVeFuMVKWxxUlIsRGuV8Vti1c/wCzXTQtziuY8UjNpJ6BT/SqhuTPYoaA3+hLkHp/hWwpzWJoLf6GOe1a6tVSIi9Cyp461MhqorVajORUlImHNKTjpTM8UbuKQ9B2eKA1N3UmaA0JgakHIqFTUw+7RYBr8UKwxTXBbvQsZApAOY5HFJtxjPFPAAHNKRSArzsBGSRkcfTGRXiupgDUZwvQORx9a9rnAMeD04FeS+K7RbTXZ0TaFfDhV6DNOWxpSMKkpTSVgbhQKBTsUwDGacDtPSjbjr+WKvxaTdSxJMU8tHO1C3Vz6KvU/hQBXjJzktj0yef51tada3F84s7OOWaRjtKop/kMf+PcV1fhb4U3l+sd1rMhtbVvmWFQDIy9eR2Fev6Noel6FbLBp1okK45Zcktx1Jzk/Q8Vr7flQezbPPvDnwkLOt3r7gL94Wy4Zh9WwPyr0i30+zsIFgtLaKKNegjUAfU1c3DoOmT7U1z8xySfrXPOblqbQgoorlQOTzn8qCvbGRT2WkPWsma3G7evtTTgfWnkAMfemMKCiB8dBVSZdw4GTV1+tVZAexoBGTcRgdcHPvWBrFsrxYxkYOa6adeCawNT6d8HrWlNilqjyTX7Q29zkfdOcViqMuMnqa6rxTGu4EAnGa5Qfe4+tdDON6DiPmPOOteyeAbCS18LQy4IMzNIyHuOg/l+teVaXp7anqttZqSPOYAkDoO/6c177ZW8dpbQ20XEcaBFOO2BRsS2Tx4YZAHvUijFNQAcgYp46mpYxKMe9LRigQ3FFLiigBtH407HPFJtoAYcjvRkEU4ik7UgG0w8Dmn4psikqcdapARNIBmjIYHBrNuGkR+hxUtrKWxk0CuSuoz71VkAaTGKuSYwSKqSAl+lAWJoUEaZHamO3vmnR/cIpCAozQwHoPl6GipEzj/PpRRcRyiv1FW4W4FZqN1/z61bhbjrW7RypmrDzUrttqK3yRntSzcGoNVsSwuSSe1c94mObaQdsf0rbgbBP1rA8SNm3ce39KqC1Jl8JS0TAswfatIMKydEb/Q19xWkpyOtW0ZotIcmraHiqUXTNWA3HWsxon3cUZ4qNfc07PFIocTxSA0meKVRmgCdOQKl3cYqJOKlUZxSuNDlXmn0uBS4qQGnGDSH/ClOcHFNJPYelMZG20DJ6jGM/jXlfjMZ19zjHyLxXqxQvtVjy3H41xvjHSHuo0uorVvNgOyXA+8pH9DmplI2o0m1c82YY60lTTqBIQMgDsajC+1QaDQKt2tnLcvtXgd2PQVNpOlXGq3iwQgDuzHoor17Q/B1pa26BgZDgNuPQ/h/jSvYuMWzkvDnhaS4Km1s1Jz813eL8ij/AGU7/U16Vo2gabpEwuZpPtN6QA08xyRjoFHRRx2/Ora6TEYgivIo9A3FL/YURVsTNk9zzU8xtGCNyG4hZtyurE84z1Puf6VdLI2Oee9cXNo93CcxS78+nBqncaxqOmnDRTsB3IyKTVx2O8LKDxTSwzmuRtPFyykLKGDY5JGK24NSiuB8pH51LVhpGkW5600kHFQCTLetP3cipHYkPPJpjHmkLgY5qJpR34/GgYjnmoTzmkMq5zkD8etRtPEoJLqPbNOzArXOB64rm78jJGTW5d3URT5ZBz71zl62WJzwe9XFMls4rxPCDbM47H+dcOo+cYr0LxEQdOl4rgY1/ec9q6bHLM7z4c6fnUpL9gMKjRqfQkDn+f516rGuUB7gZrg/h0pNlKOMBunrwP8ACu8jOAOtKRnEeg45604DnrSJ3p1QimA5pAMigUopiEo680Ec0YoAAaTPPWikxQAtN7UuaKAEGMZoxxxTc9qN+BzSAoXyfITjP/66oQl0YZ9a2ZNrrjIqt9mXdkDmnYhoiMhx9KiVge9PljZVOKqqpHqaewr2LqAbCc0j4HBIpqthcYxxVeWfDY96CmaMYUgksB/+qiqKynb1xRQF0coDgmrcBqkM8k9KtWxya6WjkiblqfkpLhvzpLXhBUd02Dwe9ZJGvQIXwxrB8QuPJdQeta0cpXJz0rm9fkZ8gelaRIlsO0UYtFq+DgCqekoRZjI6VZJ5qpEq9i3E3y1YDcVRR8DFTo+ahoC2DxSg9KhDfLTg2RUlpk+flFOXpUII21KDSsFywtToOKrx8irK9KTGiSg8LSA8Uh5HNSAdSKQ4AHHpTgMYpsnG386AK08joyLD94YY/gc1ofaEiZQ0bZlJbB54NUm4Uk43uc/lV6wkgvLCGZAN0ayJI3fI65+gqKi0OzCytoeYePrW3F99o+zi3mkxhE+6w/xri9pJAHU9K7vX7WbX/F6acnAjGX9FHr+WKxbXSY28ZJYRnfGkoyccYFJbFzS5j0fwR4bjsNMSaZcyyAORj+dduilRnAyfTiq9ogVVVRj/APVWhGuQBWDep0KNkMJ2DmneciZ3EYHWlu4mMJK9QODXHa5aazPbGOG4jgx0ZUJOMHnrQtSktDpJdcsI1YtPwpAY9OOccH3FVotf0u6iYedHnptLA54rzXWvCws/DdzrF7qFxeXIKxoMnC5OB79zXIR2kZukWG8zDsRpXjyCu7AIwepBNdCpKxzurZ2sfQD2ljeRhhGjg9CuOPypsOnRW8gMZIA4xXCafNqfhnW4LK4uTNbTY8uVupBH3T6GvRo5fMAI7jpWU4uJrGV1ctQoSafLlRT0HJNVrxyqGsgvqVZ7kqxANc3qVxfzN+4jLDOM5xWuP3sm0cknBA5p1w9rZQ+dcMqjBI3npVIuVjkza69Mw2lEJ6kv1FWE0bVGA82dcj3ouviBolu4RJFYdsMeBx14x61Ja+M9OvNo8xVZuAFbIrbWxleJUuNIukYnzsHHaqCi6ify58SDs2ea6W4u0kXKsMeuaxZ8SMxx0Bwaal3Bo5vxCw/s2U+1cDCMyj3OK7fxa/laWF7s+K421A89DjIDjP61sjmmj1j4ewlbKR/Q4/rXbAgYrkvAcbR6Q24cs/8AhXW+3epmRHYVOByeTT+vFMHytzTxz0qBhjml+lNyc9KcDTATqTSUv4UDrQA2k70vU0mKADFJmgdSKa7BRmkAH6iqV5eJboxJ+7gVL9oJztGa5HxZdypbPt4PXr+FVGzYmna4+68URxSFBIMip7LxCJSBu+teUq0hm3ElmJ5JrobCc7AcH0wKqasxwVz1KKVbiPcOtOEAz0FYuh3JeNee1b689DSBqxE0IAqnLb88VpUwj1pITVzOELAdqKv7B6UUC5Tgs/LVi2aqRfC4/wA96sW7c49q62ciN62b5RUN0/FNhfC1Xun9KyW5pfQSNgSRWbqEAdiauQNljnmorzBB4xxVoncbZqEt8e1Nc8mnQn9zUDnk4p9RPQlV8HrU0T8daoBx+NTxvgUNEpl8PgVKjcZqisnFTLJxUNDuXVYVNHziqKvzjNWo2yoOakpMurxU6njmqyHirKA1LKRKMBaaDmkfOMUxQQRUgTkgAVC53OFpwzyfSmIN0hIpgMlIVA/XC8Z+orB8G63GJdTtJXAeFjcR5ONwIwce/NbeoEQ2csncIcZ6A9f6V45c3LLeu8DlMgrnOOMYNVJXia0XZnfeFfOuJdV1/wAnIlkYqpGeM5xz06jn61R8N2pPjGadwBmST7ozj6V0WhXVjqHhO2W02pJFiOVeh6c5pum2lvGLS6iQrI8kodT93Oe1YdLHcknqd1bN09uwrSj7Vj2cgZRyT+NbEJBxXM9ze2hbUZqG5t0cYK5Gfy9xVhAKkYeo6UE2aOavNEs7y1mtG+WKYYkTOM+/TrXLWfw406x1M3eHlRW3JCW+Uc5HPGQPQ16HNbpKfu1XNhngDj0NWqjE6cXqc9qOnvfARyQxOndSMZ/w/CtrTbOSK1RZG3yIuCex9s1ci05FbJHPbAHFXBGsSYUAf40OTluJ2SsiONay9UfYhweTWzHw3NYesYLqP9qpsJGaZBa6ZPduhZUA4xnknj6ZPf2rhfF+mXt3ocWtSStOTMN8Cn5UTOAMj73uc/gO3qNpGptguMkDg4Bx6jn1FRXMSMX8y2iw3B2RgZB69MVpGSQNc2h4Po8MNzrUIgsyrSz7kidtwWLOcAnqe3vW/wCIPDFu1+Z7ErZtngBcBj6YHSvRY9I061ZmtIUtmb73kRqmf++cD9KrPpMUzABWYZz85yP5Vp7ZWIVG3U4zRo9XhRba8gwoOBIDkN9a6GWARwZI5roltUgi2ooHttrI1XGxgABx2qOa7NbaHnvil1zGjYIVWbB965qwi2qJC3zbvlUd+pJ/IGtzxJ+8mlPVVwv6GsKIYll24yF4OT8v+f6muqC0OSo9T17wc6to8LqRtbJGPTOBXSjqT3rhvA9202jABGzGzA4I6Z4rtoJBLErjoR3qZ7mSZL1/KhSCcYoHpRzUFDsUUDmg8UAFIKUUlABikp3ammgBvrUcqFlOKlpuT0pAZhhYNndgelZGsaYbu3J65H9a6gopHIFMZFKkYGP/ANdOKs7jlLSx5GdJ+yTyI6cE8ce5rTtdPSMDAx+FdFrVqivu2nk5x/n61Ti2mDLcYHUVc3cdNFrSI/LxXSjGMiuf00AshBNb/ABqUJ7i544pO1MaQBagebI4xRYRY3D2/Ois8zc0VVhXOH6irNucHk1nrLkdangl+frXU0cKZuQtleDUFz0NS2wymcVDecA1mlqa30I7VsyYpt1357U20OZcU656H6VQIbDgwCq0hwTVmEYg/CqU7DmqiiZMYCd3JqZXqorZqQPxTsZ3LSv71MHIHWqKuM1Kr571LQ00aMRyQavw/d5rNgY8VowkH86yasUi9EuRVwABcVFbR5OB+dacdnkev4Vk2aqLKYXIFLtAHvV17XYMgVXYY7UXHYjI4Ge1RoAATUrj5W560xR8pGadyWjM159uj3I7mE4+teNTkbyV6ECvZdbwbJlI4IGfoCCa8Xf7o+gqm9DSmjT0DXZtEvxOnzRuCsqdiD/WvTdIv7bUbBJ4WG4SZCjt7YrxvNdV4au1t3sPLuGWRrkrLGMjKkcGsbXOqMrHsVmxVse1bcLcCsCH5WXtxW1buCBXLJWZ2xd0a8fOMVO6981UibgY61a3AjrRHVCY0L9KcBgdKTIFNd/eq0JJR1pshHpUDXCqcdaa0wb2pXBxJVPU1h6ydpVsdDW1GflP0rF1w/ueOuafQlInsGzED6irxjDY9qyNHmEkYUHkHBrYU8D3pF2IvssZ/hXPrim/Z0jA+UA1Yyc1FI/AyKVmMzr3CLXK6rOBkE4HXrXRahJ8p54Ga8/8UaibOwnmB+fG1fY5rSCCVkjl/EFwoeCBWUyFnklAOcccCueaQLHMw6MxUGo4nMs8krklhlsnrn1ps/EcS56jJ/GuuLsjz5u7O/8Ah1dBA4LL8x29/b/GvSLYFQRnA56etePeB7jZqHl84bI/Ht/KvXbGTfCuevGfY0S1RHUuggnrS9sU0AcYpxHbvWSLFzzSdTQBzS9DTEFIOtLRQAlJSiigBKbTjTeKAEP0phIp/rTGYA84poZiauocgVlfZGEJ9K2rqAzSZxxmnLakIeKmSbNKcklqUNNXy2AIrWkfrUUVvtOcdKllUYBxV2sjJ6szri5K02CTeCW5HtT7iAOOneo44hGvB60rj5RxZAcbTRTCdxOO3FFWI86E3y9ewqa1m/e8mmX1k9q5ZR8p7elVreTEnNdujR5iudvYENFnNQahgA80mmuDEOai1N8DrWPLqbKWhDaN+84qW5PBHtVSwbMhqW7cAHPpTSKT0FRv3WKzbiTk1eiOYPw/pWTcn5j9auKM5MQS0omwKqMSKATiqsZXLwl4q3ByOay4hkita0Q461DQ46mhByOK1bSMnBNVLSHdjity3twFHFYSkbpGjZW5CD1rXiQqBnrUFrHhRxV8JXM3qdUFoQSJnp3qjNbk9BWuVGKY0QIzQNxMF4mBxtqJEcgjbgg1vG3GDjrUQtQsgJA5xk00yHE5DxLi10e8lbI2wN+ZwK8XfG3nvXs3xClWLQrqPH34x0+teNSEbVXuM1o/hKirEI4NdRZ2EFlpVhqTeZ5rzqMZ4xmuXzir0mqXk1nDavMxhhOY19KhOxoe8QNuRT1759a1rV+BmuY0G8F5o9pMrZzGoP1roImxjBrmqLU7qextRyDjFWRJx1rKjl6c1ZEnFZpmltC002BVOe7wdoPJpk0p28Go7eLzH3NzVBYmt28wlqmZSGFM8p4gXRd3+wBzVEaw0khV7K5h2nH7xOP50E7m5EMxk+1YWun9ycDqcVdjvTtxnj61k6zfIVO4hR6k8U0KxS0mV7e/UMSElBx9a6xX56cdK4e5nU2lv5MgaRZA+RntXT2F39os45G4JUZHvimyrF93warzSfKRnmh3yOtZ11NtJpXBFDU7jbG3PtXlHja/3zRWYbIX53A9e1eg6pdYDEn5QOfpXlHiW3lg1PzpJFYzr5igdh2FawiY15dDOhysUjDkkbT+NNvMC4ZB0TC/lVi1TKAY4JGffGT/AEqlK4klZvU10SVonEjX8OTtDqkW0ncWUnHoDz+le16c/GM8Mqn+p/Q/pXgunyNDfQupIIcfzr23RJfMhhkJ3Art/Ki90J6M6AEYFOA5psYwCPxqRagYCjFFKOaAExSYp1AoAYKKXFJQAlNx1p4pjnrSAid9ucGq+d/TqalbkkGjaPwqwuNVNpyakbawx2puRg+lRn3oAbtAY4oJ4waVTzTG65pMcSGTpVGZvkGevpV2QjBPYVnyHc/XilFXZcnZBCMISSeTnrRT0iAWitjDmMW6tEmDHHFZL6OOoBB+ldEoKkg9KkRUc8iqUmjm5TMsrOSGPGMiqOqbgvTqa7KC3TyzgZrNv7BZMgp+lONTUPZs5nS4mLk4qPUi6MQR1FdbY6dGi5C9B6Vj63bIj5I6e1UpXYctkUrG2kmtQcdRVefTZCTxXTaRGhtBnHT0q59miPOAaTqWBxujgn02YDIGartazL/BXoo09GH3R+VMOkxsDkD8qFVI9kcHb20zP9wjFb9np8uACK3o9LjU/dH5VfhtEUY2j8BUyqlRp2KdnZ7FGRzWxbwjcOOKaiAD5av20JC/596wk7m8YlqBMIKs01FwKdWZutEGeKCOKO3TNBoAD0qJxgqc98Gpj0qNxk/y+tNCPOPiIWkikjjIOcbuegBX/GvI5DwK9P8AG84MF2zFSXLKCP8AdH+FeYSitpLQmJGT2oBpO9L9axND0r4d6mJLKWwc/PE29eeSD/hXo8TA856/rXgOh6k2k6pDdKcqrYdR3XvXuthPHcwJLGSysAy+4+tZTR1UZdDSUEEVYDMBzmoFKnvzVhlGznHQ81jY6GytdXCxgs5Xjrk8D3q9auGReMeo79K5zUtCk8QyNFJcPBbxsDhOrEcj8Keb3UtIYRODKgGASMnHrxWijcpRctjrSN3t7EVBPGGjI2isSDxRvABEJ7YB2/mDVwa3G334HA9VFNwkhuhNDbmzmZCYXCn/AGhmueu9JvJpR5pU4/KulfVLJeS3Prtz+uKpS6rZ5OGfPcBSSPyoUZE+ykZltpbI4MwHXsfpWzGyIiogwBjtXO6l4ktbdWWNjLKeBEg5P1PatTRzc3enxzT2/kMw4jLZP5jiiSfUWq3NCSbC81kX11xgGtG4Xy1OcdK5y9mBkIHY4JqIq4m7HNeKdVNhYll2+bJ8qqw6jvXAahfvqN4Z5AqnAUKvQACr3ifVf7Q1VljP7qElVweCfWsVeXGD3rpgcNSV2aAYLAp3fNtb+QH9TWa3XirtwQqnGQAoA+tUTWtR9DJEkTbZFY5wD2r17wdcmXSEHPDdzXjy4r0fwDPiC4iJy2FcDPbkUQ1Qpbnp6kABfSpQaoSXcUSbiRz1JPSoV1iDON4/76qeVhzGqDS55qrBeQy/dYf99VYHIyOaWw73HijFIKKBhSZoFGKBCZFRueT6U/uc1DIwGc0WGyMnHNR78Z/z601nOODkVGTk8EVRJaBBXgUxvQUkZ9+lNY8k0DF3Y61DLIAaSSXaD71nT3eAfmqXqaRXUknmyCB3qFIyRnmooWaWT7xIrQf91bk+1aLQyk7uxRedkbaBnFFVJNxkJGaKfMLkL3lce9IIsvxV0JwQRQsYByAaSZikTWyER1DcqRmr0I2oAaglXdnilcfQggBUH3Fc5rwJZq6uOPG76Vy+vjBf6VUHqTJaEukk/ZQfbFaCE/rVHRx/oi1pqgz+NEmJbEiseKlUnbmo1XB4qZV3dOKm5SQoyRkAVIgYgY709Iy3GePpVqGA96hyKUR1vBgc1oRoFWmxIAue9TCpuaxVgxgCilx70lIoO1GQR0pDgU0vQA81Gx2gnAPel39M0j8qcdcVSC55N43Km1TJUHDnHrz/AIZrziYbeK9E8e7EWIqoOUZB7ZPH9a8+uiGYuOQTwa3fwmcdytRmijFYWuaihj+NelfDrWZJoJdMl5WEBo2bnAPb+tealcDNdd8PX26xL/uD+tKUSouzPZ4pN44bNXEJJA96yIW2KG7EZrTgfPcVzyidaldF2Nfl+p5I/wA8iq1/AJVJb746GraHj7vWnuoIJwTxiknY0jJxdzmktrV7nbdRAEf8tFGT+nNMudJsTI5imZs87ldgR+vFal1ZMTlAMelZrw3MX/LEsvouK6VVTVjthXTWpTXQjt3x3V7sPQh9361j6lZQgkPeXVwR94GUj+WM/jW/IbrPMMwA5GTmqqWU8kuRGVPYkdKpzikE6sbB4f0S1++8QyD1JzXVs6Rx7QqhR/COlZ1tGtnCEHJzkn1NQ3F3t2+rEdBXNJ8zOKTvqR6ndZbGRk+led+Ltc+w2/2WGQfaJQdx7oP/AK9a3iPxHFpUWMrJdvwkY7dOT6V5Vc3ct1ctPM5eRzuZjWkY2OepU0siBid1S2wLTKM8E1CetWbNf3xPYCtYLU5mNnbJJ9WqDrUkhOQD061EDzRPcSFU4PSuy8IXohOSR8vyk/XPFcaDg1taJLsZgTxwT+FVT3FLY6rXfEMqkxQt8zE5rnU1O/8ANDG5b86ZfiVLgM6nGOv50kDKzD+VdsIxZyzk1qdbo2tXSyqkrFh65r0DTb0ToM9T715nZshAAGD612WhTMyZb6VlWgkRQrOUrM6scUtRxsGUH2p4PGa5DtDvSZpQKbQAnrVSdwAelWXbCms6Z9xI4qkgYwZA6incAkjtUQJ69KdvIBPHNUBMrYzTSQR1pinAqGaYIrc4oYlqyC8k2rgHGKxnkZ3xnP41JeXqkkFsmm2URnkyBkZoUTSUrI1NPg2ruIPJzSXkoyBzgU+a4W0hwMLisxbkTMenXqKpmUVd3JQBj5s5ooZlJ/8Ar0VBpc6ExcE1EOuKnLZQ1EvLUkzmJ0+7TW5PIxUiplaYyH1ouAqLlT9K5XX1yzfSuujTCtnniuU19fmbHpVweopLQl0lcWY+laSjgVS0lB9kGa1UUY6UpMSI0j3HJNWIk7DkUBRVi3TPas2y0iaKLAzjFWY15pVXC9KcvSpNbWJk4UHFOPSkX7tRyyALQUOZwvfmmh84qsCXbPvVqNOMmgB3UUmzNSEYFJ2oAhKkdaM4HQ88ZqU9MVHIOOO4poTPHfGzqXVXJXa7AD6HI/nXC3HCL6ECu/8AGEPn+I5rRgQVUy/hj/DFcDOC1urDoOK6X8BnHcq5pwppGBTh0rBOxqxXaum8Dtt1Vz7AH865c9a6Twj8l0z88sBQ9Ro9otsFQMcYq0N0L7geD29Kq6fiWFCO4rQDYwpXOTWLOiLLMUwZetW0IbHPFZpjaMkr8wz0Hap4bkDgH86yaNlJMvtCp6jNQyWqNyRTluBjOQfpTGm3duKVhkEkSAcCqkpCHg1PcTYyOBWNd3QG7BwKq1wYXF0A3Xgc1ganqnkRSGL5nUHB7DirM85bKjp6+tYl8u6CYY9f6VoomcnZHltxdTXVw80zl5JDlmJ61AasQW7XNyIYxlicCmz20ttL5c0bI46g1rY5LkPer1im6OQjtVLGGrRs/ksJnx1OKcNxPYz3OWpnelPXNJSk7sYoIzWvor7bocAgg9axx1q9p8pjmyODj86um7MmS0PV38PQ3dnE7RgsRyMVkXHg5UJeIFTXb6Sd2mQcjlA36VfaJTnjH9at1GmZuCa1PPtN0OVblUmGU9a7K2sUtVyO4pkyCFi2BxzTGvWZMA0SqOW5FOgou6NW3cHIHWrGf5VjWs7BgT0xWup3DIrL0OhocCaaWxk44pSQvWqFxdYBANCQrhdXAXIzVJXySc9aaA8zZxxUotmI4UiqYrkJbBPNOWUYx6U823tUToIwcihA2OMq4POOawdWuyikCTFN1TUfIQjJHPasRLuK7DZJY+hraNO+5HtLBZsbqf5mJGa7Kzihgg44OK5a2i8iQsI+OtX3v5GTGwjFNroiXO4mqzLIzKG+b0FVbYGMdaryI7OXw2TT4ywIBBqXA0hVSLjStnpRTCCeRRT5CvaI7Mt8rUkRG6oyTtNERGRXOjnNFD8vSkO0mmK3y8U3f1pMaZOmBurlNb5Zs10qPya5fXGxuzjmtICk9C/pQH2YfQVe8zHFZ2kt/on4Crw55FKSDoWE7c1pQJgDn9KpWyc1rQr8uazNIIQ4HFKvTpQx4FKnSkaEpOFqEruNSk5pVGOaBjY4wBUvQCgdOKKAF6ikIpe1IaAE9sVERllXtkVISBUJYKCRgkD16VSEzyjxIfN8a37hcmK0wRn/AGa4FSDZTK38JUj9K6zWZ2k13V7oE5dNgJPsB/jXKQrujulx0iz+WK6X8JnHcotjP5UoGRQw5xU1vbyT3EUMKlpJGAUD1Nc1tTU1vD/hqfX2uHRxFBAm55COK2LSxh0y6EUMhdcgliOtepWeh23hvwNJaFQspizK395iK8ykBSUE+taxWgI9M0eQPaRnr8tbsa7hjpkVy/hyUPaoM9q6qHGRXNPc3ixVO1tp4FOe1jk5B2n1BxUzRBhSCNl6c1mWmZ8yXFvjBJHoetZ8uoyKcFSPwroGSQ8bTiqNxpwmP3cUWNFLuc/NqEkp+VT+OBVRpC5+Y546Cti40Rw2QM+lUTYSxEjb7dKpWBtFCQZz9f61mal8lpKxOBtJ/Kt2W2IUgrzgn9axtXspruNLOJGYzuFbb0C9ea0jqYzZ5fpjeXrFvJg4WUNjvivX7/w/Y6zZh5og2+PeHGAwz9K4HWdJGleKraMjYjKjgY6DOD/KvXdDt2NgF5/dNtAx268/nXRypI5HueCaxo8+jai9tOMYOVbswphOzSFCnln5r3zxH4Ls/EFmqSDZInzKwPI/GuItPhpFqUd3bR3bqts5VJARhj7jrU20KueUkY70mOa7PWPhtr2lZdLf7VGByYxz+VcnPbS20hSeJ4nH8MilT+tZtFEFWLV9lwh98VDjnrTojtcN6GiO4me8eGpVm0mI/wCzit36dlxXLeDJRJpSEcc4/SuoySOB+NXLclFO8jDKeD0qpFbKT05PTmtG6Hy4HpUECFhuA6dKTVy07FSXEYJHQU+DUMAqagvVIBA9aqxxsWDe1StCpa6mq95v7/pVR5MnJ6VHjHFTR2vn9CafNYShclivI042086gmDUTacwHWoTZnJBP6U7icQe+VjtB5JqC4nyhJcVOunZJODzUFxYnBXn/ACapSM5Qdjm7uE3khA5XNXNO0JY+VXk+ta1rpwQ7ioFaiFY1Py1bqdiVT7metgI0+YCoTZIScir89wsa/NVUXcXc/rSu+ocpCbZV4CVBJbKedgq09yH4Wow2eDT5mLlRXEagfdFFWPLB70U+Zhyo0WYhT9KSE0xz8h96SE81jYg0d3y1GW60BsqahZuetKw0WI2GGNcvrrDeR7V0Mb8NiuX1pyZm6VpTQpPQ09Kb/RBWlFyPxrI0pj9nGOgFa8GSRSaEmalqvAzWpGo8uqNqnyitDGFrHqdEVoQv06ULwtD/AHaavAxQMmzUgyAKjAzUnagaF7UUlKOlABmkJ6UE45pp5+90oARjwayddvU0/RpZi2Dt2KSehPf3rVcjaQB24rjvFc/2m7t7PA2RDzpM+o7VpTWpEnoeZ6tGwllSNiSiAyHPf/PP41i2xO6bn70TA/jV24lkYTyscCZ+/Uj/AD/SswOUbIrplqiIEJ5b0r0L4V6JHfay+oy4KWn8J7kivPm4PTrXsPwVQPZamSMbZU/kawZqzqvFksuy3sivyTZkY9cAV5jqAAuXAFeu+Kow5gfB3AEZ9eK8q1WErcv9aUZGqWh03haXMSjvjpXawEHHNcJ4VyG5ruIeHHpispoa0NFBnrUw4x2qOPkD6VLg1lY0QHk0mzmn45p2OaBsieMHAxVaS3U9utXiDmoX+Xn2osTcybi1jA6fWjTdJVbmKSX/AFrjeAeo6gfyq/DbNd3kcZHy7gW+laawg6jIVOP3YQD69quCInI8k+JVoJPG2mRhV3S27jp/dau28MJ51qM4O6JGJ9TjB/lXJ/ERivxE0JUOSUKj2+YZ/Suo8MyhJDGpyEleI/TORXQ3oYLc3JG2W2ccLnP4c1i+H7aQWSyuGzMzSc9Bk1oa1O0OlTbSytI4jUjuTxVmyia0tY1LZCRDI9ahMpokVJI4xtY4HUdqydS0+y1IGO7063uM/wB6PNdBMw8sFCp3DgGmw2OMngk+jD+tK4jzm/8AhR4ev9zRR3FnL/0yI2j6gj+Vcpf/AAa1ONy+nX1tdJ2Rzsb6ele6m3I42EemKb9m55A3Hg5HamnYZ5f4X0PWNHU2+pWE1uigbWIyrEDnkV1CqMYBHH92ulNrt+VSdpPIxxVeTTYnb5oxntg4P4f/AF6JSuJI5m74GR0xTLWQKmMjk1r3uivKT5bgHGAHGf1HFZX9n3do376JsH+IfdI+vb8aaYbFG9UsScLwc1WGAPwrde186LOCTj0zWRLaSIxGOKTQ0ykHy/H8q2bFdsYPeqUNmxfJB/OtBD5a4xzU2uaPRFpwCuOM1B5fJphlLHrS+ZgcmrsZ3GybUXk1Rkk3s1STzFmOOaiVCQSwxTUQbHbwiEtWXd6rHASCaffXARCM1ydzDJczk/NjNJsqKT3NK41Iyco4/GmQSSu+CKS0sNi85xWtBBtHyjFVe45KK2IIwcDqDmrSAheamWHI5qVEGelXcxKwkwMf0oqcw5JPP50UBYR3yOtELfNUDPxToiA/WoMEzUVvlNVZJOetO3/LxVUt8xNJIdy7FyhPtXKa62yUk11lscxGuS8R8k4PetKZMtjQ0d/9GHuK6CzAZhXM6Nn7ImfSup09CQD9Kie5UEbVuuF6VYLcUyJcR0McCsToWwx2OOKarc0hPFCcj8aExItp06U4dKauQtO520DCijtR1HFAxO9Jzt4pxyCKVEZgcKTQBXmJEbH2wK891Z2luNQnjR3kyFVV68YNenrpzSptc4U9fWprLRbGxLeTAoZzlmPJJrSEuUiSueL2Hw71fVLO3MqC1iBLuZOWOemK5bxToaaHqS2kchlypyx9a+mJkPlHI3H5eB04NeA/EpGj8QYwMqDk56nr/WtVPmEo2OFc5bkdhXr/AMFpgum6vGOvmIevsa8g78nivVvg0cW+r/NgfJn9amRZ0/iHU2/ti3tXYmNRuYZ/KuT1VQ0zH061s+K3RdVgmRvmKc/QGsO6cu2c8EVnE6YrQ3PDCHeK7ZFORxXIeGEy2TXcKnAIqJkliFTxVgKSRio4VJIqyq4NQO43ZmjFSsQOlRs4U80AMbg1WYbjjtUkkyetVjOOnOTgCpuO2hq6VBtdpT9AanVdl47Yq1DEIoVXvUE45PArWJjI8i+IxWPx/wCF5NpBLvnPf5lxXRWC/ZNfuoMAMXWT2JIrA8fr9p8e6FHwTbxl/wDx5TXRTkr4qfIG0ohU/hWvQhIv6sDNf6bZEkB5GkYEdh0/nWw+1pViHoAePwrKt187xQjHBWC3AB9CTWzbAS6jK/YfrUFNDbGLLtGxyYyVGR+tXxGGbJXP1FRSIYZknX7vIYe1XwOmBn0PtSYiFU25OKXgkBl56ZqRzwQPSmAcjFIYwxqe7L9aY8OAcjPtUxHpTdpLUwKhjAbnpjpxULxAqTng9Rjj8R3rRZgDgj9ahKBvuH1oAxZrFSSyEKx7jOKyri2ZJsSqVOOM9D9K6goGODkH3qrPbiRWDL8jdvQ07iMDyFUZ2iq0kec8Vo3Uclu2GO5D0b09qpOwzVJCuVimPWoJWPKjp61Zdtw5qo52nnpVBchMI6k1DJIYyVUA1LJOSCqjjNQpbO79Tmq0EVJrVrgncuAagFksZOV5+ldHFbbEw33qY8AfjFSw1MeGA54Wp1hPfitFYAo6UFFUZNFwZS8rA61Fv2sPr/SrzMpGMVnzghuB3zTQiRWBHWiqwZsd6KYFdm4p8Lc5z2qBm5p0bD8aTOe5dD5Q1CW+Y0b8LxURbnNCQ0y9HJtjNclrkpeU/WujjfKGuX1XHnMc81dMmT0NrRhut1GPSuz0+HCdPSuY8OW5NqjHqQDXa20e2Pp1rGb1NqcdCwvQcYqOXG33qTtUcuNtQbdCBTmpY156VCp+arMY5FSSiYdOlKPelHAp6RtIwCjvTGMH3eR+VSx27ycqvHvV6GwAQF+WJq/FAgPIJPTinYDNiscYLjPtV5YQqgbQF9qtCJQB8pxTgijoPzoGQLGuOOfqKXyhxnluwHSpzk8UYwKAKcoyjKnsPrXgXxOhMGp8Ywwbr2P/AOoivoUxlyoBxzivAfi0jJqMOSNjjn13AgfyrSDEzzEtwffmvS/hHLtj1hABnYjV5k3HfNei/CZ/9I1VP70A/nVMHsb+vRPP5EyjIClDWOVZiFI7V1axrcCa1kx84O3PY1iCLE21l2sHwQaiWh00pXR0PhyIxqvpXYx4wM1jaFAjQr06elbywEsAKwkymiaE/lU+8DFVgfLGPSkklJPekQ0SPN83FV3Yv3NKMntU8cYxyBQ9RlYRZHA5qextVkvEDLwOT+RqYxg8jirenxBZJH7kYFCWpMpWVjQPJPOD1qnc52tjuD09xjP61aLdeuKqTMOXY4VVOT/s1qtzE8l1gNqHxUJjCt5McKAkdMksf51vOksuvTHadkZCLz9a5zw75t94gvNVdf8Aj4u8oSeqr8o4rq4YyL4sB96cg/lWjAvaUG/tHU5+MqVi6+g/+vXQWkG1eXXPU1z+kjdFM6/8tp2Y/icV08SbQFAHXH8qhsY/YsqEMTtPH4GnWbbS0LnLx8fh2NKoKvjt/Son/c3SS4wr/I3sOxqQLDk5+79aMAnpinH72fajpnNADAvBx1prYAx3p68NTGyzYIoAiCbmyc08RZB4+tPA3HYoGakbABFAFTZuyh/A1A6ncVP3hVxgDnrUbrvXj74H6UCsZs8CMrB1yp4Peudv7RrWXBzsPKt611ki8HGao3kCz27RsuSPun39PpiquFjkSMAjNQyRM461cmiZHKsDkYqMEc4HSrTFYrR2hzk+tW0jRPrSBgDzTt6kdf0pMAY9aYCOeadgYPNV3JXOKBD2bg4qtISeAak35HNRswweBQBD82eppjDPens2AaiZ6sRGFznmimeZgmimGplswyeadE+ATk/nVTzhzzR9oCggVVjlbL4fg5NRtJj6VV+0cdR+BqPzGkbamTStYabZoxSZU4JOPasG6hae+wQdoPPFdXptg5hLMOcc1TuLERXRY+tNSNFC5v6HahLRRg/dFdAgwAO1Z+ljFsB9K0QeM1zy1ZslZDx0prfdpQeKG+7SGVwuD0FTx9OlREZPSrtlbtOwAHGeTUiRLb2xuDwGVfWteG2CICowB60+KFIlAAH4VP1GcVSGRqgLYBBHrU6Lxnt7UkalUycZqWMZTsKLjsMPAqTGAKQgU8AdwTQA3bk4pHARcmnFuOOBUMp64oAjY7iT2xXgvxkUDV4+xD4HuMf/AFq97IxGeMkj0rwf41qY9YgHytgg5z32irgI8ofJOD3r0D4VH/iY6iAeTbZA/GvPz1HrXefCzYNbveufsp/nV9Qex2xLCfj7wP61Hq0Gy7juxjy5+TjoCBzUpGJeh69PUVqmw/tLSpIF+V1G9CR3A6flmpmiqUrM0dAfECnsVrfEm3p1rmvD7F7ZeRkZDY9RXQquAM9a5mdDHOdx5zRsGe9KBx+NSHilcAAAFOI54owO9IzAHjr9aAHZI4xWjaDEIyPmPNZY3O4AzkmtWN3RQO4GK0iZTZI3J7YrnfGN/wD2d4Yu5lLCR08mLb6sQOPpnNdCN+BnGSR1+tcZ42lFzLaWXyFQfMZT0z7/AJCr6kHOeHLY2zWseTjBkH0HGa6G0OPLm5ZfnkJ9ap6ekieawLDyLbB529T6fhWncyGLSyMAhItvIHXGKpslk+joFt4do7h/rnJrpIgSQfYH8ayrEBPKjVAAEUAAYxx/+utaNmXgE8e9S2ND+e45ptxH50TjuRx74qZXJIBpcYJwBkc1IyKJjJGrnuDn/CnHPHvTYhskkjGcDkD69f6VIx+cZxQAw5B4oRcvk+lO4IyeKSVxFbu6nkDP9P60AJbYIklPXO1aecuTTIl8uBE54GPxp4yDgUARsMnFNIOfpUmPmppFAEEg+bP8LZ4qpIMZz17H0q+w4Ofw4rMvWIIUnP6U0IzNVs95EkQ4IyOOp71iNHweewOK7OePNoD0ZcYNczqdr9nkVxxE4JUent/OmBmlMdKYcj1p5f5evOKYX4IqxCrJ2NNkweRTTgZPFRtIdvWgQw5GetRM1PLqfSo2ZcGiwELMecVESc1MWGOoqF2HqKpE3IyTmimlhnrRVBc5P7aXX5VY/TNOQXMxAVGANdhF4eRRwgH4Vdh0ZE/hGRQ6qRh7JnM2OjzScyEnPbFdLY6GseMqPqRWxbWMaDgfpV9UVVwBWTnc2hTVirFaCKIjj8BXL6qcXe2uzkOYjxXFasc3f40R1KlodJpX/HsBWgPu1n6X/wAewrQHSpe5S2AdKcelAHFIwOBgZ5pAEMLzyhFBz34rpLe3SCEKqjPc1U0u18uMysOvStGMZWhIkcqkjpUgQ7eeMU5cAcChs4NBQjErGuTnOakHEI9TUU3yxj2HH51KD+5SgoRTwKfnjmmbTxyKec46igQ0nrURAb73SpGcAYAyabjIyRQIR24AHQY/nXgfxsiC6paS7TllIJ/4Ch/rXvEnIIx1GK8O+NxJu7AbR/Gf/QRVwEzyHua7j4ZSLH4guBzzbNj0/GuIxluBXXfDxwniOTA5+zvj8q0SGeikjex966Xw8HeUAHJHOTXJocynHeu58KwAAOw+tKWxK3IYYF03XLmxxtjf97F9D/8AXrXPB/Om+KbPbbwakuRJbHDY7o3f8MUxZfMUODwRn9Of61yyOuLuicf1p2Bnk1FnmnFuakokLe9QvIc46D6UjMTx6URo00qIO5oQnsathDsiV3X5m5I9qtrHzmjJUjGOOKdnPAH61qloc7GTNsTcTwDz+Brgb9Tca1dTOSUQIgyemfmPH0xXcaiwW3YHOCCvH0z/AErjwDLNMwUESSnbn0G7+iirQhlirNHLnrK6jmr16ge1CjG2RgD9c80ywfzIWfA4Ykce+B/Krd4N0ltGQCM9vpmkBegQtePjPyhB+lai8OKqWyqAXCk7iD6dquKcsCB70gHhsNz61IRk57VEWDHkAVOvI4xikMgkJEiSDOAcH6U9wN4I6dqcVzkevFMjyUG7qOKABT0yKjuB+5Rcj5mAqRTk96iuBmWJfQk/kKAJCeSMU/p+VMXn5s06gAOSPpUY59akJ5yenSoJH2oVB69KAHnLDjrgnNY2o8Txsf4vm+h71tINqY9s1k6iuQh9GI/OmhFqIedakc8qf8aoaraiXTWAHzJ8w4rR04jyEH4Us6DymVunI/WjqB5y7ZGcEVAzHnFWr2Iw3c0XOFaqMhPNaE3AuecmomemM+KhZzzTFckMhxUTSHHWoy/FR7uKpIVxWkbsaiLkjn1oLVExB71WwhDIc/8A16KhI560UwPRCAB0qPjNOJyKizzXKWy3F0qaq8R+Wpc8CkhxFf8A1ZriNU/4+/xH8q7SQ4jNcRqTZvAPf+lawFI6nS8/ZRWgOlZ+mf8AHsp+lXx0qZbjWxIOBxVizgNxOB1Qcmq4GRWzpMI8tn9akC/EoCEAdOlOjGFHrRghW9qdH0BpoRIpx2604c9qjbjHOOamXgdfWkUMm/1ZFORt1oufWmTEeWSR2ogbMG2gB/pStxk0nQA0O3NADW60uPlpOfanNyFHr1oEiGTKr0ySRXhvxojP22yJ5yGVSPcj/Cvc2Uu2BwABgk14v8ZwhNnhuUBJG33PetIAzxnoAa6HwRMYvEkRyAHR1OfpXP8AVCMcitHw6wXX7RicDzAK1YHrUCYYZOT6CvR/DUWLZc8A89K85tuX3f3v0NeneH1IsY89SM1lJkxRrTxpcWskEgzG6kH8q5iziaOAwOfniYxN+Hy11WMgAY+bI596xb1RFqZYEAXILAf7fUisJI6ISIMd/UA/mKGBqbaOhBJ9qTZz1qLGt7EJU/jV2xgBcykHA6UyKHe/3cj1rRjjVVAwRxVKJlOQ8KWb2qT7oz3pyjBx7VFMeMAc4qzIoalJhMkE4G4j6Vg6dA2ERuSqyH8ThR/7NW5qIJgkO0nAx/49z+lZdiBHbu/JIhB698j/AOLFUgINHU/YyGJJcMc+5OP5YP41fuBnULWM/wC036Yqvp0awxvEAdyuBnP0/wDrVfkRW1eIhWwIz+HIFAGhbLtjVSPu4qxsIbGMAf1qOFSEGCCQAOfSpfvjg/jSuAnfHqKkgJbPsQDTcDpkA+9IhMdwvPD0DJiODg1EhP2gg9GXj8P61MeD+lQSfKwIHIP6UgHIR3Heq9wxFzCAfWrHcHj/AOtVWUbryM+gJoAsxcrjHaheSaRflGc8U1jjoeKYCSuFB9MVDEGl2MexP8him3L471NaD9xg9Su6gCUnGfYVjXpJcjpg1rNxk9zWPdNmRz3NCEX9OG22HrnNWJwSG6HIqKz+W2Rf9mpm5jB70dQPLfH2pw6JfwSynalwDj6iuMfxlZsMhxXV/G2x83wzDdAfNb3AOfRW3f1ArwTLVqp2JcT0d/GFqc4amf8ACWW5/i/WvOtxo3Gn7Rdhch6GfFNuf4sU0+J7f+9mvPdx9aXcfU1XtV2Dk8zvT4ng55pv/CSwEda4TcfU0Bj60varsL2fmdufEcWetFcQGPvRR7Vdg9n5n0pupm/mmluD2pgbOMVgUy9E/FS5qrEelTg80DQSN+7auIvzm/A9/wCgrtJifKauKvTnUPxH8q0gTI6zTf8Aj0WtFelZ2m/8ey1oIflqHuUtiVfuntXSafGFhXtgVziHLD6109uP9HUD6mkA6Q5Jx3FOhwYwO9MlAA3Clh6AA80+gmSMuYueoNLESY8HqDTs7zgcUigq+3rmkMScjZj1psIIQcZFLMcLz1psRJHWgZYyMD5aRiA33RmkPagjLdKAEJYn5u1Obv7UBe+KJCFGO5oC1iDrzjOa8Z+M0WWgcE8RYAHQ/N/9evadvyjH0/OvIPjIh+wAgL95gT7fJ0/WrpiZ4gG+XHeptMk8rU7Z/SVT+tQdxk96dbkrcxkHo4P61qwPabM7ppYl653p7nvXq+jKF0+M9flHNeUaYrSTRyJyVwf5Zr17TwBZR7RhcdPwrKYRLTZI64FZus2bXFhKYuJ4svGe+QMkfjWmRkdM0h45I3Y6j8Kga0MGxne6s0nMZVjkEehBwauR27SuVK4Hc1JHbtDcTRZxGw3qB2x1/Wr8MYjXB5P86nlL520IsITAAAFSBeO1O69MD2Ipc8dKaRBHIdoIB5quSS3Pent80h54FImD8x9aAKOpMYtOuH+YlYi3HqAMfzrJsgPsbADGFVT9Dt/+JrX1bH9l3uGx+6Y8eyn/AD+FZunIXsCCOSmST1Bwo5/HP61S2AawENxIp4zgj8//AK1aCDzL0r6RDGfc1DqMZJkk2n5ducdMcn+RNP0otJes7r/yyQChsDVizxwOacBhxjpSbdhwT0pQfmFSA7GWyD0qGfhCwB3IcirGOTjpTHwCM9zigZOGWQK6g7W5B9Kjddx9iCDUFk+0vbnqp+X6VYxkZxzigCKM/IAc/LxUYG64z6KadG+2Xb1DUoXLMwGKAFPXHpSL0IP1/Cl9ailY4J7YpgVJWLzKvqcfyrRTjhRhcFR+FZdqDcX3mZ+VCWP44FacRO7HsaAI34BNZMg3yfjWnOcR4HpVGJN8oNAjQt48RgE4qXI8s45OabGOhIpTjB7UMDh/iZaLeeB9TRl5WHzM+6nNfMOeCK+sfF0P2jw9qMGCfMtpF/8AHTXyd6g/SmAyjFFFFxhRRRRcAoopKQC5opKKAPolpeM9qFkycVVQlh/n3qVG5wKpozkX4zwOanDccVUjap1bipsNbD5mxA1cVetnUPxrsJ2/cmuKvD/xMPxFaQQpHZaac2wrRU/L1/zxWXprYth+FaCHjpUvcpbFqLl0Hqa6qMfIAPSuTgb96mR/EK6uE8D6VIhWXKkUsAxk/wB2gH5sZoKsucGmh2HZywI4p7EkKR+NMXpyKcwzET6UhoSb7gNMhyRwKeAGjx1qKM+xFAFracrkfrRtJBPajjApG+7gUAKvIANNY/NnGfenA4BPtTByNx/KgGDk4HPPX8iK8k+MEQk0h3f7qMCuegJ28V624OR715R8Yh/xTkj9BvGfzFVAR4MTwo+nekjBLcetIwxtI7inQ5MyjOMsK1Ge0+EAZWgDHLFVBz24r2KxVVgQccDnjrXkPgtG/tOBc8Yx+leuxHbtz2wPrWcyUW3BPCjAzmgAZ9acR0OeozQflFZlkbxh3UgH5T1HUmpthzn+VG3jFPKkYoEJyOtNkbA+tPIwKruN0uOcA0ANkz5YC4BzzUifcHyCo5PvAe9WCmFA9KYFHUI/Msp4yBiRCuMdjx/WsnSzvhye+Ac98jP9a3JgDEw+v9KwrAGGeRC3DRrgehRih/pQhlu4G8OhH3l2H9KTRlzE27PmBiM+oqaVR9pQDqx/z/KixBDyAYBDkj6f5FAF1XJ7sDT8+oz7mo87mJ704HHUUgJV2FiuD60xlByOeelKCNwp5XPGaAKM58maOYfRqvBwygjuKqzR74ipHfinWL+ZCVDAkcdKAGS4ikVj0DVKpO8j14NJcpmM+uDUULmSNWHGOPypgTDAbHWql0wSPPYnAyetWyoJHH1OaydSffPBCv8AEc/qKTBFjTI8Wm4jl+/4iry53Z6cmmRxiGPy8cLxj6c04k9qAK1yQFIB5I4/Oo7dBnNOlGZD9afbj5Bx3pgWl4puAytxTh90mmfwNj1FIGY2tRlrOdCcAowr5DlUpK6H+FiK+wtQG6NuM8HivkXU12apeKR0mcf+PGqAqdqSlpKkAooooAKKXtSUAFFFFAHvkbbY/qKVXy2OlVvNUJjNCyru4atWjFs1ImqdW54rPimX+8KsLMo70rMqLLEzZgP0rirth/aB/wB6uslmHkHmuNuGDXx5zzWkEJtHbacw8hfwrQRjj/PtWRpzjyE5rSR+B0qJLUuOxejJyuOua6u2OYQD1wK5K0IkmRR611NsSU61FhMsOMc0SOSQ4+lP+8Krgjc0Z9aBlheV46U9MKGzyMGq6sRkZ6CpyR5YH96kCGxkAHPrn+dRZ/eHPAPSnMdr5HfilbBYduKBk0ZHFDHLelCgYHNI4XeNv60AOZsqFUfjShCSATgCoznOAfyp5+VM5JJwOaAI368c4x/SvLPjI6/8I35ZGN0nGPYrXqJ46dhz+leV/GPH9hQPn5w5IHrVwEzwU/wj0FOhBNxH7sP5009s1La/8fUWf74/nWjC57X4NyusWy5IIXd+leuKgbdgkEDivNPBFoZNXkmxlUXA+tenIpUk9OBxWcwSJLaXzLfJ5KErVny8nJ7VTsUKvMhHAfI/MGtHAAH0rMobt5p3OKMUp4WgRDITuxUSgmTcxG2pGBZ85qPGC2T0oGJtLTEelWWBOBxn1qvbrlmb14qU5zTEMdAwK+vFc5ny7/OcbJXT8JFDL/48DXS9xntXN6ovl3bgAgsglB/3SM/luoTGadyFE0TgcAgdabaoFkz03A/zp7nfahvYfpSQf8svXH9M0wLRAB4JzS9TxSA55xyaTueoqQJFb5hUtVwcEVMpyuKAEcDOOxNZ8H7i9ZM4Vq0T94A1QuVBCyr1U0AWx0POeKht0xGSRxuNOgkDqD3IoHG4ds0wFLjnsKy1Uza2RjiIbf61pNgKWPbn8qpaXmaSW6YDLMf5Uho0zgsW/wB5sfXAqJzwT0wM089SP1qNicEexpiIJB+9P0yKnhXEZ45HNQyfNN+FWIeMj1oAd9RTT9xj3zTj0OKaf9Xjjk0gZmXp4+9j8K+TvEMfk+I9TQ87bmQf+PGvrC+6Hack18ueN4xF4z1ZAMfvyce+MmqA56kpaO1SAlFLRTASilxRQAlFLRQB3Mmvt2JqMeIJB/erDMhPYUm8+lel7OJw3kdJH4mkXsxqZfFTDs1cwjcZpTKPShUoj5mdM/ioshX5uax21SdrjzMnGelZxkHpSiUAdKpQSFeR1lr4qEMQBLZH/wBara+NVx1OelcQHDDp/nil3gfz/SodNblKUke3eAtSGt+fcj7kfyj64rvLZyMgd64z4b6X/Z/hKJj9+f5zx9f6YrsoAFY+tcU9zWLbNKNgxwR2qpcKFxIvY5qzDkDjrSMm5MVBsiLcGIIx81St29hVSAmOUxsPcDtVncWycYPcUhDJD3FAORSuMpimoCqjjqMigZZU4AFISS9MB6ZqQbcHHWgTCPl+RTpCT9BSR4UMRye1BGfxxSGhpHBJ9K8i+Mzsui2e3+N2XBHUY/8Ar165NkxEAcnAX36df1ryL42yFNN0tMfK0jMWPXGOP0FaUxM8PY8n0qa1GbuDPTzFH61Cw6HA6Cp4iRPDjjEi1sI+k/ANsUt5JtuQzc12x4Y8etcz4Jj26ShH8WCSK6uJSx3EYHp61hLcpMkiQonucZqc89aaBgAU7rUANPBprnPANPYgCov4s9qBiZyT0qI/cY1KRyagmIWBjzxQBLaf6sH1OalI5qO2AEC4qQ9eBTAb1bj+8DWJrcW2aznxwshiPuGGP54rbxyP896o6urPp8uOqYcfUEGkBXs2LWAGeQg/kB/OpUbEiDHP+PFVdNk82Mr0GTj8TkVZbcs8R/28UwLP3XJ7HrSnhval/iHFJ1bHvSAQgEdakibrmmY4PNIp2sD60AWMAEGq0ihi6eoOKs+49Kgl+8aAKlplHaNvSrHRnB9Kpz5iuFcdDVoSFpFz34zTAjum22UvrsP8qXSkMdnCCBllJ/Oor8H7EV6Mx2rmr0CBFjToFwPp3pDFO0ZzknP+FQsWOeBipG4Y/jTG96BEbffJ9hUsec5qDO6RQOPlGfzq2gCjBOTTASU8kVGwxESfUU5zlzx2pkrfuG+opAzMuj+7JzjH+NfM3xEiMPjjUBzhvLf841J/XNfS15/qyM4yf0r55+K+3/hN5ABgi3jz78H+mKp7AcLSjpSUo6UgCiiimAvakpaTtSEFFFFMDX7f596b060zzwKha4znmvR50cqgyxvo3VV87ilEwHrS9oivZlnqKUDIqus4Ao89fWn7RC5GWlFWLKA3V7Bbr/y1kVazhccYB5rp/AVp/aXi20jAysZMh/Dp/MUpTVhcjPoPT7cW2mW8CgAKoXj2FW4j0z1pAuECjA4zTlzw3pXE3dmsdjRh6ZNO4wOTzUcLYj5GKk7DmoZoincoUZJFzkHmpgQe/J5qRxvXGKgPDMvcdqQxZM7cj86ZI2J1QdAnSnFiMfyqrO+NVQHvkH8KAL6yLjGwfnQvLDAqJeVB/P61NFyRTEyX7q4Hak6gZoc/MRRzxwKQxjHc8SkZAdcj16V5D8ZYTPpdpIGG1JF3MfQqcY/SvW3J3g4HAGPrmvNPinEJfCl7IADtlAU/7IYDP61cBM8CYZx7jFOR9s8bE/dYH9aRvvLjp+v401j+9zjODW7EfU/gV/O8PwyL3wa7JV2gAHp3rlPh/ZPaeFLRpB88qBgPQYrrsngdh0rmluUkB4xSk4FIeetITgc1IxJSc03B65pGOTzSc80AK5A9arXfFq2O9Tv92q16T9kP4UIC5GNsScdRmnZ56Ukf+pQ+wpOc8GmAHhutRzKGVlJJBAB/GpD15/z0qN8jJwSccfXApAYGksyylW7Y4+grTnUnac4IIbFUI0MWryqAdpkIPsOD/WtNwcKTjOMUDJFbdil/iB/OolBJ9zxUgyT04oEOUcfhUfXHanZxkelJ6YoAkjcjg5psxwee9IPvU6XnaPahAVbiNZIu+QKgjkZowMgEcg1bxkMD3GKzgTG7j8qYFi5Bne364aQFs8881e43DAxyapWZEoLnJxkfQ1dOTk+pzQAP99vqf5VG3547mp3AGfcVVmbgADGTQAyPmQ4Axx/WrY+UFR27iq9uvyqeOhqZj0UfjQAhAOTk81HMR5XCnG481IThaimwIl46lqQGbdoChXJHbn0/yK8B+L8W3xVbz7QBPbA9O4dx/hX0DdDKnnHB59OteKfGi2bzNKuwPkzLGfqTnH86voI8n7Ug6UppKkYUUUdqAF6d6QdOtFA6UAFFFFGoFpoZOeKj8l+eK35o4lyDVNvKya9CVFHLCq2ZflsKcInPatD91npU0bQAUlRXcr2rMryHxn+lJ5TVrtJDtxioDJGTQ6SQlVbM8RNnkV6x8GNJY3F7qMi8qBGn9a85SSErg9P/AK4r3X4WWKw+GUlA5mYv/Os5pRWhXM3ozuHTAUdcAD9P/r0n/LP8aduJJPuf1xQB+7Y+9c1y1sWbfmEn3pxOF5qG36EZ4zU+Pkz9aTKQqtlRx0pky/vBIOo60qNkNnjAzT8fKQehyKQyuwwR78VQvDtukl9H5/GtEjcABwR61n34bJ6EZB/WmBeXqalh5fB44qBWBAbNSqcSA5pklhuhHem/dA5pX5NJjj60iiGeUQwySvnaik8VwPxHt9nw/uSx+cAFiT95sgn8/wCldxejckcXeRhn/dVSx/8AQa5T4nweZ4J1BMcqqkD0xiqhuJnzW3VfWmkEk09hnaAecD86YepOcdxW7JR9b+B5fP8ABulSHq1smcc84xXSZ4Fcb8NGP/CB6Wd2T5eK7Hoa5pLU0Q7qcGonYk44xT2b5c4qFj83FSA/sMGg5oxSkZHWgBkh+XBNV7v/AI9iKnfHQZqG75iP0zQBYt2L20Z46VITk5qCyY/ZU56Eip8+tMBhb+VNblhnpjJpz45pr/XtikBj32Ir5JB1YAH8P/1irxOVHsag1JTiOQc/OVIx2Ix/OnxvujUk5+n0oAen+sPXHFTEfLkGoh9+pRxgUwE3cAcUDaRnkc009TjtzQvXH0NJAOAyRzxT5AcjjtUQGWA9B+tSnPXPQUARE59qz74mN2bgjbmtHPPNZmtbhp7Ooyc449KbGixpgxp8b4wzZY1dBJ4/z2qhpjj+z4x/d4P86uoec0kDJHGEBz3xVFzukK5+lWZnxFgdmBqsFweaYizAAI2BoXljkU+OP5D7VIkSk4yTQBCTUUrYjX1z/MmrRRQTx0qGVEwB7CgDNnPXb26H0PrXkPxlUnR7JuQgu3yPcq2P5H869kkiDbue1eU/F62L+FXlwf3V2jD8d4poR4Z2pKMUUhhRRRQAUUUUAFFFFAGmJmmPJNNcYPWoI2ZeRT2d2616Cm2c/KkShQyZzyKgActgc09A7HAPWrUQEQOcU0rg3YjW2cpliRUfkMWwCDVprjK7RzVf5kcE96rlRKbJ4tOklZY15ZyFAHqa+mfDVh/Zug21sox5cYH445/WvEfAlr/aXiW2j8slYyXbP04r6BgAS3Gf4sf1rnr2Q6bctxw+6uPSn/8ALLjrmlCAjilKlY8+9cpt0GwHDVZBBXNVlB34qZTxj3oGhhYpv/3anQ7os1DMoII9qjtJcgo3UE1IyeUZOf8APes29XIIx1rTYZ5zxis+bliD+FMAsZfNt8N95eKucgisi2cx3mznD1qA/KB6UxFtyTsNDn0J46DtSZyij1pHIWNicYALH6f5FAyrhZL4tjiGMr+J+bP6VzfxART4Y1RdxUfZXbgex/xNdJYqRGJGwTKzSE+uTkf4Vz/jnnwzqRPzAWzjn/d/+v8Azpx3Ez5il+UryeAD+JqNhkn6U887ST1ApGI3dfYZrpvoQj6h+GEwbwPp6Z5RBkV2u7OK81+Fl1s8P2sbHhkzn6V6MScn1HFc09zQkBBGevao24b0pynGRTG561Ax/fOaUnikOBxTiPm9qAIm+9Udx/qm+mKe/X8aSbOwgDtQAmn5a3GezVaIPpVHTXbY4PY5q62SaAGlSeopGRuKRh60jY20AVr6MtazfOPlG4EdsGqlu43MAeB/n+taBUHKnoRgj2rKgDKq7s7h8p/PFAGipywIPaptuQOKrR8gA9cVOuB0PamIbjkD3xTRw+PpTxuyOO9R55BP+eCaSGSJjd708j5Vz1xUarlsZxUzIc8ZPFAEP8WKp6hF51hOoH8HerhA3DIpjqCjemMcUwRk6PMHAiz94ZraQdh0rldPl8vxAbfJLI7DA+ldRkhTjHSkimNcZjfv3/SmxAM29ug6UqqGdk5+ZT/hU2FUbc8D2oZI5X5IA603ex6HH0pybRnA5pgJ65oAQknqWPr700j5WPuKeScU3ornntTAzpt6KSGzkHt71518UpEPgu/jcc+ZGVb1PmD+hP616TNu2Ng9uPrzXmXxajV/BtxJyNs0ZXHruP8ARv5U+gjwSkooqRh60UUUAFFJSimAuKKUYop2AsQsBUxAPpVWnqx29a7YytoZSWpOG2d6N3mNgZphqWL7prSLuQ9CZFCrzTHO40m44605Dgcd6szS6npvwitt1zeXJByAFB/I17GoA2gZAxXk/wAI/wDj1vz/ANNSP0Fesjr+A/lXFW3NIEqgkYzT2BEYGM5pI+VFTYG1fpWBqiqPvVMowD9ajk4J+tSR8g5oGKwBwScVmSsbe43Zyuea1GAGMVnamAYm+lJDLYk/dF+m4VWuU3KQpP4VDBI5tlBY4zU0hIJwe1MDPErLcqJOCDxWworKnH75T3yK0k6D60ITLURzGR3qO8GbcRj+NlUE9Md6dB0f6VDdEtdQIeVwTikxj3ZI48kjCgY+mef5CsTxVCJ/CuoK6Ft8J3AdSeePyJ/SteX76jtvx+lVdd50CcnqV5/ShbgfJRPypzk460MCQfrQOEWhuWOfauu2hB7j4Fufs9jY7PYN7f8A169bSTcoyeSAa8Z8HHZploF4FevWxJiXJ7VzTRZfByelNfqKlUDiopfvVAyT/gQpTtxyxzUY6040AMk6ilcbkGT14pHpW+4v1/rQBT05gHcZ9q0zhTWTZ/8AH0/+fWtUkjjNMBrmkYZHSl7L9R/OmnoKQDWxu61myHF3KvQZD9eg6Vp1lXPGqcd4Tn8xQBYiweTVhQo+p6Z71UjH7sVZXk89ulMQueV92FR5Ofw/pipCef8AgVMbrQMkQDIHpUhODwTUcf8An9alb71JgRNjdyKZhdhOe3Gf6mpHHzUw/d/H+lAHMbFg8epGUI860aTI7kED+tdMOQR6j+WP8ayLqNP+Eh06XaPMEUw3d8ZWtZfvMP8APU00hkiLg5NOx70dqAKGIEB+cY7f1pg6VKg5f6U1QOaQDT0ph/1b8+lPP3ab/A/4UAVHwAfTvXmHxcl8rwZIAMCS4jTj65/9lr06bof8+teW/GEn/hDv+3qP/wBnqugjwg8U2lPWkqRhRRQaAClpKKAHDpRSr0orQD//2Q==","/temp/123.jpg");
	}*/
    
    
}  

