package com.psk.hms.base.exception;
/**
 * 描述: 业务逻辑异常类
 * 作者: xiangjz
 * 时间: 2018-04-24
 * 版本: 1.0
 *
 */
public class BizException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * 团队模块异常
	 */
	public static final int TEAM_ERROR = 10020000;
	/**
	 * 项目模块异常
	 */
	public static final int PROJECT_ERROR = 10030000;
	/**
	 * 设备模块异常
	 */
	public static final int DEVICE_ERROR = 10040000;
	/**
	 * 劳务模块异常
	 */
	public static final int LABOR_ERROR = 10050000;
	/**
	 * 巡检模块
	 */
	public static final int INSPECT_ERROR = 10080000;
	/**
	 * 第三方-轻推 异常
	 */
	public static final int QT_ERROR = 20010000;
	/**
	 * 第三方-微信 异常
	 */
	public static final int WX_ERROR = 20020000;
	
	/**
	 * 第三方管理
	 */
	public static final int THRID_PARTY = 20060000;
	
	/**
	 * api拦截器异常
	 */
	public static final int INTERCEPTOR_ERROR = 90010000;
	
	/**
	 * 登录失效
	 */
	public static final int USER_BACK_LOGIN_ERROR = 98888888;
	

	/**
	 * 数据库操作,insert返回0
	 */
	public static final BizException DB_INSERT_RESULT_0 = new BizException(90040001, "数据库操作,insert返回0");

	/**
	 * 数据库操作,update返回0
	 */
	public static final BizException DB_UPDATE_RESULT_0 = new BizException(90040002, "数据库操作,update返回0");

	/**
	 * 数据库操作,selectOne返回null
	 */
	public static final BizException DB_SELECTONE_IS_NULL = new BizException(90040003, "数据库操作,selectOne返回null");

	/**
	 * 数据库操作,list返回null
	 */
	public static final BizException DB_LIST_IS_NULL = new BizException(90040004, "数据库操作,list返回null");

	/**
	 * Token 验证不通过
	 */
	public static final BizException TOKEN_IS_ILLICIT = new BizException(90040005, "Token 验证非法");
	/**
	 * 会话超时　获取session时，如果是空，throws 下面这个异常 拦截器会拦截爆会话超时页面
	 */
	public static final BizException SESSION_IS_OUT_TIME = new BizException(90040006, "会话超时");

	/**
	 * 获取序列出错
	 */
	public static final BizException DB_GET_SEQ_NEXT_VALUE_ERROR = new BizException(90040007, "获取序列出错");

	/**
	 * 异常信息
	 */
	protected String msg;

	/**
	 * 具体异常码
	 */
	protected int code;

	public BizException(int code, String msgFormat, Object... args) {
		super(String.format(msgFormat, args));
		this.code = code;
		this.msg = String.format(msgFormat, args);
	}

	public BizException() {
		super();
	}

	public String getMsg() {
		return msg;
	}

	public int getCode() {
		return code;
	}

	/**
	 * 实例化异常
	 * 
	 * @param msgFormat
	 * @param args
	 * @return
	 */
	public BizException newInstance(String msgFormat, Object... args) {
		return new BizException(this.code, msgFormat, args);
	}

	public BizException(String message, Throwable cause) {
		super(message, cause);
	}

	public BizException(Throwable cause) {
		super(cause);
	}

	public BizException(String message) {
		super(message);
	}
}
