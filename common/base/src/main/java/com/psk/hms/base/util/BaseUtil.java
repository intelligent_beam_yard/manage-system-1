package com.psk.hms.base.util;

import com.psk.hms.base.util.string.StringUtil;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 主要工具
 *
 * @author xiangjz
 */
public class BaseUtil {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @SuppressWarnings("rawtypes")
	public static boolean isEmpty(Object obj) {
    	if (obj instanceof List) {
    		List list = (List) obj;
    		return list == null || list.toString().equalsIgnoreCase("null") || list.toString().length() == 0 || list.size() <= 0;
    	}else{
    		return obj == null || obj.toString().equalsIgnoreCase("null") || obj.toString().length() == 0;
    	}
    }
    
    public static String removeLastChar(String str){
    	return isEmpty(str)?"":str.substring(0, str.length()-1);
    }
    /**
     * Object转为String
     * @param obj
     * @return
     */
    public static String retStr(Object obj) {
        return isEmpty(obj) ? "" : obj.toString();
    }
    /**
     * Object转为Integer
     * @param obj
     * @return
     */
    public static Integer retInt(Object obj){
    	return isEmpty(obj)?0:Integer.parseInt(obj.toString());
    }
    /**
     * Object转为Integer，当obj为空时，返回null
     * @param obj
     * @return
     */
    public static Integer retIntNull(Object obj){
        Integer result = null;
        try {
            result = isEmpty(obj)?null:Integer.parseInt(obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * 将字符串末尾指定的串截取
     * @param str
     * @param suffix
     * @return
     */
    public static String cutStrEndWithChar(String str,String suffix){
    	suffix = isEmpty(suffix)?",":suffix;
    	if(isEmpty(str)){
    		return "";
    	}else{
    		return str.endsWith(suffix)?str.substring(0,str.length()-1):str;
    	}
    }
    
    /**
     * 转obj为map
     * @param obj
     * @return
     */
    public static Map<String, Object> transBean2Map(Object obj) {

        if (isEmpty(obj)) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();

                // 过滤class属性  
                if (!key.equals("class")) {
                    // 得到property对应的getter方法  
                    Method getter = property.getReadMethod();
                    if(!BaseUtil.isEmpty(getter)) {
                    	Object value = getter.invoke(obj);
                    	map.put(key, value);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;

    }

    /**
     * 转map为obj
     * @param map
     * @param beanClass
     * @return
     * @throws Exception
     */
    public static Object transMapToBean(Map<String, Object> map, Class<?> beanClass) throws Exception {
        if (isEmpty(map))
            return null;

        Object obj = beanClass.newInstance();
        //循环向上转型获取父类属性
        for(; beanClass != Object.class ; beanClass = beanClass.getSuperclass()) {    
            try {    
            	Field[] fields = beanClass.getDeclaredFields();
                for (Field field : fields) {
                    int mod = field.getModifiers();
                    String fieldName = underlineToCamel(field.getName());
                    if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                        continue;
                    }
                	field.setAccessible(true);
                	Object value = null;
                	String simpleTypeName = field.getType().getSimpleName();
                	if(simpleTypeName.equals("Double") || simpleTypeName.equals("double")){
                		value = isEmpty(map.get(fieldName))
                				?0.0
                				:Double.parseDouble(retStr(map.get(fieldName)));
                	}else if(simpleTypeName.equals("Integer") || simpleTypeName.equals("int")){
                		value = isEmpty(map.get(fieldName))
                				?0
                				:Integer.parseInt(retStr(map.get(fieldName)));
                	}else if(simpleTypeName.equals("Float") || simpleTypeName.equals("float")){
                		value = isEmpty(map.get(fieldName))
                				?0
                				:Float.parseFloat(retStr(map.get(fieldName)));
                	}else if(simpleTypeName.equals("Date")){
                		String fieldValue = retStr(map.get(fieldName));
                		if(!isEmpty(fieldValue)) {
                			if(StringUtil.isNumeric(fieldValue))
                				value = new Date(Long.valueOf(fieldValue));
                			else 
                				value = sdf.parse(fieldValue);
                		}
                	}else{
                		value = map.get(fieldName);
                	}
                	field.set(obj, value);
                }
            } catch (Exception e) {  
            	e.printStackTrace();
            }     
        }  

        return obj;
    }
    
    /**
     * 下划线格式字符串转换为驼峰格式字符串
     * 
     * @param param
     * @return
     */
    public static String underlineToCamel(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c == '_') {
                if (++i < len) {
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
}