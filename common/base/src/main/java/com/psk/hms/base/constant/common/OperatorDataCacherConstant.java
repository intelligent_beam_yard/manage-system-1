package com.psk.hms.base.constant.common;

/**
 * 权限数据缓存常量
 * @author xiangjz
 */
public class OperatorDataCacherConstant {
	// 权限数据，用于返回前端的简短字段数据集（用于redis的key） operatorId为key
	public static final String OPERATOR_DATA = "operatorData";
	// 权限数据，用于返回前端的简短字段数据集（用于redis的key） url为key
	public static final String OPERATOR_DATA_URL = "operatorDataUrl";
	// 权限biz验证数据key（用于redis的key） applyType == 1 应用
	public static final String OPERATOR_BIZ_VALID_DATA_APPLYTO_1 = "operatorBizValidDataApplyTo1";
	// 权限biz验证数据key（用于redis的key） applyType == 2 菜单
	public static final String OPERATOR_BIZ_VALID_DATA_APPLYTO_2 = "operatorBizValidDataApplyTo2";
	// 权限biz验证数据key（用于redis的key） applyType == 3 api页面
	public static final String OPERATOR_BIZ_VALID_DATA_APPLYTO_3 = "operatorBizValidDataApplyTo3";
	// 权限biz验证数据key（用于redis的key） applyType == 4 功能
	public static final String OPERATOR_BIZ_VALID_DATA_APPLYTO_4 = "operatorBizValidDataApplyTo4";
	// 权限biz验证数据key（用于redis的key） applyType == 5 admin页面
	public static final String OPERATOR_BIZ_VALID_DATA_APPLYTO_5 = "operatorBizValidDataApplyTo5";
	
	// 权限通过
	public static final String OPERATOR_VALID_PASS = "passed";
	// 权限拒绝
	public static final String OPERATOR_VALID_DENY = "denied";
	
	//缓存和数据库是否一致(用于刷新缓存的判断，不一致则刷缓存)
	public static final String OPERATOR_SAME_AS_DB = "operatorSameAsDb";
}
