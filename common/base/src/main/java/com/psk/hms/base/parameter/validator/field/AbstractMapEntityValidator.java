package com.psk.hms.base.parameter.validator.field;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

import java.util.Map;

/**
 * Map键值校验器抽象类
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public abstract class AbstractMapEntityValidator<K, V> extends AbstractMapValidator<K, V> implements MapEntityValidatable<K, V> {
	
	protected AbsentMsgGeneratable<K, V> absentMsgGenerator;
	
	public AbstractMapEntityValidator(K key, boolean keyRequired) {
		super(key);
		this.absentMsgGenerator = keyRequired ? new RequiredKeyGenerator<>() : new AlternativeKeyGenerator<>();
	}
	
	public AbstractMapEntityValidator(K key, boolean keyRequired, String errorMsg) {
		super(key, errorMsg);
		this.absentMsgGenerator = keyRequired ? new RequiredKeyGenerator<>() : new AlternativeKeyGenerator<>();
	}

	@Override
	public final MapValidateResult<K> check(Map<K, V> map) {
		if(!map.containsKey(key)) {
			return onAbsent(key);
		}
		return checkEntity(key, map.get(key));
	}
	
	@Override
	public final MapValidateResult<K> onAbsent(K key) {
		// 不存在时的校验方法
		return this.absentMsgGenerator.getDefaultAbsentResult(key);
	}

}
