package com.psk.hms.base.parameter.validator.field;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

/**
 * Map键值校验能力接口。实现此接口将可以对单一的map键值进行校验
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public interface MapEntityValidatable<K, V> extends MapValidatable<K, V> {

	/**
	 * 校验指定的map键值并返回校验结果。当此key在map中存在时才会调用此方法。
	 * 
	 * @param key 校验的map key
	 * @param value 校验的map value
	 * @return
	 */
	MapValidateResult<K> checkEntity(K key, V value);
	
	/**
	 * 字段不存在时的处理方法。当此key在map中不存在时才会调用此方法
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	MapValidateResult<K> onAbsent(K key);
}
