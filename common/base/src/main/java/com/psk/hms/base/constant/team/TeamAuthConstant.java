package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队授权常量类
 * @author xiangjz
 * @date 2018年8月28日
 * @version 1.0.0
 *
 */
public class TeamAuthConstant extends Constant {
    /**
     *  数据库字段-团队id
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据库字段-项目授权业务id(1-项目id  2-应用id...根据biz_type来定类型)
     */
    public static final String BIZ_ID = "bizId";
    /**
     *  数据库字段-项目授权业务类型(参考字典)
     */
    public static final String BIZ_TYPE = "bizType";
    /**
     *  数据库字段-授权码
     */
    public static final String AUTH_CODE = "authCode";
    
    /**
     *  业务类型1-项目  ==>对应projectId
     */
    public static final String BIZ_TYPE_1 = "1";
    /**
     *  业务类型2-应用  ==>对应applicationId
     */
    public static final String BIZ_TYPE_2 = "2";
}
