package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队职务常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamJobConstant extends Constant {    
    /**
     *  数据表字段名称 - teamId
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
}
