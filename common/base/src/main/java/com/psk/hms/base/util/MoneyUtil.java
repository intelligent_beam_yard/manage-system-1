package com.psk.hms.base.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 描述: 金额工具类
 * 作者: xiangjz
 * 时间: 2017年8月27日
 * 版本: 1.0
 *
 */
public class MoneyUtil {

	private static String[] CH = { "", "", "拾", "佰", "仟", "萬" };
	private static String[] CHS_NUMBER = { "零", "壹", "贰", "叁", "肆", "伍", "陆",
			"柒", "捌", "玖" };
	private static String[] CHS = { "萬", "亿", "兆" };
	private static DecimalFormat df = new DecimalFormat(
			"#########################0.0#");

	/**
	 * 传入数字金额双精度型值，返回数字金额对应的中文大字与读法
	 * 
	 * @param money
	 *            金额
	 * @return 金额中文大写
	 */
	public static String transFormMoney(double money) {
		return transFormMoney(df.format(money));
	}

	/**
	 * 传入数字金额浮点型值，返回数字金额对应的中文大字与读法
	 * 
	 * @param money
	 *            金额
	 * @return 金额中文大写
	 */
	public static String transFormMoney(float money) {
		return transFormMoney(df.format(money));
	}

	/**
	 * 传入数字金额字符串，返回数字金额对应的中文大字与读法
	 * 
	 * @param money
	 *            金额字符串
	 * @return 金额中文大写
	 */
	public static String transFormMoney(String money) {
		String result = "";
		try {
			BigDecimal big = new BigDecimal(money);
			String[] t = null;
			try {
				t = big.toString().replace(".", ";").split(";");
			} catch (Exception e) {
				// 金额如果没有小数位时,也要加上小数位
				t = (big.toString() + ".0").replace(".", ";").split(";");
			}

			String[] intString = splitMoney(t[0]);
			String tmp_down = t[1];

			for (int i = 0; i < intString.length; i++) {
				result = result + count(intString[i]);

				if (i == intString.length - 2 || i == intString.length - 3)
					continue;

				if (i != intString.length - 1) {
					result = result + CHS[intString.length - 2 - i];
				}
			}

			if (Integer.parseInt(tmp_down) == 0) {
				result = result + (intString[0].equals("0") ? "零圆" : "圆整");
			} else {
				result = result
						+ (intString[0].equals("0") ? "" : tmp_down
								.startsWith("0") ? "圆零" : "圆")
						+ getFloat(tmp_down);
			}

		} catch (Exception e) {
			return "你輸入的不是数字符串";
		}
		return result;
	}

	/**
	 * 对整数部分字符串进行每四位分割放置分割符
	 * 
	 * @param money
	 *            整数部分字符串
	 * @return 放置分割符后的字符串
	 */
	public static String[] splitMoney(String money) {
		StringBuffer tmp_int = new StringBuffer();
		tmp_int.append(money);

		// 先對整數位進行分割，每四位爲一組。
		int i = tmp_int.length();
		do {
			try {
				// 進行try..catch是因爲當位數不能滿足每四位放分割符時，就退出循環
				i = i - 4;
				if (i == 0)
					break;
				tmp_int.insert(i, ";");
			} catch (Exception e) {
				break;
			}
		} while (true);
		return tmp_int.toString().split(";");
	}

	/**
	 * 转换整数部分
	 * 
	 * @param money
	 *            整数部分金额
	 * @return 整数部分大写
	 */
	public static String count(String money) {
		String tmp = "";
		Integer money_int = 0;
		char[] money_char;
		// 如果數字開始是零時
		if (money.startsWith("0")) {
			money_int = Integer.parseInt(money);
			if (money_int == 0)
				return tmp;
			else
				tmp = "零";
			money_char = money_int.toString().toCharArray();
		} else {
			money_char = money.toCharArray();
		}
		for (int i = 0; i < money_char.length; i++) {
			if (money_char[i] != '0') {
				// 如果當前位不爲“0”，才可以進行數字和位數轉換
				tmp = tmp + CHS_NUMBER[Integer.parseInt(money_char[i] + "")]
						+ CH[money_char.length - i];
			} else {
				// 要想該位轉換爲零，要滿足三個條件
				// 1.上一位沒有轉換成零，2.該位不是最後一位，3.該位的下一位不能爲零
				if (!tmp.endsWith("零") && i != money_char.length - 1
						&& money_char[i + 1] != '0') {
					tmp = tmp
							+ CHS_NUMBER[Integer.parseInt(money_char[i] + "")];
				}
			}
		}
		return tmp;
	}

	/**
	 * 转换小数部分
	 * 
	 * @param fl
	 *            小数
	 * @return 小数大写
	 */
	private static String getFloat(String fl) {
		String f = "";
		char[] ch = fl.toCharArray();
		switch (ch.length) {
		case 1:
			f = f + CHS_NUMBER[Integer.parseInt(ch[0] + "")] + "角整";
			break;
		case 2:
			if (ch[0] != '0')
				f = f + CHS_NUMBER[Integer.parseInt(ch[0] + "")] + "角"
						+ CHS_NUMBER[Integer.parseInt(ch[1] + "")] + "分";
			else
				f = f + CHS_NUMBER[Integer.parseInt(ch[1] + "")] + "分";
			break;
		}
		return f;
	}

	public static String toChinese(double n) {
		String[] fraction = { "角", "分" };
		String[] digit = { "零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖" };
		String[][] unit = { { "元", "万", "亿" }, { "", "拾", "佰", "仟" } };
		String head = n < 0 ? "负" : "";
		n = Math.abs(n);
		String s = "";
		for (int i = 0; i < fraction.length; i++) {
			s += (digit[(int) (Math.floor(n * 10 * Math.pow(10, i)) % 10)] + fraction[i])
					.replace("/零./", "");
		}
		n = Math.floor(n);
		for (int i = 0; i < unit[0].length && n > 0; i++) {
			String p = "";
			for (int j = 0; j < unit[1].length && n > 0; j++) {
				p = digit[(int) (n % 10)] + unit[1][j] + p;
				n = Math.floor(n / 10);
			}
			s = p.replace("/(零.)*零$/", "").replace("/^$/", "零") + unit[0][i]
					+ s;
		}
		return head
				+ s.replace("/(零.)*零元/", "元").replace("/(零.)*零角/", "")
						.replace("/(零.)*零分/", "").replace("/(零.)+/g", "零")
						.replace("/^整$/", "零元整");
	}

	// 进行加法运算
	/**
	 * @param d1
	 *            加数
	 * @param d2
	 *            被加数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static double add(double d1, double d2, int scale) {
		BigDecimal b1 = new BigDecimal(d1 + "");
		BigDecimal b2 = new BigDecimal(d2 + "");
		return b1.add(b2).setScale(scale, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	// 进行减法运算
	/**
	 * @param d1
	 *            减数
	 * @param d2
	 *            被减数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static double sub(double d1, double d2, int scale) {
		BigDecimal b1 = new BigDecimal(d1 + "");
		BigDecimal b2 = new BigDecimal(d2 + "");
		return b1.subtract(b2).setScale(scale, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	// 进行乘法运算
	/**
	 * @param d1
	 *            乘数
	 * @param d2
	 *            被乘数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static double mul(double d1, double d2, int scale) {
		BigDecimal b1 = new BigDecimal(d1 + "");
		BigDecimal b2 = new BigDecimal(d2 + "");
		return b1.multiply(b2).setScale(scale, BigDecimal.ROUND_HALF_UP)
				.doubleValue();
	}

	// 进行除法法运算
	/**
	 * @param d1
	 *            除数
	 * @param d2
	 *            被除数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static double div(double d1, double d2, int scale) {// 进行除法运算
		BigDecimal b1 = new BigDecimal(d1 + "");
		BigDecimal b2 = new BigDecimal(d2 + "");
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	// 进行四舍五入操作
	/**
	 * @param d
	 *            数字
	 * @param scale
	 *            四舍五入返回结果保留的小数位数
	 * @return
	 */
	public static double round(double d, int scale) {
		BigDecimal b1 = new BigDecimal(d + "");
		BigDecimal b2 = new BigDecimal(1 + "");
		// 任何一个数字除以1都是原数字
		// ROUND_HALF_UP是BigDecimal的一个常量，表示进行四舍五入的操作
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	// 进行加法运算
	/**
	 * @param d1
	 *            加数
	 * @param d2
	 *            被加数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static String add(String d1, String d2, int scale) {
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.add(b2).setScale(scale, BigDecimal.ROUND_HALF_UP).toString();
	}

	// 进行减法运算
	/**
	 * @param d1
	 *            减数
	 * @param d2
	 *            被减数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static String sub(String d1, String d2, int scale) {
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.subtract(b2).setScale(scale, BigDecimal.ROUND_HALF_UP)
				.toString();
	}

	// 进行乘法运算
	/**
	 * @param d1
	 *            乘数
	 * @param d2
	 *            被乘数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static String mul(String d1, String d2, int scale) {
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.multiply(b2).setScale(scale, BigDecimal.ROUND_HALF_UP)
				.toString();
	}

	// 进行除法法运算
	/**
	 * @param d1
	 *            除数
	 * @param d2
	 *            被除数
	 * @param scale
	 *            返回结果保留的小数位数
	 * @return
	 */
	public static String div(String d1, String d2, int scale) {// 进行除法运算
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).toString();
	}

	// 进行四舍五入操作
	/**
	 * @param d
	 *            数字
	 * @param scale
	 *            四舍五入返回结果保留的小数位数
	 * @return
	 */
	public static String round(String d, int scale) {
		BigDecimal b1 = new BigDecimal(d);
		BigDecimal b2 = new BigDecimal(1 + "");
		// 任何一个数字除以1都是原数字
		// ROUND_HALF_UP是BigDecimal的一个常量，表示进行四舍五入的操作
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).toString();
	}

	/**
	 * 比较两个数字大小
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int compare(double d1, double d2) {
		BigDecimal b1 = new BigDecimal(d1 + "");
		BigDecimal b2 = new BigDecimal(d2 + "");
		return b1.compareTo(b2);
	}

	/**
	 * 比较两个数字大小
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static int compare(String d1, String d2) {
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.compareTo(b2);
	}

	public static void main(String[] args) {
		System.out.println(toChinese(1000000000.0232));
		System.out.println(toChinese(18493847575.0232));
		System.out.println(toChinese(1844237575.02f));
		System.out.println(toChinese(18493475.02));
		System.out.println(toChinese(0.02));
		System.out.println(toChinese(0.2));
		System.out.println(toChinese(50));
		System.out.println(toChinese(0));
	}
}
