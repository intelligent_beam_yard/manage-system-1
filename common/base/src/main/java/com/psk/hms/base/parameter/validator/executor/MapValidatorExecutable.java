package com.psk.hms.base.parameter.validator.executor;

import com.psk.hms.base.parameter.validator.field.MapValidatable;

import java.util.List;
import java.util.Map;

/**
 * 字段校验执行能力接口
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public interface MapValidatorExecutable<K, V> {
	
	/**
	 * 在给定的校验map上链式执行字段校验链。
	 * 
	 * @param map 带校验的map
	 * @param validatorList 字段校验链
	 * @return
	 */
	Map<String, Object> execute(Map<K, V> map, List<MapValidatable<K, V>> validatorList);
	
}
