package com.psk.hms.base.util.validator.field;

import java.util.Map;

import com.psk.hms.base.util.validator.result.MapValidateResult;

/**
 * Map校验能力接口。实现此接口将可以在整个map范围类内进行校验
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public interface MapValidatable<K, V> extends ErrorMsgGeneratable<K, V> {

	/**
	 * 校验指定的map对象，并返回校验结果。
	 * 
	 * @param map 校验的map对象
	 * @return
	 */
	MapValidateResult<K> check(Map<K, V> map);
}
