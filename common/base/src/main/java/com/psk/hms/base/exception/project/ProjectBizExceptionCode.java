package com.psk.hms.base.exception.project;

/**
 * <p>
 * PROJECT 业务异常类
 * </p>
 * 模块异常码 : 24000001 ~ 24999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class ProjectBizExceptionCode {
	
	/***
	 * 项目模块
	 * 24010000~24019999
	 */
	//项目模块-项目信息-增 
	public static final int PROJECT_INFO_SAVE_ERROR = 24010101;
	//项目模块-项目信息-删
	public static final int PROJECT_INFO_DELETE_ERROR = 24010102;
	//项目模块-项目信息-查
	public static final int PROJECT_INFO_SELECT_ERROR = 24010103;
	//项目模块-项目信息-改
	public static final int PROJECT_INFO_UPDATE_ERROR = 24010104;
	//项目模块-项目信息-通用异常
	public static final int PROJECT_INFO_MANAGE_ERROR = 24010199;
	
	//项目模块-项目成员-增
	public static final int PROJECT_EMPLOYEE_SAVE_ERROR = 24010201;
	//项目模块-项目成员-删
	public static final int PROJECT_EMPLOYEE_DELETE_ERROR = 24010202;
	//项目模块-项目成员-查
	public static final int PROJECT_EMPLOYEE_SELECT_ERROR = 24010203;
	//项目模块-项目成员-改
	public static final int PROJECT_EMPLOYEE_UPDATE_ERROR = 24010204;
	//项目模块-项目成员-通用异常
	public static final int PROJECT_EMPLOYEE_MANAGE_ERROR = 24010299;
	
	
	//项目模块-通用异常
	public static final int PROJECT_MODULE_ERROR = 24019999;
}
