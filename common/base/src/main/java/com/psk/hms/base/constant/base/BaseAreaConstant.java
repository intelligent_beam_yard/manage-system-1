package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础地区编码常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseAreaConstant extends Constant {
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - parentCode
     */
    public static final String PARENT_CODE = "parentCode";
    /**
     *  数据表字段名称 - parentCodes
     */
    public static final String PARENT_CODES = "parentCodes";
    /**
     *  数据表字段名称 - levels
     */
    public static final String LEVELS = "levels";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
    /**
     *  数据表字段名称 - cityid
     */
    public static final String CITYID = "cityid";
    /**
     *  数据表字段名称 - address
     */
    public static final String ADDRESS = "address";
}
