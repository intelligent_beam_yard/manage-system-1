package com.psk.hms.base.entity;

import java.util.ArrayList;
import java.util.List;

public class BaseAreaParentNode extends BaseAreaNode {
	private static final long serialVersionUID = 1L;
	
	// 地区子节点列表
	private List<BaseAreaNode> c;
	
	public BaseAreaParentNode() {
		super();
		this.c = new ArrayList<>();
	}
	
	public BaseAreaParentNode(String v, String n) {
		super(v, n);
		this.c = new ArrayList<>();
	}

	public List<BaseAreaNode> getC() {
		return c;
	}

	public void setC(List<BaseAreaNode> c) {
		this.c = c;
	}
	
	public boolean addChild(BaseAreaNode node) {
		return this.c.add(node);
	}
	
	public boolean hasChild(BaseAreaNode node) {
		return this.c.contains(node);
	}

}
