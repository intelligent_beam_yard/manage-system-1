package com.psk.hms.base.parameter.validator.field.impl;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateStringFieldValidator<K, V> extends EffectiveStringFieldValidator<K, V> {

	private DateFormat dateFormatter;
	private String formatString;

	public DateStringFieldValidator(K key, String format, boolean keyRequired) {
		super(key, keyRequired);
		this.formatString = format;
		this.dateFormatter = new SimpleDateFormat(formatString);
		this.errorMsg = this.getDefaultErrorMsg(key);
	}

	public DateStringFieldValidator(K key, String format, boolean keyRequired, String errorMsg) {
		super(key, keyRequired, errorMsg);
		this.formatString = format;
		this.dateFormatter = new SimpleDateFormat(formatString);
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> mapValidateResult = super.checkEntity(key, value);
		if (!mapValidateResult.isPass()) {
			return mapValidateResult;
		}
		String dateString = (String)value;
		try {
			dateFormatter.parse(dateString);
			return mapValidateResult;
		} catch (Exception e) {
			return mapValidateResult.failure(errorMsg);
		}
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'无效的日期，应为").append(formatString).append("格式");
		return sb.toString();
	}

	public DateFormat getDateFormatter() {
		return dateFormatter;
	}

	public void setDateFormatter(DateFormat dateFormatter) {
		this.dateFormatter = dateFormatter;
	}

}
