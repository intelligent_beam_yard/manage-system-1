package com.psk.hms.base.controller;

import com.psk.hms.base.parameter.JsonResult;

import java.util.Map;

/**
 * 描述: service controller类   qz-service下的 controller继承此类
 * 作者: 李厚余
 * 时间: 2018年10月3日 
 * 版本: 1.0
 *
 */
public abstract class BaseServiceController extends BaseController{
		
	protected abstract JsonResult findById(String id);
	
	protected abstract JsonResult listBy(Map<String, Object> params, String sqlId);
	
	protected abstract JsonResult findBy(Map<String, Object> params, String sqlId);
	
	protected abstract JsonResult listPage(Map<String, Object> params, int pageNum, int pageSize, String sqlId);	
}
