package com.psk.hms.base.entity;

import java.util.List;

public class LayerTree implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	// 排序
	private Long orderNum;
	// 子节点
	private List<LayerTree> children;
	// 是否勾选状态
	private Boolean checked = false;
	// 是否是根节点
	private boolean isRoot;
	
	// id
	private String id;
	// parentId
	private String parentId;
	// 树节点名称
	private String name;
	private String 	code;
	private String 	value;
	private String 	parentIds;
	private Integer	levels;
	private Integer	sort;
	private Integer  childrenNum;
	private String teamName;
	private String url;
	private String teamId;
	private String modelId;
	
	
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public Integer getLevels() {
		return levels;
	}
	public void setLevels(Integer levels) {
		this.levels = levels;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Integer getChildrenNum() {
		return childrenNum;
	}
	public void setChildrenNum(Integer integer) {
		this.childrenNum = integer;
	}
	public String getId() {
		return id;
	}
	public void setId(String string) {
		this.id = string;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String str) {
		this.parentId = str;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}
	public List<LayerTree> getChildren() {
		return children;
	}
	public void setChildren(List<LayerTree> children) {
		this.children = children;
	}
	public Boolean getChecked() {
		return checked;
	}
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	public boolean getIsRoot() {
		return isRoot;
	}
	public void setIsRoot(boolean b) {
		this.isRoot = b;
	}
	
	
	
}
