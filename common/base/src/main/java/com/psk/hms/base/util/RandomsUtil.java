package com.psk.hms.base.util;

import java.util.Random;

/**
 * 描述: 随机数工具
 * 作者: xiangjz
 * 时间: 2017年8月27日
 * 版本: 1.0
 *
 */
public class RandomsUtil {

private static final Random random = new Random();
	
	// 定义验证码字符.去除了O、I、l、、等容易混淆的字母
	public static final char authCodeAll[] = { 
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 
		'a', 'c', 'd', 'e', 'f', 'g', 'h', 'k', 'm', 'n', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 
		'3', '4', '5', '7', '8' };
	
	public static final int authCodeAllLength = authCodeAll.length;
	
	
	/**
	 * 产生0--number的随机数,不包括num
	 * @param number   数字
	 * @return int 随机数字
	 */
	public static int number(int number) {
		return random.nextInt(number);
	}
	
	/**
	 * 生成验证码
	 * @return
	 */
	public static char getAuthCodeAllChar() {
		return authCodeAll[number(0, authCodeAllLength)];
	}
	
	/**
	 * 产生两个数之间的随机数
	 * @param min 小数
	 * @param max 比min大的数
	 * @return int 随机数字
	 */
	public static int number(int min, int max) {
		return min + random.nextInt(max - min);
	}
	
	/**
	 * 生成RGB随机数
	 * @return
	 */
	public static int[] getRandomRgb() {
		int[] rgb = new int[3];
		for (int i = 0; i < 3; i++) {
			rgb[i] = random.nextInt(255);
		}
		return rgb;
	}
}
