package com.psk.hms.base.constant.common;

/**
 * 附件常量类
 * @author xiangjz
 *
 */
public class FileAttachConstant {

	/**
	 * 单张图片最大阈值
	 */
	public static final long IMAGE_MAX_SIZE = 1024 * 1024 * 20;
	
	/**
	 * 文件名数组的键名，用于解决前端压缩成blob之后没有文件名，调用fastdfs接口无法获取后缀名的问题
	 */
	public static final String IMAGE_NAME_ARR = "attachFileName";
	
	/**
	 * 文件服务器前缀
	 */
//	public static final String FAST_DFS_PRE = "120.79.33.237:8888/"; //正式环境
//	public static final String FAST_DFS_PRE = "10.73.1.205:8888/"; //测试环境
	public static String FAST_DFS_PRE = "qzfile.cisdi.com.cn/"; //这个常量现在是通过系统初始化时将yml中的配置，重新赋值给它
	
	/**
	 * 团队-人员动态附件
	 */
	public static final String TEAM_DYNAMIC_NEWS = "dynamicNews";
	
	/**
	 * 团队-公告附件
	 */
	public static final String TEAM_PUBLIC_NEWS = "publicNews";
	/**
	 * 团队-党建附件
	 */
	public static final String TEAM_PARTY_NEWS = "partyNews";
	
	/**
	 * 通用-广告图片
	 */
	public static final String BASE_ADS = "baseAds";
	
	/**
	 * 安全巡检附件
	 */
	public static final String SAFETY_INSPECT = "safetyInspect";
	/**
	 * 安全整改附件
	 */
	public static final String SAFETY_INSPECT_RECT = "safetyInspectRect";
	/**
	 * 质量巡检附件
	 */
	public static final String QUALITY_INSPECT = "qualityInspect";
	/**
	 * 质量整改附件
	 */
	public static final String QUALITY_INSPECT_RECT = "qualityInspectRect";
	
	/**
	 * 团队成员头像
	 */
	public static final String TEAM_EMPLOYEE_AVATAR = "teamEmployeeAvatar";
	
	/**
	 * 项目施工日志附件
	 */
	public static final String PROJECT_BUILDER_DIARY = "builderDiary";
	/**
	 * 劳务奖惩附件
	 */
	public static final String LABOR_VOLATION = "laborVolation";
	/**
	 * 安全交底附件
	 */
	public static final String PROJECT_DISCLOSE = "disclose";
	
	
	/**
	 * 设备台账附件
	 */
	public static final String EQUIP_INFO = "equipInfo";
	
	/**
	 * 设备巡检附件
	 */
	public static final String EQUIP_INSPECT = "equipInspect";
	
	/**
	 * 设备人员附件
	 */
	public static final String EQUIP_PERSON = "equipPerson";
}
