package com.psk.hms.base.parameter.validator.field.impl;

public class IdCardFieldValidator<K, V> extends CustomStringFieldValidator<K, V> {
	
	private static final String ID_CARD_PATTERN = "\\d{17}[\\d|xX]|\\d{15}";
	
	public IdCardFieldValidator(K key, boolean keyRequired) {
		super(key, ID_CARD_PATTERN, keyRequired);
	}
	
	public IdCardFieldValidator(K key, boolean keyRequired, String errorMsg) {
		super(key, ID_CARD_PATTERN, keyRequired, errorMsg);
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'无效的身份证号");
		return sb.toString();
	}
}
