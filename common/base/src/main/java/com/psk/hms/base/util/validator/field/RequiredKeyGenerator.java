package com.psk.hms.base.util.validator.field;

import com.psk.hms.base.util.validator.result.MapValidateResult;

public class RequiredKeyGenerator<K, V> implements AbsentMsgGeneratable<K, V> {

	@Override
	public String getDefaultAbsentMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'必填");
		return sb.toString();
	}
	
	@Override
	public MapValidateResult<K> getDefaultAbsentResult(K key) {
		return new MapValidateResult<>(key).failure(this.getDefaultAbsentMsg(key));
	}

}
