package com.psk.hms.base.controller;

import com.psk.hms.base.constant.common.PageBeanConstant;
import com.psk.hms.base.util.string.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

/**
 * 描述: 基础controller类  所有controller继承此类
 * 作者: 李厚余
 * 时间: 2017年3月4日 下午11:36:52
 * 版本: 1.0
 *
 */
public abstract class BaseController {
	
	protected Log log = LogFactory.getLog(BaseController.class);
	
	/**
	 * 获取分页--当前页数
	 * @return
	 */
	public Integer getPageNum(Map<String, Object> params){
		int _pageNum = 1; //默认当前页是第一页
		if(!StringUtil.isBlank1(params.get(PageBeanConstant.PAGE_NUM))){
			try {
				_pageNum = Integer.parseInt(params.get(PageBeanConstant.PAGE_NUM).toString());
			} catch (Exception e) {
				log.error(e);
				_pageNum = 1;
			}
			
			if(_pageNum < 1) _pageNum = 1;
		}
		return _pageNum;
	}
	
	/**
	 * 获取分页--每页显示记录数
	 * @return
	 */
	public Integer getPageSize(Map<String, Object> params){
		int _pageSize = PageBeanConstant.DEFAULT_PAGESIZE;
		if(!StringUtil.isBlank1(params.get(PageBeanConstant.PAGE_SIZE))){
			try {
				_pageSize = Integer.parseInt(params.get(PageBeanConstant.PAGE_SIZE).toString());
			} catch (Exception e) {
				log.error(e);
				_pageSize = PageBeanConstant.DEFAULT_PAGESIZE;
			}
			
		}
		return _pageSize;
	}
}
