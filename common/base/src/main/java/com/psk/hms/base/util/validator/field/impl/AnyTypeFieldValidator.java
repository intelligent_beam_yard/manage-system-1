package com.psk.hms.base.util.validator.field.impl;

import com.psk.hms.base.util.validator.field.AbstractMapEntityValidator;
import com.psk.hms.base.util.validator.result.MapValidateResult;

public class AnyTypeFieldValidator<K, V> extends AbstractMapEntityValidator<K, V> {

	private Class<?>[] types;

	public AnyTypeFieldValidator(K key, Class<?>[] types, boolean keyRequired) {
		super(key, keyRequired);
		this.setTypes(types);
		this.errorMsg = getDefaultErrorMsg(key);
	}

	public AnyTypeFieldValidator(K key, Class<?>[] types, boolean keyRequired, String errorMsg) {
		super(key, keyRequired, errorMsg);
		this.setTypes(types);
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> mapValidateResult = new MapValidateResult<>(key);
		if (value == null) {
			return mapValidateResult.failure(errorMsg);
		}
		for (Class<?> type : types) {
			if (value.getClass().equals(type)) {
				return mapValidateResult.success();
			}
		}
		return mapValidateResult.failure(errorMsg);
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为");
		String spaceOrString = "或";
		for (Class<?> type : types) {
			sb.append(type.getSimpleName()).append(spaceOrString);
		}
		String errorMsg = sb.toString();
		errorMsg = errorMsg.substring(0, errorMsg.length() - spaceOrString.length());
		return errorMsg + "类型";
	}

	public Class<?>[] getTypes() {
		return types;
	}

	public void setTypes(Class<?>[] types) {
		this.types = (types == null ? new Class<?>[] {} : types);
	}

}
