package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础功能常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseOperatorInfoConstant extends Constant {
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - one_many
     */
    public static final String ONE_MANY = "oneMany";
    /**
     *  数据表字段名称 - return_param_keys
     */
    public static final String RETURN_PARAM_KEYS = "returnParamKeys";
    /**
     *  数据表字段名称 - return_url
     */
    public static final String RETURN_URL = "returnUrl";
    /**
     *  数据表字段名称 - row_filter
     */
    public static final String ROW_FILTER = "rowFilter";
    /**
     *  数据表字段名称 - url
     */
    public static final String URL = "url";
    /**
     *  数据表字段名称 - module_id
     */
    public static final String MODULE_ID = "moduleId";
    /**
     *  数据表字段名称 - split_page
     */
    public static final String SPLIT_PAGE = "splitPage";
    /**
     *  数据表字段名称 - form_token
     */
    public static final String FORM_TOKEN = "formToken";
    /**
     *  数据表字段名称 - ip_black
     */
    public static final String IP_BLACK = "ipBlack";
    /**
     *  数据表字段名称 - privilegess
     */
    public static final String PRIVILEGESS = "privilegess";
    /**
     *  数据表字段名称 - csrf
     */
    public static final String CSRF = "csrf";
    /**
     *  数据表字段名称 - referer
     */
    public static final String REFERER = "referer";
    /**
     *  数据表字段名称 - method
     */
    public static final String METHOD = "method";
    /**
     *  数据表字段名称 - sys_log
     */
    public static final String SYS_LOG = "sysLog";
    /**
     *  数据表字段名称 - enctype
     */
    public static final String ENCTYPE = "enctype";
    /**
     *  数据表字段名称 - apply_to
     */
    public static final String APPLY_TO = "applyTo";
    /**
     *  数据表字段名称 - is_exame
     */
    public static final String IS_EXAME = "isExame";
    
    
    
    
    /**
     * 是否拦截 -- 是 - 1 
     */
    public static final String IS_EXAME_YES = "1";
    /**
     * 是否拦截 -- 否 - 0
     */
    public static final String IS_EXAME_NO = "0";
    /**
	 * 是否记录日志 -- 是 - 1 
	 */
	public static final String SYS_LOG_YES = "1";
	/**
	 * 是否记录日志 -- 否 - 0
	 */
	public static final String SYS_LOG_NO = "0";
	/**
	 * method验证 -- 0 - 不限制
	 */
	public static final String METHOD_NOT_LIMIT = "0";
	/**
	 * method验证 -- 1 - GET
	 */
	public static final String METHOD_GET = "1";
	/**
	 * method验证 -- 2 - POST
	 */
	public static final String METHOD_POST = "2";
	/**
	 * enctype验证 -- 0 - 不限制
	 */
	public static final String ENCTYPE_NOT_LIMIT = "0";
	/**
	 * enctype验证 -- 1 - application/x-www-form-urlencoded
	 */
	public static final String ENCTYPE_APPLICATION_OR_XWWWFORMURLENCODED = "1";
	/**
	 * enctype验证 -- 2 - multipart/form-data
	 */
	public static final String ENCTYPE_MULTIPART_OR_FORMDATA = "2";
	/**
	 * enctype验证 -- 3 - text/plain
	 */
	public static final String ENCTYPE_TEXT_OR_PLAIN = "3";
	/**
	 * 是否验证csrf -- 是 - 1 
	 */
	public static final String CSRF_YES = "1";
	/**
	 * 是否验证csrf -- 否 - 0
	 */
	public static final String CSRF_NO = "0";
	/**
	 * 是否验证referer -- 是 - 1 
	 */
	public static final String REFERER_YES = "1";
	/**
	 * 是否验证referer -- 否 - 0
	 */
	public static final String REFERER_NO = "0";
	/**
	 * 是否验证formToken -- 是 - 1 
	 */
	public static final String FORMTOKEN_YES = "1";
	/**
	 * 是否验证formToken -- 否 - 0
	 */
	public static final String FORMTOKEN_NO = "0";
	/**
	 * 是否验证权限 -- 是 - 1 
	 */
	public static final String PRIVILEGES_YES = "1";
	/**
	 * 是否验证权限 -- 否 - 0
	 */
	public static final String PRIVILEGES_NO = "0";

	/**
	 * 应用于 -- 应用 - 1
	 */
	public static final String APPLY_TO_1 = "1";
	/**
	 * 应用于 -- 菜单 - 2
	 */
	public static final String APPLY_TO_2 = "2";
	/**
	 * 应用于 -- api页面 - 3
	 */
	public static final String APPLY_TO_3 = "3";
	/**
	 * 应用于 -- 功能 - 4
	 */
	public static final String APPLY_TO_4 = "4";
	/**
	 * 应用于 -- admin页面 - 5
	 */
	public static final String APPLY_TO_5 = "5";
	
}
