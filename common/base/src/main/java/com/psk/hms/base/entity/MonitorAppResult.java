package com.psk.hms.base.entity;

import java.util.Date;

/**
 * 应用监控接口返回数据
 * 
 * @author xiangjz
 * @date 2018年6月20日
 * @version 1.0.0
 *
 */
public class MonitorAppResult {
	// 应用正常运行
	public static final String ALIVE = "alive";
	// 应用被关闭
	public static final String SHUTDOWN = "shutdown";

	private String msg;
	private Date date;

	public MonitorAppResult alive() {
		return this.create(ALIVE, new Date());
	}

	public MonitorAppResult shutDown() {
		return this.create(SHUTDOWN, new Date());
	}

	public MonitorAppResult create(String msg, Date date) {
		this.msg = msg;
		this.date = date;
		return this;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
