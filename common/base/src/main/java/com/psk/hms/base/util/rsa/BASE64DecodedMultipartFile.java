package com.psk.hms.base.util.rsa;

import com.psk.hms.base.util.BaseUtil;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class BASE64DecodedMultipartFile implements MultipartFile {

    private final byte[] imgContent;
    private final String header;
    private final String fileName; //这个字段不存后缀，header中保留了base64的前缀，其中有后缀

    public BASE64DecodedMultipartFile(byte[] imgContent, String header, String fileName) {
        this.imgContent = imgContent;
        this.header = header.split(";")[0];
        if(!BaseUtil.isEmpty(fileName) && fileName.contains(".")) {
        	fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
        this.fileName = fileName;
    }

    public String getName() {
        return BaseUtil.isEmpty(fileName)
        		? System.currentTimeMillis() + Math.random() + "." + header.split("/")[1]
        		: fileName + "." + header.split("/")[1];
    }

    public String getOriginalFilename() {
    	return BaseUtil.isEmpty(fileName)
        		? System.currentTimeMillis() + (int)Math.random() * 10000 + "." + header.split("/")[1]
        		: fileName + "." + header.split("/")[1];
    }

    public String getContentType() {
        return header.split(":")[1];
    }

    public boolean isEmpty() {
        return imgContent == null || imgContent.length == 0;
    }

    public long getSize() {
        return imgContent.length;
    }

//    @Override
//    public bytegetBytes() throws IOException {
//        return imgContent;
//    }

    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(imgContent);
    }

    public void transferTo(File dest) throws IOException, IllegalStateException {
        new FileOutputStream(dest).write(imgContent);
    }

	public byte[] getBytes() throws IOException {
		// TODO Auto-generated method stub
		return imgContent;
	}
}
