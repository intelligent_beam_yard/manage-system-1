package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队信息常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamInfoConstant extends Constant {
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - typeId
     */
    public static final String TYPE_ID = "typeId";
    /**
     *  数据表字段名称 - isProtect
     */
    public static final String IS_PROTECT = "isProtect";
    
    
    /**
     *  业务逻辑 - 隐私保护 - 1(开启)
     */
    public static final String IS_PROTECT_YES = "1";
    /**
     *  业务逻辑 - 隐私保护 - 0(关闭)
     */
    public static final String IS_PROTECT_NO = "0";

    /**
     *  业务逻辑 - 授权权限 - teamInfoAuth
     */
    public static final String TEAM_INFO_AUTH = "teamInfoAuth";
    /**
     *  业务逻辑 - 参数 - projectName
     */
    public static final String PROJECT_NAME = "projectName";
    /**
     *  业务逻辑 - 参数 - projectId
     */
    public static final String PROJECT_ID = "projectId";
    /**
     *  业务逻辑 - 参数 - userId
     */
    public static final String USER_ID = "userId";
    /**
     *  业务逻辑 - 参数 - phone
     */
    public static final String PHONE = "phone";
    /**
     *  业务逻辑 - 参数 - userName
     */
    public static final String USER_NAME = "userName";
    /**
     *  业务逻辑 - 参数 - isCurrentTeam
     */
    public static final String IS_CURRENT_TEAM = "isCurrentTeam";
    /**
     *  业务逻辑 - 参数 - defaultManageId
     */
    public static final String DEFAULT_MANAGE_ID = "defaultManageId";
    /**
     *  业务逻辑 - 参数 - teamRoleId
     */
    public static final String TEAM_ROLE_ID = "teamRoleId";
    /**
     *  业务逻辑 - 参数 - roleType
     */
    public static final String ROLE_TYPE = "roleType";
    /**
     *  业务逻辑 - 参数 - teamEmployeeId
     */
    public static final String TEAM_EMPLOYEE_ID = "teamEmployeeId";
    /**
     *  业务逻辑 - 参数 - employeeId
     */
    public static final String EMPLOYEE_ID = "employeeId";
    /**
     *  业务逻辑 - 参数 - teamType
     */
    public static final String TEAM_TYPE = "teamType";
    /**
     *  业务逻辑 - 参数 - count
     */
    public static final String COUNT = "count";
    /**
     *  业务逻辑 - 参数 - organizations
     */
    public static final String ORGANIZATIONS = "organizations";
    /**
     *  业务逻辑 - 参数 - jobs
     */
    public static final String JOBS = "jobs";
    /**
     *  业务逻辑 - 参数 - teamTypeId
     */
    public static final String TEAM_TYPE_ID = "teamTypeId";
    /**
     *  业务逻辑 - 参数 - isShow
     */
    public static final String IS_SHOW = "isShow";
    /**
     *  业务逻辑 - 参数 - roles
     */
    public static final String ROLES = "roles";
    /**
     *  业务逻辑 - 参数 - nickName
     */
    public static final String NICK_NAME = "nickName";
    
}
