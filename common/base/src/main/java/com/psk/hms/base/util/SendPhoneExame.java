package com.psk.hms.base.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * Created on 17/6/7.
 * 短信API产品的DEMO程序,工程中包含了一个SmsDemo类，直接通过
 * 执行main函数即可体验短信产品API功能(只需要将AK替换成开通了云通信-短信产品功能的AK即可)
 * 工程依赖了2个jar包(存放在工程的libs目录下)
 * 1:aliyun-java-sdk-core.jar
 * 2:aliyun-java-sdk-dysmsapi.jar
 *
 * 备注:Demo工程编码采用UTF-8
 */
public class SendPhoneExame {

    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final String accessKeyId = "LTAIPVCS9GbGSu1n";
    static final String accessKeySecret = "wqkGNPahIXMlODaHsVzh1UTSC9exzf";

    /**设备低电量提醒：您好，由${name}的管理的${device}只剩下不到${number}%的电量，请联系相关人员及时充电。
     * 发送三个变量的短信
     * @param type 短信类型（模板CODE）
     * @param phone 接收短信手机号
     * @param name 第一个变量
     * @param device 第二个变量
     * @param number 第三个变量
     * @return SendSmsResponse 阿里云返回数据
     * @throws ClientException
     */
    public static  SendSmsResponse sendMsg(String type,String phone,String name,  String device,  String number) throws ClientException {
    	
    	//可自助调整超时时间
    	System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
    	System.setProperty("sun.net.client.defaultReadTimeout", "10000");
    	
    	//初始化acsClient,暂不支持region化
    	IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
    	DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
    	IAcsClient acsClient = new DefaultAcsClient(profile);
    	
    	//组装请求对象-具体描述见控制台-文档部分内容
    	SendSmsRequest request = new SendSmsRequest();
    	//必填:待发送手机号
//        request.setPhoneNumbers("18883527759");
    	request.setPhoneNumbers(phone);
    	//必填:短信签名-可在短信控制台中找到
    	request.setSignName("轻筑");
    	//必填:短信模板-可在短信控制台中找到
    	request.setTemplateCode(type);
//        request.setTemplateCode("SMS_76230017");
    	//验证码${code}，序号${serialNumber}，您正在登录园服通，若非本人操作，请勿泄露。
    	//可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
    	request.setTemplateParam("{\"name\":\""+name+"\",\"device\":\""+device+"\",\"number\":\""+number+"\"}");
//        request.setTemplateParam("{\"serialNumber\":\""+index+"\", \"code\":\""+exameNumber+"\"}");
    	//可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
//        request.setOutId("returnMsg");
    	
    	//hint 此处可能会抛出异常，注意catch
    	SendSmsResponse sendSmsResponse = null;
//        try{
    	sendSmsResponse = acsClient.getAcsResponse(request);
//        }catch (Exception e) {
//			// TODO: handle exception
//		}
    	System.out.println(sendSmsResponse.getMessage());
    	return sendSmsResponse;
    }
    /**
     * 发送两个变量的短信
     * @param type 短信类型（模板CODE）
     * @param phone 接收短信手机号
     * @param index 第一个变量
     * @param exameNumber 第二个变量
     * @return SendSmsResponse 阿里云返回数据
     * @throws ClientException
     */
    public static  SendSmsResponse sendMsg(String type,String phone,String index,  String exameNumber) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
//        request.setPhoneNumbers("18883527759");
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("轻筑");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(type);
//        request.setTemplateCode("SMS_76230017");
        //验证码${code}，序号${serialNumber}，您正在登录园服通，若非本人操作，请勿泄露。
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam("{\"name\":\""+index+"\",\"url\":\""+exameNumber+"\"}");
//        request.setTemplateParam("{\"serialNumber\":\""+index+"\", \"code\":\""+exameNumber+"\"}");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
//        request.setOutId("returnMsg");

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = null;
//        try{
        sendSmsResponse = acsClient.getAcsResponse(request);
//        }catch (Exception e) {
//			// TODO: handle exception
//		}
        System.out.println(sendSmsResponse.getMessage());
        return sendSmsResponse;
    }
    /**
     * 发送一个变量的短信
     * @param type 短信类型（模板CODE）
     * @param phone 接收短信手机号
     * @param exameNumber 第一个变量
     * @return SendSmsResponse 阿里云返回数据
     * @throws ClientException
     */
    public static  SendSmsResponse sendMsg(String type, String phone, String exameNumber) throws ClientException {
    	
    	//可自助调整超时时间
    	System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
    	System.setProperty("sun.net.client.defaultReadTimeout", "10000");
    	
    	//初始化acsClient,暂不支持region化
    	IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
    	DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
    	IAcsClient acsClient = new DefaultAcsClient(profile);
    	
    	//组装请求对象-具体描述见控制台-文档部分内容
    	SendSmsRequest request = new SendSmsRequest();
    	//必填:待发送手机号
//        request.setPhoneNumbers("18883527759");
    	request.setPhoneNumbers(phone);
    	//必填:短信签名-可在短信控制台中找到
    	request.setSignName("轻筑");
    	//必填:短信模板-可在短信控制台中找到
    	request.setTemplateCode(type);
//    	request.setTemplateCode("SMS_76020199");
    	//验证码${code}，序号${serialNumber}，您正在登录园服通，若非本人操作，请勿泄露。
    	//可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
    	if("SMS_134195114".equals(type)) {
    		request.setTemplateParam("{\"code\":\""+exameNumber+"\"}");
    	}else if("SMS_134327345".equals(type)) {
    		request.setTemplateParam("{\"inviteCode\":\""+exameNumber+"\"}");
    	}else if("SMS_134322287".equals(type)) {
    		request.setTemplateParam("{\"registerCode\":\""+exameNumber+"\"}");
    	}else if("SMS_134317262".equals(type)) {
    		request.setTemplateParam("{\"pwdModifyCode\":\""+exameNumber+"\"}");
    	}
//        request.setTemplateParam("{\"serialNumber\":\""+index+"\", \"code\":\""+exameNumber+"\"}");
    	//可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
//        request.setOutId("returnMsg");
    	
    	//hint 此处可能会抛出异常，注意catch
    	SendSmsResponse sendSmsResponse = null;
//        try{
    	sendSmsResponse = acsClient.getAcsResponse(request);
//        }catch (Exception e) {
//			// TODO: handle exception
//		}
    	System.out.println(sendSmsResponse.getMessage());
    	return sendSmsResponse;
    }
    
    public static void main(String[] args) {
    	try {
//			sendMsg("SMS_134195114", "18883527759", "1234");
			sendMsg("SMS_134310539", "18883527759", "张三","test");
		} catch (ClientException e) {
			e.printStackTrace();
		}
	}
}
