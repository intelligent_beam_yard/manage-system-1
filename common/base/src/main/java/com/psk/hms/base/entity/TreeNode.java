package com.psk.hms.base.entity;

import java.io.Serializable;

/**
 * 团队组织机构树
 * 
 * @author xiangjz
 * @date 2018年6月28日
 * @version 1.0.0
 *
 */
public class TreeNode implements Serializable {
    private static final long serialVersionUID = 1L;

    // 节点ID
    private String id;
    // 节点名称
    private String name;

    private String parentId;

    public TreeNode() {
    }

    public TreeNode(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
