package com.psk.hms.base.util.validator.field.impl;

import com.psk.hms.base.util.validator.field.AbstractMapEntityValidator;
import com.psk.hms.base.util.validator.result.MapValidateResult;

public class TypeFieldValidator<K, V> extends AbstractMapEntityValidator<K, V> {

	private Class<?> type;
	
	public TypeFieldValidator(K key, Class<?> type, boolean keyRequired) {
		super(key, keyRequired);
		this.type = type;
		this.errorMsg = getDefaultErrorMsg(key);
	}
	
	public TypeFieldValidator(K key, Class<?> type, boolean keyRequired, String errorMsg) {
		super(key, keyRequired, errorMsg);
		this.type = type;
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> mapValidateResult = new MapValidateResult<>(key);
		if(value == null) {
			return mapValidateResult.failure(errorMsg);
		}
		if(!value.getClass().equals(type)) {
			return mapValidateResult.failure(errorMsg);
		}
		return mapValidateResult.success();
	}
	
	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为").append(type.getSimpleName()).append("类型");
		return sb.toString();
	}

	public Class<?> getType() {
		return type;
	}

	public void setType(Class<?> type) {
		this.type = type;
	}

}
