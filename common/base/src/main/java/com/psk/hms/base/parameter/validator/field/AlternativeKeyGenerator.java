package com.psk.hms.base.parameter.validator.field;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

public class AlternativeKeyGenerator<K, V> implements AbsentMsgGeneratable<K, V> {

	@Override
	public String getDefaultAbsentMsg(K key) {
		return "Validated field";
	}
	
	@Override
	public MapValidateResult<K> getDefaultAbsentResult(K key) {
		return new MapValidateResult<>(key).success();
	}

}
