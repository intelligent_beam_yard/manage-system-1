package com.psk.hms.base.util.validator.field.impl;

public abstract class CompareFieldValidator<K, V, T extends Comparable<T>> extends TypeFieldValidator<K, V> {

	public CompareFieldValidator(K key, Class<?> type, boolean keyRequired) {
		super(key, type, keyRequired);
		this.errorMsg = getDefaultErrorMsg(key);
	}

	public CompareFieldValidator(K key, Class<?> type, boolean keyRequired, String errorMsg) {
		super(key, type, keyRequired, errorMsg);
	}

	protected boolean isEqualBorder(T border, T value) {
		return border == null ? false : border.compareTo(value) == 0;
	}

	protected boolean isAboveBorder(T border, T value) {
		return border == null ? false : border.compareTo(value) < 0;
	}

	protected boolean isUnderBorder(T border, T value) {
		return border == null ? false : border.compareTo(value) > 0;
	}

}
