package com.psk.hms.base.constant.common;
/**
 * 描述: 分页常量
 * 作者: xiangjz
 * 时间: 2017年6月29日
 * 版本: 1.0
 *
 */
public class PageBeanConstant {

	/**
	 * 分页参数初始化值：默认显示第几页
	 */
	public static final int DEFAULT_PAGENUMBER = 1;
	
	/**
	 * 分页参数初始化值：默认每页显示几多
	 */
	public static final int DEFAULT_PAGESIZE = 20;
	
	/**
	 * 分页参数初始化值：最多显示多少条数据
	 */
	public static final int DEFAULT_MAXSIZE = 200;

	/**
	 * 分页参数初始化值：导出，最多显示多少条数据
	 */
	public static final int DEFAULT_EXPORT_MAXSIZE = 10 * 10000;
	
	
	/**
     *  业务字段名称 - pageNum
     */
    public static final String PAGE_NUM = "pageNum";
    /**
     *  业务字段名称 - pageSize
     */
    public static final String PAGE_SIZE = "pageSize";
	/**
     *  业务字段名称 - orderColunm
     */
    public static final String ORDER_COLUNM = "orderColunm";
    /**
     *  业务字段名称 - orderMode
     */
    public static final String ORDER_MODE = "orderMode";
    /**
     *  业务字段名称 - asc
     */
    public static final String ASC = "asc";
    /**
     *  业务字段名称 - asc
     */
    public static final String DESC = "desc";
}
