package com.psk.hms.base.util.validator.field.impl;

public class MailFieldValidator<K, V> extends CustomStringFieldValidator<K, V> {
	
	private static final String MAIL_PATTERN = "\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}";
	
	public MailFieldValidator(K key, boolean keyRequired) {
		super(key, MAIL_PATTERN, keyRequired);
	}
	
	public MailFieldValidator(K key, boolean keyRequired, String errorMsg) {
		super(key, MAIL_PATTERN, keyRequired, errorMsg);
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'无效的邮箱格式");
		return sb.toString();
	}
}
