package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队公司常量类
 * @author liang
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamCompanyConstant extends Constant {    
    /**
     *  数据表字段名称 - projectId - 项目id
     */
    public static final String PROJECT_ID = "projectId";
    /**
     *  数据表字段名称 - name - 公司名称
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - teamId - 团队id
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - companyId - 公司基础信息id
     */
    public static final String COMPANY_ID = "companyId";
}
