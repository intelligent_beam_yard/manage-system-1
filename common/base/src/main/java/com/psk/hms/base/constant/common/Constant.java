package com.psk.hms.base.constant.common;

/**
 * 通用常量类
 * @author xiangjz
 *
 */
public class Constant {
	/**
	 * 父ids的分隔符
	 */
	public static final String PARENT_IDS_SEPARATOR=",";
    
    /**
     *  数据表字段名称 - id
     */
    public static final String ID = "id";
    /**
     *  数据表字段名称 - version
     */
    public static final String VERSION = "version";
    /**
     *  数据表字段名称 - createTime
     */
    public static final String CREATE_TIME = "createTime";
    /**
     *  数据表字段名称 - createUserId
     */
    public static final String CREATE_USER_ID = "createUserId";
    /**
     *  数据表字段名称 - editTime
     */
    public static final String EDIT_TIME = "editTime";
    /**
     *  数据表字段名称 - editUserId
     */
    public static final String EDIT_USER_ID = "editUserId";
    /**
     *  数据表字段名称 - isDelete
     */
    public static final String IS_DELETE = "isDelete";
    /**
     *  数据表字段名称 - remarks
     */
    public static final String REMARKS = "remarks";
    /**
     *  数据表字段名称 - isProtect
     */
    public static final String ISPROTECT = "isProtect";
    
    /**
     *  业务逻辑字段 - currentUserId
     */
    public static final String CURRENT_USER_ID = "currentUserId";
    /**
     *  业务逻辑字段名称 - ids
     */
    public static final String IDS = "ids";
    /**
     *  业务逻辑字段名称 - pageBean
     */
    public static final String PAGE_BEAN = "pageBean";
    /**
     *  业务逻辑字段名称 - 权限 - add
     */
    public static final String ADD = "add";
    /**
     *  业务逻辑字段名称 -  权限 - edit
     */
    public static final String EDIT = "edit";
    /**
     *  业务逻辑字段名称 -  权限 - delete
     */
    public static final String DELETE = "delete";
    /**
     *  业务逻辑字段名称 -  权限 - save
     */
    public static final String SAVE = "save";
    /**
     *  业务逻辑字段名称 -  权限 - update
     */
    public static final String UPDATE = "update";
    
    
    /**
     *  业务逻辑字段名称 -  参数 - params
     */
    public static final String PARAMS = "params";
    /**
     *  业务逻辑字段名称 -  参数 - exportTemplate
     */
    public static final String EXPORT_TEMPLATE = "exportTemplate";
    /**
     *  业务逻辑字段名称 -  参数 - inport
     */
    public static final String INPORT = "inport";
    /**
     *  业务逻辑字段名称 -  参数 - teamId
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  业务逻辑字段名称 - 参数 - 表单id
     */
    public static final String FORM_PARAMS = "formParams";
    /**
	 *  业务逻辑字段名称 - 参数 - pageNum
	 */
	public static final String PAGE_NUM = "pageNum";
	/**
	 *  业务逻辑字段名称 - 参数 - pageSize
	 */
	public static final String PAGE_SIZE = "pageSize";


    
    
    
    
	/**
	 * 是否删除  否-0
	 */
	public static final String IS_DELETE_NO = "0";
	/**
	 * 是否删除  是-1
	 */
	public static final String IS_DELETE_YES = "1";
	
	/**
	 * 是否父节点  是-1
	 */
	public static final String IS_PARENT_YES = "1";
	
	/**
	 * 是否父节点  否-0
	 */
	public static final String IS_PARENT_NO = "0";
	
	/**
	 * 是否需要隐私保护 是-1
	 * 
	 */
	public static final String IS_PROTECT = "1";
	
	/**
	 * 是否需要隐私保护 否-0
	 * 
	 */
	public static final String IS_PROTECT_NO = "0";
	
	/**
	 * 是否需要使用CRON表达式 是-1
	 * 
	 */
	public static final String USE_CRON = "1";
	
	/**
	 * 是否需要使用CRON表达式 否-0
	 * 
	 */
	public static final String USE_CRON_NO = "0";
	
	/**
	 * 是否需要立即启动 是-1
	 * 
	 */
	public static final String RUN_ON_READY = "1";
	
	/**
	 * 是否需要立即启动 否-0
	 * 
	 */
	public static final String RUN_ON_READY_NO = "0";
	
	/**
	 * 是否需要在上次执行完后才开始计时 是-1
	 * 
	 */
	public static final String FIXED = "1";
	
	/**
	 * 是否需要在上次执行完后才开始计时 否-0
	 * 
	 */
	public static final String FIXED_NO = "0";
	
	/**
	 * 字段version 默认值-0
	 * 
	 */
	public static final String VERSION_0 = "0";
	
	/**
	 * 字典根节点id
	 */
	public static final String DICTIONARY_ID="ffffffffffffffffffffffffffffffff";
}
