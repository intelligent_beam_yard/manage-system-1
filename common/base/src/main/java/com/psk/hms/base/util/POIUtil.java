package com.psk.hms.base.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 描述: POI工具类
 * 作者: xiangjz
 * 时间: 2018年5月23日
 * 版本: 1.0
 *
 */
public class POIUtil {

	private static final Log log = LogFactory.getLog(POIUtil.class);
	
	private static Workbook wb;
	private static Sheet sheet;
	private static Row row;

	/**
	 * excel导出
	 * 
	 * @param templatePath 模板路径
	 */
	@SuppressWarnings("unused")
	public static String export(String templatePath) {
		// 导出文件存放目录
		String filePath = DirUtil.getWebRootPath() + File.separator + "exportFile";
		File fileDir = new File(filePath);
		if (!fileDir.exists()) {
			fileDir.mkdir();
		}

		// 导出文件路径
		String path = filePath + File.separator + DateUtil.format(new Date(), "yyyyMMddHHmmssSSS") + ".xlsx";

		XSSFWorkbook wb = null;
		SXSSFWorkbook swb = null;
		FileOutputStream os = null;
		try {
			// 1.载入模板
			wb = new XSSFWorkbook(new File(templatePath)); // 初始化HSSFWorkbook对象
			wb.setSheetName(0, "用户信息导出");
			Sheet sheet = wb.getSheetAt(0); // wb.createSheet("监控点资源状态");

			// 2.读取模板处理好样式

			// 3.转换成大数据读取模式
			swb = new SXSSFWorkbook(wb, 1000); // 用于大文件导出
			sheet = swb.getSheetAt(0);

			// 4.大批量写入数据

			// 5.保存到本地文件夹
			os = new FileOutputStream(new File(path));
			swb.write(os);

			return path;
		} catch (IOException e) {
			if(log.isErrorEnabled()) log.error("导出失败：" + e.getMessage());
			e.printStackTrace();
			return null;
		} catch (InvalidFormatException e) {
			if(log.isErrorEnabled()) log.error("导出失败：" + e.getMessage());
			e.printStackTrace();
			return null;
		} finally {
			close(os, swb, wb);
		}
	}

	/**
	 * 资源关闭
	 * 
	 * @param os
	 * @param wb
	 * @param swb
	 */
	public static void close(FileOutputStream os, SXSSFWorkbook swb, XSSFWorkbook wb) {
		if (null != os) {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (null != swb) {
			try {
				swb.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (null != wb) {
			try {
				wb.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 将文档写入文件
	 * 
	 * @param wb
	 * @param name
	 */
	public static String writeExcel(SXSSFWorkbook wb, String name) {
		String filePath = DirUtil.getWebRootPath() + File.separator + "WEB-INF" + File.separator + "files" + File.separator + "export";
		File f = new File(filePath);
		if (!f.exists()) {
			f.mkdirs();
		}
		
		String path = filePath + File.separator + name + DateUtil.format(new Date(), "_yyyy_MM_dd_HH_mm_ss_SSS") + ".xlsx";
		
		FileOutputStream os = null;
		try {
			File file = new File(path);
			os = new FileOutputStream(file);
			wb.write(os);
			os.close();
		} catch (FileNotFoundException e) {
			log.error("文件路径找不到.", e);
		} catch (IOException e) {
			log.error("文件IO异常.", e);
		} finally {
			if (null != os) {
				try {
					os.close();
				} catch (IOException e) {
					log.error("文件IO异常.", e);
				}
			}
		}
		
		return path;
	}

	/**
	 * 导出标题样式
	 */
	public static void setTitleFont(SXSSFWorkbook wb, Iterator<Cell> it) {
		XSSFColor color = new XSSFColor(new java.awt.Color(219, 229, 241));
		Font font = createFont(wb, Font.BOLDWEIGHT_BOLD, Font.COLOR_NORMAL, (short) 11);
		CellStyle style = createCellStyle(wb, color, CellStyle.ALIGN_CENTER, font);
		while (it.hasNext()) {
			it.next().setCellStyle(style);
		}
	}

	/**
	 * 导出表头样式
	 */
	public static void setHeadStyle(SXSSFWorkbook wb, Iterator<Cell> it) {
		XSSFColor color = new XSSFColor(new java.awt.Color(79, 129, 189));
		Font font = createFont(wb, Font.BOLDWEIGHT_NORMAL, HSSFColor.WHITE.index, (short) 11);
		CellStyle style = createCellStyle(wb, color, CellStyle.ALIGN_CENTER, font);
		while (it.hasNext()) {
			it.next().setCellStyle(style);
		}
	}

	/**
	 * 导出表数据样式 默认居中
	 */
	public static void setContentStyle(SXSSFWorkbook wb, Iterator<Cell> it) {
		XSSFColor color = new XSSFColor(new java.awt.Color(255, 255, 255));
		Font font = createFont(wb, Font.BOLDWEIGHT_NORMAL, Font.COLOR_NORMAL, (short) 10);
		CellStyle style = createBorderCellStyle(wb, HSSFColor.WHITE.index, color, CellStyle.ALIGN_CENTER, font);
		while (it.hasNext()) {
			it.next().setCellStyle(style);
		}
	}

	/**
	 * 导出表数据样式 左对齐
	 */
	public static void setContentLeftStyle(SXSSFWorkbook wb, Cell cell) {
		XSSFColor color = new XSSFColor(new java.awt.Color(255, 255, 255));
		Font font = createFont(wb, Font.BOLDWEIGHT_NORMAL, Font.COLOR_NORMAL, (short) 10);
		CellStyle style = createBorderCellStyle(wb, HSSFColor.WHITE.index, color, CellStyle.ALIGN_LEFT, font);
		style.setWrapText(true); // 实现换行
		cell.setCellStyle(style);
	}

	/**
	 * 设置合并单元格边框
	 */
	public static void setBorderStyle(Workbook wb, Sheet sheet, CellRangeAddress cra) {
		int border = HSSFColor.WHITE.index;
		RegionUtil.setBorderBottom(border, cra, sheet, wb);
		RegionUtil.setBorderLeft(border, cra, sheet, wb);
		RegionUtil.setBorderRight(border, cra, sheet, wb);
		RegionUtil.setBorderTop(border, cra, sheet, wb);
	}

	/**
	 * 功能：创建HSSFSheet工作簿
	 *
	 * @param wb  SXSSFWorkbook
	 * @param sheetName String
	 * @return HSSFSheet
	 */
	public static Sheet createSheet(SXSSFWorkbook wb, String sheetName) {
		Sheet sheet = wb.createSheet(sheetName);
		sheet.setDefaultColumnWidth(30);
		sheet.setColumnWidth(0, 7 * 256);
		sheet.setDefaultRowHeight((short) 400);
		sheet.setDisplayGridlines(true);
		return sheet;
	}

	/**
	 * 功能：创建CellStyle样式
	 *
	 * @param wb SXSSFWorkbook
	 * @param color  背景色
	 * @param align 前置色
	 * @param font  字体
	 * @return CellStyle
	 */
	public static CellStyle createCellStyle(SXSSFWorkbook wb, XSSFColor color, short align, Font font) {
		XSSFCellStyle cs = (XSSFCellStyle) wb.createCellStyle();
		cs.setAlignment(align); // 水平居中
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cs.setFillForegroundColor(color);
		cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cs.setFont(font);
		return cs;
	}

	/**
	 * 功能：创建带边框的CellStyle样式
	 *
	 * @param wb SXSSFWorkbook
	 * @param backgroundColor 背景色
	 * @param foregroundColor 前置色
	 * @param font 字体
	 * @return CellStyle
	 */
	public static CellStyle createBorderCellStyle(SXSSFWorkbook wb, short backgroundColor, XSSFColor foregroundColor,
			short halign, Font font) {
		XSSFCellStyle cs = (XSSFCellStyle) wb.createCellStyle();
		cs.setAlignment(halign);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cs.setFillBackgroundColor(backgroundColor);
		cs.setFillForegroundColor(foregroundColor);
		cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cs.setFont(font);
		cs.setBorderLeft(CellStyle.BORDER_DASHED);
		cs.setLeftBorderColor(HSSFColor.GREY_80_PERCENT.index);
		cs.setBorderRight(CellStyle.BORDER_DASHED);
		cs.setRightBorderColor(HSSFColor.GREY_80_PERCENT.index);
		cs.setBorderTop(CellStyle.BORDER_DASHED);
		cs.setTopBorderColor(HSSFColor.GREY_80_PERCENT.index);
		cs.setBorderBottom(CellStyle.BORDER_DASHED);
		cs.setBottomBorderColor(HSSFColor.GREY_80_PERCENT.index);
		return cs;
	}

	/**
	 * 功能：创建字体
	 *
	 * @param wb HSSFWorkbook
	 * @param boldweight short
	 * @param color short
	 * @return Font
	 */
	public static Font createFont(SXSSFWorkbook wb, short boldweight, short color, short size) {
		Font font = wb.createFont();
		font.setBoldweight(boldweight);
		font.setColor(color);
		font.setFontHeightInPoints(size);
		return font;
	}

	/**
	 * 功能：合并单元格
	 *
	 * @param sheet  Sheet
	 * @param firstRow  int
	 * @param lastRow  int
	 * @param firstColumn int
	 * @param lastColumn int
	 * @return int 合并区域号码
	 */
	public static int mergeCell(Sheet sheet, int firstRow, int lastRow, int firstColumn, int lastColumn) {
		return sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstColumn, lastColumn));
	}

	/**
	 * 功能：创建Row
	 *
	 * @param sheet Sheet
	 * @param rowNum int
	 * @param height int
	 * @return HSSFRow
	 */
	public static Row createRow(Sheet sheet, int rowNum, int height) {
		Row row = sheet.createRow(rowNum);
		row.setHeight((short) height);
		return row;
	}

//	public static String nullVal(Object val){
//		if(val == null){
//			return "";
//		}
//		return val;
//	}
	
	/**
	 * 读取Excel数据内容
	 * 
	 * @param InputStream
	 * @return Map 包含单元格数据内容的Map对象
	 */
	public static List<Map<String, Object>> readExcelContent(InputStream is) {
		List<Map<String, Object>> list = null;
		try {
			wb = new XSSFWorkbook(is);

			sheet = wb.getSheetAt(0);
			// 得到总行数
			int rowNum = sheet.getLastRowNum();
			// 总列数
			row = sheet.getRow(1);
			int colNum = row.getLastCellNum();
			// 避免list多次扩容
			// list = new LinkedList<Map<String,Object>>();
			list = new ArrayList<Map<String, Object>>((int) (rowNum / 0.75));
			// 正文内容应该从第二行开始,第一行为表头的标题
			for (int i = 1; i <= rowNum; i++) {
				// 整行为空，continue
				row = sheet.getRow(i);
				if (row == null)
					continue;
				// 避免HashMap多次扩容
				Map<String, Object> rowM = new HashMap<String, Object>();
				for (int j = 0; j < colNum; j++) {
					rowM.put("field" + j, getCellFormatValue(row.getCell(j))
							.trim());
				}
				list.add(rowM);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("导入Excel文件失败");
		}
		return list;
	}
	
	
	/**
	 * 读取Excel数据内容
	 * 标题匹配
	 * @param InputStream
	 * @return Map 包含单元格数据内容的Map对象
	 */
	public static List<Map<String, Object>> readExcelContent1(InputStream is) {
		List<Map<String, Object>> list = null;
		try {
			Workbook wb = WorkbookFactory.create(is);
			//wb = new XSSFWorkbook(is);

			sheet = wb.getSheetAt(0);
			// 得到总行数
			int rowNum = sheet.getLastRowNum();
			// 总列数
			row = sheet.getRow(1);
			Row titlList = sheet.getRow(0);
			int colNum = row.getLastCellNum();
			// 避免list多次扩容
			// list = new LinkedList<Map<String,Object>>();
			list = new ArrayList<Map<String, Object>>((int) (rowNum / 0.75));
			// 正文内容应该从第二行开始,第一行为表头的标题
			for (int i = 1; i <= rowNum; i++) {
				// 整行为空，continue
				row = sheet.getRow(i);
				if (row == null)
					continue;
				// 避免HashMap多次扩容
				Map<String, Object> rowM = new HashMap<String, Object>();
				for (int j = 0; j < colNum; j++) {
					rowM.put(getCellFormatValue(titlList.getCell(j)), getCellFormatValue(row.getCell(j))
							.trim());
				}
				list.add(rowM);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("导入Excel文件失败");
		}
		return list;
	}
	
	/**
	 * 根据HSSFCell类型设置数据
	 * 
	 * @param cell
	 * @return
	 */
	private static String getCellFormatValue(Cell cell) {
		String cellvalue = "";
		if (cell != null) {
			// 判断当前Cell的Type
			switch (cell.getCellType()) {
			// 如果当前Cell的Type为NUMERIC
			case HSSFCell.CELL_TYPE_NUMERIC:
			case HSSFCell.CELL_TYPE_FORMULA: {
				// 判断当前的cell是否为Date
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					// 如果是Date类型则，转化为Data格式

					// 方法1：这样子的data格式是带时分秒的：2011-10-12 0:00:00
					// cellvalue = cell.getDateCellValue().toLocaleString();

					// 方法2：这样子的data格式是不带带时分秒的：2011-10-12
					Date date = cell.getDateCellValue();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					cellvalue = sdf.format(date);

				}
				// 如果是纯数字
				else {
					// 取得当前Cell的数值
					cellvalue = String.valueOf(cell.getNumericCellValue());
					if (cellvalue.toUpperCase().indexOf("E") > -1
							|| cellvalue.endsWith(".0") || cellvalue.endsWith(".00"))
					{
						BigDecimal bd = new BigDecimal(cell.getNumericCellValue());
						cellvalue = bd.toPlainString();
					}
				}
				break;
			}
				// 如果当前Cell的Type为STRIN
			case HSSFCell.CELL_TYPE_STRING:
				// 取得当前的Cell字符串
				cellvalue = cell.getRichStringCellValue().getString();
				break;
			// 默认的Cell值
			default:
				cellvalue = " ";
			}
		} else {
			cellvalue = "";
		}
		return cellvalue;

	}
}
