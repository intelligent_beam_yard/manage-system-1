package com.psk.hms.base.parameter.validator.field.impl;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

import java.util.regex.Pattern;

public class CustomStringFieldValidator<K, V> extends TypeFieldValidator<K, V> {

	private String pattern;

	public CustomStringFieldValidator(K key, String pattern, boolean keyRequired) {
		super(key, String.class, keyRequired);
		this.pattern = pattern;
	}

	public CustomStringFieldValidator(K key, String pattern, boolean keyRequired, String errorMsg) {
		super(key, String.class, keyRequired, errorMsg);
		this.pattern = pattern;
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> result = super.checkEntity(key, value);
		if (!result.isPass()) {
			return result;
		}
		String stringValue = (String) value;
		if (!Pattern.matches(pattern, stringValue)) {
			result.failure(errorMsg);
		}
		return result;
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'不匹配的字符串");
		return sb.toString();
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
