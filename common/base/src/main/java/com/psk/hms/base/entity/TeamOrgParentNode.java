package com.psk.hms.base.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 团队组织机构树
 * 
 * @author xiangjz
 * @date 2018年5月14日
 * @version 1.0.0
 *
 */
public class TeamOrgParentNode extends TeamOrgNode {
	private static final long serialVersionUID = 1L;

	private List<TeamOrgNode> children;
	
	public TeamOrgParentNode() {
		super();
		this.children = new ArrayList<>();
	}
	
	public TeamOrgParentNode(String id, String name) {
		super(id, name);
		this.children = new ArrayList<>();
	}

	public List<TeamOrgNode> getChildren() {
		return children;
	}

	public void setChildren(List<TeamOrgNode> children) {
		this.children = children;
	}
	
	public boolean addNode(TeamOrgNode node) {
		return this.children.add(node);
	}
	
	public boolean hasNode(TeamOrgNode node) {
		return this.children.contains(node);
	}

}
