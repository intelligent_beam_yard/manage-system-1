package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 权限包基础数据常量类
 * @version 1.0.0
 *
 */
public class BaseOperatorPackageInfoConstant extends Constant {
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
   
}
