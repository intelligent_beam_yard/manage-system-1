package com.psk.hms.base.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 描述: 日期工具类 作者: xiangjz 时间: 2018年04月24日 版本: 1.0
 *
 */
public class DateUtil {

	private static final Log log = LogFactory.getLog(DateUtil.class);

	public static final String pattern_ymd = "yyyy-MM-dd"; // pattern_ymd

	public static final String pattern_ymd_hms = "yyyy-MM-dd HH:mm:ss"; // pattern_ymd
																		// timeMillisecond

	public static final String pattern_ymd_hms_s = "yyyy-MM-dd HH:mm:ss:SSS"; // pattern_ymd
																				// timeMillisecond

	public static final String pattern_ymd_hm = "yyyy-MM-dd HH:mm";

	/**
	 * 数据库只认java.sql.*
	 * 
	 * @return
	 */
	public static Timestamp getSqlTimestamp() {
		return getSqlTimestamp(new Date().getTime());
	}

	/**
	 * 获取开始时间
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static Date startDateByHour(Date start, int end) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		calendar.set(Calendar.MINUTE, end);
		Date date = calendar.getTime();
		return date;
	}

	/**
	 * 获取结束时间
	 * 
	 * @param end
	 * @param end
	 * @return
	 */
	public static Date endDateByHour(Date end) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(end);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		Date date = calendar.getTime();
		return date;
	}

	/**
	 * 数据库只认java.sql.*
	 * 
	 * @param time
	 * @return
	 */
	public static Timestamp getSqlTimestamp(long time) {
		return new java.sql.Timestamp(time);
	}

	/**
	 * 格式化
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date, String pattern) {
		DateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	/**
	 * 格式化
	 * 
	 * @param date
	 * @param pattern
	 * @param timeZone
	 * @return
	 */
	public static String format(Date date, String pattern, TimeZone timeZone) {
		DateFormat format = new SimpleDateFormat(pattern);
		format.setTimeZone(timeZone);
		return format.format(date);
	}

	/**
	 * 格式化
	 * 
	 * @param date
	 * @param parsePattern
	 * @param returnPattern
	 * @return
	 */
	public static String format(String date, String parsePattern, String returnPattern) {
		return format(parse(date, parsePattern), returnPattern);
	}

	/**
	 * 格式化
	 * 
	 * @param date
	 * @param parsePattern
	 * @param returnPattern
	 * @param timeZone
	 * @return
	 */
	public static String format(String date, String parsePattern, String returnPattern, TimeZone timeZone) {
		return format(parse(date, parsePattern), returnPattern, timeZone);
	}

	/**
	 * 解析
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static Date parse(String date, String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		try {
			return format.parse(date);
		} catch (ParseException e) {
			if (log.isErrorEnabled())
				log.error("DateUtils.parse异常：date值" + date + "，pattern值" + pattern);
			return null;
		}
	}

	/**
	 * 数据库只认java.sql.*
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getSqlTimestamp(Date date) {
		if (null == date) {
			return getSqlTimestamp();
		}
		return getSqlTimestamp(date.getTime());
	}

	/**
	 * String转timestamp
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp string2Timestamp(String date) {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		ts = Timestamp.valueOf(date);
		return ts;
	}

	/**
	 * 获取当前时间的时间戳
	 * 
	 * @return
	 */
	public static long getDateByTime() {
		return new Date().getTime();
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static Date getDate() {
		return new Date();
	}

	/**
	 * 获取开始时间
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static Date startDateByDay(Date start, int end) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		calendar.add(Calendar.DATE, end);// 明天1，昨天-1
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date date = calendar.getTime();
		return date;
	}

	/**
	 * 返回两个日期之间隔了多少小时
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDateHourSpace(Date start, Date end) {
		int hour = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 1000));
		return hour;
	}

	/**
	 * 返回两个日期之间隔了多少分钟
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDateMinuteSpace(Date start, Date end) {
		int hour = (int) ((end.getTime() - start.getTime()) / (60 * 1000));
		return hour;
	}

	/**
	 * 获取结束时间
	 * 
	 * @param start
	 * @return
	 */
	public static Date endDateByDay(Date start) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		Date date = calendar.getTime();
		return date;
	}

	/**
	 * 返回两个日期之间隔了多少天
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getDateDaySpace(Date start, Date end) {
		int day = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 24 * 1000));
		return day;
	}

	/**
	 * 获取当前月的第一天 格式eg: 2018-02-01
	 */
	public static String getCurrentMonthFirstDay() {
		SimpleDateFormat format = new SimpleDateFormat(pattern_ymd);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return format.format(cal.getTime());
	}

	/**
	 * 根据时间得出星期几
	 */
	public static String dateToWeek(Date datet) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		Calendar cal = Calendar.getInstance(); // 获得一个日历
		cal.setTime(datet);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1; // 指示一个星期中的某天。
		if (w < 0)
			w = 0;
		return weekDays[w];
	}

	/**
	 * 获取当前月的最后一天 格式eg: 2018-02-28
	 */
	public static String getCurrentMonthLastDay() {
		SimpleDateFormat format = new SimpleDateFormat(pattern_ymd);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return format.format(cal.getTime());
	}

	/*****
	 * 获取两个时间随机时间
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static Timestamp getRamdomTimestamp(long begin, long end) {
		long rtn = begin + (long) (Math.random() * (end - begin));
		if (rtn == begin || rtn == end) {
			return getRamdomTimestamp(begin, end);
		}
		return new Timestamp(rtn);
	}

	/**
	 * 获取过去第几天的日期
	 * 
	 * @param past
	 * @return
	 */
	public static String getPastDate(int past) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
		Date today = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String result = format.format(today);
		return result;
	}

	/**
	 * 获取未来 第 after 天的日期
	 * 
	 * @param after
	 * @return
	 */
	public static String getFetureDate(int after) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + after);
		Date today = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String result = format.format(today);
		return result;
	}

	/**
	 * 更具其实和结束日期获取中间的所有日期
	 * 
	 * @param start
	 * @param end
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static List<String> getDateDateSpaceList(String start, String end, String format) throws ParseException {
		DateFormat formatter = new SimpleDateFormat(format);
		List<String> result = new ArrayList<String>();
		Date startDate = formatter.parse(start);
		Date endDate = formatter.parse(end);
		Calendar calendar = Calendar.getInstance();
		
		if (startDate.after(endDate)) {
			return result;
		}
		calendar.setTime(startDate);
		while (endDate.after(calendar.getTime())) {
			result.add(formatter.format(calendar.getTime()));
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		result.add(end);
		return result;
	}
}
