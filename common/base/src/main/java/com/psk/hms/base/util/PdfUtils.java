package com.psk.hms.base.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.psk.hms.base.argument.constant.module.inspect.InspectConstant;

/**
 * pdf操作工具
 *@author 创建者：ds
 *@date 创建时间：2018年5月21日 下午4:21:32
 *@version 1.0
 *@parameter 
*/
public class PdfUtils {

	protected static Log log = LogFactory.getLog(PdfUtils.class);
	// 定义动态表的各类数据  
    private static int PDF_PAGES = 1;// PDF初始页数  
    //巡检图片张数
    private static int CHECK_PIC_NUM = 0;
    //整改回复图片张数
    private static int REPLY_PIC_NUM = 0;
    //复检图片张数
    private static int RECT_PIC_NUM = 0;
    //新建表格列数
    private static final int TABLE_COLNUM = 2;
    private static final float XPOS = 50f;// 整个表格 X坐标  
    private static final float YPOS = 50f; // 整个表格 Y坐标  
    private static final float[] CELL_WIDTHS = { 0.5f, 0.5f };// 单元格宽度 
    private static final int PAGE_PIC_NUMBER = 8;//默认每页8张图片
	private static int PAGE_NUMBER = 2;//默认最大页码，当多余PAGE_PIC_NUMBER张图片时则往下排一页
	
	/**
	 * 巡检附件-巡检图片
	 */
	public static final String INSPECT_ATTACH_PIC_CHECK = "jcImg";
	
	/**
	 * 巡检附件-整改回复图片
	 */
	public static final String INSPECT_ATTACH_PIC_REPLY = "zgImg";
	
	/**
	 * 巡检附件-复检图片
	 */
	public static final String INSPECT_ATTACH_PIC_RECT = "fjImg";
	
	// 利用模板生成pdf  
    public static Map<String,Object> createPdfFile(Map<String,Object> map) {
    	Map<String,Object> result = new  HashMap<String, Object>();
        // 模板路径  
        String templatePath = BaseUtil.retStr(map.get("templetUrl"));//"D:\\轻筑巡检单模板PDF文件\\[QZ_PFT_SRR_001] 过程文件模板_安全整改通知单_001_v1.0.pdf";//
        // 生成的新文件路径  
        String newPDFPath = BaseUtil.retStr(map.get("saveUrl"));

        PdfReader reader;
        FileOutputStream out;
        ByteArrayOutputStream bos;
        PdfStamper stamper;
        try {            
            out = new FileOutputStream(newPDFPath);// 输出流
            reader = new PdfReader(templatePath);// 读取pdf模板  
            bos = new ByteArrayOutputStream();
            stamper = new PdfStamper(reader, bos);
            //获取form表单对象
            AcroFields form = stamper.getAcroFields();            
            //向文本域插入文本
            insertText(stamper,form,map);            
            //向图片域插入图片
            int imgNum =  insertImage(stamper,form,map);
            // 如果为false，生成的PDF文件可以编辑，如果为true，生成的PDF文件不可以编辑            
            stamper.setFormFlattening(true);
            stamper.close();
            //创建一个文档对象
            Document doc = new Document();
            PdfCopy copy = new PdfCopy(doc, out);
            doc.open();
            //读取数据流
            PdfReader pdfr = new PdfReader(bos.toByteArray());
            //获取模板页数
            int n = imgNum>0?pdfr.getNumberOfPages():PDF_PAGES;
            doc.newPage();  
        	PdfImportedPage importPage0 = copy.getImportedPage(pdfr, 1);
            copy.addPage(importPage0);
            if(n>1){//2018-12-28 任务紧急，临时解决办法，后面需要重新规划
            	//检查图片：2/3/4页
            	if(CHECK_PIC_NUM==0){//没有巡检图片，什么也不做            		
            	}else if(CHECK_PIC_NUM>0 && CHECK_PIC_NUM<=4){//只有第二页
            		doc.newPage();  
                	PdfImportedPage importPage1 = copy.getImportedPage(pdfr, 2);
                    copy.addPage(importPage1);
            	}else if(CHECK_PIC_NUM>4 && CHECK_PIC_NUM<=8){//有二、三页
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 2);
                    copy.addPage(importPage2);
                    doc.newPage();  
                	PdfImportedPage importPage3 = copy.getImportedPage(pdfr, 3);
                    copy.addPage(importPage3);
            	}else if(CHECK_PIC_NUM>8){//有二、三、四页
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 2);
                    copy.addPage(importPage2);
                    doc.newPage();  
                	PdfImportedPage importPage3 = copy.getImportedPage(pdfr, 3);
                    copy.addPage(importPage3);
                    doc.newPage();  
                	PdfImportedPage importPage4 = copy.getImportedPage(pdfr, 4);
                    copy.addPage(importPage4);
            	}
            	//整改回复图片：5/6/7页
            	if(REPLY_PIC_NUM==0){//没有巡检图片
            	}else if(REPLY_PIC_NUM>0 && REPLY_PIC_NUM<=4){
            		doc.newPage();  
                	PdfImportedPage importPage1 = copy.getImportedPage(pdfr, 5);
                    copy.addPage(importPage1);
            	}else if(REPLY_PIC_NUM>4 && REPLY_PIC_NUM<=8){
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 5);
                    copy.addPage(importPage2);
                    doc.newPage();  
                	PdfImportedPage importPage3 = copy.getImportedPage(pdfr,6);
                    copy.addPage(importPage3);
            	}else if(REPLY_PIC_NUM>8){
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 5);
                    copy.addPage(importPage2);
                    doc.newPage();  
                	PdfImportedPage importPage3 = copy.getImportedPage(pdfr, 6);
                    copy.addPage(importPage3);
                    doc.newPage();  
                	PdfImportedPage importPage4 = copy.getImportedPage(pdfr, 7);
                    copy.addPage(importPage4);
            	}
            	
            	//复检图片：8/9/10页
            	if(RECT_PIC_NUM==0){//没有巡检图片
            	}else if(RECT_PIC_NUM>0 && RECT_PIC_NUM<=4){
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 8);
                    copy.addPage(importPage2);
            	}else if(RECT_PIC_NUM>4 && RECT_PIC_NUM<=8){
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 8);
                    copy.addPage(importPage2);
                    doc.newPage();  
                	PdfImportedPage importPage3 = copy.getImportedPage(pdfr,9);
                    copy.addPage(importPage3);
            	}else if(RECT_PIC_NUM>8){
            		doc.newPage();  
                	PdfImportedPage importPage2 = copy.getImportedPage(pdfr, 8);
                    copy.addPage(importPage2);
                    doc.newPage();  
                	PdfImportedPage importPage3 = copy.getImportedPage(pdfr, 9);
                    copy.addPage(importPage3);
                    doc.newPage();  
                	PdfImportedPage importPage4 = copy.getImportedPage(pdfr, 10);
                    copy.addPage(importPage4);
            	}
            	
            	
            }              
            
            doc.add(createPdfPTable());
            doc.close();
            result.put("code", "true");
        } catch (IOException e) {
        	log.error("生成巡检单时错误：",e);
        } catch (DocumentException e) {
        	log.error("创建文档对象时错误：",e);
        }
        return result;
    }
    
    /**
     * 向指定位置插入文本
     * @param ps
     * @param form
     * @param txtMap
     */
    @SuppressWarnings({ "unchecked", "unused" })
	public static void insertText(PdfStamper ps, AcroFields form,Map<String, Object> map){
    	BaseFont bf;
		try {
			bf = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);
			Font FontChinese = new Font(bf, 5, Font.NORMAL);
			//文字类的内容集合
            Map<String,Object> datemap = (Map<String,Object>)map.get("datemap");
            form.addSubstitutionFont(bf);
            for(String key : datemap.keySet()){
                String value = BaseUtil.retStr(datemap.get(key));
                form.setField(key,value);
            }
		} catch (DocumentException e) {
			log.error("插入文本错误：",e);
			e.printStackTrace();
		} catch (IOException e) {
			log.error("插入文本错误：",e);
			e.printStackTrace();
		} 
        
    }
    
    /**
     * 向指定位置插入图片,适用于图片张数和放置位置确定，即在模板中定义了图片域
     * @param stamper
     * @param form
     * @param imgMap
     */
    @SuppressWarnings("unchecked")
	public static int insertImage(PdfStamper stamper, AcroFields form,Map<String, Object> map){
    	int imgNum =0;
    	try {
			//图片类的内容处理
	        Map<String,Object> imgmap = (Map<String,Object>)map.get("imgmap");
	        for(String key : imgmap.keySet()) {
	        	imgNum++;
	        	if(key.contains(INSPECT_ATTACH_PIC_CHECK)){
	        		CHECK_PIC_NUM+=1;
	        	}else if(key.contains(INSPECT_ATTACH_PIC_REPLY)){
	        		REPLY_PIC_NUM+=1;
	        	}else if(key.contains(INSPECT_ATTACH_PIC_RECT)){
	        		RECT_PIC_NUM+=1;
	        	}
	            String imgpath = BaseUtil.retStr(imgmap.get(key));
	            if(BaseUtil.isEmpty(imgpath))
	            	continue;
	            //获取图片域集合
	            List<AcroFields.FieldPosition> list = form.getFieldPositions(key);
	            // 通过域名获取所在页和坐标，左下角为起点
	            int pageNo = list.get(0).page;
	            //根据路径读取图片
	            Image image = Image.getInstance(imgpath);
	            // 获取操作的页面
	            PdfContentByte under = stamper.getOverContent(pageNo);
	            //图片位置
	            Rectangle signRect = list.get(0).position;
	            float x = signRect.getLeft();
	            float y = signRect.getBottom();
	            //添加图片
	            image.setAbsolutePosition(x, y);
	            image.setAlignment(Image.ALIGN_CENTER);
	            image.setBorder(0);
	            //图片大小自适应
	            image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	            under.addImage(image);
	        }
		} catch (IOException e) {
			log.error("插入图片时错误：",e);
			e.printStackTrace();
		}catch (DocumentException e) {
			log.error("插入图片时错误：",e);
			e.printStackTrace();
		}
    	return imgNum;
    }
    
    /**
     * 向指定页面插入PDF表格，在参数不确定的情况下适用
     * @param stamper
     * @param form
     * @param map
     */
    @SuppressWarnings({ "unchecked", "unused" })
	public static void insertPDFTable(PdfStamper stamper, AcroFields form,Map<String, Object> map){
    	try {
			//图片列表
	        List<Map<String,String>> imglist = (List<Map<String, String>>)map.get("imgmap");
	        if(null!=imglist && imglist.size()>0){
	        	int  tempNum= imglist.size();
	        	//总页数=（总记录 - 总记录/每页张数）+1 +1
	        	PAGE_NUMBER=(tempNum-tempNum/PAGE_PIC_NUMBER)+1 +1;	        	
	            // 在指定页面添加表格-图片
	            for (int pageNo = 2; pageNo <= PAGE_NUMBER; pageNo++) { 
	            	//创建指定列数的表格
		        	PdfPTable datatable = createPdfPTable();
		        	//循环起始位置
		        	int start = (pageNo-2)*8;
		        	//循环终止位置
		        	int end = pageNo<PAGE_NUMBER?(pageNo-1)*8:imglist.size();
		        	// 拿到层,可以有页数
	            	PdfContentByte under = stamper.getUnderContent(pageNo);
	            	for (int j = start; j < end; j++) {
	            		//获取图片路径
	            		String imgpath = imglist.get(j).get("imgUrl");
	    	            //根据路径读取图片
	    	            Image image = Image.getInstance(imgpath);
	    	            //图片位置	    	            
	    	            image.setAbsolutePosition(XPOS, YPOS);
	    	            //设置对齐方式
	    	            image.setAlignment(Image.ALIGN_CENTER);
	    	            //图片大小自适应
	    	            //image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	    	            //添加图片
	            		datatable.addCell(image);
					}
	            	
	            }
	            
	        }
		} catch (IOException e) {
			log.error("插入表格时错误：",e);
			e.printStackTrace();
		}catch (DocumentException e) {
			log.error("插入表格时错误：",e);
			e.printStackTrace();
		}
    }
    
    /**
     * 创建PDF表格
     * @return
     * @throws IOException 
     * @throws MalformedURLException 
     */
    public static PdfPTable createPdfPTable(){
    	//创建指定列数的表格
    	PdfPTable datatable = new PdfPTable(TABLE_COLNUM);
    	//设置单元格宽度
    	try {
    		datatable.setSpacingBefore(20f);// 设置表格上面空白行  
			datatable.setWidths(CELL_WIDTHS);
			// 表格的宽度百分比  
	        datatable.setWidthPercentage(100);	   
	        // 单元格的间隔  
	        datatable.getDefaultCell().setPadding(10);
	        // 边框宽度  
	        datatable.getDefaultCell().setBorderWidth(0);
	        // 设置表格的对齐方式
	        datatable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
	        datatable.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);	
	        datatable.setSpacingAfter(20f);// 设置表格下面空白行  
		} catch (DocumentException e) {
			log.error("创建PDF表格时错误：",e);
			e.printStackTrace();
		}    	
        return datatable;
    }
    
    /** 
     * 支持中文 
     *  
     * @return 
     */  
    public static Font getChineseFont() {  
        BaseFont bfChinese;  
        Font fontChinese = null;  
        try {  
            bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);  
            fontChinese = new Font(bfChinese, 12, Font.NORMAL);  
           // fontChinese = new Font(bfChinese, 12, Font.NORMAL, BaseColor.BLUE);  
        } catch (DocumentException e) { 
        	log.error("设置文件字体样式时错误：",e);
            e.printStackTrace();  
        } catch (IOException e) {  
        	log.error("设置文件字体样式时错误：",e);
            e.printStackTrace();  
        }  
        return fontChinese;   
    }

   /* public static void main(String[] args) {
        Map<String,String> map = new HashMap<String,String>();
        map.put("billCode","张三");
        map.put("billDate","2018年05月21日");
        map.put("rstContent","整改内容");
        map.put("checkTime","2018年05月01日");
        map.put("rectContent","整改结果");
        map.put("rectTime","2018年05月18日");
        map.put("replayContent","复检结构");
        map.put("replayTime","2018年05月19日");
        map.put("replayer","张三丰");
        map.put("rectPeriod","2018年05月18日");
        map.put("projectName","江北嘴金融街4号");
        map.put("checkPart","一号楼#8");
        map.put("checkUnit","中冶建工十八局");
        map.put("notifiUnit","项目部");
        map.put("checker","王伟、李斯、毛利");
        map.put("notifier","王麻子、二狗");
        Map<String,String> map2 = new HashMap<String,String>();
        map2.put("Text1","E:\\2.jpg");
        map2.put("Text2","C:\\Users\\Administrator\\Pictures\\Saved Pictures\\3.jpg");
        map2.put("Text3","E:\\2.jpg");
        map2.put("Text4","C:\\Users\\Administrator\\Pictures\\Saved Pictures\\3.jpg");
        Map<String,Object> o=new HashMap<String,Object>();
        o.put("datemap",map);
        o.put("imgmap",map2);
        createPdfFile(o);
        
        
        
        
    }*/
}
