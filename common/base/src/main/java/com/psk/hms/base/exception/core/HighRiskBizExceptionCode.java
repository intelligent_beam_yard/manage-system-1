package com.psk.hms.base.exception.core;
/**
 * <p>
 * inspect 业务异常类
 * </p>
 * 模块异常码 : 22000001 ~ 22999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年11月26日
 * @version 1.0.0
 *
 */
public class HighRiskBizExceptionCode {

	/***
	 * 危大工程 
	 * 22010000~22019999
	 */
	//危大工程-验收-增
	public static final int HIGHRISKPROJECT_CHECK_INSERT_ERROR = 22010101;
	//危大工程-验收-删
	public static final int HIGHRISKPROJECT_CHECK_DELETE_ERROR = 22010102;
	//危大工程-验收-查
	public static final int HIGHRISKPROJECT_CHECK_SELECT_ERROR = 22010104;
	//危大工程-验收-改
	public static final int HIGHRISKPROJECT_CHECK_UPDATE_ERROR = 22010105;
	//危大工程-验收-通用异常
	public static final int HIGHRISKPROJECT_CHECK_ERROR = 22010199;
	
	//危大工程-巡检关联工程列表-增
	public static final int HIGHRISKPROJECT_INSPECT_LIST_INSERT_ERROR = 22010201;
	//危大工程-巡检关联工程列表-删
	public static final int HIGHRISKPROJECT_INSPECT_LIST_DELETE_ERROR = 22010202;
	//危大工程-巡检关联工程列表-查
	public static final int HIGHRISKPROJECT_INSPECT_LIST_FINDBY_ERROR = 22010203;
	//危大工程-巡检关联工程列表-改
	public static final int HIGHRISKPROJECT_INSPECT_LIST_UPDATE_ERROR = 22010204;
	//危大工程-巡检关联工程列表-通用异常
	public static final int HIGHRISKPROJECT_INSPECT_LIST_ERROR = 22010299;
	
	//危大工程-巡检记录-增
	public static final int HIGHRISKPROJECT_INSPECT_RECORD_INSERT_ERROR = 22010301;
	//危大工程-巡检记录-删
	public static final int HIGHRISKPROJECT_INSPECT_RECORD_DELETE_ERROR = 22010302;
	//危大工程-巡检记录-查
	public static final int HIGHRISKPROJECT_INSPECT_RECORD_FINDBY_ERROR = 22010303;
	//危大工程-巡检记录-改
	public static final int HIGHRISKPROJECT_INSPECT_RECORD_UPDATE_ERROR = 22010304;
	//危大工程-巡检记录-通用异常
	public static final int HIGHRISKPROJECT_INSPECT_RECORD_ERROR = 22010399;
	
	//危大工程-整改回复-增
	public static final int HIGHRISKPROJECT_INSPECT_REPLAY_INSERT_ERROR = 22010401;
	//危大工程-整改回复-删
	public static final int HIGHRISKPROJECT_INSPECT_REPLAY_DELETE_ERROR = 22010402;
	//危大工程-整改回复-查
	public static final int HIGHRISKPROJECT_INSPECT_REPLAY_FINDBY_ERROR = 22010403;
	//危大工程-整改回复-改
	public static final int HIGHRISKPROJECT_INSPECT_REPLAY_UPDATE_ERROR = 22010404;
	//危大工程-整改回复-通用异常
	public static final int HIGHRISKPROJECT_INSPECT_REPLAY_ERROR = 22010499;
	
	//危大工程-关联人员-增
	public static final int HIGHRISKPROJECT_PERSON_INSERT_ERROR = 22010501;
	//危大工程-关联人员-删
	public static final int HIGHRISKPROJECT_PERSON_DELETE_ERROR = 22010502;
	//危大工程-关联人员-查
	public static final int HIGHRISKPROJECT_PERSON_FINDBY_ERROR = 22010503;
	//危大工程-关联人员-改
	public static final int HIGHRISKPROJECT_PERSON_UPDATE_ERROR = 22010504;
	//危大工程-关联人员-通用异常
	public static final int HIGHRISKPROJECT_PERSON_ERROR = 22010599;
	
	//危大工程-巡检性质-增
	public static final int HIGHRISKPROJECT_PROPERTY_INSERT_ERROR = 22010601;
	//危大工程-巡检性质-删
	public static final int HIGHRISKPROJECT_PROPERTY_DELETE_ERROR = 22010602;
	//危大工程-巡检性质-查
	public static final int HIGHRISKPROJECT_PROPERTY_FINDBY_ERROR = 22010603;
	//危大工程-巡检性质-改
	public static final int HIGHRISKPROJECT_PROPERTY_UPDATE_ERROR = 22010604;
	//危大工程-质量巡检性质-通用异常
	public static final int HIGHRISKPROJECT_PROPERTY_ERROR = 22010699;
	
	//危大工程-方案附件-增
	public static final int HIGHRISKPROJECT_SCHEME_ATTACH_INSERT_ERROR = 22010701;
	//危大工程-方案附件-删
	public static final int HIGHRISKPROJECT_SCHEME_ATTACH_DELETE_ERROR = 22010702;
	//危大工程-方案附件-查
	public static final int HIGHRISKPROJECT_SCHEME_ATTACH_FINDBY_ERROR = 22010703;
	//危大工程-方案附件-改
	public static final int HIGHRISKPROJECT_SCHEME_ATTACH_UPDATE_ERROR = 22010704;
	//危大工程-方案附件-通用异常
	public static final int HIGHRISKPROJECT_SCHEME_ATTACH_ERROR = 22010799;
	
	//危大工程-方案关联工程列表-增
	public static final int HIGHRISKPROJECT_SCHEME_LIST_INSERT_ERROR = 22010801;
	//危大工程-方案关联工程列表-删
	public static final int HIGHRISKPROJECT_SCHEME_LIST_DELETE_ERROR = 22010802;
	//危大工程-方案关联工程列表-查
	public static final int HIGHRISKPROJECT_SCHEME_LIST_FINDBY_ERROR = 22010803;
	//危大工程-方案关联工程列表-改
	public static final int HIGHRISKPROJECT_SCHEME_LIST_UPDATE_ERROR = 22010804;
	//危大工程-方案关联工程列表-通用异常
	public static final int HIGHRISKPROJECT_SCHEME_LIST_ERROR = 22010899;
	
	//危大工程-方案-增
	public static final int HIGHRISKPROJECT_SCHEME_INSERT_ERROR = 22010901;
	//危大工程-方案-删
	public static final int HIGHRISKPROJECT_SCHEME_DELETE_ERROR = 22010902;
	//危大工程-方案-查
	public static final int HIGHRISKPROJECT_SCHEME_FINDBY_ERROR = 22010903;
	//危大工程-方案-改
	public static final int HIGHRISKPROJECT_SCHEME_UPDATE_ERROR = 22010904;
	//危大工程-方案-通用异常
	public static final int HIGHRISKPROJECT_SCHEME_ERROR = 22010999;
	
	//危大工程-工程列表-增
	public static final int HIGHRISKPROJECT_LIST_INSERT_ERROR = 22011001;
	//危大工程-工程列表-删
	public static final int HIGHRISKPROJECT_LIST_DELETE_ERROR = 22011002;
	//危大工程-工程列表-查
	public static final int HIGHRISKPROJECT_LIST_FINDBY_ERROR = 22011003;
	//危大工程-工程列表-改
	public static final int HIGHRISKPROJECT_LIST_UPDATE_ERROR = 22011004;
	//危大工程-工程列表-通用异常
	public static final int HIGHRISKPROJECT_LIST_ERROR = 22011099;
	
	//危大工程-范围-增
	public static final int HIGHRISKPROJECT_SCOPE_INSERT_ERROR = 22011101;
	//危大工程-范围-删
	public static final int HIGHRISKPROJECT_SCOPE_DELETE_ERROR = 22011102;
	//危大工程-范围-查
	public static final int HIGHRISKPROJECT_SCOPE_FINDBY_ERROR = 22011103;
	//危大工程-范围-改
	public static final int HIGHRISKPROJECT_SCOPE_UPDATE_ERROR = 22011104;
	//危大工程-范围-通用异常
	public static final int HIGHRISKPROJECT_SCOPE_ERROR = 22011199;
	
	//危大工程类型增
	public static final int HIGHRISKPROJECT_TYPE_INSERT_ERROR = 22011201;
	//危大工程类型删
	public static final int HIGHRISKPROJECT_TYPE_DELETE_ERROR = 22011202;
	//危大工程类型查
	public static final int HIGHRISKPROJECT_TYPE_FINDBY_ERROR = 22011203;
	//危大工程类型改
	public static final int HIGHRISKPROJECT_TYPE_UPDATE_ERROR = 22011204;
	//危大工程类型通用异常
	public static final int HIGHRISKPROJECT_TYPE_ERROR = 22011299;
}
