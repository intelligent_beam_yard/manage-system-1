package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础公司常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseCompanyConstant extends Constant {
    /**
     *  数据表字段名称 - name - 公司名称
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - shortName - 简称
     */
    public static final String SHORT_NAME = "shortName";
    /**
     *  数据表字段名称 - code - 公司编码
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - legalPerson - 法人
     */
    public static final String LEGAL_PERSON = "legalPerson";
    /**
     *  数据表字段名称 - registerCapital - 注册资金
     */
    public static final String REGISTER_CAPITAL = "registerCapital";
    /**
     *  数据表字段名称 - registerTime - 注册时间
     */
    public static final String REGISTER_TIME = "registerTime";
    /**
     *  数据表字段名称 - companyNature - 公司类型
     */
    public static final String COMPANY_NATURE = "companyNature";
    /**
     *  数据表字段名称 - address - 注册地址
     */
    public static final String ADDRESS = "address";
}
