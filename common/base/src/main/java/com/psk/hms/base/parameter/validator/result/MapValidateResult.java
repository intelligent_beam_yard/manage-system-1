package com.psk.hms.base.parameter.validator.result;

/**
 * 校验返回结果
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 */
public class MapValidateResult<K> {
	// 校验是否成功标志位
	protected boolean pass;
	// 校验不匹配信息
	protected String errorMsg;
	// 校验关联的map key
	private K key;

	public MapValidateResult(K key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(": pass(").append(pass).append("), errorMsg(")
				.append(errorMsg).append(")");
		return sb.toString();
	}

	/**
	 * 返回校验成功信息，将pass设置为true，并将errorMsg设置为null
	 * 
	 * @return
	 */
	public MapValidateResult<K> success() {
		this.pass = true;
		this.errorMsg = null;
		return this;
	}

	/**
	 * 返回校验失败消息，将pass设置为false，并将errorMsg设置为指定值
	 *  
	 * @param errorMsg 校验不匹配消息
	 * @return
	 */
	public MapValidateResult<K> failure(String errorMsg) {
		this.pass = false;
		this.errorMsg = errorMsg;
		return this;
	}

	public boolean isPass() {
		return pass;
	}

	public void setPass(boolean pass) {
		this.pass = pass;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

}
