package com.psk.hms.base.enums;

import com.psk.hms.base.enums.bean.BaseDataBean;
import com.psk.hms.base.util.BaseUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * 
 * @功能说明：枚举 转换
 * @作者：xiangjz
 * @创建日期： 2018-01-02
 */
public  class CommonEnumTypeToList {

	private static CommonEnumTypeToList instance;
	
	public static synchronized CommonEnumTypeToList getInstance() {
		if (BaseUtil.isEmpty(instance))
			instance = new CommonEnumTypeToList();
		return instance;
	}

	/**
	 * 系统加载枚举数据
	 * @return
	 */
	public static Map<Object, Object> writeMemoryTempleStatus() {

		Map<Object, Object> dictionaryParams= new HashMap<Object, Object>();

		Class enumTypeCLass = CommonEnumType.class;
		//获取内部类的class对象
		Class innerClazz[] = enumTypeCLass.getDeclaredClasses();
		try {
			for (Class cls : innerClazz) {
				Object[] os = cls.getEnumConstants();

				Method getValue = cls.getMethod("getValue");
				Method getName = cls.getMethod("name");

				List<BaseDataBean> dataList = new ArrayList<BaseDataBean>();
				Map<Object, Object> dictionary = new HashMap<Object, Object>();
				for (Object o : os) {
					String name = BaseUtil.retStr(getName.invoke(o));
					String value = BaseUtil.retStr(getValue.invoke(o));

					BaseDataBean bd = new BaseDataBean();
					bd.setName(name);
					bd.setValue(value);
					dictionary.put(value, name);
					dataList.add(bd);
				}

				String className = cls.getSimpleName();
				dictionaryParams.put("comEnum_"+className+"_map", dictionary);
				dictionaryParams.put("comEnum_"+className+"_list", dataList);
			}
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return dictionaryParams;
	}

}