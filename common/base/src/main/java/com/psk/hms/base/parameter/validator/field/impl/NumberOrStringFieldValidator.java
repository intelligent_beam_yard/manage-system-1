package com.psk.hms.base.parameter.validator.field.impl;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;

public class NumberOrStringFieldValidator<K, V> extends AnyTypeFieldValidator<K, V> {

	private static final Class<?>[] TYPES = new Class<?>[] { 
		String.class, 
		Integer.class, 
		Double.class, 
		Long.class, 
		Float.class 
		};

	public NumberOrStringFieldValidator(K key, boolean keyRequired) {
		super(key, TYPES, keyRequired);
	}

	public NumberOrStringFieldValidator(K key, boolean keyRequired, String errorMsg) {
		super(key, TYPES, keyRequired, errorMsg);
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> mapValidateResult = super.checkEntity(key, value);
		if (!mapValidateResult.isPass()) {
			return mapValidateResult;
		}
		if (value.getClass().equals(String.class)) {
			String regex = "^[-+]?[0-9]+(.[0-9]+)?$";
			if (!value.toString().matches(regex)) {
				return mapValidateResult.failure(errorMsg);
			}
		}
		return mapValidateResult;
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为数字或数字字符串");
		return sb.toString();
	}

}
