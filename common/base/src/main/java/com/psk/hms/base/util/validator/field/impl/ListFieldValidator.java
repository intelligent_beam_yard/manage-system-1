package com.psk.hms.base.util.validator.field.impl;

import java.util.List;

import com.psk.hms.base.util.validator.field.AbstractMapEntityValidator;
import com.psk.hms.base.util.validator.result.MapValidateResult;

public class ListFieldValidator<K, V> extends AbstractMapEntityValidator<K, V> {

	private Class<?> type;
	
	public ListFieldValidator(K key, Class<?> type, boolean keyRequired) {
		super(key, keyRequired);
		this.type = type;
		this.errorMsg = getDefaultErrorMsg(key);
	}
	
	public ListFieldValidator(K key, Class<?> type, boolean keyRequired, String errorMsg) {
		super(key, keyRequired, errorMsg);
		this.type = type;
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> mapValidateResult = new MapValidateResult<>(key);
		if (value == null) {
			return mapValidateResult.failure(errorMsg);
		}
		// 检测数据类型
		if (!(value instanceof List<?>)) {
			return mapValidateResult.failure(errorMsg);
		}
		// 列表非空检测
		List<?> rawList = (List<?>) value;
		if (rawList.isEmpty()) {
			return mapValidateResult.success(); 
		}
		// 检测列表泛型
		Class<?> actualType = (Class<?>) rawList.get(0).getClass();
		if (!(type.isAssignableFrom(actualType)) && !actualType.equals(type)) {
			return mapValidateResult.failure(errorMsg);
		}
		return mapValidateResult.success();
	}
	
	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为").append(type.getSimpleName()).append("类型列表");
		return sb.toString();
	}

	public Class<?> getType() {
		return type;
	}

	public void setType(Class<?> type) {
		this.type = type;
	}

}
