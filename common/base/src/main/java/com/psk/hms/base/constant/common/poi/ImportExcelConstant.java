package com.psk.hms.base.constant.common.poi;

public class ImportExcelConstant {
	
	public static final String ENUM_VALUE_KEY = "EnumValue";
	
	public static final String DICTIONARY_CODE_KEY = "DictionaryCode";
	
	public static final String COMPANY_ID_KEY = "CompanyId";
	
}
