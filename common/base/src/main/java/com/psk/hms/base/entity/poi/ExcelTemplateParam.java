package com.psk.hms.base.entity.poi;

import org.springframework.web.multipart.MultipartFile;

/**
 * 导入excel模板业务数据的参数
 * @author xiangjz
 *
 */
public class ExcelTemplateParam {
	
	private MultipartFile importFile;
	
	private String templateExcelType;
	
	private Integer startRow = 1;

	public MultipartFile getImportFile() {
		return importFile;
	}

	public void setImportFile(MultipartFile importFile) {
		this.importFile = importFile;
	}

	public String getTemplateExcelType() {
		return templateExcelType;
	}

	public void setTemplateExcelType(String templateExcelType) {
		this.templateExcelType = templateExcelType;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}
	
	public ExcelTemplateParam(MultipartFile importFile, String templateExcelType) {
		super();
		this.importFile = importFile;
		this.templateExcelType = templateExcelType;
	}

	public ExcelTemplateParam(MultipartFile importFile, String templateExcelType, Integer startRow) {
		super();
		this.importFile = importFile;
		this.templateExcelType = templateExcelType;
		this.startRow = startRow;
	}

	public ExcelTemplateParam() {
		super();
	}

	
}
