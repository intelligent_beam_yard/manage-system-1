package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础地区编码常量类
 * @author xiangjz
 * @date 2018年11月06日
 * @version 1.0.0
 *
 */
public class BaseIdTypeConstant extends Constant {
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
}
