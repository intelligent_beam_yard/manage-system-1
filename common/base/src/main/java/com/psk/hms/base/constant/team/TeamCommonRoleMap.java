package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队角色关联类型常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamCommonRoleMap extends Constant {    
    /**
     *  数据表字段名称 - teamId
     */
    public static final String TEAMID = "teamId";
    /**
     *  数据表字段名称 - roleId
     */
    public static final String ROLEID = "baseRoleId";
    
}
