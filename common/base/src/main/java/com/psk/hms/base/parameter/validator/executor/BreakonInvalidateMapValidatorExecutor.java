package com.psk.hms.base.parameter.validator.executor;

import com.psk.hms.base.parameter.validator.field.MapValidatable;
import com.psk.hms.base.parameter.validator.result.MapValidateResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 不匹配中止校验执行器。此执行器将会一次调用校验链中的校验器，但校验到第一个不匹配的字段时，
 * 便会将校验结果写入结果集中，并跳过后续的校验器直接返回结果集。所以结果集中至多会包含一条
 * 校验不匹配数据。
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public class BreakonInvalidateMapValidatorExecutor<K, V> implements MapValidatorExecutable<K, V> {

	@Override
	public Map<String, Object> execute(Map<K, V> map, List<MapValidatable<K, V>> validatorList) {
		Map<String, Object> result = new HashMap<>();
		for (MapValidatable<K, V> validator : validatorList) {
			MapValidateResult<K> mapValidateResult = validator.check(map);
			if(!mapValidateResult.isPass()) {
				result.put(mapValidateResult.getKey().toString(), mapValidateResult.getErrorMsg());
				break;
			}
		}
		return result;
	}

}
