package com.psk.hms.base.entity;

import java.util.List;

/**
 * layui提供数据网格的指定返回参数
 * 
 * @author xiangjz
 * 
 */
public class LayGridReturn<T> {
	private Integer code = 200;// 返回状态码
	private String msg = "操作成功";// 提示信息
	private Long count = 0L;// 条数
	private List<T> data = null;// 数据主体，实为json数组

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}

	public LayGridReturn() {
		super();
	}

	public LayGridReturn(Integer code, String msg, List<T> data, Long count) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.count = count;
	}

	public LayGridReturn success(List<T> data, Long count) {
		this.data = data;
		this.count = count;
		return this;
	}

	public LayGridReturn failure(String msg) {
		this.code = 500;
		this.msg = msg;
		return this;
	}
	
}
