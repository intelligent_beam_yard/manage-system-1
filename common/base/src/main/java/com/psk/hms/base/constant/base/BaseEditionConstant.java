package com.psk.hms.base.constant.base;
import com.psk.hms.base.constant.common.Constant;

/**
 * 关于轻筑常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseEditionConstant  extends Constant {
	 /**
     *  数据表字段名称 - edition_no
     */
    public static final String EDITION_NO = "editionNo";
    /**
     *  数据表字段名称 - logo_image
     */
    public static final String LOGO_IMAGE = "logoImage";
    /**
     *  数据表字段名称 - content
     */
    public static final String CONTENT = "content";
    /**
     *  数据表字段名称 - copyright
     */
    public static final String COPYRIGHT = "copyright";
}
