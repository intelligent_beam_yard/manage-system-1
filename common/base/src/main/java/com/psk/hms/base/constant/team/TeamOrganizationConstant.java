package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队组织机构常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamOrganizationConstant extends Constant {    
    /**
     *  数据表字段名称 - teamId
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - parentId
     */
    public static final String PARENT_ID = "parentId";    
    /**
     *  数据表字段名称 - parentIds
     */
    public static final String PARENT_IDS = "parentIds";
    /**
     *  数据表字段名称 - levels
     */
    public static final String LEVELS = "levels";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
    /**
     *  数据表字段名称 - isParent
     */
    public static final String IS_PARENT = "isParent";
    
    
    /**
     *  业务逻辑段名称 - level
     */
    public static final String LEVEL = "level";    
    /**
     *  业务逻辑 - 父节点标志位 - 1（父节点）
     */
    public static final String IS_PARENT_YES = "1";
    /**
     *  业务逻辑 - 父节点标志位 - 0（叶子节点）
     */
    public static final String IS_PARENT_NO = "0";
}
