package com.psk.hms.base.constant.common;
/**
 * 描述: 登录常量
 * 作者: xiangjz
 * 时间: 2017年4月9日
 * 版本: 1.0
 *
 */
public class LoginConstant {
	
	/**
	 * 是否记住密码  1-记住
	 */
	public static final String REMEMBER_PASSWORD_YES = "1";
	
	/**
	 * 是否记住密码  0-不记住
	 */
	public static final String REMEMBER_PASSWORD_NO = "0";
	
	/**
	 * 密码输入错误次数限制  10次
	 */
	public static final Integer WEB_PWD_INPUT_ERROR_LIMIT = 10;
	
	/** 
	 * 锁定多少小时之后再可登陆 
	 */
	public static final Integer PASSERRORHOUR = 6;
	
}
