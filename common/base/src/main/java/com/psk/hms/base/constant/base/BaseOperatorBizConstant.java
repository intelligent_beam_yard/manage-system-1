package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础功能关联常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseOperatorBizConstant extends Constant {
    /**
     *  数据表字段名称 - biz_id
     */
    public static final String BIZ_ID = "bizId";
    /**
     *  数据表字段名称 - biz_type
     */
    public static final String BIZ_TYPE = "bizType";
    /**
     *  数据表字段名称 - permission_id
     */
    public static final String PERMISSION_ID = "permissionId";
    /**
     *  数据表字段名称 - permission_type
     */
    public static final String PERMISSION_TYPE = "permissionType";
    
	/**
	 * 业务类型 -- 团队类型 - 1 
	 */
	public static final String BIZ_TYPE_1 = "1";
	/**
	 * 业务类型 -- 团队信息 - 2 
	 */
	public static final String BIZ_TYPE_2 = "2";
	/**
	 * 业务类型 -- 通用团队角色 - 3 
	 */
	public static final String BIZ_TYPE_3 = "3";
	/**
	 * 业务类型 -- 自定义团队角色 - 4 
	 */
	public static final String BIZ_TYPE_4 = "4";
	/**
	 * 业务类型 -- 团队角色 - 5
	 */
	public static final String BIZ_TYPE_5 = "5";
	/**
	 * 业务类型 -- 团队人员 - 6
	 */
	public static final String BIZ_TYPE_6 = "6";
	
	
	/**
	 * 业务类型 -- 权限类型 - 功能 - 1
	 */
	public static final String PERMISSION_TYPE_1 = "1";
	/**
	 * 业务类型 -- 权限类型 - 权限包 - 2
	 */
	public static final String PERMISSION_TYPE_2 = "2";
	
    /**
     *  业务参数 - packageId
     */
    public static final String PACKAGEID = "packageId";
    
}
