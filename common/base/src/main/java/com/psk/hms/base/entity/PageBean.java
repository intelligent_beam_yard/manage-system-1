package com.psk.hms.base.entity;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

public class PageBean<T> extends PageInfo<T>{

	private static final long serialVersionUID = 1L;
	
	private Object orderColunm;
	private Object orderMode;
	
	private Object extData; // 返回扩展数据
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PageBean(Page listPage) {
		super(listPage);
	}

	public Object getOrderColunm() {
		return orderColunm;
	}

	public void setOrderColunm(Object orderColunm) {
		this.orderColunm = orderColunm;
	}

	public Object getOrderMode() {
		return orderMode;
	}

	public void setOrderMode(Object orderMode) {
		this.orderMode = orderMode;
	}

	public Object getExtData() {
		return extData;
	}

	public void setExtData(Object extData) {
		this.extData = extData;
	}
}
