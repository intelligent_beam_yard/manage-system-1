package com.psk.hms.base.util.file;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
/*************
 * file转MultipartFile辅助类
 * @author xiangjz
 * @date 2018年6月21日
 * @version 1.0.0
 *
 */
public class File2MultipartFile implements MultipartFile {
	private File file;
	
	public File2MultipartFile(File file) {
		super();
		this.file = file;
	}
	
	public File2MultipartFile(){};
	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getName() {
		return file.getName();
	}

	public String getOriginalFilename() {
		return file.getPath();
	}

	public String getContentType() {
		return null;
	}

	public boolean isEmpty() {
		return false;
	}

	public long getSize() {
		return file.length();
	}

	public byte[] getBytes() throws IOException {
		return null;
	}

	public InputStream getInputStream() throws IOException {
		return new FileInputStream(file);
	}

	public void transferTo(File dest) throws IOException, IllegalStateException {
		dest = file;
	}

}
