package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础用户信息常量类
 * @author xiangjz
 * @editor xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseUserInfoConstant extends Constant {
    /**
     *  数据表字段名称 - userName - 用户名
     */
    public static final String USER_NAME = "userName";
    /**
     *  数据表字段名称 - password - 密码
     */
    public static final String PASSWORD = "password";
    /**
     *  数据表字段名称 - passwordForMD5 - 密码
     */
    public static final String PASSWORD_FOR_MD5 = "passwordForMD5";
    /**
     *  数据表字段名称 - errorCount - 密码错误次数
     */
    public static final String ERROR_COUNT = "errorCount";
    /**
     *  数据表字段名称 - salt - 盐
     */
    public static final String SALT = "salt";
    /**
     *  数据表字段名称 - state - 用户状态
     */
    public static final String STATE = "state";
    /**
     *  数据表字段名称 - headUrl - 头像
     */
    public static final String HEAD_URL = "headUrl";
    /**
     *  数据表字段名称 - email - 邮箱
     */
    public static final String EMAIL = "email";
    /**
     *  数据表字段名称 - nickName - 昵称
     */
    public static final String NICK_NAME = "nickName";
    /**
     *  数据表字段名称 - secretKey - 秘钥
     */
    public static final String SECRET_KEY = "secretKey";
    /**
     *  数据表字段名称 - phone - 电话
     */
    public static final String PHONE = "phone";
    /**
     *  数据表字段名称 - sex - 性别
     */
    public static final String SEX = "sex";
    /**
     *  数据表字段名称 - idCard - 身份证
     */
    public static final String ID_CARD = "idCard";
    /**
     *  数据表字段名称 - age - 年龄
     */
    public static final String AGE = "age";
    /**
     *  数据表字段名称 - education - 学历
     */
    public static final String EDUCATION = "education";
    /**
     *  数据表字段名称 - passwordType - 密碼類型
     */
    public static final String PASSWORD_TYPE = "passwordType";
    
    
    /**
     * 业务逻辑字段名称 - newPwd - 新密码
     */
    public static final String NEW_PWD = "newPwd";
    /**
     * 业务逻辑字段名称 - confirmPwd - 确认密码
     */
    public static final String CONFIRM_PWD = "confirmPwd";
    /**
     * 业务逻辑字段名称 - validateCode - 验证码
     */
    public static final String VALIDATE_CODE = "validateCode";
    /**
     * 业务逻辑字段名称 - type - 验证码来源类型
     */
    public static final String TYPE = "type";
    /**
     * 业务逻辑字段名称 - passwordType - 1 旧密码
     */
    public static final String PASSWORD_TYPE_1 = "1";
    /**
     * 业务逻辑字段名称 - passwordType - 2 新密码
     */
    public static final String PASSWORD_TYPE_2 = "2";
}
