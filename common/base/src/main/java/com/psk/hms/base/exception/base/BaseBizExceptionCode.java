package com.psk.hms.base.exception.base;

/**
 * <p>
 * base 业务异常类
 * </p>
 * 模块异常码 : 10000001 ~ 19999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class BaseBizExceptionCode {

	/***
	 * 用户模块
	 * 10010000~10019999
	 */
	//用户模块-登录-api验证
	public static final int USER_LOGIN_VALIDATE_ERROR = 10010101;
	//用户模块-登录-api验证
	public static final int USER_LOGIN_APIVALIDATE_ERROR = 10010102;
	//用户模块-登录-admin验证
	public static final int USER_LOGIN_ADMINVALIDATE_ERROR = 10010103;
	//用户模块-登录-通用异常
	public static final int USER_LOGIN_ERROR = 10010199;

	//用户模块-个人中心-修改密码
	public static final int USER_PERSONAL_CHANGEPASSWORD_ERROR = 10010201;
	//用户模块-个人中心-切换团队
	public static final int USER_PERSONAL_CHANGETEAM_ERROR = 10010202;
	//用户模块-个人中心-通用异常
	public static final int USER_PERSONAL_ERROR = 10010299;

	//用户模块-用户管理-增
	public static final int USER_MANAGE_SAVE_ERROR = 10010301;
	//用户模块-用户管理-删
	public static final int USER_MANAGE_DELETE_ERROR = 10010302;
	//用户模块-用户管理-查
	public static final int USER_MANAGE_SELECT_ERROR = 10010303;
	//用户模块-用户管理-改
	public static final int USER_MANAGE_UPDATE_ERROR = 10010304;
	//用户模块-用户管理-通用异常
	public static final int USER_MANAGE_ERROR = 10010399;

	//用户模块-注册-绑定wx
	public static final int USER_REGISTER_BINDWX_ERROR = 10010401;
	//用户模块-注册-绑定qt
	public static final int USER_REGISTER_BINDQT_ERROR = 10010402;
	//用户模块-注册-绑定api
	public static final int USER_REGISTER_BINDAPI_ERROR = 10010403;
	//用户模块-注册-通用异常
	public static final int USER_REGISTER_ERROR = 10010499;

	//用户模块-邀请-增
	public static final int USER_INVITE_INSERT_ERROR = 10010501;
	//用户模块-邀请-删
	public static final int USER_INVITE_DELETE_ERROR = 10010502;
	//用户模块-邀请-查
	public static final int USER_INVITE_SELECT_ERROR = 10010503;
	//用户模块-邀请-改
	public static final int USER_INVITE_UPDATE_ERROR = 10010504;
	//用户模块-邀请-通用异常
	public static final int USER_INVITE_ERROR = 10010599;

	//用户模块-单点登录-增
	public static final int USER_SSO_INSERT_ERROR = 10010601;
	//用户模块-单点登录-删
	public static final int USER_SSO_DELETE_ERROR = 10010602;
	//用户模块-单点登录-查
	public static final int USER_SSO_SELECT_ERROR = 10010603;
	//用户模块-单点登录-改
	public static final int USER_SSO_UPDATE_ERROR = 10010604;
	//用户模块-单点登录-通用异常
	public static final int USER_SSO_ERROR = 10010699;

	//用户模块-绑定轻推-增
	public static final int USER_BIND_INSERT_ERROR = 10010701;
	//用户模块-绑定轻推-删
	public static final int USER_BIND_DELETE_ERROR = 10010702;
	//用户模块-绑定轻推-查
	public static final int USER_BIND_SELECT_ERROR = 10010703;
	//用户模块-绑定轻推-改
	public static final int USER_BIND_UPDATE_ERROR = 10010704;
	//用户模块-绑定轻推-通用异常
	public static final int USER_BIND_ERROR = 10010799;

	//用户模块-通用异常
	public static final int USER_ERROR = 10019999;

	/***
	 * 团队类型模块
	 * 10020000~10029999
	 */
	//团队类型模块-团队类型管理-增
	public static final int TEAMTYPE_MANAGE_SAVE_ERROR = 10020101;
	//团队类型模块-团队类型管理-删
	public static final int TEAMTYPE_MANAGE_DELETE_ERROR = 10020102;
	//团队类型模块-团队类型管理-查
	public static final int TEAMTYPE_MANAGE_SELECT_ERROR = 10020103;
	//团队类型模块-团队类型管理-改
	public static final int TEAMTYPE_MANAGE_UPDATE_ERROR = 10020104;
	//团队类型模块-团队类型管理-通用异常
	public static final int TEAMTYPE_MANAGE_ERROR = 10020199;

	//团队类型模块-通用异常
	public static final int TEAMTYPE_ERROR = 10029999;

	/***
	 * 模块模块
	 * 10030000~10039999
	 */
	//模块模块-模块管理-增
	public static final int MODULE_MANAGE_SAVE_ERROR = 10030101;
	//模块模块-模块管理-删
	public static final int MODULE_MANAGE_DELETE_ERROR = 10030102;
	//模块模块-模块管理-查
	public static final int MODULE_MANAGE_SELETE_ERROR = 10030103;
	//模块模块-模块管理-改
	public static final int MODULE_MANAGE_UPDATE_ERROR = 10030104;
	//模块模块-模块管理-通用异常
	public static final int MODULE_MANAGE_ERROR = 10030199;

	//模块模块-通用异常
	public static final int MODULE_ERROR = 10039999;

	/***
	 * 菜单模块
	 * 10040000~10049999
	 */
	//菜单模块-菜单管理-增
	public static final int MENU_MANAGE_SAVE_ERROR = 10040101;
	//菜单模块-菜单管理-删
	public static final int MENU_MANAGE_DELETE_ERROR = 10040102;
	//菜单模块-菜单管理-查
	public static final int MENU_MANAGE_SELETE_ERROR = 10040103;
	//菜单模块-菜单管理-改
	public static final int MENU_MANAGE_UPDATE_ERROR = 10040104;
	//菜单模块-菜单管理-授权
	public static final int MENU_MANAGE_AUTH_ERROR = 10040105;
	//菜单模块-菜单管理-通用异常
	public static final int MENU_MANAGE_ERROR = 10040199;

	//菜单模块-通用异常
	public static final int MENU_ERROR = 10049999;

	/***
	 * 功能模块
	 * 10050000~10059999
	 */
	//功能模块-功能管理-增
	public static final int OPERATE_MANAGE_SAVE_ERROR = 10050101;
	//功能模块-功能管理-删
	public static final int OPERATE_MANAGE_DELETE_ERROR = 10050102;
	//功能模块-功能管理-查
	public static final int OPERATE_MANAGE_SELETE_ERROR = 10050103;
	//功能模块-功能管理-改
	public static final int OPERATE_MANAGE_UPDATE_ERROR = 10050104;
	//功能模块-功能管理-授权
	public static final int OPERATE_MANAGE_AUTH_ERROR = 10050105;
	//功能模块-功能管理-通用异常
	public static final int OPERATE_MANAGE_ERROR = 10050199;

	//功能模块-权限验证-api验证
	public static final int OPERATE_VALIDATE_API_ERROR = 10050201;
	//功能模块-权限验证-admin验证
	public static final int OPERATE_VALIDATE_ADMIN_ERROR = 10050202;
	//功能模块-权限验证-open验证
	public static final int OPERATE_VALIDATE_OPEN_ERROR = 10050203;
	//功能模块-权限验证-通用异常
	public static final int OPERATE_VALIDATE_ERROR = 10050299;

	//功能模块-通用异常
	public static final int OPERATE_ERROR = 10059999;

	/***
	 * 授权模块
	 * 10060000~10069999
	 */
	//授权模块-授权管理-增
	public static final int OPERATEBIZ_MANAGE_SAVE_ERROR = 10060101;
	//授权模块-授权管理-删
	public static final int OPERATEBIZ_MANAGE_DELETE_ERROR = 10060102;
	//授权模块-授权管理-查
	public static final int OPERATEBIZ_MANAGE_SELECT_ERROR = 10060103;
	//授权模块-授权管理-改
	public static final int OPERATEBIZ_MANAGE_UPDATE_ERROR = 10060104;
	//授权模块-授权管理-通用异常
	public static final int OPERATEBIZ_MANAGE_ERROR = 10060199;

	//授权模块-权限包基础管理-增
	public static final int OPERATEBIZ_PACKAGE_SAVE_ERROR = 10060201;
	//授权模块-权限包基础管理-删
	public static final int OPERATEBIZ_PACKAGE_DELETE_ERROR = 10060202;
	//授权模块-权限包基础管理-查
	public static final int OPERATEBIZ_PACKAGE_SELECT_ERROR = 10060203;
	//授权模块-权限包基础管理-改
	public static final int OPERATEBIZ_PACKAGE_UPDATE_ERROR = 10060204;
	//授权模块-权限包基础管理-通用异常
	public static final int OPERATEBIZ_PACKAGE_ERROR = 10060299;

	//授权模块-功能权限包-增
	public static final int OPERATEBIZ_TO_PACKAGE_SAVE_ERROR = 10060301;
	//授权模块-功能权限包-删
	public static final int OPERATEBIZ_TO_PACKAGE_DELETE_ERROR = 10060302;
	//授权模块-功能权限包-通用异常
	public static final int OPERATEBIZ_TO_PACKAGE_ERROR = 10060399;

	//授权模块-团队通用角色关联-增
	public static final int TEAM_COMMONROLE_SAVE_ERROR = 10060401;
	//授权模块-团队通用角色关联-删
	public static final int TEAM_COMMONROLE_DELETE_ERROR = 10060402;
	//授权模块-团队通用角色关联-查
	public static final int TEAM_COMMONROLE_SELECT_ERROR = 10060403;
	//授权模块-团队通用角色关联-改
	public static final int TEAM_COMMONROLE_UPDATE_ERROR = 10060404;
	//授权模块-团队通用角色关联-通用异常
	public static final int TEAM_COMMONROLE_ERROR = 10060499;

	//授权模块-通用异常
	public static final int OPERATEBIZ_ERROR = 10069999;

	/***
	 * 字典模块
	 * 10070000~10079999
	 */
	//字典模块-字典管理-增
	public static final int DICTIONARY_MANAGE_SAVE_ERROR = 10070101;
	//字典模块-字典管理-删
	public static final int DICTIONARY_MANAGE_DELETE_ERROR = 10070102;
	//字典模块-字典管理-查
	public static final int DICTIONARY_MANAGE_SELECT_ERROR = 10070103;
	//字典模块-字典管理-改
	public static final int DICTIONARY_MANAGE_UPDATE_ERROR = 10070104;
	//字典模块-字典管理-通用异常
	public static final int DICTIONARY_MANAGE_ERROR = 10070199;

	//字典模块-通用异常
	public static final int DICTIONARY_ERROR = 10079999;

	/***
	 * 通用角色模块
	 * 10080000~10089999
	 */
	//通用角色模块-通用角色管理-增
	public static final int ROLE_MANAGE_SAVE_ERROR = 10080101;
	//通用角色模块-通用角色管理-删
	public static final int ROLE_MANAGE_DELETE_ERROR = 10080102;
	//通用角色模块-通用角色管理-查
	public static final int ROLE_MANAGE_SELECT_ERROR = 10080103;
	//通用角色模块-通用角色管理-改
	public static final int ROLE_MANAGE_UPDATE_ERROR = 10080104;
	//通用角色模块-通用角色管理-拖拽
	public static final int ROLE_MANAGE_DEAGE_ERROR = 10080105;
	//通用角色模块-通用角色管理-通用异常
	public static final int ROLE_MANAGE_ERROR = 10080199;

	//通用角色模块-通用异常
	public static final int ROLE_ERROR = 10089999;

	/***
	 * 角色员工关联模块
	 * 10090000~10099999
	 */
	//角色员工关联模块-角色员工关联管理-增
	public static final int ROLE_EMPLOYEE_MANAGE_SAVE_ERROR = 10090101;
	//角色员工关联模块-角色员工关联管理-删
	public static final int ROLE_EMPLOYEE_MANAGE_DELETE_ERROR = 10090102;
	//角色员工关联模块-角色员工关联管理-查
	public static final int ROLE_EMPLOYEE_MANAGE_SELECT_ERROR = 10090103;
	//角色员工关联模块-角色员工关联管理-改
	public static final int ROLE_EMPLOYEE_MANAGE_UPDATE_ERROR = 10090104;
	//角色员工关联模块-角色员工关联管理-通用异常
	public static final int ROLE_EMPLOYEE_MANAGE_ERROR = 10090199;

	//角色员工关联模块-通用异常
	public static final int ROLE_EMPLOYEE_ERROR = 10099999;

	/***
	 * 操作日志模块
	 * 10100000~10109999
	 */
	//操作日志模块-操作日志管理-增
	public static final int LOGOPERATE_MANAGE_SAVE_ERROR = 10100101;
	//操作日志模块-操作日志管理-删
	public static final int LOGOPERATE_MANAGE_DELETE_ERROR = 10100102;
	//操作日志模块-操作日志管理-查
	public static final int LOGOPERATE_MANAGE_SELECT_ERROR = 10100103;
	//操作日志模块-操作日志管理-改
	public static final int LOGOPERATE_MANAGE_UPDATE_ERROR = 10100104;
	//操作日志模块-操作日志管理-导出
	public static final int LOGOPERATE_MANAGE_EXPORT_ERROR = 10100105;
	//操作日志模块-操作日志管理-通用异常
	public static final int LOGOPERATE_MANAGE_ERROR = 10100199;

	//操作日志模块-通用异常
	public static final int LOGOPERATE_ERROR = 10109999;

	/***
	 * poi导入配置模块
	 * 10110000~10119999
	 */
	//poi导入配置模块-poi导入配置管理-增
	public static final int IMPORT_EXCEL_TEMPLATE_MANAGE_SAVE_ERROR = 10110101;
	//poi导入配置模块-poi导入配置管理-删
	public static final int IMPORT_EXCEL_TEMPLATE_MANAGE_DELETE_ERROR = 10110102;
	//poi导入配置模块-poi导入配置管理-查
	public static final int IMPORT_EXCEL_TEMPLATE_MANAGE_SELECT_ERROR = 10110103;
	//poi导入配置模块-poi导入配置管理-改
	public static final int IMPORT_EXCEL_TEMPLATE_MANAGE_UPDATE_ERROR = 10110104;
	//poi导入配置模块-poi导入配置管理-导出
	public static final int IMPORT_EXCEL_TEMPLATE_MANAGE_EXPORT_ERROR = 10110105;
	//poi导入配置模块-poi导入配置管理-通用异常
	public static final int IMPORT_EXCEL_TEMPLATE_MANAGE_ERROR = 10110199;

	//poi导入配置模块-通用异常
	public static final int IMPORT_EXCEL_TEMPLATE_ERROR = 10119999;

	/***
	 * 缓存模块
	 * 10120000~10129999
	 */
	//缓存模块-redis-set
	public static final int CACHE_REDIS_SET_ERROR = 10120101;
	//缓存模块-redis-get
	public static final int CACHE_REDIS_GET_ERROR = 10120102;
	//缓存模块-redis-通用异常
	public static final int CACHE_REDIS_ERROR = 10120199;

	//缓存模块-通用异常
	public static final int CACHE_ERROR = 10129999;

	/***
	 * 文件类型模块
	 * 10160000~10169999
	 */
	//文件类型模块-文件类型管理-增
	public static final int FILETYPE_MANAGE_SAVE_ERROR = 10160101;
	//文件类型模块-文件类型管理-删
	public static final int FILETYPE_MANAGE_DELETE_ERROR = 10160102;
	//文件类型模块-文件类型管理-查
	public static final int FILETYPE_MANAGE_SELECT_ERROR = 10160103;
	//文件类型模块-文件类型管理-改
	public static final int FILETYPE_MANAGE_UPDATE_ERROR = 10160104;
	//文件类型模块-文件类型管理-通用异常
	public static final int FILETYPE_MANAGE_ERROR = 10160199;

	//文件类型模块-通用异常
	public static final int FILETYPE_ERROR = 10169999;

	/***
	 * 文件模块
	 * 10170000~10179999
	 */
	//文件模块-文件管理-增
	public static final int FILE_MANAGE_SAVE_ERROR = 10170101;
	//文件模块-文件管理-删
	public static final int FILE_MANAGE_DELETE_ERROR = 10170102;
	//文件模块-文件管理-查
	public static final int FILE_MANAGE_SELECT_ERROR = 10170103;
	//文件模块-文件管理-改
	public static final int FILE_MANAGE_UPDATE_ERROR = 10170104;
	//文件模块-文件管理-上传
	public static final int FILE_MANAGE_UPLOAD_ERROR = 10170105;
	//文件模块-文件管理-下载
	public static final int FILE_MANAGE_DOWNLOAD_ERROR = 10170106;
	//文件模块-文件管理-通用异常
	public static final int FILE_MANAGE_ERROR = 10170199;

	//文件模块-通用异常
	public static final int FILE_ERROR = 10179999;

	/***
	 * token模块
	 * 10200000~10209999
	 */
	//token模块-access-token管理-增
	public static final int TOKEN_ACCESSMANAGE_SAVE_ERROR = 10200101;
	//token模块-access-token管理-删
	public static final int TOKEN_ACCESSMANAGE_DELETE_ERROR = 10200102;
	//token模块-access-token管理-查
	public static final int TOKEN_ACCESSMANAGE_SELECT_ERROR = 10200103;
	//token模块-access-token管理-改
	public static final int TOKEN_ACCESSMANAGE_UPDATE_ERROR = 10200104;
	//token模块-access-token管理-刷新
	public static final int TOKEN_ACCESSMANAGE_REFRESH_ERROR = 10200105;
	//token模块-access-token管理-通用异常
	public static final int TOKEN_ACCESSMANAGE_ERROR = 10200199;

	//token模块-auth-token管理-增
	public static final int TOKEN_AUTHMANAGE_SAVE_ERROR = 10200201;
	//token模块-auth-token管理-删
	public static final int TOKEN_AUTHMANAGE_DELETE_ERROR = 10200202;
	//token模块-auth-token管理-查
	public static final int TOKEN_AUTHMANAGE_SELECT_ERROR = 10200203;
	//token模块-auth-token管理-改
	public static final int TOKEN_AUTHMANAGE_UPDATE_ERROR = 10200204;
	//token模块-auth-token管理-刷新
	public static final int TOKEN_AUTHMANAGE_REFRESH_ERROR = 10200205;
	//token模块-auth-token管理-通用异常
	public static final int TOKEN_AUTHMANAGE_ERROR = 10200299;

	//token模块-验证模块-token不存在
	public static final int TOKEN_VALIDATE_NULL_ERROR = 10200301;
	//token模块-验证模块-过期
	public static final int TOKEN_VALIDATE_EXPIRED_ERROR = 10200302;
	//token模块-验证模块-验证失败
	public static final int TOKEN_VALIDATE_FAIL_ERROR = 10200303;
	//token模块-验证模块-通用异常
	public static final int TOKEN_VALIDATE_ERROR = 10200399;

	//token模块-通用异常
	public static final int TOKEN_ERROR = 10209999;

	/***
	 * 基础平台
	 */
	//基础平台-连接异常
	public static final int CONNECT_ERROR = 19999997;
	//基础平台-参数异常
	public static final int PARAM_ERROR = 19999998;
	//基础平台-通用异常
	public static final int BASE_ERROR = 19999999;

}
