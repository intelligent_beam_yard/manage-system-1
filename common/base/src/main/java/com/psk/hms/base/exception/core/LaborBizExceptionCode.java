package com.psk.hms.base.exception.core;

/**
 * <p>
 * labor 业务异常类
 * </p>
 * 模块异常码 : 24000001 ~ 24999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class LaborBizExceptionCode {
		
	/***
	 * 劳务模块
	 * 24010000~24019999
	 */
	//劳务模块-公司-增 
	public static final int LABOR_COMPANY_INSERT_ERROR = 24010101;
	//劳务模块-公司-删
	public static final int LABOR_COMPANY_DELETE_ERROR = 24010102;
	//劳务模块-公司-查
	public static final int LABOR_COMPANY_SELECT_ERROR = 24010103;
	//劳务模块-公司-改
	public static final int LABOR_COMPANY_UPDATE_ERROR = 24010104;
	//劳务模块-公司-通用异常
	public static final int LABOR_COMPANY_MANAGE_ERROR = 24010199;
	
	//劳务模块-人员-增
	public static final int LABOR_INFO_INSERT_ERROR = 24010201;
	//劳务模块-人员-删
	public static final int LABOR_INFO_DELETE_ERROR = 24010202;
	//劳务模块-人员-查
	public static final int LABOR_INFO_SELECT_ERROR = 24010203;
	//劳务模块-人员-改
	public static final int LABOR_INFO_UPDATE_ERROR = 24010204;
	//劳务模块-人员-通用异常
	public static final int LABOR_INFO_MANAGE_ERROR = 24010299;

	//劳务模块-基础班组-增
	public static final int LABOR_WORKTEAM_INSERT_ERROR = 24010301;
	//劳务模块-基础班组-删
	public static final int LABOR_WORKTEAM_DELETE_ERROR = 24010302;
	//劳务模块-基础班组-查
	public static final int LABOR_WORKTEAM_SELECT_ERROR = 24010303;
	//劳务模块-基础班组-改
	public static final int LABOR_WORKTEAM_UPDATE_ERROR = 24010304;
	//劳务模块-基础班组-通用异常
	public static final int LABOR_WORKTEAM_MANAGE_ERROR = 24010399;
	
	//劳务模块-基础工种-增
	public static final int LABOR_WORKTYPE_INSERT_ERROR = 24010401;
	//劳务模块-基础工种-删
	public static final int LABOR_WORKTYPE_DELETE_ERROR = 24010402;
	//劳务模块-基础工种-查
	public static final int LABOR_WORKTYPE_SELECT_ERROR = 24010403;
	//劳务模块-基础工种-改
	public static final int LABOR_WORKTYPE_UPDATE_ERROR = 24010404;
	//劳务模块-基础工种-通用异常
	public static final int LABOR_WORKTYPE_MANAGE_ERROR = 24010499;
	
	//劳务模块-奖惩信息-奖惩配置-增
	public static final int LABOR_VOLATION_SETTING_INSERT_ERROR = 24010501;
	//劳务模块-奖惩信息-奖惩配置-删
	public static final int LABOR_VOLATION_SETTING_DELETE_ERROR = 24010502;
	//劳务模块-奖惩信息-奖惩配置-查
	public static final int LABOR_VOLATION_SETTING_SELECT_ERROR = 24010503;
	//劳务模块-奖惩信息-奖惩配置-改
	public static final int LABOR_VOLATION_SETTING_UPDATE_ERROR = 24010504;
	//劳务模块-奖惩信息-奖惩配置-通用异常
	public static final int LABOR_VOLATION_SETTING_MANAGE_ERROR = 24010599;
	
	//劳务模块-资格证书-增
	public static final int LABOR_QUALIFICATION_INSERT_ERROR = 24010601;
	//劳务模块-资格证书-删
	public static final int LABOR_QUALIFICATION_DELETE_ERROR = 24010602;
	//劳务模块-资格证书-查
	public static final int LABOR_QUALIFICATION_SELECT_ERROR = 24010603;
	//劳务模块-资格证书-改
	public static final int LABOR_QUALIFICATION_UPDATE_ERROR = 24010604;
	//劳务模块-资格证书-通用异常
	public static final int LABOR_QUALIFICATION_MANAGE_ERROR = 24010699;
	
	// 劳务模块-奖惩附件删除失败
	public static final int LABOR_VOLATION_ATTACH_DELETE_ERROR = 24010701;
	// 劳务模块-奖惩附件上传失败
	public static final int LABOR_VOLATION_ATTACH_UPLOAD_ERROR = 24010702;
	
	// 劳务模块-奖惩信息流水保存失败
	public static final int LABOR_VOLATIONINFO_RECORD_INSERT_ERROR = 24010801;
	
	// 劳务模块-奖惩信息奖惩配置中间表删除失败
	public static final int LABOR_VOLATION_SETTING_RELATION_DELETE_ERROR = 24010902;
	// 劳务模块-奖惩信息奖惩配置中间表插入失败
	public static final int LABOR_VOLATION_SETTING_RELATION_INSERT_ERROR = 24010901;
	
	// 劳务模块-奖惩信息删除失败
	public static final int LABOR_VOLATIONINFO_DELETE_ERROR = 24011002;
	// 劳务模块-奖惩信息插入失败
	public static final int LABOR_VOLATIONINFO_INSERT_ERROR = 24011001;
	
	// 劳务模块-劳务奖惩中间表删除失败
	public static final int LABOR_VOLATION_RELATION_DELETE_ERROR = 24011102;
	// 劳务模块-劳务奖惩中间表插入失败
	public static final int LABOR_VOLATION_RELATION_INSERT_ERROR = 24011101;
	
	//劳务模块-劳务岗位-劳务岗位管理-增
	public static final int LABOR_JOBS_INSERT_ERROR = 24011201;
	//劳务模块-劳务岗位-劳务岗位管理-删
	public static final int LABOR_JOBS_DELETE_ERROR = 24011202;
	//劳务模块-劳务岗位-劳务岗位管理-查
	public static final int LABOR_JOBS_SELECT_ERROR = 24011203;
	//劳务模块-劳务岗位-劳务岗位管理-改
	public static final int LABOR_JOBS_UPDATE_ERROR = 24011204;
	//劳务模块-劳务岗位-劳务岗位管理-通用异常
	public static final int LABOR_JOBS_MANAGE_ERROR = 2401129;
	
	//劳务模块-劳务组织信息-劳务组织信息管理-增
	public static final int LABOR_ORGNATION_INFO_INSERT_ERROR = 24011301;
	//劳务模块-劳务组织信息-劳务组织信息管理-删
	public static final int LABOR_ORGNATION_INFO_DELETE_ERROR = 24011302;
	//劳务模块-劳务组织信息-劳务组织信息管理-查
	public static final int LABOR_ORGNATION_INFO_SELECT_ERROR = 24011303;
	//劳务模块-劳务组织信息-劳务组织信息管理-改
	public static final int LABOR_ORGNATION_INFO_UPDATE_ERROR = 24011304;
	//劳务模块-劳务组织信息-劳务组织信息管理-通用异常
	public static final int LABOR_ORGNATION_INFO_MANAGE_ERROR = 24011399;
	
	//劳务模块-劳务与组织关联信息-劳务与组织关联信息管理-增
	public static final int LABOR_ORGNATION_INSERT_ERROR = 24011401;
	//劳务模块-劳务与组织关联信息-劳务与组织关联信息管理-删
	public static final int LABOR_ORGNATION_DELETE_ERROR = 24011402;
	//劳务模块-劳务与组织关联信息-劳务与组织关联信息管理-查
	public static final int LABOR_ORGNATION_SELECT_ERROR = 24011403;
	//劳务模块-劳务与组织关联信息-劳务与组织关联信息管理-改
	public static final int LABOR_ORGNATION_UPDATE_ERROR = 24011404;
	//劳务模块-劳务与组织关联信息-劳务与组织关联信息管理-通用异常
	public static final int LABOR_ORGNATION_MANAGE_ERROR = 24011499;
	
	//劳务模块-项目人员信息-项目人员信息管理-增
	public static final int PROJECT_LABOR_INFO_INSERT_ERROR = 24011501;
	//劳务模块-项目人员信息-项目人员信息管理-删
	public static final int PROJECT_LABOR_INFO_DELETE_ERROR = 24011502;
	//劳务模块-项目人员信息-项目人员信息管理-查
	public static final int PROJECT_LABOR_INFO_SELECT_ERROR = 24011503;
	//劳务模块-项目人员信息-项目人员信息管理-改
	public static final int PROJECT_LABOR_INFO_UPDATE_ERROR = 24011504;
	//劳务模块-项目人员信息-项目人员信息管理-通用异常
	public static final int PROJECT_LABOR_INFO_MANAGE_ERROR = 24011599;

	//劳务模块-通用异常
	public static final int LABOR_MODULE_ERROR = 24019999;
}
