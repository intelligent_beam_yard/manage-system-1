package com.psk.hms.base.util.validator.field.impl;

import java.util.Map;

import com.psk.hms.base.util.validator.field.AbstractMapValidator;
import com.psk.hms.base.util.validator.field.RequiredKeyGenerator;
import com.psk.hms.base.util.validator.result.MapValidateResult;

public class ExistFieldValidator<K, V> extends AbstractMapValidator<K, V> {
	
	public ExistFieldValidator(K key) {
		super(key);
		this.errorMsg = getDefaultErrorMsg(key);
	}
	
	public ExistFieldValidator(K key, String errorMsg) {
		super(key, errorMsg);
	}

	@Override
	public MapValidateResult<K> check(Map<K, V> map) {
		MapValidateResult<K> result = new MapValidateResult<>(key);
		if(key == null || !map.containsKey(key))
			return result.failure(errorMsg);
		return result.success();
	}
	
	@Override
	public String getDefaultErrorMsg(K key) {
		return new RequiredKeyGenerator<>().getDefaultAbsentMsg(key);
	}

}
