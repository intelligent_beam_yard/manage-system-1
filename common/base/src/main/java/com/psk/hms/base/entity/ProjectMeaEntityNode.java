package com.psk.hms.base.entity;

import java.io.Serializable;

/**
 * 项目测量实体树
 * 
 * @author xiangjz
 * @date 2018-8-17
 * @version 1.0.0
 *
 */
public class ProjectMeaEntityNode implements Serializable {
	private static final long serialVersionUID = 1L;

	// 实体Id
	private String id;
	// 实体名称
	private String name;
	// 父级Id
	private String parentId;
	// 爆点个数
	private int burstPoint;
	//合格总数
	private int qualified_point;
	//未完成总数
	private int unfinished_point;
	

	public ProjectMeaEntityNode() {
	}

	public ProjectMeaEntityNode(String id, String name, int burstPoint, int qualifiedpoint,
			int unfinishedPoint) {
		super();
		this.id = id;
		this.name = name;
		this.burstPoint = burstPoint;
		this.qualified_point = qualifiedpoint;
		this.unfinished_point = unfinishedPoint;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getBurstPoint() {
		return burstPoint;
	}

	public void setBurstPoint(int burstPoint) {
		this.burstPoint = burstPoint;
	}

	public int getQualified_point() {
		return qualified_point;
	}

	public void setQualified_point(int qualified_point) {
		this.qualified_point = qualified_point;
	}

	public int getUnfinished_point() {
		return unfinished_point;
	}

	public void setUnfinished_point(int unfinished_point) {
		this.unfinished_point = unfinished_point;
	}

}
