package com.psk.hms.base.exception.core;

/**
 * <p>
 * base 业务异常类
 * </p>
 * 模块异常码 : 20000001 ~ 20999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class TeamBizExceptionCode {
	
	/***
	 * 知识库模块
	 * 20010000~20019999
	 */
	//知识库模块-知识库管理-增 
	public static final int KNOWLEDGE_MANAGE_SAVE_ERROR = 20010101;
	//知识库模块-知识库管理-删
	public static final int KNOWLEDGE_MANAGE_DELETE_ERROR = 20010102;
	//知识库模块-知识库管理-查
	public static final int KNOWLEDGE_MANAGE_SELECT_ERROR = 20010103;
	//知识库模块-知识库管理-改
	public static final int KNOWLEDGE_MANAGE_UPDATE_ERROR = 20010104;
	//知识库模块-知识库管理-通用异常
	public static final int KNOWLEDGE_MANAGE_ERROR = 20010199;
	
	//知识库模块-通用异常
	public static final int KNOWLEDGE_ERROR = 20019999;
	
	/***
	 * 广告模块
	 * 20020000~20029999
	 */
	//广告模块-广告管理-增
	public static final int ADS_MANAGE_SAVE_ERROR = 20020101;
	//广告模块-广告管理-删
	public static final int ADS_MANAGE_DELETE_ERROR = 20020102;
	//广告模块-广告管理-查
	public static final int ADS_MANAGE_SELECT_ERROR = 20020103;
	//广告模块-广告管理-改
	public static final int ADS_MANAGE_UPDATE_ERROR = 20020104;
	//广告模块-广告管理-通用异常
	public static final int ADS_MANAGE_ERROR = 20020199;
	
	//广告模块-通用异常
	public static final int ADS_ERROR = 20029999;
	
	/***
	 * 人员消息模块
	 * 20030000~20039999
	 */
	//人员消息模块-人员消息管理-增
	public static final int EMPLOYEEMSG_MANAGE_SAVE_ERROR = 20030101;
	//人员消息模块-人员消息管理-删
	public static final int EMPLOYEEMSG_MANAGE_DELETE_ERROR = 20030102;
	//人员消息模块-人员消息管理-查
	public static final int EMPLOYEEMSG_MANAGE_SELECT_ERROR = 20030103;
	//人员消息模块-人员消息管理-改
	public static final int EMPLOYEEMSG_MANAGE_UPDATE_ERROR = 20030104;
	//人员消息模块-人员消息管理-通用异常
	public static final int EMPLOYEEMSG_MANAGE_ERROR = 20030199;
	
	//人员消息模块-通用异常
	public static final int EMPLOYEEMSG_ERROR = 20039999;
	
	/***
	 * 职务模块
	 * 20040000~20049999
	 */
	//职务模块-职务管理-增
	public static final int JOB_MANAGE_SAVE_ERROR = 20040101;
	//职务模块-职务管理-删
	public static final int JOB_MANAGE_DELETE_ERROR = 20040102;
	//职务模块-职务管理-查
	public static final int JOB_MANAGE_SELECT_ERROR = 20040103;
	//职务模块-职务管理-改
	public static final int JOB_MANAGE_UPDATE_ERROR = 20040104;
	//职务模块-职务管理-通用异常
	public static final int JOB_MANAGE_ERROR = 20040199;
	
	//职务模块-通用异常
	public static final int JOB_ERROR = 20049999;
	
	/***
	 * 团队模块
	 * 20050000~20059999
	 */
	//团队模块-组织-增
	public static final int TEAM_ORG_INSERT_ERROR = 20050101;
	//团队模块-组织-删
	public static final int TEAM_ORG_DELETE_ERROR = 20050102;
	//团队模块-组织-查
	public static final int TEAM_ORG_SELECT_ERROR = 20050103;
	//团队模块-组织-改
	public static final int TEAM_ORG_UPDATE_ERROR = 20050104;
	//团队模块-组织-通用异常
	public static final int TEAM_ORG_MANAGE_ERROR = 20050199;
	
	//团队模块-人员职位-增
	public static final int TEAM_JOB_EMPLOYEE_INSERT_ERROR = 20050201;
	//团队模块-人员职位-删
	public static final int TEAM_JOB_EMPLOYEE_DELETE_ERROR = 20050202;
	//团队模块-人员职位-查
	public static final int TEAM_JOB_EMPLOYEE_SELECT_ERROR = 20050203;
	//团队模块-人员职位-改
	public static final int TEAM_JOB_EMPLOYEE_UPDATE_ERROR = 20050204;
	//团队模块-人员职位-通用异常
	public static final int TEAM_JOB_EMPLOYEE_MANAGE_ERROR = 20050299;	
	
	
	//团队模块-动态-增
	public static final int TEAM_DYNAMIC_INSERT_ERROR = 20050301;
	//团队模块-动态-删
	public static final int TEAM_DYNAMIC_DELETE_ERROR = 20050302;
	//团队模块-动态-查
	public static final int TEAM_DYNAMIC_SELECT_ERROR = 20050303;
	//团队模块-动态-改
	public static final int TEAM_DYNAMIC_UPDATE_ERROR = 20050304;
	//团队模块-动态-通用异常
	public static final int TEAM_DYNAMIC_MANAGE_ERROR = 20050399;	
	
	
	//团队模块-动态评论-增
	public static final int TEAM_DYNAMIC_CMT_INSERT_ERROR = 20050401;
	//团队模块-动态评论-删
	public static final int TEAM_DYNAMIC_CMT_DELETE_ERROR = 20050402;
	//团队模块-动态评论-查
	public static final int TEAM_DYNAMIC_CMT_SELECT_ERROR = 20050403;
	//团队模块-动态评论-改
	public static final int TEAM_DYNAMIC_CMT_UPDATE_ERROR = 20050404;
	//团队模块-动态评论-通用异常
	public static final int TEAM_DYNAMIC_CMT_MANAGE_ERROR = 20050499;	
	
	//团队模块-动态附件-增
	public static final int TEAM_DYNAMIC_ATTACH_INSERT_ERROR = 20050501;
	//团队模块-动态附件-删
	public static final int TEAM_DYNAMIC_ATTACH_DELETE_ERROR = 20050502;
	//团队模块-动态附件-查
	public static final int TEAM_DYNAMIC_ATTACH_SELECT_ERROR = 20050503;
	//团队模块-动态附件-改
	public static final int TEAM_DYNAMIC_ATTACH_UPDATE_ERROR = 20050504;
	//团队模块-动态附件-通用异常
	public static final int TEAM_DYNAMIC_ATTACH_MANAGE_ERROR = 20050599;	
	
	//团队模块-公告-增
	public static final int TEAM_PUBLIC_NEWS_INSERT_ERROR = 20050601;
	//团队模块-公告-删
	public static final int TEAM_PUBLIC_NEWS_DELETE_ERROR = 20050602;
	//团队模块-公告-查
	public static final int TEAM_PUBLIC_NEWS_SELECT_ERROR = 20050603;
	//团队模块-公告-改
	public static final int TEAM_PUBLIC_NEWS_UPDATE_ERROR = 20050604;
	//团队模块-公告-通用异常
	public static final int TEAM_PUBLIC_NEWS_MANAGE_ERROR = 20050699;	
	
	//团队模块-应用屏蔽-增
	public static final int TEAM_APP_SHOW_INSERT_ERROR = 20050701;
	//团队模块-应用屏蔽-删
	public static final int TEAM_APP_SHOW_DELETE_ERROR = 20050702;
	//团队模块-应用屏蔽-查
	public static final int TEAM_APP_SHOW_SELECT_ERROR = 20050703;
	//团队模块-应用屏蔽-改
	public static final int TEAM_APP_SHOW_UPDATE_ERROR = 20050704;
	//团队模块-应用屏蔽-通用异常
	public static final int TEAM_APP_SHOW_MANAGE_ERROR = 20050799;	
	
	//团队模块-成员角色映射-增
	public static final int TEAM_EMPLOYEE_ROLE_INSERT_ERROR = 20050801;
	//团队模块-成员角色映射-删
	public static final int TEAM_EMPLOYEE_ROLE_DELETE_ERROR = 20050802;
	//团队模块-成员角色映射-查
	public static final int TEAM_EMPLOYEE_ROLE_SELECT_ERROR = 20050803;
	//团队模块-成员角色映射-改
	public static final int TEAM_EMPLOYEE_ROLE_UPDATE_ERROR = 20050804;
	//团队模块-成员角色映射-通用异常
	public static final int TEAM_EMPLOYEE_ROLE_MANAGE_ERROR = 20050899;	
	
	//团队模块-角色管理-增
	public static final int TEAM_ROLE_SAVE_ERROR = 20050901;
	//团队模块-角色管理-删
	public static final int TEAM_ROLE_DELETE_ERROR = 20050902;
	//团队模块-角色管理-查
	public static final int TEAM_ROLE_SELECT_ERROR = 20050903;
	//团队模块-角色管理-改
	public static final int TEAM_ROLE_UPDATE_ERROR = 20050904;
	//团队模块-角色管理-通用异常
	public static final int TEAM_ROLE_UPDATE_MANAGE_ERROR = 20050999;	
	
	//团队模块-公司关联-增
	public static final int TEAM_COMPANY_INSERT_ERROR = 20051001;
	//团队模块-公司关联-删
	public static final int TEAM_COMPANY_DELETE_ERROR = 20051002;
	//团队模块-公司关联-查
	public static final int TEAM_COMPANY_SELECT_ERROR = 20051003;
	//团队模块-公司关联-改
	public static final int TEAM_COMPANY_UPDATE_ERROR = 20051004;
	//团队模块-公司关联-通用异常
	public static final int TEAM_COMPANYE_MANAGE_ERROR = 20051099;
	
	//团队模块-团队成员关联-增
	public static final int TEAM_EMPLOYEE_INSERT_ERROR = 20051101;
	//团队模块-团队成员关联-删
	public static final int TEAM_EMPLOYEE_DELETE_ERROR = 20051102;
	//团队模块-团队成员关联-查
	public static final int TEAM_EMPLOYEE_SELECT_ERROR = 20051103;
	//团队模块-团队成员关联-改
	public static final int TEAM_EMPLOYEE_UPDATE_ERROR = 20051104;
	//团队模块-团队成员关联-通用异常
	public static final int TEAM_EMPLOYEEE_MANAGE_ERROR = 20051199;	
	
	//团队模块-通用异常
	public static final int TEAM_ERROR = 20059999;

}
