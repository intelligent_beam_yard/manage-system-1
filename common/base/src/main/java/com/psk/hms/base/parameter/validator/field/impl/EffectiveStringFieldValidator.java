package com.psk.hms.base.parameter.validator.field.impl;

import com.psk.hms.base.parameter.validator.result.MapValidateResult;
import com.psk.hms.base.util.string.StringUtil;

public class EffectiveStringFieldValidator<K, V> extends TypeFieldValidator<K, V> {

	public EffectiveStringFieldValidator(K key, boolean keyRequired) {
		super(key, String.class, keyRequired);
	}

	public EffectiveStringFieldValidator(K key, boolean keyRequired, String errorMsg) {
		super(key, String.class, keyRequired, errorMsg);
	}

	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> mapValidateResult = super.checkEntity(key, value);
		if (!mapValidateResult.isPass()) {
			return mapValidateResult;
		}
		if (StringUtil.isEmpty(value.toString().trim())) {
			return mapValidateResult.failure(errorMsg);
		}
		return mapValidateResult;
	}

	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为非空字符串");
		return sb.toString();
	}

}
