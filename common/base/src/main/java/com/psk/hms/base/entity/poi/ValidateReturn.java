package com.psk.hms.base.entity.poi;

/**
 * 参数验证返回值
 *
 * @author xiangjz
 */
public class ValidateReturn {

	// 验证是否通过
	private boolean passed;
	// 返回消息
	private String msg;
	// 返回数据
	private Object data;

	public boolean isPassed() {
		return passed;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @param passed 验证是否成功
	 * @param msg 返回消息
	 * @param data 返回数据
	 */
	public static ValidateReturn validateReturn(boolean passed, String msg, Object data) {
		ValidateReturn validateReturn = new ValidateReturn();
		validateReturn.setPassed(passed);
		validateReturn.setMsg(msg);
		validateReturn.setData(data);
		return validateReturn;
	}
	
	/**
	 * @param passed 验证是否成功
	 * @param msg 返回消息
	 */
	public static ValidateReturn validateReturn(boolean passed, String msg) {
		ValidateReturn validateReturn = new ValidateReturn();
		validateReturn.setPassed(passed);
		validateReturn.setMsg(msg);
		return validateReturn;
	}
}

