package com.psk.hms.base.entity;

import java.io.Serializable;

/**
 * 地区对象
 * 
 * @author xiangjz
 * @date 2018年5月16日
 * @version 1.0.0
 *
 */
public class BaseAreaNode implements Serializable {

	private static final long serialVersionUID = 1L;

	// 地区编码
	private String v;

	// 地区名称
	private String n;
	
	public BaseAreaNode() {}
	
	public BaseAreaNode(String v, String n) {
		this.v = v;
		this.n = n;
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

}
