package com.psk.hms.base.constant.common;
/**
 * 描述： 权限常量类
 * 作者： xiangjz
 * 时间： 2018年4月2日
 * 版本： 1.0v
 */
public class AuthConstant {

	
	/**
	 * 权限标示验证码
	 */
	public static final String COOKIE_AUTHMARK = "authmark";
	
	/**
	 * 权限验证：请求编码验证
	 */
	public static final String AUTH_ENCTYPE = "enctype";
	
	/**
	 * 权限验证：csrf值为空
	 */
	public static final String AUTH_CSRF_EMPTY = "csrf_empty";
	
	/**
	 * 权限验证：csrf校验失败
	 */
	public static final String AUTH_CSRF = "csrf";
	
	/**
	 * 权限验证：referer校验失败
	 */
	public static final String AUTH_REFERE = "referer";
	
	/**
	 * 权限验证：重复提交表单
	 */
	public static final String AUTH_FORM = "form";
	
	/**
	 * 验证码key
	 */
	public static final String REQUEST_AUTHCODE = "authCode";
	
	/**
	 * 权限验证：加密验证标示存储位置cookie
	 */
	public static final String AUTH_STORE_COOKIE = "cookie";
	
	/**
	 * 权限验证：没权限
	 */
	public static final String AUTH_NO_PERMISSIONS = "no_permissions";
	
	/**
	 * 权限验证：功能尚未配置
	 */
	public static final String AUTH_PERMISSIONS_NOT_EXSIT = "permissions_not_exsit";
	
	/**
	 * 权限验证：加密验证标示存储位置header
	 */
	public static final String AUTH_STORE_HEADER = "header";
	
	/**
	 * 权限验证：业务代码异常
	 */
	public static final String AUTH_EXCEPTION = "exception";
	
	/**
	 * 权限验证：未登录
	 */
	public static final String AUTH_NO_LOGIN = "no_login";
	
	/**
	 * 权限验证：请求方法验证
	 */
	public static final String AUTH_METHOD = "method";
}
