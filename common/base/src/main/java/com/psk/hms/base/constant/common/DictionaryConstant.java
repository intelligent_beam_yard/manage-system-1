package com.psk.hms.base.constant.common;
/**
 * 描述： 字典code常量类
 * 作者： xiangjz
 * 时间： 2018年6月4日
 * 版本： 1.0v
 */
public class DictionaryConstant {

	
	/**
	 * 基础-权限-用户-性别：男
	 */
	public static final String Dictionary_USER_SEX_0 ="qz_base_permission_user_sex_0";
	/**
	 * 基础-权限-用户-性别：女
	 */
	public static final String Dictionary_USER_SEX_1 ="qz_base_permission_user_sex_1";
	
	
	
	
	/**
	 * 基础-资源管理-文件-类型：用户头像
	 */
	public static final String Dictionary_FILE_TYPE_1="qz_base_resource_file_type_1";
	/**
	 * 基础-资源管理-文件-类型：动态
	 */
	public static final String Dictionary_FILE_TYPE_2="qz_base_resource_file_type_2";
	/**
	 * 基础-资源管理-文件-类型：安全巡检
	 */
	public static final String Dictionary_FILE_TYPE_3="qz_base_resource_file_type_3";
	/**
	 * 基础-资源管理-文件-类型：质量巡检
	 */
	public static final String Dictionary_FILE_TYPE_4="qz_base_resource_file_type_4";
	/**
	 * 基础-资源管理-文件-类型：视频封面
	 */
	public static final String Dictionary_FILE_TYPE_5="qz_base_resource_file_type_5";
	/**
	 * 基础-资源管理-文件-类型：模板
	 */
	public static final String Dictionary_FILE_TYPE_6="qz_base_resource_file_type_6";
	/**
	 * 基础-资源管理-文件-类型：知识库
	 */
	public static final String Dictionary_FILE_TYPE_7="qz_base_resource_file_type_7";

	/**
	 * 基础-资源管理-文件-类型：设备台账、人员、巡检
	 */
	public static final String Dictionary_FILE_TYPE_8="qz_base_resource_file_type_8";
	
	/**
	 * 基础-资源管理-文件-类型：质量交底
	 */
	public static final String Dictionary_FILE_TYPE_9="qz_base_resource_file_type_9";
	
	/**
	 * 基础-资源管理-文件-类型：施工日志
	 */
	public static final String Dictionary_FILE_TYPE_10="qz_base_resource_file_type_10";
	
	/**
	 * 基础-资源管理-文件-类型：工资公示
	 */
	public static final String Dictionary_FILE_TYPE_11="qz_base_resource_file_type_11";
	
	/**
	 * 基础-资源管理-文件-类型：党建
	 */
	public static final String Dictionary_FILE_TYPE_12="qz_base_resource_file_type_12";
	
	/**
	 * 基础-资源管理-文件-知识库：文件夹
	 */
	public static final String DICTIONARY_FILE_REPOSITORY_0="qz_base_resource_file_repository_0";
	
	/**
	 * 基础-资源管理-文件-知识库：链接
	 */
	public static final String DICTIONARY_FILE_REPOSITORY_1="qz_base_resource_file_repository_1";
	/**
	 * 基础-资源管理-文件-知识库：文件
	 */
	public static final String DICTIONARY_FILE_REPOSITORY_2="qz_base_resource_file_repository_2";
	
	/**
	 * 基础-资源管理-模板-类型：劳务人员导入模板
	 */
	public static final String Dictionary_FILE_TEMPLATE_TYPE_1="qz_base_resource_file_template_type_1";
	/**
	 * 基础-资源管理-模板-类型：劳务人员批量导入模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_1="qz_base_resource_template_type_1";
	/**
	 * 基础-资源管理-模板-类型：员工批量导入模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_2="qz_base_resource_template_type_2";
	/**
	 * 基础-资源管理-模板-类型：安全巡检公共模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_3="qz_base_resource_template_type_3";
	/**
	 * 基础-资源管理-模板-类型：质量巡检公共模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_4="qz_base_resource_template_type_4";
	/**
	 * 基础-资源管理-模板-类型：安全巡检项目模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_5="qz_base_resource_template_type_5";
	/**
	 * 基础-资源管理-模板-类型：质量巡检项目模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_6="qz_base_resource_template_type_6";
	/**
	 * 基础-资源管理-模板-类型：智能安全带模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_7="qz_base_resource_template_type_7";
	
	/**
	 * 基础-资源管理-模板-类型：权限包批量导入模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_8="qz_base_resource_template_type_8";
	/**
	 * 基础-资源管理-模板-类型：功能批量导入模板
	 */
	public static final String Dictionary_RESOURCE_TEMPLATE_TYPE_9="qz_base_resource_template_type_9";
	
	/**
	 * 基础-资源管理-模板-类型：用户导入模板
	 */
	public static final String Dictionary_FILE_TEMPLATE_TYPE_2="qz_base_resource_file_template_type_2";
	/**
	 * 基础-资源管理-模板-类型：智能安全带导入模板
	 */
	public static final String Dictionary_FILE_TEMPLATE_TYPE_7="qz_base_resource_file_template_type_7";
	
	
	
	
	/**
	 * 基础-定时任务-worker：设备预警
	 */
	public static final String  Dictionary_WORKER_DEVICEWARNINGWORKER="qz_timertask_worker_devicewarningworker";
	/**
	 * 基础-定时任务-worker：设备在离线
	 */
	public static final String  Dictionary_WORKER_DEVICEONLINEWORKER="qz_timertask_worker_deviceonlineworker";
	/**
	 * 基础-定时任务-worker：测试worker
	 */
	public static final String  Dictionary_WORKER_TESTWORKER="qz_timerTask_worker_testWorker";
	
	
	
	/**
	 * 基础-公司-类型：股份有限公司
	 */
	public static final String  Dictionary_COMPANY_TYPE_1="qz_base_company_type_1";
	/**
	 * 基础-公司-类型：集体企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_2="qz_base_company_type_2";
	/**
	 * 基础-公司-类型：国防军事企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_3="qz_base_company_type_3";
	/**
	 * 基础-公司-类型：有限责任公司
	 */
	public static final String  Dictionary_COMPANY_TYPE_4="qz_base_company_type_4";
	/**
	 * 基础-公司-类型：国家行政企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_5="qz_base_company_type_5";
	/**
	 * 基础-公司-类型：公私合作企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_6="qz_base_company_type_6";
	/**
	 * 基础-公司-类型：中外合资企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_7="qz_base_company_type_7";
	/**
	 * 基础-公司-类型：社会组织机构
	 */
	public static final String  Dictionary_COMPANY_TYPE_8="qz_base_company_type_8";
	/**
	 * 基础-公司-类型：国防军事企业
	 */
	public static final String  Dictionary_COMPANY_TYPE_9="qz_base_company_type_9";
	/**
	 * 基础-公司-类型：国际组织机构
	 */
	public static final String  Dictionary_COMPANY_TYPE_10="qz_base_company_type_10";
	/**
	 * 基础-公司-类型：外资企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_11="qz_base_company_type_11";
	/**
	 * 基础-公司-类型：私营企业事业单位
	 */
	public static final String  Dictionary_COMPANY_TYPE_12="qz_base_company_type_12";
	/**
	 * 基础-公司-类型：全名所有制
	 */
	public static final String  Dictionary_COMPANY_TYPE_13="qz_base_company_type_13";
	
	
	
	
	/**
	 * 项目-巡检：安全
	 */
	public static final String  Dictionary_PROJECT_INSPECT_1="qz_project_inspect_1";
	
	/**
	 * 项目-巡检：质量
	 */
	public static final String  Dictionary_PROJECT_INSPECT_2="qz_project_inspect_2";
	
	
	
	/**
	 * 劳务-文件-图片：指纹
	 */
	public static final String  Dictionary_LABOR_FILE_PICTURE_FINGERPRINT="qz_labor_file_picture_fingerprint";
	/**
	 * 劳务-文件-图片：头像
	 */
	public static final String  DICTIONARY_LABOR_FILE_PICTURE_HEADPORTRAIT="qz_labor_file_picture_headportrait";
	/**
	 * 劳务-文件-图片：考勤
	 */
	public static final String  DICTIONARY_LABOR_FILE_PICTURE_CHECKING="qz_labor_file_picture_checking";
	/**
	 *  劳务-文件-图片：奖惩
	 */
	public static final String  DICTIONARY_LABOR_FILE_PICTURE_VOLATION="qz_labor_file_picture_volation";
	/**
	 *  劳务-文件-图片：资格证书
	 */
	public static final String  DICTIONARY_LABOR_FILE_PICTURE_QUALIFICATION="qz_labor_file_picture_qualification";
	/**
	 * 设备-设备类型：塔吊
	 */
	public static final String  Dictionary_deviceType_1="qz_equipment_deviceType_1";
	/**
	 * 设备-设备类型：升降机
	 */
	public static final String  Dictionary_deviceType_2="qz_equipment_deviceType_2";
	/**
	 * 设备-设备类型：卸料平台
	 */
	public static final String  Dictionary_deviceType_3="qz_equipment_deviceType_3";
	/**
	 * 设备-设备类型：环境监测
	 */
	public static final String  Dictionary_deviceType_4="qz_equipment_deviceType_4";
	/**
	 * 设备-设备类型：门禁
	 */
	public static final String  Dictionary_deviceType_5="qz_equipment_deviceType_5";
	/**
	 * 设备-设备类型：视频监控
	 */
	public static final String  Dictionary_deviceType_6="qz_equipment_deviceType_6";
	/**
	 * 设备-设备类型：人员定位
	 */
	public static final String  Dictionary_deviceType_7="qz_equipment_deviceType_7";
	/**
	 * 设备-设备类型：区域人员定位
	 */
	public static final String  Dictionary_deviceType_8="qz_equipment_deviceType_8";
	/**
	 * 设备-设备类型：人员分布定位
	 */
	public static final String  Dictionary_deviceType_9="qz_equipment_deviceType_9";
	/**
	 * 设备-设备类型：智能安全带
	 */
	public static final String  Dictionary_deviceType_10="qz_equipment_deviceType_10";
	/**
	 * 设备-设备类型：塔吊攀爬安全带
	 */
	public static final String  Dictionary_deviceType_11="qz_equipment_deviceType_11";
	/**
	 * 设备-设备类型：安全培训机
	 */
	public static final String  Dictionary_deviceType_12="qz_equipment_deviceType_12";

	
	
	/**
	 * 设备-设备状态：设备离线
	 */
	public static final String  Dictionary_deviceState_deviceState_0="qz_equipment_deviceState_deviceState_0";
	/**
	 * 设备-设备状态：设备正常
	 */
	public static final String  Dictionary_deviceState_deviceState_1="qz_equipment_deviceState_deviceState_1";
	/**
	 * 设备-设备状态：设备异常
	 */
	public static final String  Dictionary_deviceState_deviceState_2="qz_equipment_deviceState_deviceState_2";
	

	
	/**
	 * 设备-设备预警-预警操作：大于
	 */
	public static final String  Dictionary_deviceWarningMethod_1="qz_equipment_deviceWarning_deviceWarningMethod_1";
	/**
	 * 设备-设备预警-预警操作：大于等于
	 */
	public static final String  Dictionary_deviceWarningMethod_2="qz_equipment_deviceWarning_deviceWarningMethod_2";
	/**
	 * 设备-设备预警-预警操作：小于
	 */
	public static final String  Dictionary_deviceWarningMethod_3="qz_equipment_deviceWarning_deviceWarningMethod_3";
	/**
	 * 设备-设备预警-预警操作：小于等于
	 */
	public static final String  Dictionary_deviceWarningMethod_4="qz_equipment_deviceWarning_deviceWarningMethod_4";
	
	
	/**
	 * 设备：封面
	 */
	public static final String  Dictionary_equipment_cover="qz_equipment_cover";

	
	/**
	 * 设备-文件-图片：人员分布图
	 */
	public static final String  QZ_EQUIPMENT_FILE_PICTURE_DISTRIBUTION="qz_equipment_file_picture_distribution";
	
	/**
	 * 设备-视频监控截取
	 */
	public static final String    QZ_EQUIPMENT_FILE_VIDEO_1  ="qz_equipment_file_video_1";
	/**
	 * 设备-基站类型
	 */
	public static final String    QZ_EQUIPMENT_BASESATION_TYPE  ="  qz_equipment_basesation_type";
	/**
	 * 设备-基站类型：基站默认类型
	 */
	public static final String   QZ_EQUIPMENT_BASESATION_TYPE_1 =" qz_equipment_basesation_type_1";
	/**
	 * 设备-基站类型：升降机类型
	 */
	public static final String   QZ_EQUIPMENT_BASESATION_TYPE_2 =" qz_equipment_basesation_type_2";
	
	
	
	/**
	 * 设备-视频监控类型
	 */
	public static final String    QZ_EQUIPMENT_MONITOR_TYPE  ="  qz_equipment_monitor_type";
	/**
	 * 设备-视频监控类型：默认类型
	 */
	public static final String   QZ_EQUIPMENT_MONITOR_TYPE_1 =" qz_equipment_monitor_type_1";
	

	public static final String QZ_CLIENT = "qz_client";

}
