package com.psk.hms.base.util.validator.field.impl;

import com.psk.hms.base.util.validator.result.MapValidateResult;

public class LeftBorderFieldValidator<K, V, T extends Comparable<T>> extends CompareFieldValidator<K, V, T> {

	private T leftBorder;
	private boolean includeBorder;
	
	public LeftBorderFieldValidator(K key, T leftBorder, boolean includeBorder, boolean keyRequired) {
		super(key, leftBorder.getClass(), keyRequired);
		this.leftBorder = leftBorder;
		this.includeBorder = includeBorder;
		this.errorMsg = this.getDefaultErrorMsg(key);
	}
	
	public LeftBorderFieldValidator(K key, T leftBorder, boolean includeBorder, boolean keyRequired, String errorMsg) {
		super(key, leftBorder.getClass(), keyRequired, errorMsg);
		this.leftBorder = leftBorder;
		this.includeBorder = includeBorder;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public MapValidateResult<K> checkEntity(K key, V value) {
		MapValidateResult<K> result = super.checkEntity(key, value);
		if (!result.isPass()) {
			return result;
		}		
		T tValue = (T)value;
		boolean inRange = false;
		if(this.includeBorder) {
			inRange = this.inOrOnBorder(tValue);
		} else {
			inRange = this.inBorder(tValue);
		}		
		if(!inRange) {
			result.failure(errorMsg);
		}		
		return result;
	}
	
	private boolean inBorder(T value) {
		return this.isAboveBorder(leftBorder, value);
	}
	
	private boolean inOrOnBorder(T value) {
		return this.inBorder(value) || this.isEqualBorder(leftBorder, value);
	}
	
	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'应为").append(this.getType().getSimpleName()).append("类型，大于");
		if(this.includeBorder) {
			sb.append("等于");
		}
		sb.append(leftBorder);
		return sb.toString();
	}

	public T getLeftBorder() {
		return leftBorder;
	}
}
