package com.psk.hms.base.entity;
/**
 *巡检工具类
 *@author 创建者：ds
 *@date 创建时间：2018年4月28日 下午2:17:33
 *@version 1.0
 *@parameter 
*/
public class InspectVO {

	/*巡检记录ID*/
	private String recordId;
	/*巡查项*/
	private String checkItem;
	/*巡查时间*/
	private String checkTime;	
	/*人员ID*/
	private String userId;
	/*人员姓名*/
	private String userName;
	/*项目ID*/
	private String projectId;
	/*人员类型编码*/
	private String userTypeCode;
	/*检查结果*/
	private String checkResult;
	
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	
	public String getCheckItem() {
		return checkItem;
	}
	public void setCheckItem(String checkItem) {
		this.checkItem = checkItem;
	}
	public String getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getUserTypeCode() {
		return userTypeCode;
	}
	public void setUserTypeCode(String userTypeCode) {
		this.userTypeCode = userTypeCode;
	}
	public String getCheckResult() {
		return checkResult;
	}
	public void setCheckResult(String checkResult) {
		this.checkResult = checkResult;
	}
		
}
