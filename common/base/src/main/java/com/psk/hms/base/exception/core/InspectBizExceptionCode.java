package com.psk.hms.base.exception.core;

/**
 * <p>
 * inspect 业务异常类
 * </p>
 * 模块异常码 : 22000001 ~ 22999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class InspectBizExceptionCode {
	
	
	/***
	 * 巡检模块 
	 * 22010000~22019999
	 */
	//巡检模块-巡检项-增
	public static final int INSPECT_ITEM_INSERT_ERROR = 22010101;
	//巡检模块-巡检项-删
	public static final int INSPECT_ITEM_DELETE_ERROR = 22010102;
	//巡检模块-巡检项-查
	public static final int INSPECT_ITEM_SELECT_ERROR = 22010104;
	//巡检模块-巡检项-改
	public static final int INSPECT_ITEM_UPDATE_ERROR = 22010105;
	//巡检模块-巡检项-通用异常
	public static final int INSPECT_ITEM_ERROR = 22010199;
	
	//巡检模块-质量巡检-增
	public static final int INSPECT_QUALITY_INSERT_ERROR = 22010201;
	//巡检模块-质量巡检-删
	public static final int INSPECT_QUALITY_DELETE_ERROR = 22010202;
	//巡检模块-质量巡检-查
	public static final int INSPECT_QUALITY_FINDBY_ERROR = 22010203;
	//巡检模块-质量巡检-改
	public static final int INSPECT_QUALITY_UPDATE_ERROR = 22010204;
	//巡检模块-质量巡检-通用异常
	public static final int INSPECT_QUALITY_ERROR = 22010299;
	
	//巡检模块-质量巡检关联人员-增
	public static final int INSPECT_QUALITY_JOIN_PERSON_INSERT_ERROR = 22010301;
	//巡检模块-质量巡检关联人员-删
	public static final int INSPECT_QUALITY_JOIN_PERSON_DELETE_ERROR = 22010302;
	//巡检模块-质量巡检关联人员-查
	public static final int INSPECT_QUALITY_JOIN_PERSON_FINDBY_ERROR = 22010303;
	//巡检模块-质量巡检关联人员-改
	public static final int INSPECT_QUALITY_JOIN_PERSON_UPDATE_ERROR = 22010304;
	//巡检模块-质量巡检关联人员-通用异常
	public static final int INSPECT_QUALITY_JOIN_PERSON_ERROR = 22010399;
	
	//巡检模块-质量巡检结果-增
	public static final int INSPECT_QUALITY_RESULT_INSERT_ERROR = 22010401;
	//巡检模块-质量巡检结果-删
	public static final int INSPECT_QUALITY_RESULT_DELETE_ERROR = 22010402;
	//巡检模块-质量巡检结果-查
	public static final int INSPECT_QUALITY_RESULT_FINDBY_ERROR = 22010403;
	//巡检模块-质量巡检结果-改
	public static final int INSPECT_QUALITY_RESULT_UPDATE_ERROR = 22010404;
	//巡检模块-质量巡检结果-通用异常
	public static final int INSPECT_QUALITY_RESULT_ERROR = 22010499;
	
	//巡检模块-巡检记录关联巡检项-增
	public static final int INSPECT_RECORD_JOIN_ITEM_INSERT_ERROR = 22010501;
	//巡检模块-巡检记录关联巡检项-删
	public static final int INSPECT_RECORD_JOIN_ITEM_DELETE_ERROR = 22010502;
	//巡检模块-巡检记录关联巡检项-查
	public static final int INSPECT_RECORD_JOIN_ITEM_FINDBY_ERROR = 22010503;
	//巡检模块-巡检记录关联巡检项-改
	public static final int INSPECT_RECORD_JOIN_ITEM_UPDATE_ERROR = 22010504;
	//巡检模块-巡检记录关联巡检项-通用异常
	public static final int INSPECT_RECORD_JOIN_ITEM_ERROR = 22010599;
	
	//巡检模块-质量巡检性质-增
	public static final int INSPECT_QUALITY_PROPERTY_INSERT_ERROR = 22010601;
	//巡检模块-质量巡检性质-删
	public static final int INSPECT_QUALITY_PROPERTY_DELETE_ERROR = 22010602;
	//巡检模块-质量巡检性质-查
	public static final int INSPECT_QUALITY_PROPERTY_FINDBY_ERROR = 22010603;
	//巡检模块-质量巡检性质-改
	public static final int INSPECT_QUALITY_PROPERTY_UPDATE_ERROR = 22010604;
	//巡检模块-质量巡检性质-通用异常
	public static final int INSPECT_QUALITY_PROPERTY_ERROR = 22010699;
	
	//巡检模块-安全巡检-增
	public static final int INSPECT_SAFETY_INSERT_ERROR = 22010701;
	//巡检模块-安全巡检-删
	public static final int INSPECT_SAFETY_DELETE_ERROR = 22010702;
	//巡检模块-安全巡检-查
	public static final int INSPECT_SAFETY_FINDBY_ERROR = 22010703;
	//巡检模块-安全巡检-改
	public static final int INSPECT_SAFETY_UPDATE_ERROR = 22010704;
	//巡检模块-安全巡检-通用异常
	public static final int INSPECT_SAFETY_ERROR = 22010799;
	
	//巡检模块-安全巡检关联人员-增
	public static final int INSPECT_SAFETY_JOIN_PERSON_INSERT_ERROR = 22010801;
	//巡检模块-安全巡检关联人员-删
	public static final int INSPECT_SAFETY_JOIN_PERSON_DELETE_ERROR = 22010802;
	//巡检模块-安全巡检关联人员-查
	public static final int INSPECT_SAFETY_JOIN_PERSON_FINDBY_ERROR = 22010803;
	//巡检模块-安全巡检关联人员-改
	public static final int INSPECT_SAFETY_JOIN_PERSON_UPDATE_ERROR = 22010804;
	//巡检模块-安全巡检关联人员-通用异常
	public static final int INSPECT_SAFETY_JOIN_PERSON_ERROR = 22010899;
	
	//巡检模块-安全巡检结果-增
	public static final int INSPECT_SAFETY_RESULT_INSERT_ERROR = 22010901;
	//巡检模块-安全巡检结果-删
	public static final int INSPECT_SAFETY_RESULT_DELETE_ERROR = 22010902;
	//巡检模块-安全巡检结果-查
	public static final int INSPECT_SAFETY_RESULT_FINDBY_ERROR = 22010903;
	//巡检模块-安全巡检结果-改
	public static final int INSPECT_SAFETY_RESULT_UPDATE_ERROR = 22010904;
	//巡检模块-安全巡检结果-通用异常
	public static final int INSPECT_SAFETY_RESULT_ERROR = 22010999;
	
	//巡检模块-安全巡检性质-增
	public static final int INSPECT_SAFETY_PROPERTY_INSERT_ERROR = 22011001;
	//巡检模块-安全巡检性质-删
	public static final int INSPECT_SAFETY_PROPERTY_DELETE_ERROR = 22011002;
	//巡检模块-安全巡检性质-查
	public static final int INSPECT_SAFETY_PROPERTY_FINDBY_ERROR = 22011003;
	//巡检模块-安全巡检性质-改
	public static final int INSPECT_SAFETY_PROPERTY_UPDATE_ERROR = 22011004;
	//巡检模块-安全巡检性质-通用异常
	public static final int INSPECT_SAFETY_PROPERTY_ERROR = 22011099;
	
	//巡检模块-巡检附件-增
	public static final int INSPECT_SAFETY_ATTACH_INSERT_ERROR = 22011101;
	//巡检模块-巡检附件-删
	public static final int INSPECT_SAFETY_ATTACH_DELETE_ERROR = 22011102;
	//巡检模块-巡检附件-查
	public static final int INSPECT_SAFETY_ATTACH_FINDBY_ERROR = 22011103;
	//巡检模块-巡检附件-改
	public static final int INSPECT_SAFETY_ATTACH_UPDATE_ERROR = 22011104;
	//巡检模块-巡检附件-通用异常
	public static final int INSPECT_SAFETY_ATTACH_ERROR = 22011199;
	
	//巡检模块-巡检记录查看状态-增
	public static final int INSPECT_SAFETY_OPTERATE_ADD_ERROR = 22011201;
	//巡检模块-巡检记录查看状态-删
	public static final int INSPECT_SAFETY_OPTERATE_DELETE_ERROR = 22011202;
	//巡检模块-巡检记录查看状态-查
	public static final int INSPECT_SAFETY_OPTERATE_FINDBY_ERROR = 22011203;
	//巡检模块-巡检记录查看状态-改
	public static final int INSPECT_SAFETY_OPTERATE_UPDATE_ERROR = 22011204;
	//巡检模块-巡检记录查看状态-通用异常
	public static final int INSPECT_SAFETY_OPTERATE_ERROR = 22011299;
	
	//巡检模块-设备巡检类型-增
	public static final int PROJECT_EQUIP_TYPE_INFO_INSERT_ERROR = 22011301;
	//巡检模块-设备巡检类型-删
	public static final int PROJECT_EQUIP_TYPE_INFO_DELETE_ERROR = 22011302;
	//巡检模块-设备巡检类型-查
	public static final int PROJECT_EQUIP_TYPE_INFO_FINDBY_ERROR = 22011303;
	//巡检模块-设备巡检类型-改
	public static final int PROJECT_EQUIP_TYPE_INFO_UPDATE_ERROR = 22011304;
	//巡检模块-设备巡检类型-通用异常
	public static final int PROJECT_EQUIP_TYPE_INFO_ERROR = 22011399;
	
	//巡检模块-设备巡检性质-增
	public static final int PROJECT_EQUIP_INSPECT_PROPERTY_INSERT_ERROR = 22011401;
	//巡检模块-设备巡检性质-删
	public static final int PROJECT_EQUIP_INSPECT_PROPERTY_DELETE_ERROR = 22011402;
	//巡检模块-设备巡检性质-查
	public static final int PROJECT_EQUIP_INSPECT_PROPERTY_FINDBY_ERROR = 22011403;
	//巡检模块-设备巡检性质-改
	public static final int PROJECT_EQUIP_INSPECT_PROPERTY_UPDATE_ERROR = 22011404;
	//巡检模块-设备巡检性质-通用异常
	public static final int PROJECT_EQUIP_INSPECT_PROPERTY_ERROR = 22011499;
	
	//巡检模块-设备巡检项-增
	public static final int PROJECT_EQUIP_INSPECT_ITEM_INSERT_ERROR = 22011501;
	//巡检模块-设备巡检项-删
	public static final int PROJECT_EQUIP_INSPECT_ITEM_DELETE_ERROR = 22011502;
	//巡检模块-设备巡检项-查
	public static final int PROJECT_EQUIP_INSPECT_ITEM_FINDBY_ERROR = 22011503;
	//巡检模块-设备巡检项-改
	public static final int PROJECT_EQUIP_INSPECT_ITEM_UPDATE_ERROR = 22011504;
	//巡检模块-设备巡检项-通用异常
	public static final int PROJECT_EQUIP_INSPECT_ITEM_ERROR = 21011599;
	
	//巡检模块-设备台账-增
	public static final int PROJECT_EQUIP_INFO_INSERT_ERROR = 22011601;
	//巡检模块-设备台账-删
	public static final int PROJECT_EQUIP_INFO_DELETE_ERROR = 22011602;
	//巡检模块-设备台账-查
	public static final int PROJECT_EQUIP_INFO_FINDBY_ERROR = 22011603;
	//巡检模块-设备台账-改
	public static final int PROJECT_EQUIP_INFO_UPDATE_ERROR = 22011604;
	//巡检模块-设备台账-通用异常
	public static final int PROJECT_EQUIP_INFO_ERROR = 22011699;
	
	//巡检模块-设备人员-增
	public static final int PROJECT_EQUIP_PERSON_INFO_INSERT_ERROR = 22011701;
	//巡检模块-设备人员-删
	public static final int PROJECT_EQUIP_PERSON_INFO_DELETE_ERROR = 22011702;
	//巡检模块-设备人员-查
	public static final int PROJECT_EQUIP_PERSON_INFO_FINDBY_ERROR = 22011703;
	//巡检模块-设备人员-改
	public static final int PROJECT_EQUIP_PERSON_INFO_UPDATE_ERROR = 22011704;
	//巡检模块-设备人员-通用异常
	public static final int PROJECT_EQUIP_PERSON_INFO_ERROR = 22011799;
	
	//巡检模块-设备人员关联-增
	public static final int PROJECT_EQUIP_EMPLOYEE_INSERT_ERROR = 22011801;
	//巡检模块-设备人员关联-删
	public static final int PROJECT_EQUIP_EMPLOYEE_DELETE_ERROR = 22011802;
	//巡检模块-设备人员关联-查
	public static final int PROJECT_EQUIP_EMPLOYEE_FINDBY_ERROR = 22011803;
	//巡检模块-设备人员关联-改
	public static final int PROJECT_EQUIP_EMPLOYEE_UPDATE_ERROR = 22011804;
	//巡检模块-设备人员关联-通用异常
	public static final int PROJECT_EQUIP_EMPLOYEE_ERROR = 22011899;
	
	//巡检模块-设备巡检-增
	public static final int PROJECT_EQUIP_INSPECT_RECORD_INSERT_ERROR = 22011901;
	//巡检模块-设备巡检-删
	public static final int PROJECT_EQUIP_INSPECT_RECORD_DELETE_ERROR = 22011902;
	//巡检模块-设备巡检-查
	public static final int PROJECT_EQUIP_INSPECT_RECORD_ERROR = 22011903;
	//巡检模块-设备巡检-改
	public static final int PROJECT_EQUIP_INSPECT_RECORD_UPDATE_ERROR = 22011904;
	//巡检模块-设备巡检-通用异常
	public static final int PROJECT_EQUIP_INSPECT_ERROR = 22011999;
	
	//巡检模块-设备巡检-人员-增
	public static final int PROJECT_EQUIPINSPECT_EMPLOYEE_INSERT_ERROR = 22012001;
	//巡检模块-设备巡检-人员-删
	public static final int PROJECT_EQUIPINSPECT_EMPLOYEE_DELETE_ERROR = 22012002;
	//巡检模块-设备巡检-人员-查
	public static final int PROJECT_EQUIPINSPECT_EMPLOYEE_FINDBY_ERROR = 22012003;
	//巡检模块-设备巡检-人员-改
	public static final int PROJECT_EQUIPINSPECT_EMPLOYEE_UPDATE_ERROR = 22012004;
	//巡检模块-设备巡检-人员-通用异常
	public static final int PROJECT_EQUIPINSPECT_EMPLOYEE_ERROR = 22012099;
	
	//巡检模块-设备巡检-巡检项-增
	public static final int PROJECT_EQUIP_INSPECT_PROBLEM_INSERT_ERROR = 22012101;
	//巡检模块-设备巡检-巡检项-删
	public static final int PROJECT_EQUIP_INSPECT_PROBLEM_DELETE_ERROR = 22012102;
	//巡检模块-设备巡检-巡检项-查
	public static final int PROJECT_EQUIP_INSPECT_PROBLEM_FINDBY_ERROR = 22012103;
	//巡检模块-设备巡检-巡检项-改
	public static final int PROJECT_EQUIP_INSPECT_PROBLEM_UPDATE_ERROR = 22012104;
	//巡检模块-设备巡检-巡检项-通用异常
	public static final int PROJECT_EQUIP_INSPECT_PROBLEM_ERROR = 22012199;
	
	//巡检模块-质量巡检整改回复
	public static final int INSPECT_QUALITY_RECT_REPLAY_ERROR = 22012201;
	//巡检模块-质量巡检整改回复
	public static final int INSPECT_SAFETY_RECT_REPLAY_ERROR = 22012299;
	
	//设备性质-巡检项-增
	public static final int PROJECT_EQUIP_PROPERTY_ITEM_INSERT_ERROR = 22012201;
	//设备性质-巡检项-改
	public static final int PROJECT_EQUIP_PROPERTY_ITEM_UPDATE_ERROR = 22012202;
	//设备性质-巡检项-删
	public static final int PROJECT_EQUIP_PROPERTY_ITEM_DELETE_ERROR = 22012203;
	//设备性质-巡检项-查
	public static final int PROJECT_EQUIP_PROPERTY_ITEM_FIND_ERROR = 22012204;
	
	//设备性质-巡检项-增
	public static final int PROJECT_EQUIPINSPECT_REPLAY_INSERT_ERROR = 22012301;
	//设备性质-巡检项-改
	public static final int PROJECT_EQUIPINSPECT_REPLAY_UPDATE_ERROR = 22012302;
	//设备性质-巡检项-删
	public static final int PROJECT_EQUIPINSPECT_REPLAY_DELETE_ERROR = 22012303;
	//设备性质-巡检项-查
	public static final int PROJECT_EQUIPINSPECT_REPLAY_FIND_ERROR = 22012304;
	
	//巡检模块-通用异常
	public static final int INSPECT_MODULE_ERROR = 22019999;
}
