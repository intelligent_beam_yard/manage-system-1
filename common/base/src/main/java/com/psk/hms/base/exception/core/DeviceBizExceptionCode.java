package com.psk.hms.base.exception.core;

/**
 * <p>
 * device 业务异常类
 * </p>
 * 模块异常码 : 21000001 ~ 21999999 <br/>
 * 
 * @author xiangjz
 * @date 2018年9月16日
 * @version 1.0.0
 *
 */
public class DeviceBizExceptionCode {

	/***
	 * 设备模块 21010000~21019999
	 */
	// 设备模块-预警设置-增
	public static final int DEVICE_WARNING_SETTING_INSERT_ERROR = 21010101;
	// 设备模块-预警设置-删
	public static final int DEVICE_WARNING_SETTING_DELETE_ERROR = 21010102;
	// 设备模块-预警设置-查
	public static final int DEVICE_WARNING_SETTING_SELECT_ERROR = 21010103;
	// 设备模块-预警设置-改
	public static final int DEVICE_WARNING_SETTING_UPDATE_ERROR = 21010104;
	// 设备模块-预警设置-通用异常
	public static final int DEVICE_WARNING_SETTING_ERROR = 21010199;

	// 设备模块-基础预警项-增
	public static final int DEVICE_WARNING_INSERT_ERROR = 21010201;
	// 设备模块-基础预警项-删
	public static final int DEVICE_WARNING_DELETE_ERROR = 21010202;
	// 设备模块-基础预警项-查
	public static final int DEVICE_WARNING_SELECT_ERROR = 21010203;
	// 设备模块-基础预警项-改
	public static final int DEVICE_WARNING_UPDATE_ERROR = 21010204;
	// 设备模块-基础预警项-通用异常
	public static final int DEVICE_WARNING_ERROR = 20010299;

	// 设备模块-门禁-增
	public static final int DEVICE_GUARD_INSERT_ERROR = 21010301;
	// 设备模块-门禁-删
	public static final int DEVICE_GUARD_DELETE_ERROR = 21010302;
	// 设备模块-门禁-查
	public static final int DEVICE_GUARD_SELECT_ERROR = 21010303;
	// 设备模块-门禁-改
	public static final int DEVICE_GUARD_UPDATE_ERROR = 21010304;
	// 设备模块-门禁-通用异常
	public static final int DEVICE_GUARD_ERROR = 21010399;

	// 设备模块-门禁历史-增
	public static final int DEVICE_GUARD_HISTORY_INSERT_ERROR = 21010401;
	// 设备模块-门禁历史-删
	public static final int DEVICE_GUARD_HISTORY_DELETE_ERROR = 21010402;
	// 设备模块-门禁历史-查
	public static final int DEVICE_GUARD_HISTORY_SELECT_ERROR = 21010403;
	// 设备模块-门禁历史-改
	public static final int DEVICE_GUARD_HISTORY_UPDATE_ERROR = 21010404;
	// 设备模块-门禁历史-通用异常
	public static final int DEVICE_GUARD_HISTORY_ERROR = 21010499;

	// 设备模块-设备预警日志-增
	public static final int DEVICE_WARNING_LOG_INSERT_ERROR = 21010501;
	// 设备模块-设备预警日志-删
	public static final int DEVICE_WARNING_LOG_DELETE_ERROR = 21010502;
	// 设备模块-设备预警日志-查
	public static final int DEVICE_WARNING_LOG_SELECT_ERROR = 21010503;
	// 设备模块-设备预警日志-改
	public static final int DEVICE_WARNING_LOG_UPDATE_ERROR = 21010504;
	// 设备模块-设备预警日志-通用异常
	public static final int DEVICE_WARNING_LOG_ERROR = 21010599;

	// 设备模块-视频-增
	public static final int DEVICE_MONITOR_INSERT_ERROR = 21010601;
	// 设备模块-视频-删
	public static final int DEVICE_MONITOR_DELETE_ERROR = 21010602;
	// 设备模块-视频-查
	public static final int DEVICE_MONITOR_GET_ERROR = 21010603;
	// 设备模块-视频-改
	public static final int DEVICE_MONITOR_UPDATE_ERROR = 21010604;
	// 设备模块-视频-通用异常
	public static final int DEVICE_MONITOR_ERROR = 21010699;

	// 设备模块-设备在线日志-增
	public static final int DEVICE_ONLINE_LOG_INSERT_ERROR = 21010701;
	// 设备模块-设备在线日志-删
	public static final int DEVICE_ONLINE_LOG_DELETE_ERROR = 21010702;
	// 设备模块-设备在线日志-查
	public static final int DEVICE_ONLINE_LOG_SELECT_ERROR = 21010703;
	// 设备模块-设备在线日志-改
	public static final int DEVICE_ONLINE_LOG_UPDATE_ERROR = 21010704;
	// 设备模块-设备在线日志-通用异常
	public static final int DEVICE_ONLINE_LOG_ERROR = 21010799;

	// 设备模块-设备在线设置-增
	public static final int DEVICE_ONLINE_SETTING_INSERT_ERROR = 21010801;
	// 设备模块-设备在线设置-删
	public static final int DEVICE_ONLINE_SETTING_DELETE_ERROR = 21010802;
	// 设备模块-设备在线设置-查
	public static final int DEVICE_ONLINE_SETTING_SELECT_ERROR = 21010803;
	// 设备模块-设备在线设置-改
	public static final int DEVICE_ONLINE_SETTING_UPDATE_ERROR = 21010804;
	// 设备模块-设备在线设置-通用异常
	public static final int DEVICE_ONLINE_SETTING_ERROR = 21010899;

	// 设备模块-设备在线通知人-增
	public static final int DEVICE_ONLINE_RECIEVER_INSERT_ERROR = 21010901;
	// 设备模块-设备在线通知人-删
	public static final int DEVICE_ONLINE_RECIEVER_DELETE_ERROR = 21010902;
	// 设备模块-设备在线通知人-查
	public static final int DEVICE_ONLINE_RECIEVER_SELECT_ERROR = 21010903;
	// 设备模块-设备在线通知人-改
	public static final int DEVICE_ONLINE_RECIEVER_UPDATE_ERROR = 21010904;
	// 设备模块-设备在线通知人-通用异常
	public static final int DEVICE_ONLINE_RECIEVER_ERROR = 21010999;

	// 设备模块-设备预警通知人-增
	public static final int DEVICE_WARNING_RECIEVER_INSERT_ERROR = 21011001;
	// 设备模块-设备预警通知人-删
	public static final int DEVICE_WARNING_RECIEVER_DELETE_ERROR = 21011002;
	// 设备模块-设备预警通知人-查
	public static final int DEVICE_WARNING_RECIEVER_SELECT_ERROR = 21011003;
	// 设备模块-设备预警通知人-改
	public static final int DEVICE_WARNING_RECIEVER_UPDATE_ERROR = 21011004;
	// 设备模块-设备预警通知人-通用异常
	public static final int DEVICE_WARNING_RECIEVER_ERROR = 21011099;

	// 设备模块-设备人员分布地图-增
	public static final int DEVICE_LABOR_DISTRIBUTION_INSERT_ERROR = 21011101;
	// 设备模块-设备人员分布地图-删
	public static final int DEVICE_LABOR_DISTRIBUTION_DELETE_ERROR = 21011102;
	// 设备模块-设备人员分布地图-查
	public static final int DEVICE_LABOR_DISTRIBUTION_SELECT_ERROR = 21011103;
	// 设备模块-设备人员分布地图-改
	public static final int DEVICE_LABOR_DISTRIBUTION_UPDATE_ERROR = 21011104;
	// 设备模块-设备人员分布地图坐标点-删
	public static final int DEVICE_LABOR_DISTRIBUTION_POSITION_DELETE_ERROR = 21011105;
	// 设备模块-设备人员分布地图-通用异常
	public static final int DEVICE_LABOR_DISTRIBUTION_ERROR = 21011199;

	// 设备模块-门禁数据模拟配置-增
	public static final int DEVICE_GUARD_HISTORY_CONFIG_INSERT_ERROR = 21011201;
	// 设备模块-门禁数据模拟配置-删
	public static final int DEVICE_GUARD_HISTORY_CONFIG_DELETE_ERROR = 21011202;
	// 设备模块-门禁数据模拟配置-查
	public static final int DEVICE_GUARD_HISTORY_CONFIG_SELECT_ERROR = 21011203;
	// 设备模块-门禁数据模拟配置-改
	public static final int DEVICE_GUARD_HISTORY_CONFIG_UPDATE_ERROR = 21011204;
	// 设备模块-门禁数据模拟配置-通用异常
	public static final int DEVICE_GUARD_HISTORY_CONFIG_ERROR = 21011299;

	// 设备模块-数据迁移-增
	public static final int DEVICE_DATA_MIGRATE_INSERT_ERROR = 21011301;
	// 设备模块-数据迁移-删
	public static final int DEVICE_DATA_MIGRATE_DELETE_ERROR = 21011302;
	// 设备模块-数据迁移-查
	public static final int DEVICE_DATA_MIGRATE_SELECT_ERROR = 21011303;
	// 设备模块-数据迁移-改
	public static final int DEVICE_DATA_MIGRATE_UPDATE_ERROR = 21011304;
	// 设备模块-数据迁移-通用异常
	public static final int DEVICE_DATA_MIGRATE_ERROR = 21011399;

	// 设备模块-门禁数据工时统计-增
	public static final int DEVICE_GUARD_RECORD_INSERT_ERROR = 21011401;
	// 设备模块-门禁数据工时统计-删
	public static final int DEVICE_GUARD_RECORD_DELETE_ERROR = 21011402;
	// 设备模块-门禁数据工时统计-查
	public static final int DEVICE_GUARD_RECORD_SELECT_ERROR = 21011403;
	// 设备模块-门禁数据工时统计-改
	public static final int DEVICE_GUARD_RECORD_UPDATE_ERROR = 21011404;
	// 设备模块-门禁数据工时统计-通用异常
	public static final int DEVICE_GUARD_RECORD_ERROR = 21011499;

	// 设备模块-设备信息-新增
	public static final int DEVICE_INFO_INSERT_ERROR = 21011501;
	// 设备模块-设备信息-删
	public static final int DEVICE_INFO_DELETE_ERROR = 21011502;
	// 设备模块-设备信息-改
	public static final int DEVICE_INFO_UPDATE_ERROR = 21011503;
	// 设备模块-设备信息-查
	public static final int DEVICE_INFO_SELECT_ERROR = 21011504;
	// 设备模块-设备信息-通用异常
	public static final int DEVICE_INFO_ERROR = 21011599;

	// 设备模块-塔吊-增
	public static final int DEVICE_CRANE_INSERT_ERROR = 21011601;
	// 设备模块-塔吊-删
	public static final int DEVICE_CRANE_DELET_ERROR = 21011602;
	// 设备模块-塔吊-查
	public static final int DEVICE_CRANE_SELECT_ERROR = 21011603;
	// 设备模块-塔吊-改
	public static final int DEVICE_CRANE_UPDATE_ERROR = 21011604;
	// 设备模块-塔吊-通用异常
	public static final int DEVICE_CRANE_ERROR = 21011699;

	// 设备模块-升降机-增
	public static final int DEVICE_ELEVATOR_INSERT_ERROR = 21011701;
	// 设备模块-升降机-删
	public static final int DEVICE_ELEVATOR_DELET_ERROR = 21011702;
	// 设备模块-升降机-查
	public static final int DEVICE_ELEVATOR_SELECT_ERROR = 21011703;
	// 设备模块-升降机-改
	public static final int DEVICE_ELEVATOR_UPDATE_ERROR = 21011704;
	// 设备模块-升降机-通用异常
	public static final int DEVICE_ELEVATOR_ERROR = 21011799;

	// 设备模块-环境监测数据-增
	public static final int DEVICE_ENVIRONMENT_INSERT_ERROR = 21011801;
	// 设备模块-环境监测数据-删
	public static final int DEVICE_ENVIRONMENT_DELET_ERROR = 21011802;
	// 设备模块-环境监测数据-查
	public static final int DEVICE_ENVIRONMENT_SELECT_ERROR = 21011803;
	// 设备模块-环境监测数据-改
	public static final int DEVICE_ENVIRONMENT_UPDATE_ERROR = 21011804;
	// 设备模块-环境监测数据-通用异常
	public static final int DEVICE_ENVIRONMENT_ERROR = 21011899;

	// 设备模块-卸料平台-增
	public static final int DEVICE_UNLOADING_INSERT_ERROR = 21011901;
	// 设备模块-卸料平台-删
	public static final int DEVICE_UNLOADING_DELET_ERROR = 21011902;
	// 设备模块-卸料平台-查
	public static final int DEVICE_UNLOADING_SELECT_ERROR = 21011903;
	// 设备模块-卸料平台-改
	public static final int DEVICE_UNLOADING_UPDATE_ERROR = 21011904;
	// 设备模块-卸料平台-通用异常
	public static final int DEVICE_UNLOADING_ERROR = 21011999;

	// 设备模块-区域定位-增
	public static final int DEVICE_AREA_INSERT_ERROR = 21012001;
	// 设备模块-区域定位-删
	public static final int DEVICE_AREA_DELET_ERROR = 21012002;
	// 设备模块-区域定位-查
	public static final int DEVICE_AREA_SELECT_ERROR = 21012003;
	// 设备模块-区域定位-改
	public static final int DEVICE_AREA_UPDATE_ERROR = 21012004;
	// 设备模块-区域定位-通用异常
	public static final int DEVICE_AREA_ERROR = 21012099;

	// 设备模块-预警检测-通用异常
	public static final int DEVICE_WARNING_CHECK_ERROR = 21012199;

	// 设备模块-安全培训-增
	public static final int DEVICE_SAFETY_TRAIN_INSERT_ERROR = 21012201;
	// 设备模块-安全培训-删
	public static final int DEVICE_SAFETY_TRAIN_DELET_ERROR = 21012202;
	// 设备模块-安全培训-查
	public static final int DEVICE_SAFETY_TRAIN_SELECT_ERROR = 21012203;
	// 设备模块-安全培训-改
	public static final int DEVICE_SAFETY_TRAIN_UPDATE_ERROR = 21012204;
	// 设备模块-安全培训-设备外部接口外层循环数据格式错误
	public static final int DEVICE_SAFETY_TRAIN_OUT_DATA_FORMAT_ERROR = 21012205;
	// 设备模块-安全培训-设备外部接口内层循环数据格式错误
	public static final int DEVICE_SAFETY_TRAIN_IN_DATA_FORMAT_ERROR = 21012206;
	// 设备模块-安全培训-人员没匹配成功异常
	public static final int DEVICE_SAFETY_TRAIN_NOT_MATCH_ERROR = 21012207;
	// 设备模块-安全培训-未找到项目相关信息
	public static final int DEVICE_SAFETY_TRAIN_RELATED_NOT_FOUND = 21012208;
	// 设备模块-安全培训-未找到设备相关信息
	public static final int DEVICE_SAFETY_TRAIN_DEVICE_NOT_FOUND = 21012209;
	// 设备模块-安全培训-设备插入数据重复
	public static final int DEVICE_SAFETY_TRAIN_DEVICE_DATA_DUPLICATE = 21012210;
	// 设备模块-安全培训-通用异常
	public static final int DEVICE_SAFETY_TRAIN_ERROR = 21012299;

	// 设备模块-卫星导航-增
	public static final int DEVICE_SATELLITE_NAVIGATION_INSERT_ERROR = 21012301;
	// 设备模块-卫星导航-删
	public static final int DEVICE_SATELLITE_NAVIGATION_DELET_ERROR = 21012302;
	// 设备模块-卫星导航-查
	public static final int DEVICE_SATELLITE_NAVIGATION_SELECT_ERROR = 21012303;
	// 设备模块-卫星导航-改
	public static final int DEVICE_SATELLITE_NAVIGATION_UPDATE_ERROR = 21012304;
	// 设备模块-卫星导航-通用异常
	public static final int DEVICE_SATELLITE_NAVIGATION_ERROR = 21012399;

	// 设备模块-通用异常
	public static final int DEVICE_MODULE_ERROR = 21019999;

	/***
	 * 基站模块 21020000~21029999
	 */
	// 基站模块-基站管理-增
	public static final int BASESTATION_MANAGE_SAVE_ERROR = 21020101;
	// 基站模块-基站管理-删
	public static final int BASESTATION_MANAGE_DELETE_ERROR = 21020102;
	// 基站模块-基站管理-查
	public static final int BASESTATION_MANAGE_SELECT_ERROR = 21020103;
	// 基站模块-基站管理-改
	public static final int BASESTATION_MANAGE_UPDATE_ERROR = 21020104;
	// 基站模块-基站管理-通用异常
	public static final int BASESTATION_MANAGE_ERROR = 21020199;

	// 基站模块-人员分布-增
	public static final int BASESTATION_AREA_SAVE_ERROR = 21020201;
	// 基站模块-人员分布-删
	public static final int BASESTATION_AREA_DELETE_ERROR = 21020202;
	// 基站模块-人员分布-查
	public static final int BASESTATION_AREA_SELECT_ERROR = 21020203;
	// 基站模块-人员分布-改
	public static final int BASESTATION_AREA_UPDATE_ERROR = 21020204;
	// 基站模块-人员分布-通用异常
	public static final int BASESTATION_AREA_ERROR = 21020299;
	// 基站模块-通用异常
	public static final int BASESTATION_ERROR = 21029999;

	/***
	 * 智能安全带模块 21030000~21039999
	 */
	// 智能安全带模块-智能安全带管理-增
	public static final int SAFETYBELT_MANAGE_SAVE_ERROR = 21030101;
	// 智能安全带模块-智能安全带管理-删
	public static final int SAFETYBELT_MANAGE_DELETE_ERROR = 21030102;
	// 智能安全带模块-智能安全带管理-查
	public static final int SAFETYBELT_MANAGE_SELECT_ERROR = 21030103;
	// 智能安全带模块-智能安全带管理-改
	public static final int SAFETYBELT_MANAGE_UPDATE_ERROR = 21030104;
	// 智能安全带模块-智能安全带管理-通用异常
	public static final int SAFETYBELT_MANAGE_ERROR = 21030199;
	// 智能安全带模块-通用异常
	public static final int SAFETYBELT_ERROR = 21039999;

}
