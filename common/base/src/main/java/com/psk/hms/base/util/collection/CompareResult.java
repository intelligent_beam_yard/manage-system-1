package com.psk.hms.base.util.collection;

/**
 * 集合比对结果。结果包含三部分：
 * 1. leftResult：左源集合特有元素结果集；leftResultSize：左源集合特有元素结果集元素个数
 * 2. rightResult：右源集合特有元素结果集；rightResultSize：右源集合特有元素结果集元素个数
 * 3. bothResult：共有元素结果集；leftResultSize：共有元素结果集元素个数
 * 
 * @author xiangjz
 * @date 2018年11月19日
 * @version 1.0.0
 *
 * @param <T>
 */
public class CompareResult<T> {
	private T leftResult;
	private T rightResult;
	private T bothResult;
	private int leftResultSize;
	private int rightResultSize;
	private int bothResultSize;

	/**
	 * 相等判断。判断两个集合元素是否一致，仅比对元素值和元素个数，不会匹配元素在集合中的顺序（如果有）
	 * 
	 * @return
	 */
	public boolean isMatch() {
		return leftResultSize == 0 && rightResultSize == 0;
	}
	
	/**
	 * 左包含判断。判断左集合是否包含右集合。
	 * 
	 * @return
	 */
	public boolean isLeftContains() {
		return leftResultSize != 0 && rightResultSize == 0;
	}
	
	/**
	 * 右包含判断。判断右集合是否包含左集合。
	 * 
	 * @return
	 */
	public boolean isRightContains() {
		return leftResultSize == 0 && rightResultSize != 0;
	}
	
	/**
	 * 不相交判断。判断两个集合是否未含有任一一个相同元素。
	 * 
	 * @return
	 */
	public boolean isDifferent() {
		return bothResultSize == 0 && leftResultSize != 0 && rightResultSize != 0;
	}
	
	public T getLeftResult() {
		return leftResult;
	}

	public void setLeftResult(T leftResult) {
		this.leftResult = leftResult;
	}

	public T getRightResult() {
		return rightResult;
	}

	public void setRightResult(T rightResult) {
		this.rightResult = rightResult;
	}

	public T getBothResult() {
		return bothResult;
	}

	public void setBothResult(T bothResult) {
		this.bothResult = bothResult;
	}

	public int getLeftResultSize() {
		return leftResultSize;
	}

	public void setLeftResultSize(int leftResultSize) {
		this.leftResultSize = leftResultSize;
	}

	public int getRightResultSize() {
		return rightResultSize;
	}

	public void setRightResultSize(int rightResultSize) {
		this.rightResultSize = rightResultSize;
	}

	public int getBothResultSize() {
		return bothResultSize;
	}

	public void setBothResultSize(int bothResultSize) {
		this.bothResultSize = bothResultSize;
	}

}
