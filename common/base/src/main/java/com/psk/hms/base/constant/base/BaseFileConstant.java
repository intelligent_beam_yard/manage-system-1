package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础文件常量类
 * @author xiangjz
 * @date 2018年10月25日
 * @version 1.0.0
 *
 */
public class BaseFileConstant extends Constant {

	/**
	 * 文件业务关联id
	 */
	public static final String BIZ_ID = "bizId";
	
	/**
	 * 文件类型
	 */
	public static final String BIZ_TYPE = "bizType";
	
	/**
	 * 文件名
	 */
	public static String ORGINAL_NAME = "orginalName";
	
	/**
	 * 文件内容
	 */
	public static final String CONTENT_TYPE = "contentType";
	
	/**
	 * 文件大小
	 */
	public static final String SIZE = "size";
	
	/**
	 * 文件url
	 */
	public static final String URL = "url";
	
	/**
	 * 文件类型id
	 */
	public static final String FILE_TYPE_ID = "fileTypeId";
	
	/**
	 * 文件类型id
	 */
	public static final String BIZ_TYPE_CODE = "bizTypeCode";
	
	/**
	 * 文件类型id
	 */
	public static final String FILE_TYPE_CODE = "fileTypeCode";
	
	/**
	 * 文件名称  参数用
	 */
	public static final String FILE_NAME = "fileName";
}
