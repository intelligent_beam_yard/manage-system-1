package com.psk.hms.base.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用树结构查询参数
 * @author xiangjz
 *
 */
public class CommonTreeParam {
	
	public static final String ROOT_ID = "rootId";
	
	private String id;
	
	private String cycleName = "parentId";
	
	private Integer levels;
	
	private String levelsName = "levels";
	
	private String sqlId = "listBy";
	
	private Map<String, Object> sqlParam = null;
	
	private Map<String, Object> otherParams;

	public Map<String, Object> getOtherParams() {
		return otherParams;
	}

	public void setOtherParams(Map<String, Object> otherParams) {
		this.otherParams = otherParams;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	public Integer getLevels() {
		return levels;
	}

	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	public String getLevelsName() {
		return levelsName;
	}

	public void setLevelsName(String levelsName) {
		this.levelsName = levelsName;
	}

	public String getSqlId() {
		return sqlId;
	}

	public void setSqlId(String sqlId) {
		this.sqlId = sqlId;
	}

	public Map<String, Object> getSqlParam() {
		return sqlParam;
	}

	public void setSqlParam(Map<String, Object> sqlParam) {
		this.sqlParam = sqlParam;
	}

	public CommonTreeParam(String id, String cycleName, 
							Integer levels, String levelsName, 
							String sqlId, Map<String, Object> sqlParam) {
		super();
		this.id = id;
		this.cycleName = cycleName;
		this.levels = levels;
		this.levelsName = levelsName;
		this.sqlId = sqlId;
		this.sqlParam = sqlParam;
	}

	public CommonTreeParam(String id, String cycleName, Integer levels, String levelsName) {
		super();
		this.id = id;
		this.cycleName = cycleName;
		this.levels = levels;
		this.levelsName = levelsName;
		this.sqlParam = new HashMap<String, Object>();
	}

	public CommonTreeParam(String id, String cycleName) {
		super();
		this.id = id;
		this.cycleName = cycleName;
		this.sqlParam = new HashMap<String, Object>();
	}
	
	public CommonTreeParam(String id, String cycleName, Map<String, Object> otherParams) {
		super();
		this.id = id;
		this.cycleName = cycleName;
		this.otherParams = otherParams;
		this.sqlParam = new HashMap<String, Object>();
	}
	
	public CommonTreeParam(String id) {
		super();
		this.id = id;
		this.sqlParam = new HashMap<String, Object>();
	}
	
	public CommonTreeParam(Integer levels) {
		super();
		this.levels = levels;
		this.sqlParam = new HashMap<String, Object>();
	}
	
	public CommonTreeParam() {
		super();
		this.sqlParam = new HashMap<String, Object>();
	}
	
}
