package com.psk.hms.base.constant.base;

import com.psk.hms.base.constant.common.Constant;

/**
 * 基础字典常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class BaseDictionaryInfoConstant extends Constant {
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - value
     */
    public static final String VALUE = "value";
    /**
     *  数据表字段名称 - parentId
     */
    public static final String PARENT_ID = "parentId";
    /**
     *  数据表字段名称 - parentIds
     */
    public static final String PARENT_IDS = "parentIds";
    /**
     *  数据表字段名称 - levels
     */
    public static final String LEVELS = "levels";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
}
