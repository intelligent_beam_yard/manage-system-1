package com.psk.hms.base.util.validator.field;

/**
 * map校验器抽象类
 * 
 * @author xiangjz
 * @date 2018年8月7日
 * @version 1.0.0
 *
 * @param <K>
 * @param <V>
 */
public abstract class AbstractMapValidator<K, V> implements MapValidatable<K, V> {

	// 校验的map key
	protected K key;
	// 校验不匹配消息
	protected String errorMsg;
	
	public AbstractMapValidator(K key, String errorMsg) {
		this.key = key;
		this.errorMsg = errorMsg;
	}
		
	public AbstractMapValidator(K key) {
		this.key = key;
	}
	
	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}
	
	@Override
	public String getDefaultErrorMsg(K key) {
		StringBuilder sb = new StringBuilder();
		sb.append("'").append(key).append("'校验失败");
		return sb.toString();
	}
}
