package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队角色常量类
 * @author xiangjz
 * @date 2018年9月17日
 * @version 1.0.0
 *
 */
public class TeamRoleConstant extends Constant {
    /**
     *  数据表字段名称 - teamId
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - parentId
     */
    public static final String PARENT_ID = "parentId";
    /**
     *  数据表字段名称 - parentIds
     */
    public static final String PARENT_IDS = "parentIds";
    /**
     *  数据表字段名称 - isParent
     */
    public static final String IS_PARENT = "isParent";
    /**
     *  数据表字段名称 - levels
     */
    public static final String LEVELS = "levels";
    /**
     *  数据表字段名称 - sort
     */
    public static final String SORT = "sort";
    /**
     *  数据表字段名称 - images
     */
    public static final String IMAGES = "images";
    
    
    
    
}
