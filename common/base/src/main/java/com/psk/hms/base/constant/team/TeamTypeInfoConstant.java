package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队类型常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamTypeInfoConstant extends Constant {    
    /**
     *  数据表字段名称 - code
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - name
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - defaultRoleId
     */
    public static final String DEFAULT_ROLE_ID = "defaultRoleId";    
    /**
     *  数据表字段名称 - defaultManageId
     */
    public static final String DEFAULT_MANAGE_ID = "defaultManageId";
    
    
    /**
     *  业务逻辑字段名称 - 权限 - module
     */
    public static final String MODULE = "module";
    /**
     *  业务逻辑字段名称 - 权限 - menu
     */
    public static final String MENU = "menu";
    
    
    
}
