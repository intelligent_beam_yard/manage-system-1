package com.psk.hms.base.constant.team;

import com.psk.hms.base.constant.common.Constant;

/**
 * 团队应用常量类
 * @author xiangjz
 * @date 2018年7月12日
 * @version 1.0.0
 *
 */
public class TeamApplicationConstant extends Constant {    
    /**
     *  数据表字段名称 - parentId - 项目id
     */
    public static final String PARENT_ID = "parentId";
    /**
     *  数据表字段名称 - name - 应用名称
     */
    public static final String NAME = "name";
    /**
     *  数据表字段名称 - teamId - 团队id
     */
    public static final String TEAM_ID = "teamId";
    /**
     *  数据表字段名称 - url - 文件url
     */
    public static final String URL = "url";
    /**
     *  数据表字段名称 - code - 
     */
    public static final String CODE = "code";
    /**
     *  数据表字段名称 - icon
     */
    public static final String ICON = "icon";
    /**
     *  数据表字段名称 - operatorId
     */
    public static final String OPERATOR_ID = "operatorId";
    /**
     *  数据表字段名称 - sort - 排序
     */
    public static final String SORT = "sort";
}
