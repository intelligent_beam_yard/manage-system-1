package com.psk.hms.base.util.validator.chain;

import com.psk.hms.base.util.validator.executor.BreakonInvalidateMapValidatorExecutor;
import com.psk.hms.base.util.validator.executor.DefaultMapValidatorExecutor;
import com.psk.hms.base.util.validator.executor.MapValidatorExecutable;
import com.psk.hms.base.util.validator.field.MapValidatable;
import com.psk.hms.base.util.validator.field.impl.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口参数校验器
 * 
 * @author xiangjz
 * @date 2018年9月21日
 * @version 1.1.2
 *
 * @param <K> 参数map key数据类型
 * @param <V> 参数map value数据类型
 */
public class ApiParameterValidator<K, V> {

	// 校验参数map对象
	private Map<K, V> params;
	// 字段校验责任链
	private List<MapValidatable<K, V>> validateChain;
	// 字段校验执行器
	private MapValidatorExecutable<K, V> executor;
	
	public ApiParameterValidator() {
		this(new HashMap<K, V>());
	}
	
	public ApiParameterValidator(Map<K, V> map) {
		this.params = (map == null ? new HashMap<K, V>() : map);
		this.validateChain = new ArrayList<MapValidatable<K, V>>();
		this.executor = new DefaultMapValidatorExecutor<K, V>();
	}
	
	/**
	 * 创建校验器.初始化方法，此方法必须getResult()方法之前调用，如果在构造方法中传入了
	 * map对象，就不用调用此方法。
	 * 
	 * @param map 校验参数map对象
	 * @return
	 */
	public ApiParameterValidator<K, V> create(Map<K, V> map) {
		if(map != null) {
			this.params = map;
		}
		return this;
	}
	
	/**
	 * 不匹配时退出。校验器行为控制。当调用此方法后，校验器会在校验到第一个不匹配的字段后退出后续校验，
	 * 所以校验结果最多只会存在一条校验失败信息。在默认情况下（不调用此方法）校验器会校验所有给定字段并返
	 * 回校验结果信息。需要注意：此方法需要在调用getResult()方法前调用才会生效。
	 * 
	 * @return
	 */
	public ApiParameterValidator<K, V> breakOnInvalidation() {
		setExecutor(new BreakonInvalidateMapValidatorExecutor<K, V>());
		return this;
	}
	
	/**
	 * 验证字段的存在性，若字段不存在将会在结果集中写默认校验信息('key'字段必填)
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> exist(K key) {
		addFieldValidator(new ExistFieldValidator<K, V>(key));
		return this;
	}
	
	/**
	 * 验证字段的存在性，若字段不存在将会在结果集中写入自定义的校验不匹配信息
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> exist(K key, String errorMsg) {
		addFieldValidator(new ExistFieldValidator<K, V>(key, errorMsg));
		return this;
	}
	
	/**
	 * 检验字段类型 [强制]。此方法要求字段必须存在并且字段对应的value数据类型必须匹配给定的数据类型。
	 * 匹配失败时将会在结果集中写入默认的校验信息（'key'应为type类型）
	 * 
	 * @param key 校验的map key
	 * @param type 数据类型
	 * @return
	 */
	public ApiParameterValidator<K, V> type(K key, Class<?> type) {
		addFieldValidator(new TypeFieldValidator<K, V>(key, type, true));
		return this;
	}
	
	/**
	 * 检验字段类型 [强制]。此方法要求字段必须存在并且字段对应的value数据类型必须匹配给定的数据类型。
	 * 匹配失败时将会在结果集中写入自定义的校验不匹配信息.
	 * 
	 * @param key 校验的map key
	 * @param type 数据类型
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> type(K key, Class<?> type, String errorMsg) {
		addFieldValidator(new TypeFieldValidator<K, V>(key, type, true, errorMsg));
		return this;
	}
	
	/**
	 * 检验字段类型 [非强制]。只有当字段值类型和给定类型匹配时才会校验成功。校验失败时将会在结果集中写
	 * 入默认的校验信息（'key'应为type类型）。此方法不会校验字段的存在性，若字段不存在则会
	 * 跳过校验，并不会再结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param type 数据类型
	 * @return
	 */
	public ApiParameterValidator<K, V> typeIfExist(K key, Class<?> type) {
		addFieldValidator(new TypeFieldValidator<K, V>(key, type, false));
		return this;
	}
	
	/**
	 * 检验字段类型 [非强制]。只有当字段值类型和给定类型匹配时才会校验成功。校验失败时将会在结果集中写
	 * 入自定义的校验不匹配信息。此方法不会校验字段的存在性，若字段不存在则会跳过校验，并不会再结果
	 * 集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param type 数据类型
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> typeIfExist(K key, Class<?> type, String errorMsg) {
		addFieldValidator(new TypeFieldValidator<K, V>(key, type, false, errorMsg));
		return this;
	}
	
	/**
	 * 有效字符串校验 [强制]。当map中含有此字段、字段值类型为String、字段值不为null且字段值去掉
	 * 首尾空格后不为空字符串（""）时才会校验成功。校验失败时将会在结果集中写入默认校验信息（'key'
	 * 应为非空字符串）.
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> effectiveString(K key) {
		addFieldValidator(new EffectiveStringFieldValidator<K, V>(key, true));
		return this;
	}
	
	/**
	 * 有效字符串校验 [强制]。当map中含有此字段、字段值类型为String、字段值不为null且字段值去掉
	 * 首尾空格后不为空字符串（""）时才会校验成功。校验失败时将会在结果集中写入自定义的校验不匹配信息.
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> effectiveString(K key, String errorMsg) {
		addFieldValidator(new EffectiveStringFieldValidator<K, V>(key, true, errorMsg));
		return this;
	}
	
	/**
	 * 有效字符串校验 [非强制]。当字段值类型为String、字段值不为null且字段值去掉 首尾空格后不为空
	 * 字符串（""）时才会校验成功。校验失败时将会在结果集中写入默认的校验不匹配信息（'key'应为非空
	 * 字符串）.此方法不要求字 段的存在性，当字段不在map中时，会跳过此校验也不会在结果集中写入校验不
	 * 匹配信息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> effectiveStringIfExist(K key) {
		addFieldValidator(new EffectiveStringFieldValidator<K, V>(key, false));
		return this;
	}
	
	/**
	 * 有效字符串校验 [非强制]。当字段值类型为String、字段值不为null且字段值去掉 首尾空格后不为空
	 * 字符串（""）时才会校验成功。校验失败时将会在结果集中写入自定义的校验不匹配信息.此方法不要求字
	 * 段的存在性，当字段不在map中时，会跳过此校验也不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> effectiveStringIfExist(K key, String errorMsg) {
		addFieldValidator(new EffectiveStringFieldValidator<K, V>(key, false, errorMsg));
		return this;
	}
	
	/**
	 * 任一类型校验[强制]。当map中含有此字段并且该字段值的数据类型和候选类型集合中的任一匹配时才会校验
	 * 成功。校验失败时将会在结果集中写入默认的校验不匹配信息（'key'应为type1或type2...类型）.
	 * 
	 * @param key 校验的map key
	 * @param types 候选类型集合
	 * @return
	 */
	public ApiParameterValidator<K, V> anyType(K key, Class<?> ...types) {
		addFieldValidator(new AnyTypeFieldValidator<K, V>(key, types, true));
		return this;
	}
	
	/**
	 * 任一类型校验[强制]。当map中含有此字段并且该字段值的数据了行和候选类型集合中的任一匹配时才会校验
	 * 成功。校验失败时将会在结果集中写入自定义的校验不匹配信息.
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配信息
	 * @param types 候选类型集合
	 * @return
	 */
	public ApiParameterValidator<K, V> anyType(K key, String errorMsg, Class<?> ...types) {
		addFieldValidator(new AnyTypeFieldValidator<K, V>(key, types, true, errorMsg));
		return this;
	}
	
	/**
	 * 任一类型校验[非强制]。当该字段值的数据了行和候选类型集合中的任一匹配时才会校验 成功。校验失败时将会
	 * 在结果集中写入默认的校验不匹配信息（'key'应为type1或type2...类型）.此方法不会校验字段
	 * 的存在性，若字段不存在则会跳过校验，并不会再结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param types 候选类型集合
	 * @return
	 */
	public ApiParameterValidator<K, V> anyTypeIfExist(K key, Class<?> ...types) {
		addFieldValidator(new AnyTypeFieldValidator<K, V>(key, types, false));
		return this;
	}
	
	/**
	 * 任一类型校验[非强制]。当该字段值的数据了行和候选类型集合中的任一匹配时才会校验 成功。校验失败时将会
	 * 在结果集中写入自定义的校验不匹配信息.此方法不会校验字段 的存在性，若字段不存在则会跳过校验，并不会
	 * 在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配信息
	 * @param types 候选类型集合
	 * @return
	 */
	public ApiParameterValidator<K, V> anyTypeIfExist(K key, String errorMsg, Class<?> ...types) {
		addFieldValidator(new AnyTypeFieldValidator<K, V>(key, types, false, errorMsg));
		return this;
	}
	
	/**
	 * 数字字符串校验 [强制].当map中包含此字段、字段数据类型为String或数字类型（Integer、Double
	 * Long、Float）且String可以转换为一个有效数字时才会校验成功。校验失败时将会在结果集中写入默认的
	 * 校验不匹配信息（'key'应为数字或数字字符串）.
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> numberString(K key) {
		addFieldValidator(new NumberOrStringFieldValidator<K, V>(key, true));
		return this;
	}
	
	/**
	 * 数字字符串校验 [强制].当map中包含此字段、字段数据类型为String或数字类型（Integer、Double
	 * Long、Float）且String可以转换为一个有效数字时才会校验成功。校验失败时将会在结果集中写入自定义
	 * 的校验不匹配消息.
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配消息
	 * @return
	 */
	public ApiParameterValidator<K, V> numberString(K key, String errorMsg) {
		addFieldValidator(new NumberOrStringFieldValidator<K, V>(key, true, errorMsg));
		return this;
	}
	
	/**
	 * 数字字符串校验 [非强制].当字段数据类型为String或数字类型（Integer、Double、Long、Float）
	 * 且String可以转换为一个有效数字时才会校验成功。校验失败时将会在结果集中写入默认的 校验不匹配信息
	 * （'key'应为数字或数字字符串）.此方法不会校验字段的 存在性，若字段不存在则会跳过校验并不会在结果集中
	 * 写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> numberStringIfExist(K key) {
		addFieldValidator(new NumberOrStringFieldValidator<K, V>(key, false));
		return this;
	}
	
	/**
	 * 数字字符串校验 [非强制].当字段数据类型为String或数字类型（Integer、Double、Long、Float）
	 * 且String可以转换为一个有效数字时才会校验成功。校验失败时将会在结果集中写入自定义的校验不匹配信息.
	 * 此方法不会校验字段的存在性，若字段不存在则会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> numberStringIfExist(K key, String errorMsg) {
		addFieldValidator(new NumberOrStringFieldValidator<K, V>(key, false, errorMsg));
		return this;
	}
	
	/**
	 * 自定义字符串校验 [强制]。当字段存在并且数据类型为String并且满足给定的正则表达式时，才会校验成功。
	 * 校验失败时将会在结果集中写入默认的校验不匹配信息（'key'不匹配的字符串）。
	 * 
	 * @param key 校验的map key
	 * @param pattern 使用的正则表达式
	 * @return
	 */
	public ApiParameterValidator<K, V> customString(K key, String pattern) {
		addFieldValidator(new CustomStringFieldValidator<K, V>(key, pattern, true));
		return this;
	}
	
	/**
	 * 自定义字符串校验 [强制]。当字段存在并且数据类型为String并且满足给定的正则表达式时，才会校验成功。
	 * 校验失败时 将会在结果集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param pattern 使用的正则表达式
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> customString(K key, String pattern, String errorMsg) {
		addFieldValidator(new CustomStringFieldValidator<K, V>(key, pattern, true, errorMsg));
		return this;
	}
	
	/**
	 * 自定义字符串校验 [非强制]。当字段数据类型为String并且满足给定的正则表达式时，才会校验成功。校验失败时
	 * 将会在结果集中写入默认的校验不匹配信息（'key'不匹配的字符串）。此方法不会校验字段的存在性，若字段不存在
	 * 则会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param pattern 使用的正则表达式
	 * @return
	 */
	public ApiParameterValidator<K, V> customStringIfExist(K key, String pattern) {
		addFieldValidator(new CustomStringFieldValidator<K, V>(key, pattern, false));
		return this;
	}
	
	/**
	 * 自定义字符串校验 [非强制]。当字段数据类型为String并且满足给定的正则表达式时，才会校验成功。校验失败时
	 * 将会在结果集中写入自定义的校验不匹配信息。此方法不会校验字段的存在性，若字段不存在则会跳过校验并不会在结
	 * 果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param pattern 使用的正则表达式
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> customStringIfExist(K key, String pattern, String errorMsg) {
		addFieldValidator(new CustomStringFieldValidator<K, V>(key, pattern, false, errorMsg));
		return this;
	}
	
	/**
	 * 邮箱校验 [强制]。当字段存在，数据类型为String并且符合邮箱格式时，才会校验成功。校验失败时将会在结果集中写入
	 * 默认的校验不匹配信息（'key'无效的邮箱格式）。
	 * 果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> mail(K key) {
		addFieldValidator(new MailFieldValidator<K, V>(key, true));
		return this;
	}
	
	/**
	 * 邮箱校验 [强制]。当字段存在，数据类型为String并且符合邮箱格式时，才会校验成功。校验失败时将会在结果集中写入
	 * 自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> mail(K key, String errMsg) {
		addFieldValidator(new MailFieldValidator<K, V>(key, true, errMsg));
		return this;
	}
	
	/**
	 * 邮箱校验 [非强制]。当字段数据类型为String并且符合邮件格式时，才会校验成功。校验失败时将会在结果
	 * 集中写入默认的校验不匹配信息（'key'无效的邮箱格式）。此方法不会校验字段的存在性，若字段不存在则会跳过校验
	 * 并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> mailIfExist(K key) {
		addFieldValidator(new MailFieldValidator<K, V>(key, false));
		return this;
	}
	
	/**
	 * 邮箱校验 [非强制]。当字段数据类型为String并且符合邮件格式时，才会校验成功。校验失败时将会在结果
	 * 集中写入自定义的校验不匹配信息。此方法不会校验字段的存在性，若字段不存在则会跳过校验并不会在结果集中写入校
	 * 验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> mailIfExist(K key, String errMsg) {
		addFieldValidator(new MailFieldValidator<K, V>(key, false, errMsg));
		return this;
	}
	
	/**
	 * 身份证校验 [强制]。当字段存在，数据类型为String并且符合身份证格式，才会校验成功。校验失败时将会在结果
	 * 集中写入默认的校验不匹配信息（'key'无效的身份证号）。
	 * 果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> idCard(K key) {
		addFieldValidator(new IdCardFieldValidator<K, V>(key, true));
		return this;
	}
	
	/**
	 * 身份证校验 [强制]。当字段存在，数据类型为String并且符合身份证格式，才会校验成功。校验失败时将会在结果
	 * 集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> idCard(K key, String errMsg) {
		addFieldValidator(new IdCardFieldValidator<K, V>(key, true, errMsg));
		return this;
	}
	
	/**
	 * 身份证校验 [非强制]。当字段数据类型为String并且符合身份证格式，才会校验成功。校验失败时将会在结果
	 * 集中写入默认的校验不匹配信息（'key'无效的邮箱格式）。此方法不会校验字段的存在性，若字段不存在则会
	 * 跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @return
	 */
	public ApiParameterValidator<K, V> idCardIfExist(K key) {
		addFieldValidator(new IdCardFieldValidator<K, V>(key, false));
		return this;
	}
	
	/**
	 * 身份证校验 [非强制]。当字段数据类型为String并且符合身份证格式时，才会校验成功。校验失败时将会在结果
	 * 集中写入自定义的校验不匹配信息。此方法不会校验字段的存在性，若字段不存在则会跳过校验并不会在结果集中写
	 * 入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param errMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> idCardIfExist(K key, String errMsg) {
		addFieldValidator(new IdCardFieldValidator<K, V>(key, false, errMsg));
		return this;
	}
	
	/**
	 * 闭区间校验[强制]，当字段存在，符合指定的数据类型，并处于给定的区间范围内（包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入默认的校验不匹配信息（'key'应为type类型，大于等于leftBorder且小于
	 * 等于rightBorder）。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeClose(K key, T leftBorder, T rightBorder) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, true, true));
		return this;
	}
	
	/**
	 * 闭区间校验[强制]，当字段存在，符合指定的数据类型，并处于给定的区间范围内（包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeClose(K key, T leftBorder, T rightBorder, String errorMsg) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, true, true, errorMsg));
		return this;
	}
	
	/**
	 * 闭区间校验[非强制]，当字段符合指定的数据类型，并处于给定的区间范围内（包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入默认的校验不匹配信息（'key'应为type类型，大于等于leftBorder且小
	 * 于等于rightBorder）。若字段不存在则会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeCloseIfExist(K key, T leftBorder, T rightBorder) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, true, false));
		return this;
	}
	
	/**
	 * 闭区间校验[非强制]，当字段符合指定的数据类型，并处于给定的区间范围内（包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入自定义的校验不匹配信息。若字段不存在则会跳过校验并不会在结果集中写入校
	 * 验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeCloseIfExist(K key, T leftBorder, T rightBorder, String errorMsg) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, true, false, errorMsg));
		return this;
	}
	
	/**
	 * 开区间校验[强制]，当字段存在，符合指定的数据类型，并处于给定的区间范围内（不包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入默认的校验不匹配信息（'key'应为type类型，大于leftBorder且小于
	 * rightBorder）。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeOpen(K key, T leftBorder, T rightBorder) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, false, true));
		return this;
	}
	
	/**
	 * 开区间校验[强制]，当字段存在，符合指定的数据类型，并处于给定的区间范围内（不包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeOpen(K key, T leftBorder, T rightBorder, String errorMsg) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, false, true, errorMsg));
		return this;
	}
	
	/**
	 * 开区间校验[非强制]，当字段符合指定的数据类型，并处于给定的区间范围内（不包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入默认的校验不匹配信息（'key'应为type类型，大于leftBorder且小于
	 * rightBorder）。若字段不存在则会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeOpenIfExist(K key, T leftBorder, T rightBorder) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, false, false));
		return this;
	}
	
	/**
	 * 开区间校验[非强制]，当字段符合指定的数据类型，并处于给定的区间范围内（不包括边界值），才会校验成功。
	 * 校验失败时将会在结果集中写入自定义的校验不匹配信息。若字段不存在则会跳过校验并不会在结果集中写入校
	 * 验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param rightBorder 右边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> rangeOpenIfExist(K key, T leftBorder, T rightBorder, String errorMsg) {
		addFieldValidator(new RangeFieldValidator<K, V, T>(key, leftBorder, rightBorder, false, false, errorMsg));
		return this;
	}
	
	/**
	 * 大于校验[强制]，当字段存在，符合指定数据类型，并大于左边界值，才会校验成功。校验失败时将会在结果集中写入默
	 * 认校验不匹配信息（'key'应为type类型，大于leftBorder）。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gt(K key, T leftBorder) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, false, true));
		return this;
	}
	
	/**
	 * 大于校验[强制]，当字段存在，符合指定数据类型，并大于左边界值，才会校验成功。校验失败时将会在结果集中写入自
	 * 定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gt(K key, T leftBorder, String errorMsg) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, false, true, errorMsg));
		return this;
	}
	
	/**
	 * 大于校验[非强制]，当字段符合指定数据类型，并大于左边界值，才会校验成功。校验失败时将会在结果集中写入默
	 * 认校验不匹配信息（'key'应为type类型，大于leftBorder）。若字段不存在则会跳过校验并不会在结果集中
	 * 写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gtIfExist(K key, T leftBorder) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, false, false));
		return this;
	}
	
	/**
	 * 大于校验[非强制]，当字段符合指定数据类型，并大于左边界值，才会校验成功。校验失败时将会在结果集中写入
	 * 自定义的校验不匹配信息。若字段不存在则会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gtIfExist(K key, T leftBorder, String errorMsg) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, false, false, errorMsg));
		return this;
	}
	
	/**
	 * 大于等于校验[强制]，当字段存在，符合指定数据类型，并大于等于左边界值，才会校验成功。校验失败时将会在
	 * 结果集中写入默认校验不匹配信息（'key'应为type类型，大于等于leftBorder）。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gte(K key, T leftBorder) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, true, true));
		return this;
	}
	
	/**
	 * 大于等于校验[强制]，当字段存在，符合指定数据类型，并大于等于左边界值，才会校验成功。校验失败时将会在
	 * 结果集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gte(K key, T leftBorder, String errorMsg) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, true, true, errorMsg));
		return this;
	}
	
	/**
	 * 大于等于校验[非强制]，当字段符合指定数据类型，并大于等于左边界值，才会校验成功。校验失败时将会在
	 * 结果集中写入默认校验不匹配信息（'key'应为type类型，大于等于leftBorder）。若字段不存在则
	 * 会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gteIfExist(K key, T leftBorder) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, true, false));
		return this;
	}
	
	/**
	 * 大于等于校验[非强制]，当字段符合指定数据类型，并大于等于左边界值，才会校验成功。校验失败时将会在
	 * 结果集中写入自定义的校验不匹配信息。若字段不存在则会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param leftBorder 左边界值
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> gteIfExist(K key, T leftBorder, String errorMsg) {
		addFieldValidator(new LeftBorderFieldValidator<K, V, T>(key, leftBorder, true, false, errorMsg));
		return this;
	}
	
	/**
	 * 列表校验[强制]，当字段存在，符合指定数据类型的列表数据才会校验成功。校验失败时将会在
	 * 结果集中写入默认校验不匹配信息（'key'应为type类型列表）。
	 * 
	 * @param key 校验的map key
	 * @param type 列表泛型类型
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> list(K key, Class<?> type) {
		addFieldValidator(new ListFieldValidator<K, V>(key, type, true));
		return this;
	}
	
	/**
	 * 列表校验[强制]，当字段存在，符合指定数据类型的列表数据才会校验成功。校验失败时将会在
	 * 结果集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param type 列表泛型类型
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> list(K key, Class<?> type, String errorMsg) {
		addFieldValidator(new ListFieldValidator<K, V>(key, type, true, errorMsg));
		return this;
	}
	
	/**
	 * 列表[非强制]，当字段符合指定数据类型的列表数据才会校验成功。校验失败时将会在
	 * 结果集中写入默认校验不匹配信息（'key'应为type类型列表）。若字段不存在则
	 * 会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param type 列表泛型类型
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> listIfExist(K key, Class<?> type) {
		addFieldValidator(new ListFieldValidator<K, V>(key, type, false));
		return this;
	}
	
	/**
	 * 列表[非强制]，当字段符合指定数据类型的列表数据才会校验成功。校验失败时将会在
	 * 结果集中写入自定义的校验不匹配信息。若字段不存在则会跳过校验并不会在结果集中写
	 * 入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param type 列表泛型类型
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> listIfExist(K key, Class<?> type, String errorMsg) {
		addFieldValidator(new ListFieldValidator<K, V>(key, type, false, errorMsg));
		return this;
	}

	/**
	 * 字段可选值校验
	 * @param k
	 * @param isCheckKeyExist true 校验字段存在性
	 * @param errMsg null or "" 将使用默认消息
	 * @param values
	 * @param <T>
	 * @return
	 */
	public <T extends Comparable<T>> ApiParameterValidator<K, V> valueOf(K k,
																		 boolean isCheckKeyExist,
																		 String errMsg,
																		 Object... values) {
		addFieldValidator(new OptionalValueValidator<K, V>(k, isCheckKeyExist, errMsg, values));
		return this;
	}

	public <T extends Comparable<T>> ApiParameterValidator<K, V> valueOf(K k, String errMsg, Object... values){

		return valueOf(k, true, errMsg, values);
	}

//	public <T extends Comparable<T>> ApiParameterValidator<K, V> valueOf(K k, Object... values){
//
//		return valueOf(k, true, null, values);
//	}
	

	/**
	 * 日期字符串校验[强制]，当字段存在，符合指格式的日期字符串数据才会校验成功。校验失败时将会在
	 * 结果集中写入默认校验不匹配信息（'key'无效的日期，应为pattern格式）。
	 * 
	 * @param key 校验的map key
	 * @param pattern 日期格式，例如：yyyy-MM-dd
	 * @return
	 */
	public ApiParameterValidator<K, V> dateString(K key, String pattern) {
		addFieldValidator(new DateStringFieldValidator<K, V>(key, pattern, true));
		return this;
	}
	
	/**
	 * 日期字符串校验[强制]，当字段存在，符合指格式的日期字符串数据才会校验成功。校验失败时将会在
	 * 结果集中写入自定义的校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param pattern 日期格式，例如：yyyy-MM-dd
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> dateString(K key, String pattern, String errorMsg) {
		addFieldValidator(new DateStringFieldValidator<K, V>(key, pattern, true, errorMsg));
		return this;
	}
	
	/**
	 * 日期字符串校验[非强制]，当字段符合指格式的日期字符串数据才会校验成功。校验失败时将会在
	 * 结果集中写入默认校验不匹配信息（'key'无效的日期，应为pattern格式）。若字段不存在则
	 * 会跳过校验并不会在结果集中写入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param pattern 日期格式，例如：yyyy-MM-dd
	 * @return
	 */
	public ApiParameterValidator<K, V> dateStringIfExist(K key, String pattern) {
		addFieldValidator(new DateStringFieldValidator<K, V>(key, pattern, false));
		return this;
	}
	
	/**
	 * 日期字符串校验[非强制]，当字段符合指格式的日期字符串数据才会校验成功。校验失败时将会在
	 * 结果集中写入自定义的校验不匹配信息。若字段不存在则会跳过校验并不会在结果集中写
	 * 入校验不匹配信息。
	 * 
	 * @param key 校验的map key
	 * @param pattern 日期格式，例如：yyyy-MM-dd
	 * @param errorMsg 自定义的校验不匹配信息
	 * @return
	 */
	public ApiParameterValidator<K, V> dateStringIfExist(K key, String pattern, String errorMsg) {
		addFieldValidator(new DateStringFieldValidator<K, V>(key, pattern, false, errorMsg));
		return this;
	}

	/**
	 * 获取校验结果。当调用此方法时才会执行之前的字段校验配置信息并返回结果。此方法应该在最后调用。
	 * 
	 * @return
	 */
	public Map<String, Object> getResult() {
		return executor.execute(params, validateChain);
	}

	/**
	 * 向责任链中添加一个字段校验器
	 * 
	 * @param fieldValidator
	 */
	private void addFieldValidator(MapValidatable<K, V> fieldValidator) {
		if(fieldValidator == null) {
			return;
		}
		validateChain.add(fieldValidator);
	}
	
	/**
	 * 设置新的校验执行器
	 * 
	 * @param executor 校验执行器
	 */
	private void setExecutor(MapValidatorExecutable<K, V> executor) {
		if(executor == null) {
			return;
		}
		this.executor = executor;
	}
	
}
