package com.psk.hms.base.util.file;

import com.psk.hms.base.util.BaseUtil;
import com.psk.hms.base.util.rsa.Base64Util;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * 描述: base64与multipart list互转工具类
 * 作者: xiangjz  
 */
public class Base64MultipartUtil {
	
	//base64字符串转化成图片  
    public static MultipartFile[] GenerateImages(List<Map<String, Object>> base64s){  
    	if(BaseUtil.isEmpty(base64s))
    		return new MultipartFile[0];
    	
    	List<MultipartFile> resultList = new ArrayList<MultipartFile>();
    	MultipartFile[] resultArr = new MultipartFile[base64s.size()];
    	for (Map<String, Object> fileMap : base64s) {
			String fileName = BaseUtil.retStr(fileMap.get("fileName"));
			String base64 = BaseUtil.retStr(fileMap.get("base64"));
			
			Map<String, Object> resultMap = Base64Util.GenerateImage(base64, fileName);
			MultipartFile mf = (MultipartFile) resultMap.get("file");
			resultList.add(mf);
		};
		
    	return resultList.toArray(resultArr);
    } 
    
    
    /**
     * 文件list转base64字符串 list
     * @param files
     * @param attachFilenames
     * @return
     */
    public static List<Map<String, Object>> filesToBase64(MultipartFile[] files, String[] attachFilenames) {
    	List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
    	if(BaseUtil.isEmpty(files)) 
    		return resultList;
    	int index = 0;
		for (MultipartFile file : files) {
			Map<String, Object> base64FileMap = new HashMap<String, Object>();	
			String fileName = "";
			if(BaseUtil.isEmpty(attachFilenames) || attachFilenames.length < index+1 || BaseUtil.isEmpty(attachFilenames[index]))
				fileName = file.getOriginalFilename();
			else
				fileName = attachFilenames[index];
			String base64 = Base64Util.fileToBase64(file);
			
			base64FileMap.put("fileName", fileName);
			base64FileMap.put("base64", base64);
			resultList.add(base64FileMap);
			index ++;
		}
        return resultList;
    }
    
}  

