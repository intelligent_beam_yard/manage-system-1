package com.psk.hms.base.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 项目测量实体树
 * 
 * @author xiangjz
 * @date 2018-8-17
 * @version 1.0.0
 *
 */
public class ProjectMeaEntityParentNode extends ProjectMeaEntityNode {
	private static final long serialVersionUID = 1L;

	private List<ProjectMeaEntityNode> children;

	public ProjectMeaEntityParentNode() {
		super();
		this.children = new ArrayList<>();
	}

	public ProjectMeaEntityParentNode(String id, String name, int burstPoint, int qualified_point,
			int unfinished_point) {
		super(id, name, burstPoint, qualified_point, unfinished_point);
		this.children = new ArrayList<>();
	}

	public List<ProjectMeaEntityNode> getChildren() {
		return children;
	}

	public void setChildren(List<ProjectMeaEntityNode> children) {
		this.children = children;
	}

	public boolean addNode(ProjectMeaEntityNode node) {
		return this.children.add(node);
	}

	public boolean hasNode(ProjectMeaEntityNode node) {
		return this.children.contains(node);
	}

}
